var Vertical = true;
var wrapper = document.getElementById("signature-pad"),
    clearButton = wrapper.querySelector("[data-action=clear]"),
    saveButton = wrapper.querySelector("[data-action=save]"),
    canvas = wrapper.querySelector("canvas"), signaturePad;

function resizeCanvas() {
    reSize();
    var ratio = Math.max(window.devicePixelRatio || 1, 1);
    canvas.width = canvas.offsetWidth * ratio;
    canvas.height = canvas.offsetHeight * ratio;
    canvas.getContext("2d").scale(ratio, ratio);
}

window.onresize = resizeCanvas;
resizeCanvas();
signaturePad = new SignaturePad(canvas);
clearButton.addEventListener("click", function(event) {
    signaturePad.clear();
});

saveButton.addEventListener("click", function(event) {
    var id=$("#subBtn").attr('data-id');
    if (signaturePad.isEmpty()) {
        alert("请书写您的签名。");
    } else {
        var imgdata = signaturePad.toDataURL(); // base64
        //alert(Vertical);
        upload(imgdata,id);
        // 当Vertical为true的时候，后台需要转90度；
        // 当Vertical为false的时候，后台不需要转；
        // document.getElementById('signimg').src=imgdata; //test
    }
});

function reSize() {
    var height = document.documentElement.clientHeight;
    var width = document.documentElement.clientWidth;
    // $("#test").text("width:"+width+"height:"+height);
    if (width > height) {
        document.getElementById('signature-pad-footer').className = "m-signature-pad--footer";
        document.getElementById('signature-pad-title').className = "m-signature-pad--title";
        Vertical = false;
    } else {
        document.getElementById('signature-pad-footer').className = "m-signature-pad--footer-vi";
        document.getElementById('signature-pad-title').className = "m-signature-pad--title-vi";
        Vertical = true;
    }
}

function upload(imgdata,id) {
    $("#clearButton,#backBtn,#subBtn").hide();
    $("#msg").show();
    $.ajax({
        type : 'POST',
        dataType : 'json',
        url : sy.contextPath+'/baseUtil/base64',
        data : {
            media : imgdata,
            id : id
        },
        success : function(data) {
            //console.log(data)
            // if (data['error'] != null) {
            //     //alert(data['error']);
            //     $("#clearButton,#backBtn,#subBtn").show();
            //     $("#msg").hide();
            // } else {
            //     if (data.url){
            //         location.href = data.url;
            //     }else{
            //         alert("签署成功");
            //     }
            // }
            if(data.code == 0){
                 layer.msg("签署成功");
                $("#clearButton,#backBtn,#subBtn").show();
                     $("#msg").hide();
                //
                // var url  = sy.contextPath + '/contract/save';
                // var obj = {
                //     id : id,
                //     signPath : data.msg
                // }
                // $.post(url,obj,function () {
                //
                // });
            }else{
                layer.msg(data.msg)
                $("#clearButton,#backBtn,#subBtn").show();
                $("#msg").hide();
            }
        },
        error : function(e) {
            layer.msg(e);
           // alert("错误：" + e);
            $("#clearButton,#backBtn,#subBtn").show();
            $("#msg").hide();
        }
    });
}