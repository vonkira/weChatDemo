function exeSocketMsg(json) {
	var type = json['type'];
	switch (type){
		case 'TEXT':
            socketText(json);
			break;
		default:
			break;
	}
}

function socketText(json) {
    layer.open({
        type: 0,
        move:false,
        title: "新消息",
        closeBtn: 1,
        shade: false,
        area: ['340px', '215px'],
        offset: 'rb',
        time: 0,
        shift: 2,
        content: json.msg,
        yes: function(index){
            layer.close(index);
        }
    });
}

function setupWebSocket(){
    if(wsIESupport){
        try {
            var websocket = null;
            var pp = window.location.host + sy.contextPath;
            if ('WebSocket' in window) {
                websocket = new WebSocket("ws://" + pp + "/wsServer");
            } else if ('MozWebSocket' in window) {
                websocket = new MozWebSocket("ws://" + pp + "/wsServer");
            } else {
                websocket = new SockJS("http://" + pp + "/sockjs/wsServer");
            }
            websocket.onopen = function(event) {
            };
            websocket.onerror = function (evnt) {
            };
            websocket.onclose = function (evnt) {
                //setTimeout(setupWebSocket, 1000);
            };
            websocket.onmessage = function(event) {
                var json = sy.stringToJSON(event.data);
                exeSocketMsg(json);
            };
        } catch (ex) {
        }
    }
}