<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/static/admin/jsp/include.jsp"%>
<script type="text/javascript">
	var grid;
	
//	var addFun = function($dialog, $grid, $pjq) {
//		var dialog = parent.sy.modalDialog({
//			title : '添加用户合同',
//			width : 800,
//			height : 600,
//			url : sy.contextPath + '/go?path=contract/contractEdit',
//			buttons : [ {
//				text : '保存',
//				handler : function() {
//					dialog.find('iframe').get(0).contentWindow.submitForm(dialog, grid, parent.$);
//				}
//			} ]
//		});
//	};
	
//	var editFun = function() {
//		var rows = grid.datagrid('getSelections');
//		if (rows.length != 1) {
//			parent.$.messager.w('请选择一条记录进行编辑！');
//			return;
//		}
//		var dialog = parent.sy.modalDialog({
//			title : '编辑',
//            width : 800,
//            height : 600,
//			url : sy.contextPath + '/go?path=contract/contractEdit&id=' + rows[0].id,
//			buttons : [ {
//				text : '保存',
//				handler : function() {
//					dialog.find('iframe').get(0).contentWindow.submitForm(dialog, grid, parent.$);
//				}
//			} ]
//		});
//	};
	
//	var delFun = function() {
//		var rows = grid.datagrid('getSelections');
//		if (rows.length == 0) {
//			parent.$.messager.w('请选择需要删除的记录！');
//			return;
//		}
//		parent.$.messager.confirm('询问', '您确定要删除此记录？', function(r) {
//			if (r) {
//				var ids = [];
//				for ( var i = 0, l = rows.length; i < l; i++) {
//					var r = rows[i];
//					ids.push(r.id);
//				}
//				var id = ids.join(',');
//
//				$.post(sy.contextPath + '/contract/del', {
//					id : id
//				}, function(res) {
//					if (res.code == 0) {
//						rows.length = 0;//必须，否则有bug
//						grid.datagrid('reload');
//					} else {
//						parent.$.messager.w(res.msg);
//					}
//				}, 'json');
//			}
//		});
//	};
	
	$(function() {
		grid = $('#grid').datagrid({
			url : sy.contextPath + '/contractPath/list',
			singleSelect : false,
			frozenColumns : [ [ {
				width : '100',
				checkbox:true,
				field : 'id',
				align : 'center'
			}] ],
			columns : [ [
                {
                    width : $(this).width() * 0.4,
                    title : '合同类型',
                    field : 'conName',
                    align : 'center'
                },{
					width : $(this).width() * 0.4,
					title : '机构名称',
					field : 'orgName',
					align : 'center'
				},{
                    width : $(this).width() * 0.4,
                    title : '城市',
                    field : 'cityName',
                    align : 'center'
                },{
                    width : $(this).width() * 0.4,
                    title : '站点',
                    field : 'pointName',
                    align : 'center'
                },{
                    width : $(this).width() * 0.4,
                    title : '职业状态',
                    field : 'state',
                    align : 'center',

                },{
                    width : $(this).width() * 0.4,
                    title : '性别',
                    field : 'sex',
                    align : 'center',
					formatter : function (v,r,i) {
						if(v == 1){
						    return '男';
						}else if(v == 2){
						    return '女';
						}
                    }
                },{
                    width : $(this).width() * 0.4,
                    title : '手机号码',
                    field : 'phone',
                    align : 'center',
                },{
                    width : $(this).width() * 0.4,
                    title : '身份证号码',
                    field : 'idCard',
                    align : 'center',
                },{
                    width : $(this).width() * 0.4,
                    title : '入职时间',
                    field : 'joinTime',
                    align : 'center',
					formatter : function(v,r,i){
                        return v.substr(v,10);
					}
                },{
					width : $(this).width() * 0.6,
					title : '创建时间',
					field : 'createTime',
					align : 'center'
				},{
                    width : $(this).width() * 0.6,
                    title : '操作',
                    field : 'createCon',
                    align : 'center',
					formatter : function(v,r,i){
                        return '<a href="javascript:void(0);" onclick="createCon(\''+r.id+'\');" class="button button-warning" title="生成合同">生成合同</a>';
					}
                }
			] ]
		});
	});

	function createCon(id){

        var dialog = parent.sy.modalDialog({
            title : '填写合同路径',
            width : 800,
            height : 600,
            url : sy.contextPath + '/go?path=contract/createUserCon&id='+id,
            buttons : [ {
                text : '生成合同',
                handler : function() {
                    dialog.find('iframe').get(0).contentWindow.submitForm(dialog, grid, parent.$);
                }
            } ]
        });

//	    var url = sy.contextPath + '/contract/createCon';
//	    data = {id : id}
//	    $.post(url,data,function(result){
//	        if(result.code == 1){
//	            layer.msg(result.msg)
//			}else{
//	            layer.msg(result.msg);
//			}
//		},'json')
	}
</script>
</head>
<body>
	<div id="toolbar">
		<form id="searchForm">
		<div>
			<input type="text" class="easyui-textbox" name="orgName" style="width: 150px" prompt="机构名称"/>
			<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-search'" onclick="grid.datagrid('load',sy.serializeObject($('#searchForm')));">过滤</a> 
			<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-search-minus'" onclick="$('#searchForm input').val('');grid.datagrid('load',{});">重置过滤</a>
		</div>
		</form>
		<div class="tbbutton">
			<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-plus-circle',plain:true" onclick="addFun();">添加</a>
			<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-pencil',plain:true" onclick="editFun();">修改</a>
			<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-trash',plain:true" onclick="delFun();">删除</a>
		</div>
	</div>
	<table id="grid" data-options="fit:true,border:false"></table>
</body>
</html>