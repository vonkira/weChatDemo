<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/static/admin/jsp/include.jsp"%>
<script type="text/javascript">
	var grid;
	
	var addFun = function($dialog, $grid, $pjq) {
		var dialog = parent.sy.modalDialog({
			title : '添加用户合同',
			width : 800,
			height : 600,
			url : sy.contextPath + '/go?path=contract/contractEdit',
			buttons : [ {
				text : '保存',
				handler : function() {
					dialog.find('iframe').get(0).contentWindow.submitForm(dialog, grid, parent.$);
				}
			} ]
		});
	};
	
	var editFun = function() {
		var rows = grid.datagrid('getSelections');
		if (rows.length != 1) {
			parent.$.messager.w('请选择一条记录进行编辑！');
			return;
		}
		var dialog = parent.sy.modalDialog({
			title : '编辑',
            width : 800,
            height : 600,
			url : sy.contextPath + '/go?path=contract/contractEdit&id=' + rows[0].id,
			buttons : [ {
				text : '保存',
				handler : function() {
					dialog.find('iframe').get(0).contentWindow.submitForm(dialog, grid, parent.$);
				}
			} ]
		});
	};
	
	var delFun = function() {
		var rows = grid.datagrid('getSelections');
		if (rows.length == 0) {
			parent.$.messager.w('请选择需要删除的记录！');
			return;
		}
		parent.$.messager.confirm('询问', '您确定要删除此记录？', function(r) {
			if (r) {
				var ids = [];
				for ( var i = 0, l = rows.length; i < l; i++) {
					var r = rows[i];
					ids.push(r.id);
				}
				var id = ids.join(',');
				
				$.post(sy.contextPath + '/contract/delRecord', {
					id : id,
					isDel : 1,
				}, function(res) {
					if (res.code == 0) {
						rows.length = 0;//必须，否则有bug
						grid.datagrid('reload');
					} else {
						parent.$.messager.w(res.msg);
					}
				}, 'json');
			}
		});
	};
	
	$(function() {

	    //查找用户权限，如果是admin则不展示增删改查按钮

		var url = sy.contextPath + '/contract/userRole';
		var obj = {};
		$.post(url,obj,function(result){
		    console.log(result)
		    if(result.code == 1){
		        //如果是admin登录则隐藏div
				$(".tbbutton").hide();
			}
		},'json');

		grid = $('#grid').datagrid({
			url : sy.contextPath + '/contract/list',
			singleSelect : false,
			frozenColumns : [ [ {
				width : '100',
				checkbox:true,
				field : 'id',
				align : 'center'
			}] ],
			columns : [ [
                {
                    width : $(this).width() * 0.2,
                    title : '合同类型',
                    field : 'conName',
                    align : 'center'
                },{
					width : $(this).width() * 0.2,
					title : '机构名称',
					field : 'orgName',
					align : 'center'
				},{
                    width : $(this).width() * 0.2,
                    title : '城市',
                    field : 'cityName',
                    align : 'center'
                },{
                    width : $(this).width() * 0.2,
                    title : '站点',
                    field : 'pointName',
                    align : 'center'
                },{
                    width : $(this).width() * 0.4,
                    title : '身份证号码',
                    field : 'idCard',
                    align : 'center',
                },{
                    width : $(this).width() * 0.4,
                    title : '手机号码',
                    field : 'phone',
                    align : 'center',
                },{
                    width : $(this).width() * 0.3,
                    title : '用户姓名',
                    field : 'name',
                    align : 'center',
                },{
                    width : $(this).width() * 0.3,
                    title : '学历',
                    field : 'eduction',
                    align : 'center',
                },{
                    width : $(this).width() * 0.3,
                    title : '入职时间',
                    field : 'joinTime',
                    align : 'center',
					formatter : function(v,r,i){
                        if(v){
                            return v.substr(0,10);
						}

					}
                },{
					width : $(this).width() * 0.3,
					title : '合同状态',
					field : 'type',
					align : 'center',
					formatter : function(v){
					    if(0 == v){
					        return '还未签署';
						}else if(1 == v){
					        return '已经签署';
						}else if(2 == v){
							return '合同已到期';
						}
					}
				},{
                    width : $(this).width() * 0.3,
                    title : '查看详情',
                    field : 'detail',
                    align : 'center',
                    formatter : function(v,r,i){
                        return '<a href="javascript:void(0);" onclick="detail(\''+r.id+'\');" class="button button-info" title="查看详情">查看详情</a>';
                    }
				},{
                    width : $(this).width() * 0.3,
                    title : '员工离职',
                    field : 'resign',
                    align : 'center',
                    formatter : function(v,r,i){
                        if(r.role == 1){
							//admin--不可操作
							if(2 != r.type){
							    return '在职'
							}else{
                                return '已离职'
							}
						}else if(r.role == 0){
                            //kehu
                            if(2 != r.type){
                                return '<a href="javascript:void(0);" onclick="userResign(\''+r.id+'\');" class="button button-info" title="离职">离职</a>';
                            }else{
                                return '已离职'
                            }
						}

                    }
                },{
                    width : $(this).width() * 0.6,
                    title : '操作',
                    field : 'createCon',
                    align : 'center',
					formatter : function(v,r,i){
                        return		'<a href="javascript:void(0);" onclick="downFile(\''+r.id+'\');" class="button button-info" title="下载合同">下载合同</a><a href="javascript:void(0);" onclick="copyUrl(\''+r.id+'\');" id="copyUrl" class="button button-warning" title="复制地址">复制地址</a>';
                        /*<a href="javascript:void(0);" onclick="createCon(\''+r.id+'\');" class="button button-warning" title="生成合同">生成合同</a>';*/
					}
                }
			] ]
		});

		//获取合同模板
        $('#conId').combobox({
            url:sy.contextPath + '/contractPath/contractPathList.do',
            valueField:'id',
            textField:'title',
            value:'--添加--',
            editable:false,//不可编辑，只能选择
			onClick : function(type){
                        var dialog = parent.sy.modalDialog({
                            title : '添加用户合同',
                            width : 800,
                            height : 600,
                            url : sy.contextPath + '/go?path=contract/contractEdit&type='+type.id,
                            buttons : [ {
                                text : '保存',
                                handler : function() {
                                    dialog.find('iframe').get(0).contentWindow.submitForm(dialog, grid, parent.$);
                                }
                            } ],

                        });
					}

        });

	});

	function downFile(id){

		var url = sy.contextPath + '/contract/downFile';
		var data = {id : id};
		$.post(url,data,function(result){
		    if(result.code == 1){
                window.location.href="${path}/download/?id="+result.msg;
			}else{
		        return layer.msg(result.msg);
            }
		},'json');

	}

	function copyUrl(id){
        var curWwwPath = window.document.location.href;
        //获取主机地址之后的目录，如： test/test/test.htm
        var pathName = window.document.location.pathname;
        var pos = curWwwPath.indexOf(pathName); //获取主机地址，如： http://localhost:8080
        var localhostPaht = curWwwPath.substring(0, pos); //获取带"/"的项目名，如：/web
        var projectName = pathName.substring(0, pathName.substr(1).indexOf('/') + 1);
        var rootPath = localhostPaht + projectName;
        layer.open({
            type: 1,
            skin: 'layui-layer-demo', //样式类名
            closeBtn: 0, //不显示关闭按钮
            anim: 2,
            shadeClose: true, //开启遮罩关闭
            content: rootPath + '/m/sign/userSign?id='+id,
            area: ['500px', '300px'],
			title:'网页链接地址'
        });
	}

	function detail(id){
        var dialog = parent.sy.modalDialog({
            title : '详情',
            width : 800,
            height : 800,
            url : sy.contextPath + '/go?path=contract/detail&id=' + id,
        });
	}

    /**
	 * 后台设置离职
     * @param id
     */
	function userResign(id){
	    var url = sy.contextPath + '/contract/userResign';
	    var obj = {
	        id : id,
			type : 2
		}
		$.post(url,obj,function(result){
            location.reload();
		});
	}

</script>
</head>
<body>
	<div id="toolbar">
		<form id="searchForm">
		<div>
			<input type="text" class="easyui-textbox" id="idCard" name="idCard" style="width: 150px" prompt="用户身份证"/>
			<input type="text" class="easyui-textbox" id="name" name="name" style="width: 150px" prompt="用户姓名"/>
			<input type="text" class="easyui-textbox" id="phone" name="phone" style="width: 150px" prompt="用户联系方式"/>
			<input type="text" class="easyui-combobox" id="type" name="type" style="width: 150px"
				   data-options="valueField:'type',textField:'value',
					data:[{type:'0',value:'未签署'},{type:'1',value:'已签'},{type:'2',value:'已到期'}]" prompt="合同状态"/>
			<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-search'" onclick="grid.datagrid('load',sy.serializeObject($('#searchForm')));">过滤</a>
			<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-search-minus'" onclick="$('#searchForm input').val('');grid.datagrid('load',{});">重置过滤</a>
		</div>
		</form>
		<div class="tbbutton">
			<%--<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-plus-circle',plain:true" onclick="addFun();">添加</a>--%>
			<a style="width:200px" href="javascript:void(0);" id="conId" class="easyui-combobox">添加</a>
			<%--<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-pencil',plain:true" onclick="editFun();">修改</a>--%>
			<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-trash',plain:true" onclick="delFun();">删除</a>
		</div>
	</div>
	<table id="grid" data-options="fit:true,border:false"></table>
</body>
</html>