<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/static/admin/jsp/include.jsp"%>
<script type="text/javascript">
	var id = "${id}";
	var v = "${v}";
	var type = "${type}";
	var submitForm = function($dialog, $grid, $pjq) {
        if ($('form').form('validate')) {
            var obj=sy.serializeObject($('form'));
            obj.joinTime +=" 00:00:00";
            obj.city = $("#cityId").combobox('getValue');
            obj.point = $("#pointId").combobox('getValue');
            //obj.conId = $("#conId").combobox('getValue');
			obj.conId = type;
            obj.type = 0;
            obj.isDel = 0;
            obj.sex = $("#sex").combobox('getValue');
            console.log(obj.joinTime)
            var url=sy.contextPath + '/contract/save';
            $.post(url, obj, function(result) {
                if (result.code == 0) {
                    $grid.datagrid('reload');
                    $dialog.dialog('destroy');
                } else {
                    $pjq.messager.e('添加失败,'+result.msg);
                }
            }, 'json');
        }
	};
	
	$(function() {
		if (id != '') {
			parent.$.messager.progress({
				text : '数据加载中....'
			});
			$.post(sy.contextPath + '/contract/findById', {
				id : id
			}, function(result) {
				if (result) {
					$('form').form('load', result);


				}
				parent.$.messager.progress('close');
			}, 'json');
		} else {
            var url = sy.contextPath + '/contract/selectOrg';
            data = {};
            $.post(url,data,function(record){
                $("#orgName").textbox("setValue", record.orgName);
            },'json');
		}

        $('#cityId').combobox({
            url:sy.contextPath + '/city/cityList.do',
            valueField:'id',
            textField:'cityName',
            onChange:function(cityId){
                //$('#city').combobox('clear');
                $('#pointId').combobox({
                    valueField:'id', //值字段
                    textField:'pointName', //显示的字段
                    url:sy.contextPath + '/point/pointList.do?cityId='+cityId,
                    panelHeight:'auto',
                    required:true,
                    editable:false,//不可编辑，只能选择
                    value:'--请选择--'
                });
            }
        });
		//获取合同模板
        $('#conId').combobox({
            url:sy.contextPath + '/contractPath/contractPathList.do',
            valueField:'id',
            textField:'title',
        });


	});
</script>
</head>
<body>
	<form id="form" method="post">
        <input name="id" type="hidden" />
        <div style="padding:15px;font-size: 12px">
            <table style="table-layout:fixed;" border="0" cellspacing="0" class="formtable" id="v1">
				<tr>
					<%--<th style="width:100px;">使用合同类型：</th>--%>
					<%--<td>--%>
						<%--<input id="conId" class="easyui-combobox" name="conId" style="width:100%;" data-options="editable:false"/>--%>
					<%--</td>--%>
					<th style="width:100px;"><span style="color: red">*</span>组织名称：</th>
					<td colspan="3">
						<input id="orgName" class="easyui-textbox" name="orgName" style="width:100%;" data-options="editable:false,required:true" readonly="readonly"/>
					</td>
				</tr>
		    	<tr>
					<th style="width:100px;"><span style="color: red">*</span>城市名称：</th>
					<td>
						<input id="cityId" class="easyui-combobox" name="cityId" style="width:100%;" data-options="editable:false,required:true"/>
					</td>
					<th style="width:100px;"><span style="color: red">*</span>站点名称：</th>
					<td>
						<input id="pointId" class="easyui-combobox" name="pointId" style="width:100%;" data-options="editable:false,required:true"/>
					</td>
		    	</tr>
				<tr>
					<th style="width:100px;">住址：</th>
					<td>
						<input id="address" class="easyui-textbox" name="address" style="width:100%;" data-options="editable:true"/>
					</td>
					<th style="width:100px;">户籍地址：</th>
					<td>
						<input id="idAddress" class="easyui-textbox" name="idAddress" style="width:100%;" data-options="editable:true"/>
					</td>
				</tr>
				<tr>
					<th style="width:100px;">紧急联系人姓名：</th>
					<td>
						<input id="cirName" class="easyui-textbox" name="cirName" style="width:100%;" data-options="editable:true"/>
					</td>
					<th style="width:100px;">紧急联系人电话：</th>
					<td>
						<input id="cirPhone" class="easyui-textbox" name="cirPhone" style="width:100%;" data-options="editable:true"/>
					</td>
				</tr>
				<tr>
					<th style="width:100px;">乙方工作性质：</th>
					<td>
						<input id="job" class="easyui-textbox" name="job" style="width:100%;" data-options="editable:true"/>
					</td>
					<th style="width:100px;">学历：</th>
					<td>
						<input id="eduction" class="easyui-textbox" name="eduction" style="width:100%;" data-options="editable:true"/>
					</td>
				</tr>
				<tr>
					<th style="width:100px;">工作状态：</th>
					<td>
						<input id="state" class="easyui-combobox" name="state" style="width:100%;"
							   data-options="editable:false,valueField: 'label',textField: 'value',data: [{label: '全职',value: '全职',selected:true},{label: '兼职',value: '兼职'}]"/>
					</td>
					<th style="width:100px;"><span style="color: red">*</span>人员姓名：</th>
					<td>
						<input id="name" class="easyui-textbox" name="name" style="width:100%;" data-options="editable:true,required:true"/>
					</td>
				</tr>
				<tr>
					<th style="width:100px;">性别：</th>
					<td>
						<input id="sex" class="easyui-combobox" name="sex" style="width:100%;"
							   data-options="editable:false,valueField: 'label',textField: 'value',data: [{label: '1',value: '男',selected:true},{label: '0',value: '女'}]"/>
					</td>
					<th style="width:100px;"><span style="color: red">*</span>手机号码：</th>
					<td>
						<input id="phone" class="easyui-textbox" name="phone" style="width:100%;" data-options="editable:true,required:true"/>
					</td>
				</tr>
				<tr>
					<th style="width:100px;"><span style="color: red">*</span>身份证号码：</th>
					<td>
						<input id="idCard" class="easyui-textbox" name="idCard" style="width:100%;" data-options="editable:true,required:true"/>
					</td>
					<th style="width:100px;"><span style="color: red">*</span>入职日期：</th>
					<td>
						<input id="joinTime" class="easyui-datebox" name="joinTime" style="width:100%;" data-options="editable:false,required:true"/>
					</td>
				</tr>
            </table>
        </div>
	</form>
</body>
</html>