<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/static/admin/jsp/include.jsp"%>
<script type="text/javascript">
	var id = "${id}";
	
	$(function() {
		if (id != '') {
			parent.$.messager.progress({
				text : '数据加载中....'
			});
			$.post(sy.contextPath + '/contract/detail', {
				id : id
			}, function(result) {
			    console.log(result)
				if (result) {
                    $('form').form('load', result);
                    for (var a in result) {
                        $("#" + a).html(result[a]);
                        if (a == "type") {
                            if (result[a] == 0) {
                                $("#type").html('未签署')
                            } else if (result[a] == 1) {
                                $("#type").html('已签')
                            } else if (result[a] == 2) {
                                $("#type").html('合同到期')
                            }
                        }
                            if(a == "joinTime"){
                                $("#joinTime").html(result[a].substr(0,10));
                            }
                            if(a == "endTime"){
                                $("#endTime").html(result[a].substr(0,10));
                            }

                            if(a == "sex"){
                                if(result[a]==1){
                                    $("#sex").html('男');
                                }else if(result[a]==0){
                                    $("#sex").html('女');
                                }else{
                                    $("#sex").html('未知');
								}

                            }

                    }
                }
				parent.$.messager.progress('close');
			}, 'json');
		} else {

		}
	});
</script>
</head>
<body>
	<form id="form" method="post">
        <input name="id" type="hidden" />
        <div style="padding:15px;font-size: 12px">
            <table style="table-layout:fixed;" border="0" cellspacing="0" class="formtable">
				<tr>
					<th style="width:100px;">使用合同类型：</th>
					<td id="conTitle">

					</td>
					<th style="width:100px;">组织名称：</th>
					<td id="orgName">

					</td>
				</tr>
		    	<tr>
					<th style="width:100px;">城市名称：</th>
					<td id="cityName">

					</td>
					<th style="width:100px;">站点名称：</th>
					<td id="pointName">

					</td>
		    	</tr>
				<tr>
					<th style="width:100px;">住址：</th>
					<td id="address">

					</td>
					<th style="width:100px;">户籍地址：</th>
					<td id="idAddress">

					</td>
				</tr>
				<tr>
					<th style="width:100px;">紧急联系人姓名：</th>
					<td id="cirName">

					</td>
					<th style="width:100px;">紧急联系人电话：</th>
					<td id="cirPhone">

					</td>
				</tr>
				<tr>
					<th style="width:100px;">乙方工作性质：</th>
					<td id="job">
					</td>
					<th style="width:100px;">学历：</th>
					<td id="eduction">

					</td>
				</tr>
				<tr>
					<th style="width:100px;">工作状态：</th>
					<td id="state">

					</td>
					<th style="width:100px;">人员姓名：</th>
					<td id="name">

					</td>
				</tr>
				<tr>
					<th style="width:100px;">性别：</th>
					<td id="sex">

					</td>
					<th style="width:100px;">手机号码：</th>
					<td id="phone">

					</td>
				</tr>
				<tr>
					<th style="width:100px;">身份证号码：</th>
					<td id="idCard">

					</td>
					<th style="width:100px;">入职日期：</th>
					<td id="joinTime">

					</td>
				</tr>
				<tr>
					<th style="width:100px;">合同到期时间：</th>
					<td id="endTime">

					</td>
					<th style="width:100px;">合同状态：</th>
					<td id="type">

					</td>
				</tr>
            </table>
        </div>
	</form>
</body>
</html>