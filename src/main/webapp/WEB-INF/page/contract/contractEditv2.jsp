<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/static/admin/jsp/include.jsp"%>
<script type="text/javascript">
	var id = "${id}";

	var submitForm = function($dialog, $grid, $pjq) {
        if ($('form').form('validate')) {
            var obj=sy.serializeObject($('form'));
            obj.conId = 7;
            obj.type = 0;
            var url=sy.contextPath + '/contract/save';
            $.post(url, obj, function(result) {
                if (result.code == 0) {
                    $grid.datagrid('reload');
                    $dialog.dialog('destroy');
                } else {
                    $pjq.messager.e('添加失败,'+result.msg);
                }
            }, 'json');
        }
	};
	
	$(function() {
		if (id != '') {
			parent.$.messager.progress({
				text : '数据加载中....'
			});
			$.post(sy.contextPath + '/contract/findById', {
				id : id
			}, function(result) {
				if (result) {
					$('form').form('load', result);


				}
				parent.$.messager.progress('close');
			}, 'json');
		} else {
            var url = sy.contextPath + '/contract/selectOrg';
            data = {};
            $.post(url,data,function(record){
                $("#orgName").textbox("setValue", record.orgName);
            },'json');
		}

		//获取合同模板
        $('#conId').combobox({
            url:sy.contextPath + '/contractPath/contractPathList.do',
            valueField:'id',
            textField:'title',
        });


	});
</script>
</head>
<body>
	<form id="form" method="post">
        <input name="id" type="hidden" />
        <div style="padding:15px;font-size: 12px">
            <table style="table-layout:fixed;" border="0" cellspacing="0" class="formtable" id="v1">
				<tr>
					<th style="width:100px;">组织名称：</th>
					<td colspan="3">
						<input id="orgName" class="easyui-textbox" name="orgName" style="width:100%;" data-options="editable:false" readonly="readonly"/>
					</td>
				</tr>
				<tr>
					<th style="width:100px;">身份证号码：</th>
					<td>
						<input id="idCard" class="easyui-textbox" name="idCard" style="width:100%;" data-options="editable:true,required:true"/>
					</td>
					<th style="width:100px;">人员姓名：</th>
					<td>
						<input id="name" class="easyui-textbox" name="name" style="width:100%;" data-options="editable:true,required:true"/>
					</td>
				</tr>
            </table>
        </div>
	</form>
</body>
</html>