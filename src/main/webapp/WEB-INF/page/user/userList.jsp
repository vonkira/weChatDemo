<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/static/admin/jsp/include.jsp"%>
<script type="text/javascript">
	var grid;
	var index;
	
	var addFun = function($dialog, $grid, $pjq) {
		var dialog = parent.sy.modalDialog({
			title : '添加新用户',
			width : 500,
			height : 400,
			url : sy.contextPath + '/go?path=user/userEdit',
			buttons : [ {
				text : '保存',
				handler : function() {
					dialog.find('iframe').get(0).contentWindow.submitForm(dialog, grid, parent.$);
				}
			} ]
		});
	};
	
	var editFun = function($dialog, $grid, $pjq) {
		var rows = grid.datagrid('getSelections');
		if (rows.length != 1) {
			parent.$.messager.w('请选择一条记录进行编辑！');
			return;
		}
		var dialog = parent.sy.modalDialog({
			title : '修改资料',
			width : 600,
			height : 400,
			url : sy.contextPath + '/go?path=user/userEdit&id=' + rows[0].id,
			buttons : [ {
				text : '保存',
				handler : function() {
					dialog.find('iframe').get(0).contentWindow.submitForm(dialog, grid, parent.$);
				}
			} ]
		});
	};
	
	var creditsFun = function() {
		var rows = grid.datagrid('getSelections');
		if (rows.length != 1) {
			parent.$.messager.w('请选择一条记录进行查看！');
			return;
		}
		var nickName = "";
		if (rows[0].nickName != null && rows[0].nickName != "") {
			nickName = rows[0].nickName;
		}
		var url = sy.contextPath + '/go.do?path=user/userCreditLogList&id=' + rows[0].id;
		var dialog = parent.sy.modalDialog({
			title : '积分日志——' + nickName,
			width:sy.width-150,
			height:sy.height-100,
			//width : 800,
		//	height : 620,
			url : url
		});
		index = dialog.selector.substring(12);
		console.log(dialog.selector.substring(12));
	};

	
	var pwdFun = function(id) {
		/* var rows = grid.datagrid('getSelections');
		if (rows.length != 1) {
			parent.$.messager.alert('请选择一条记录进行编辑！');
			return;
		} */
		var dialog = parent.sy.modalDialog({
			title : '修改密码',
			width: 400,
			height : 220,
			url : sy.contextPath + '/go.do?path=common/pwdEdit&type=1&id=' + id,
			buttons : [ {
				text : '编辑',
				handler : function() {
					dialog.find('iframe').get(0).contentWindow.submitForm(dialog, grid, parent.$);
				}
			} ]
		});
	};
	
	$(function() {
		grid = $('#grid').datagrid({
			url : sy.contextPath + '/kehu/list',
			columns : [ [ {
				width : $(this).width() * 0.15,
				title : '用户名',
				field : 'account',
				align : 'center',
			},{
				width : $(this).width() * 0.1,
				title : '组织机构名称',
				field : 'orgName',
				align : 'center',
			},{
				width : $(this).width() * 0.1,
				title : '密码重置',
				field : 'initPassword',
				align : 'center',
				formatter : function (v,r,i) {
					return '<a href="javascript:void(0);" onclick="initPassword(\''+r.id+'\');" class="button button-info" title="密码重置">密码重置</a>';
                }
            },{
				width : $(this).width() * 0.15,
				title : '创建时间',
				field : 'createTime',
				align : 'center'
			}
			] ]
		});
	});

	function initPassword(id){
	    var url = sy.contextPath + '/kehu/initPassword';
	    data = {
	        id : id,
		}
		$.post(url,data,function(result){
		    console.log(result)
			var msg = JSON.parse(result)
		    if(msg.code == 1){
		        return layer.msg("修改成功");
			}else{
		        return layer.msg("失败");
			}
		});
	}
</script>
</head>
<body>
	<div id="toolbar">
		<form id="searchForm">
		<div>
			<input type="text" class="easyui-textbox" name="account" style="width: 150px" prompt="账号"/>
			<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-search'" onclick="grid.datagrid('load',sy.serializeObject($('#searchForm')));">过滤</a> 
			<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-search-minus'" onclick="$('#searchForm input').val('');grid.datagrid('load',{});">重置过滤</a>
		</div>
		</form>
		<div class="tbbutton">
			<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-plus-circle',plain:true" onclick="addFun();">添加</a>
			<%--<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-pencil',plain:true" onclick="editFun();">修改资料</a>--%>
			<!-- <a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-file',plain:true" onclick="creditsFun();">积分日志</a> -->
			<!-- <a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-pencil',plain:true" onclick="pwdFun();">修改密码</a> -->
			<%--<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-key',plain:true" onclick="idFun();">查看ID</a>--%>
			<!-- <a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-automobile',plain:true" onclick="showFun();">车辆列表</a>
			<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-calendar',plain:true" onclick="logFun();">服务记录</a> -->
		</div>
	</div>
	<table id="grid" data-options="fit:true,border:false"></table>
</body>
</html>