<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/static/admin/jsp/include.jsp"%>
<script type="text/javascript">
	var id = "${id}";
	var submitForm = function($dialog, $grid, $pjq) {
		if ($('form').form('validate')) {
			var obj=sy.serializeObject($('form'));
			var url=sy.contextPath + '/kehu/save';
			$.post(url, obj, function(result) {
				if (result.code == 0) {
					$grid.datagrid('reload');
					$dialog.dialog('destroy');
				} else {
					$pjq.messager.e('添加失败,'+result.msg);
				}
			}, 'json');
		}
	};
	
	$(function() {
		if (id != '') {
			parent.$.messager.progress({
				text : '数据加载中....'
			});
			$.post(sy.contextPath + '/kehu/findById', {
				id : id
			}, function(result) {
				if (result) {
					$('form').form('load', result);
				}
				parent.$.messager.progress('close');
			}, 'json');
		} else {
			
		}

        $('#orgId').combobox({
            url:sy.contextPath + '/org/orgList.do',
            valueField:'id',
            textField:'orgName',
        });
		
	});
</script>
</head>
<body>
	<form id="form" method="post">
        <input name="id" type="hidden" />
        <div style="padding:15px;font-size: 12px">
            <table style="table-layout:fixed;" border="0" cellspacing="0" class="formtable">
		    	<tr>
		    		<th style="width:100px;">机构名称：</th>
		    		<td>
						<input id="orgId" class="easyui-combobox" name="orgId" style="width:100%;" data-options="editable:false"/>
		    		</td>

		    	</tr>
				<tr>
					<th style="width:100px;">账号：</th>
					<td>
						<input id="account" name="account" class="easyui-textbox" missingMessage="请输入名称" prompt="请输入名称" style="width:100%;"
							   data-options="required:true,validType:['length[0,50]']" />
					</td>
				</tr>
            </table>
        </div>
	</form>
</body>
</html>