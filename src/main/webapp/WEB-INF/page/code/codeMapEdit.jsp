<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/static/admin/jsp/include.jsp"%>
<script type="text/javascript">
	var save = function() {
		if ($('form').form('validate')) {
			var url = sy.contextPath + '/code/saveAll';
			$.post(url, sy.serializeObject($('form')), function(result) {
				if (result.code==0) {
                    parent.$.messager.i('保存成功');
				} else {
                    parent.$.messager.w('操作失败');
				}
			}, 'json');
		}
	};
	
	$(function() {
        $('#tt').tabs({
            border:false,
            onSelect:function(title){
                sy.initFileUpload();
            }
        });

        $('#user_strength_time').numberbox({
            formatter:function (v) {
                if (v%60 == 0){
                    return v;
                }else{
                    return parseInt(v/60)*60;
                }
            } 
        });
//
//        $('#index_activity_content').combobox({
//            url:sy.contextPath + '/code/listNoPage',
//            textField:'',
//            valueField:'code'
//        });

        $('#index_activity_type').combobox({
            onChange:function (value) {
                $('.activityType').hide();
                $('.activityType'+value).show();
            }
        })

        parent.$.messager.progress({
            text : '数据加载中....'
        });
        $.post(sy.contextPath + '/code/listNoPage', {
            //group : 'MAP'
        }, function(result) {
            parent.$.messager.progress('close');
            if (result) {
                var ret = {};
                for (var i in result){
                    var json = result[i];
                    ret[json.code] = json.value;
                }
                $('#index_activity_img').setFileId(ret.index_activity_img,false,true,true);
                $('#user_push_img').setFileId(ret.user_push_img,false,true,true);
                $('#coin_img').setFileId(ret.coin_img,false,true,true);
                $('#coin_small_img').setFileId(ret.coin_small_img,false,true,true);
                $('#coin_ani').setFileId(ret.coin_ani,false,true,true);
                $('#coin_audio').setFileId(ret.coin_audio,false,3,true);
                $('#other_audio').setFileId(ret.other_audio,false,3,true);
                $('form').form('load', ret);

            }
        }, 'json');
	});
</script>
</head>
<body>
    <div style="padding:15px;font-size: 12px;width:50%;">
        <div>
            <a  href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-plus-circle',plain:true" onclick="save();">保存</a>
        </div>
    </div>
	<form id="form" method="post">
        <div id="tt" class="easyui-tabs" style="width:100%;">
            <div title="地图参数" style="padding:20px; ">
                <table style="table-layout:fixed;" border="0" cellspacing="0" class="formtable">
                    <tr>
                        <th style="width:150px;">虚拟点范围(米)：</th>
                        <td>
                             <input id="fictitious_range" name="fictitious_range" style="width:100px;" class="easyui-numberbox" data-options="required:true,min:1,max:9999999999"/>
                        </td>
                    </tr>
                    <tr>
                        <th style="width:150px;">可拾取范围(米)：</th>
                        <td>
                            <input id="fictitious_get_range" name="fictitious_get_range" style="width:100px;" class="easyui-numberbox" data-options="required:true,min:1,max:9999999999"/>
                        </td>
                    </tr>
                    <tr>
                        <th style="width:150px;">商家可见范围(米)：</th>
                        <td>
                            <input id="store_distance" name="store_distance" style="width:100px;" class="easyui-numberbox" data-options="required:true,min:1,max:9999999999"/>
                        </td>
                    </tr>
                    <tr>
                        <th style="width:150px;">商家可操作范围(米)：</th>
                        <td>
                            <input id="store_act_distance" name="store_act_distance" style="width:100px;" class="easyui-numberbox" data-options="required:true,min:1,max:9999999999"/>
                        </td>
                    </tr>
                    <tr>
                        <th style="width:150px;">商家AR可见范围(米)：</th>
                        <td>
                            <input id="store_ar_distance" name="store_ar_distance" style="width:100px;" class="easyui-numberbox" data-options="required:true,min:1,max:9999999999"/>
                        </td>
                    </tr>
                    <tr>
                        <th style="width:150px;">弹幕触发价值(物品金币价值)：</th>
                        <td>
                            <input id="danmu_value" name="danmu_value" style="width:100px;" class="easyui-numberbox" data-options="required:true,min:1,max:9999999999"/>
                        </td>
                    </tr>
                    <tr>
                        <th style="width:150px;">虚拟点最大数量：</th>
                        <td>
                            <input id="fictitious_total" name="fictitious_total" style="width:100px;" class="easyui-numberbox" data-options="required:true,min:1,max:100000"/>
                        </td>
                    </tr>
                    <tr>
                        <th style="width:150px;">固定点最大数量：</th>
                        <td>
                            <input id="regular_total" name="regular_total" style="width:100px;" class="easyui-numberbox" data-options="required:true,min:1,max:100000"/>
                        </td>
                    </tr>
                    <tr>
                        <th style="width:150px;">商家显示最大数量：</th>
                        <td>
                            <input id="store_total" name="store_total" style="width:100px;" class="easyui-numberbox" data-options="required:true,min:1,max:100000"/>
                        </td>
                    </tr>
                    <tr>
                        <th style="width:150px;">VR点显示最大数量：</th>
                        <td>
                            <input id="vr_total" name="vr_total" style="width:100px;" class="easyui-numberbox" data-options="required:true,min:1,max:100000"/>
                        </td>
                    </tr>
                    <tr>
                        <th style="width:150px;">用户投放显示最大数量：</th>
                        <td>
                            <input id="user_push_total" name="user_push_total" style="width:100px;" class="easyui-numberbox" data-options="required:true,min:1,max:100000"/>
                        </td>
                    </tr>
                    <%--<tr>--%>
                        <%--<th style="width:150px;">弹幕模板：</th>--%>
                        <%--<td>--%>
                            <%--<input id="danmu_message" name="danmu_message" style="width:100px;" class="easyui-textbox" data-options="required:true"/>--%>
                            <%--<label>恭喜{}获得{}x{}</label>--%>
                            <%--<label>恭喜{}获得{}x{}</label>--%>
                        <%--</td>--%>
                    <%--</tr>--%>
                </table>
            </div>
            <div title="定时宝箱" style="padding:20px; ">
                <table style="table-layout:fixed;" border="0" cellspacing="0" class="formtable">
                    <tr>
                        <th style="width:100px;">开启定时宝箱：</th>
                        <td>
                            <select id="goods_time_open" name="goods_time_open" style="width:100px;" class="easyui-combobox" panelHeight="auto" editable="false">
                                <option value="1">是</option>
                                <option value="0">否</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th style="width:100px;">定时宝箱时间(分钟)：</th>
                        <td>
                            <input id="goods_time" name="goods_time" style="width:100px;" class="easyui-numberbox" data-options="required:true,min:1,max:9999999999"/>
                        </td>
                    </tr>
                </table>
            </div>
            <%--<div title="金币参数" style="padding:20px; ">--%>
                <%--<table style="table-layout:fixed;" border="0" cellspacing="0" class="formtable">--%>
                    <%--<tr>--%>
                        <%--<th style="width:150px;">金币名称：</th>--%>
                        <%--<td>--%>
                            <%--<input id="coin_name" name="coin_name" style="width:100px;" class="easyui-textbox" data-options="required:true"/>--%>
                        <%--</td>--%>
                    <%--</tr>--%>
                    <%--<tr>--%>
                        <%--<th style="width:100px;">金币图片(小)：</th>--%>
                        <%--<td>--%>
                            <%--<div id="coin_small_img" multi="false"  required="required" type="file" showImage="true" showBtn='true' bestSize='640*320' fileType="jpg,png,gif" fileSize="200MB" buttonText="上传金币图片(小)"></div>--%>
                        <%--</td>--%>
                    <%--</tr>--%>
                    <%--<tr>--%>
                        <%--<th style="width:100px;">金币图片(大)：</th>--%>
                        <%--<td>--%>
                            <%--<div id="coin_img" multi="false"  required="required" type="file" showImage="true" showBtn='true' bestSize='640*320' fileType="jpg,png,gif" fileSize="200MB" buttonText="上传金币图片(大)"></div>--%>
                        <%--</td>--%>
                    <%--</tr>--%>
                    <%--<tr>--%>
                        <%--<th style="width:100px;">金币动画：</th>--%>
                        <%--<td>--%>
                            <%--<div id="coin_ani" multi="false"  required="required" type="file" showImage="true" showBtn='true' bestSize='640*320' fileType="jpg,png,gif" fileSize="200MB" buttonText="上传金币动画"></div>--%>
                        <%--</td>--%>
                    <%--</tr>--%>
                <%--</table>--%>
            <%--</div>--%>
            <div title="用户参数" style="padding:20px; ">
                <table style="table-layout:fixed;" border="0" cellspacing="0" class="formtable">
                    <tr>
                        <th style="width:150px;">用户体力恢复时间(秒)：</th>
                        <td>
                            <input id="user_strength_time" name="user_strength_time" style="width:100px;" class="easyui-numberbox" data-options="required:true,min:60,max:9999999999"/>
                            <label style="color: red;">*</label>
                            <label>该时间间隔请务必大于等于60秒，且必须是60的倍数</label>
                        </td>
                    </tr>
                    <tr>
                        <th style="width:100px;">用户体力最大值：</th>
                        <td>
                            <input id="user_strength_max" name="user_strength_max" style="width:100px;" class="easyui-numberbox" data-options="required:true,min:1,max:9999999999"/>
                        </td>
                    </tr>
                    <tr>
                        <th style="width:100px;">附近的人资源消耗(相对好友)：</th>
                        <td>
                            <input id="user_near_catch" name="user_near_catch" style="width:100px;" class="easyui-numberbox" data-options="required:true,min:1,max:9999999999"/>
                        </td>
                    </tr>
                    <tr>
                        <th style="width:100px;">用户投放可视距离(米)：</th>
                        <td>
                            <input id="user_push_distance" name="user_push_distance" style="width:100px;" class="easyui-numberbox" data-options="required:true,min:1,max:9999999999"/>
                        </td>
                    </tr>
                    <tr>
                        <th style="width:100px;">用户投放图片：</th>
                        <td>
                            <div id="user_push_img" multi="false"  required="required" type="file" showImage="true" showBtn='true' bestSize='640*320' fileType="jpg,png,gif" fileSize="200MB" buttonText="上传用户投放图片"></div>
                        </td>
                    </tr>

                </table>
            </div>
            <div title="活动公告" style="padding:20px; ">
                <table style="table-layout:fixed;" border="0" cellspacing="0" class="formtable">
                    <tr>
                        <th style="width:100px;">开启首页活动公告：</th>
                        <td>
                            <select id="index_activity_show" name="index_activity_show" style="width:100px;" class="easyui-combobox" panelHeight="auto" editable="false">
                                <option value="1">是</option>
                                <option value="0">否</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th style="width:100px;">首页活动公告开始时间：</th>
                        <td>
                            <input id="index_activity_begin" name="index_activity_begin" class="easyui-datetimebox" data-options="required:true" missingMessage="请输入首页活动公告开始时间"/>
                        </td>
                    </tr>
                    <tr>
                        <th style="width:100px;">首页活动公告结束时间：</th>
                        <td>
                            <input id="index_activity_end" name="index_activity_end" class="easyui-datetimebox" data-options="required:true" missingMessage="请输入首页活动公告结束时间"/>
                        </td>
                    </tr>
                    <tr>
                        <th style="width:100px;">首页活动公告类型：</th>
                        <td>
                            <select id="index_activity_type" name="index_activity_type" style="width:100px;" class="easyui-combobox" panelHeight="auto" editable="false">
                                <option value="1">URL</option>
                                <option value="2">富文本</option>
                            </select>
                        </td>
                    </tr>
                    <tr class="activityType activityType1">
                        <th style="width:100px;">首页活动URL：</th>
                        <td>
                            <input id="index_activity_url" name="index_activity_url" class="easyui-textbox" data-options="validType:['url','length[0,100]']" missingMessage="请输入"/>
                        </td>
                    </tr>
                    <tr class="activityType activityType2" style="display: none">
                        <th style="width:100px;">首页公告内容：</th>
                        <td>
                            <label style="color: red;">请去静态页面配置首页活动公告</label>
                        </td>
                    </tr>
                    <tr>
                        <th style="width:100px;">首页公告图片：</th>
                        <td>
                            <div id="index_activity_img" multi="false" fileCountLimit='2' required="required" type="file" showImage="true" showBtn='true' bestSize='640*320' fileType="jpg,png,gif" fileSize="200MB" buttonText="上传首页公告图片"></div>
                        </td>
                    </tr>
                </table>
            </div>
            <div title="音效" style="padding:20px; ">
                <table style="table-layout:fixed;" border="0" cellspacing="0" class="formtable">
                    <tr>
                        <th style="width:100px;">金币音效：</th>
                        <td>
                            <div id="coin_audio" multi="false"  required="required" type="file" showImage="true" showBtn='true' bestSize='640*320' fileType="mp3,wav" fileSize="200MB" buttonText="上传金币音效"></div>
                        </td>
                    </tr>
                    <tr>
                        <th style="width:100px;">其他音效：</th>
                        <td>
                            <div id="other_audio" multi="false"  required="required" type="file" showImage="true" showBtn='true' bestSize='640*320' fileType="mp3,wav" fileSize="200MB" buttonText="上传其他音效"></div>
                        </td>
                    </tr>
                </table>
            </div>
            <div title="客服" style="padding:20px; ">
                <table style="table-layout:fixed;" border="0" cellspacing="0" class="formtable">
                    <tr>
                        <th style="width:150px;">客服电话：</th>
                        <td>
                            <input id="service_tel" name="service_tel" style="width:100px;" class="easyui-textbox" data-options="required:true,validType:['phone']"/>
                        </td>
                    </tr>
                </table>
            </div>
            <div title="订单" style="padding:20px; ">
                <table style="table-layout:fixed;" border="0" cellspacing="0" class="formtable">
                    <tr>
                        <th style="width:150px;">订单付款时间(分钟)：</th>
                        <td>
                            <input id="order_pay_time" name="order_pay_time" style="width:100px;" class="easyui-numberbox" data-options="required:true,min:5,max:10080"/>
                            <label style="color: red;">*</label>
                            <label>5分钟到7天</label>
                        </td>
                    </tr>
                </table>
            </div>
            <div title="分享" style="padding:20px; ">
                <table style="table-layout:fixed;" border="0" cellspacing="0" class="formtable">
                    <tr>
                        <th style="width:150px;">APP分享地址：</th>
                        <td>
                            <input id="share_url" name="share_url" style="width:80%;" class="easyui-textbox" data-options="required:true,validType:['url']"/>
                        </td>
                    </tr>
                </table>
            </div>
            <div title="现金红包" style="padding:20px; ">
                <table style="table-layout:fixed;" border="0" cellspacing="0" class="formtable">
                    <tr>
                        <th style="width:150px;">每日红包使用数量：</th>
                        <td>
                            <input id="hb_day_num_limit" name="hb_day_num_limit" style="width:300px;" class="easyui-numberbox"  data-options="required:true,min:0"/>
                        </td>
                    </tr>
                    <tr>
                        <th style="width:150px;">红包祝福语：</th>
                        <td>
                            <input id="hb_wishing" name="hb_wishing" style="width:300px;" class="easyui-textbox" data-options="required:true"/>
                        </td>
                    </tr>
                    <tr>
                        <th style="width:150px;">活动名称：</th>
                        <td>
                            <input id="hb_act_name" name="hb_act_name" style="width:300px;" class="easyui-textbox" data-options="required:true"/>
                        </td>
                    </tr>
                    <tr>
                        <th style="width:150px;">备注：</th>
                        <td>
                            <input id="hb_remark" name="hb_remark" style="width:300px;" class="easyui-textbox" data-options="required:true"/>
                        </td>
                    </tr>
                    <tr>
                        <th style="width:150px;">商户名称：</th>
                        <td>
                            <input id="hb_send_name" name="hb_send_name" style="width:300px;" class="easyui-textbox" data-options="required:true"/>
                        </td>
                    </tr>
                </table>
            </div>
            <div title="API控制" style="padding:20px; ">
                <table style="table-layout:fixed;" border="0" cellspacing="0" class="formtable">
                    <th style="width:100px;">APP接口调用签名认证：</th>
                    <td>
                        <select id="app_api_verify" name="app_api_verify" style="width:100px;" class="easyui-combobox" panelHeight="auto" editable="false">
                            <option value="0">关闭</option>
                            <option value="1">开启</option>
                        </select>
                    </td>
                    <tr>
                        <th style="width:150px;">API调用签名KEY：</th>
                        <td>
                            <input id="app_api_key" name="app_api_key" style="width:300px;" class="easyui-textbox"/>
                        </td>
                    </tr>
                </table>
            </div>
        </div>

	</form>
</body>
</html>