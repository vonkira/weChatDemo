<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/static/admin/jsp/include.jsp"%>
<script type="text/javascript">
	var grid;

    var addFun = function($dialog, $grid, $pjq) {
        var dialog = parent.sy.modalDialog({
            title : '添加',
            width : 500,
            height : 300,
            url : sy.contextPath + '/go?path=interest/cateEdit',
            buttons : [ {
                text : '保存',
                handler : function() {
                    dialog.find('iframe').get(0).contentWindow.submitForm(dialog, grid, parent.$);
                }
            } ]
        });
    };

    var editFun = function() {
        var rows = grid.datagrid('getSelections');
        if (rows.length != 1) {
            parent.$.messager.w('请选择一条记录进行编辑！');
            return;
        }
//		if (rows[0].id == -1){
//            parent.$.messager.w('系统金币是特殊物品，不能编辑！');
//            return;
//		}

        var dialog = parent.sy.modalDialog({
            title : '编辑',
            width : 500,
            height : 300,
            url : sy.contextPath + '/go?path=interest/cateEdit&id=' + rows[0].id,
            buttons : [ {
                text : '保存',
                handler : function() {
                    dialog.find('iframe').get(0).contentWindow.submitForm(dialog, grid, parent.$);
                }
            } ]
        });
    };

    var delFun = function() {
        var rows = grid.datagrid('getSelections');
        if (rows.length == 0) {
            parent.$.messager.w('请选择需要删除的记录！');
            return;
        }
        parent.$.messager.confirm('询问', '您确定要删除此记录？', function(r) {
            if (r) {
                var ids = [];
                for ( var i = 0, l = rows.length; i < l; i++) {
                    var r = rows[i];
                    ids.push(r.id);
                }
                var id = ids.join(',');

                $.post(sy.contextPath + '/interest/del', {
                    id : id
                }, function(res) {
                    rows.length = 0;//必须，否则有bug
                    grid.datagrid('reload');
                }, 'json');
            }
        });
    };

	$(function() {
		grid = $('#grid').datagrid({
			url : sy.contextPath + '/interest/list',
			singleSelect : false,
            pagination:true,
			columns : [ [ 
				{
					width : $(this).width() * 0.5,
					title : '名称',
					field : 'name',
					align : 'center',
					formatter:function (v,r) {
                        return "<span style=\"width:50px;height:20px;padding:5px;color:white;background-color:"+r.style+"\">"+v+"</span>";
                    }
				}
			] ]
		});
	});
</script>
</head>
<body>
	<div id="toolbar">
		<div class="tbbutton">
			<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-plus-circle',plain:true" onclick="addFun();">添加</a>
			<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-pencil',plain:true" onclick="editFun();">修改</a>
			<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-trash',plain:true" onclick="delFun();">删除</a>
		</div>
	</div>
	<table id="grid" data-options="fit:true,border:false"></table>
</body>
</html>