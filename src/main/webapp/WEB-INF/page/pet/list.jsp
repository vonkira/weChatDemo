<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/static/admin/jsp/include.jsp"%>
<script type="text/javascript">
	var grid;
	
	var addFun = function($dialog, $grid, $pjq) {
		var dialog = parent.sy.modalDialog({
			title : '添加宠物',
			width : 700,
			height : 580,
			url : sy.contextPath + '/go?path=pet/edit',
			buttons : [ {
				text : '保存',
				handler : function() {
					dialog.find('iframe').get(0).contentWindow.submitForm(dialog, grid, parent.$);
				}
			} ]
		});
	};
	
	var editFun = function() {
		var rows = grid.datagrid('getSelections');
		if (rows.length != 1) {
			parent.$.messager.w('请选择一条记录进行编辑！');
			return;
		}
		var dialog = parent.sy.modalDialog({
			title : '编辑宠物',
			width : 700,
			height : 580,
			url : sy.contextPath + '/go?path=pet/edit&id=' + rows[0].id,
			buttons : [ {
				text : '保存',
				handler : function() {
					dialog.find('iframe').get(0).contentWindow.submitForm(dialog, grid, parent.$);
				}
			} ]
		});
	};

	var delFun = function() {
		var rows = grid.datagrid('getSelections');
		if (rows.length == 0) {
			parent.$.messager.w('请选择需要删除的记录！');
			return;
		}
		parent.$.messager.confirm('询问', '您确定要删除此记录？', function(r) {
			if (r) {
				var ids = [];
				for ( var i = 0, l = rows.length; i < l; i++) {
					var r = rows[i];
					if (r.id == '0'){
                        parent.$.messager.w('平台商家不能删除！');
                        return;
					}
					ids.push(r.id);
				}
				var id = ids.join(',');
				
				$.post(sy.contextPath + '/pet/del', {
					id : id
				}, function(res) {
					if (res.code == 0) {
						rows.length = 0;//必须，否则有bug
						grid.datagrid('reload');
					} else {
						parent.$.messager.w(res.msg);
					}
				}, 'json');
			}
		});
	};
	
	//查看ID
	var idFun = function() {
		var rows = grid.datagrid('getSelections');
		if (rows.length != 1) {
			parent.$.messager.w('请选择一条记录进行复制！');
			return;
		}
		sy.getId(rows[0].id,'复制');
	};
	
	$(function() {
		grid = $('#grid').datagrid({
			url : sy.contextPath + '/pet/list',
			singleSelect : true,
			frozenColumns : [ [
				{
				width : '150',
				title : '名称',
				field : 'name',
				align : 'center',
				formatter: function(value, row, index) {
					return UT.addTitle(value);
				}
			}
			] ],
			columns : [ [ 
				{
					width : '100',
					title : '小图',
					field : 'img',
					align : 'center',
                    formatter : function(value, row, index) {
                        return po.showImg(value,18,18);
                    }
				},{
					width : '100',
					title : '大图',
					field : 'largeImg',
					align : 'center',
                    formatter : function(value, row, index) {
                        return po.showImg(value,18,18);
                    }
				},{
					width : '100',
					title : '获取动画',
					field : 'getImg',
					align : 'center',
                    formatter : function(value, row, index) {
                        return po.showImg(value,18,18);
                    }
				}
			] ]
		});
	});

</script>
</head>
<body>
	<div id="toolbar">
		<div class="tbbutton">
			<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-plus-circle',plain:true" onclick="addFun();">添加</a>
			<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-pencil',plain:true" onclick="editFun();">修改</a>
			<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-trash',plain:true" onclick="delFun();">删除</a>
		</div>
	</div>
	<table id="grid" data-options="fit:true,border:false"></table>
</body>
</html>