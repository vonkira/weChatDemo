<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/static/admin/jsp/include.jsp"%>
<script type="text/javascript">
	var id = "${id}";
	var submitForm = function($dialog, $grid, $pjq) {
		var obj=sy.serializeObject($('form'));
		var img = $('#img').getFileId();
		if (img == '') {
			$pjq.messager.e('请上传小图');
			return;
		}
		var largeImg = $('#largeImg').getFileId();
		if (largeImg == '') {
			$pjq.messager.e('请上传大图 ');
			return;
		}
		var getImg = $('#getImg').getFileId();
		if (getImg == '') {
			$pjq.messager.e('请上传大图 ');
			return;
		}
		obj.img = img;
		obj.largeImg = largeImg;
		obj.getImg = getImg;
		if ($('form').form('validate')) {
			var url=sy.contextPath + '/pet/save';
			$.post(url, obj, function(result) {
				if (result.code == 0) {
					$grid.datagrid('reload');
					$dialog.dialog('destroy');
				} else {
					$pjq.messager.e('添加失败,'+result.msg);
				}
			}, 'json');
		}
	};
	
	$(function() {
		if (id != '') {
			parent.$.messager.progress({
				text : '数据加载中....'
			});
			$.post(sy.contextPath + '/pet/findById.do', {
				id : id
			}, function(result) {
				if (result) {
                    $('#img').setFileId(result.img, true, true, true);
                    $('#largeImg').setFileId(result.largeImg, true, true, true);
                    $('#getImg').setFileId(result.getImg, true, true, true);
					$('form').form('load', result);
				}
				parent.$.messager.progress('close');
			}, 'json');
		}
	});
</script>
</head>
<body>
	<form id="form" method="post">
        <input name="id" type="hidden" />
		<div style="padding:15px;font-size: 12px">
			<table style="table-layout:fixed;" border="0" cellspacing="0" class="formtable">
				<tr>
					<th style="width:100px;">宠物名称：</th>
					<td colspan="3">
						<input id="name" name="name" class="easyui-textbox" style="width:90%" data-options="required:true,validType:'length[0,50]'" missingMessage="请输入商家名称" prompt="请输入商家名称" />
					</td>
				</tr>
				<tr>
					<th style="width:100px;">宠物小图：</th>
					<td colspan="3">
						<div id="img" multi="false" showHeight="50" showWidth="50" required="required" type="file" showImage="true" showBtn="true" bestSize="226*226" fileType="jpg,png" fileSize="2MB" buttonText="上传小图"></div>
						<label style="color: red;">*</label>
						<label>建议尺寸：宽度500，高度500，格式JPG,PNG</label>
					</td>
				</tr>
				<tr>
					<th style="width:100px;">宠物大图：</th>
					<td colspan="3">
						<div id="largeImg" multi="false" showHeight="50" showWidth="50" required="required" type="file" showImage="true" showBtn="true" bestSize="226*226" fileType="jpg,png,gif" fileSize="2MB" buttonText="上传大图"></div>
						<label style="color: red;">*</label>
						<label>建议尺寸：宽度500，高度500，格式JPG,PNG,GIF</label>
					</td>
				</tr>
				<tr>
					<th style="width:100px;">宠物获取动画：</th>
					<td colspan="3">
						<div id="getImg" multi="false" showHeight="50" showWidth="50" required="required" type="file" showImage="true" showBtn="true" bestSize="226*226" fileType="jpg,png,gif" fileSize="2MB" buttonText="上传获取动画"></div>
						<label style="color: red;">*</label>
						<label>建议尺寸：宽度640，高度254，格式JPG,PNG,GIF</label>
					</td>
				</tr>
			</table>
		</div>
	</form>
</body>
</html>