<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/static/admin/jsp/include.jsp"%>
<script type="text/javascript">
$(document).keyup(function(event) {
	if (event.keyCode == 13) {
		submitForm();
	}
});

var submitForm = function() {
	if ($('form').form('Validate')) {
		var url = sy.contextPath + '/car/vinQuery';
		var obj = sy.serializeObject($('form'));
		parent.$.messager.progress({
			text : '提交中....'
		});
		$.post(url, obj , function(result) {
			parent.$.messager.progress('close');
			if (result.code == 0) {
                for(var key in result.data){
                    $("#"+key).html(result.data[key]);
                }
			} else {
				parent.$.messager.e(result.msg);
			}
		}, 'json');
	}
};
</script>
</head>
<body class="easyui-layout">
	<form id="form">
        <div style="padding:15px;font-size: 12px">
            <div style="margin-top: 100px;text-align: center">
                <input id="code" type="text" name="code" data-options="required:true,validType:['length[17,17]'],height:50,buttonText:'查&nbsp;&nbsp;询',onClickButton:submitForm" class="easyui-textbox" missingMessage="请输入17位车架号" prompt="请输入17位车架号" style="width:400px;"/>
            </div>
            <table style="table-layout:fixed;margin-top: 10px;margin-bottom: 15px" border="0" cellspacing="0" class="formtable">
                <tr>
                    <th style="width:100px;">厂家名称：</th>
                    <td id="MANUFACTURER"></td>
                    <th style="width:100px;">品牌：</th>
                    <td id="BRAND"></td>
                </tr>
                <tr>
                    <th style="width:100px;">车系：</th>
                    <td id="SERIES"></td>
                    <th style="width:100px;">车型：</th>
                    <td id="MODELS"></td>
                </tr>
                <tr>
                    <th style="width:100px;">车身形式：</th>
                    <td id="BODY"></td>
                    <th style="width:100px;">年款：</th>
                    <td id="YEAR"></td>
                </tr>
                <tr>
                    <th style="width:100px;">车架号：</th>
                    <td id="VIN"></td>
                    <th style="width:100px;">排量：</th>
                    <td id="DISPLACEMENT"></td>
                </tr>
                <tr>
                    <th style="width:100px;">变速箱：</th>
                    <td id="TRANSMISSION"></td>
                    <th style="width:100px;">档位数：</th>
                    <td id="GEAR"></td>
                </tr>
                <tr>
                    <th style="width:100px;">换挡类型：</th>
                    <td id="TRANSMISSIONTYPE"></td>
                    <th style="width:100px;">发动机型号：</th>
                    <td id="ENGINE"></td>
                </tr>
                <tr>
                    <th style="width:100px;">排放标准：</th>
                    <td id="DISCHARGE"></td>
                    <th style="width:100px;">上市年月：</th>
                    <td id="LISTINGDATE"></td>
                </tr>
                <tr>
                    <th style="width:100px;">生产年份：</th>
                    <td id="MANUFACTUREDATE"></td>
                    <th style="width:100px;">指导价格：</th>
                    <td id="PRICE"></td>
                </tr>
                <tr>
                    <th style="width:100px;">车门车座：</th>
                    <td id="BODYTYPE"></td>
                    <th style="width:100px;">车辆级别：</th>
                    <td id="CARLEVEL"></td>
                </tr>
                <tr>
                    <th style="width:100px;">燃油标号：</th>
                    <td id="FUEL"></td>
                    <th style="width:100px;">驱动方式：</th>
                    <td id="DRIVEMODE"></td>
                </tr>
                <tr>
                    <th style="width:100px;">发动机参数：</th>
                    <td id="ENGINETYPE"></td>
                    <th style="width:100px;">车辆配置名称：</th>
                    <td id="NAME"></td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>