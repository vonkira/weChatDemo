<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/static/admin/jsp/include.jsp"%>
<script type="text/javascript">
	var grid;
	var goodsId='${goodsId}';
	$(function() {
		grid = $('#grid').datagrid({
			url : sy.contextPath + '/storeGoodsOrderDetail/listByGoods?goodsId='+goodsId,
			columns : [ [{
				width : $(this).width() * 0.2,
				title : '时间',
				field : 'createTime',
				align : 'center'
			},
			{
				width : $(this).width() * 0.2,
				title : '数量',
				field : 'num',
				align : 'center'
			},
			{
				width : $(this).width() * 0.2,
				title : '成本',
				field : 'cost',
				align : 'center'
			},
			{
				width : $(this).width() * 0.2,
				title : '售价',
				field : 'price',
				align : 'center'
			}
			] ]
		});
	});
</script>
</head>
<body>
	<table id="grid" data-options="fit:true,border:false"></table>
</body>
</html>