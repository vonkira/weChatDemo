<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/static/admin/jsp/include.jsp"%>
<script type="text/javascript">
	var grid;
	var setNum = function(id) {
		layer.prompt({
		  formType: 0,
		  title: '请输入数量：',
		}, function(value, index, elem) {
			if (UT.IntNotZero(value)) {
				var url = sy.contextPath + '/storeGoodsCart/setNum';
				$.post(url, {goodsId:id,num:value}, function() {
					grid.datagrid('reload');
				}, 'json');
			}else{
				$.messager.w("请输入大于0的整数！");
			}
			layer.close(index);
		});
	};
	var del = function(id) {
		parent.$.messager.confirm('询问', '您确定要移除此记录？', function(r) {
			if (r) {
				$.post(sy.contextPath + '/storeGoodsCart/del', {
					id : id
				}, function() {
					grid.datagrid('reload');
				}, 'json');
			}
		});
	};
	var addFun = function(type) {
		var rows = grid.datagrid('getSelections');
		if (rows.length == 0) {
			parent.$.messager.w('请至少选择一条记录！');
			return;
		}
		
		var ids = [];
		for ( var i = 0, l = rows.length; i < l; i++) {
			var r = rows[i];
			ids.push(r.id);
		}
		var id = ids.join(',');
		
		$.post(sy.contextPath + '/storeGoodsOrder/addBefore', {
			ids : id,
			type:type
		}, function(ret) {
			if(ret.code == 0){
				parent.$.messager.confirm('询问', ret.data+'<br/>确定下单吗？', function(r) {
					if (r) {
						$.post(sy.contextPath + '/storeGoodsOrder/add', {
							ids : id,
							type:type
						}, function(result) {
							if(result.code == 0){
								$.messager.i("下单成功,请至[采购订单]查看！");
								rows.length = 0;//必须，否则有bug
								grid.datagrid('reload');
							}else{
								$.messager.e(result.msg);
							}
							
						}, 'json');
					}
				});
			}else{
				$.messager.e(ret.msg);
			}
			
		}, 'json');
		
		
	};
	$(function() {
		grid = $('#grid').datagrid({
			url : sy.contextPath + '/storeGoodsCart/list',
			singleSelect : false,
			pagination:false,
			frozenColumns : [ [ {
				width : '100',
				checkbox:true,
				field : 'id',
				align : 'center'
			}] ],
			columns : [ [ 
				 {
					width : $(this).width() * 0.2,
					title : '商品名称',
					field : 'name',
					align : 'center',
					formatter: function(value, row, index) {
						return UT.addTitle(value);
					}
				},{
					width : $(this).width() * 0.1,
					title : '商品编号',
					field : 'code',
					align : 'center'
				},{
					width : $(this).width() * 0.1,
					title : '商品分类',
					field : 'cateName',
					align : 'center'
				},{
                    width : $(this).width() * 0.08,
                    title : '品牌',
                    field : 'brandName',
                    align : 'center'
                },{
                    width : $(this).width() * 0.08,
                    title : '型号',
                    field : 'model',
                    align : 'center'
                },{
                    width : $(this).width() * 0.08,
                    title : '规格',
                    field : 'standard',
                    align : 'center'
                },{
					width : $(this).width() * 0.1,
					title : '数量',
					field : 'num',
					align : 'center'
				},{
                    width : $(this).width() * 0.1,
                    title : '本店',
                    field : 'storeTotal',
                    align : 'center',
					formatter:function (v) {
						return v?v:0;
                    }
                },{
                    width : $(this).width() * 0.1,
                    title : '总仓',
                    field : 'total',
                    align : 'center'
                },{
					width : $(this).width() * 0.2,
					title : '操作',
					field : 'op',
					align : 'center',
					formatter:function(value, row, index){
						var html = "";
						html+='<a href="javascript:void(0);" onclick="setNum(\''+row.id+'\')" class="button button-info" title="修改数量">修改数量</a>';
						html+=' <a href="javascript:void(0);" onclick="del(\''+row.id+'\')" class="button button-warning" title="移除">移除</a>';
						return html;
					}
				}
			] ]
		});
	});
</script>
</head>
<body>
	<div id="toolbar">
		<div>
			<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-plus-circle',plain:true" onclick="addFun(1);">采购</a>
			<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-plus-circle',plain:true" onclick="addFun(2);">第三方销售</a>
		</div>
	</div>
	<table id="grid" data-options="fit:true,border:false"></table>
</body>
</html>