<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <%@ include file="/static/admin/jsp/include.jsp" %>
    <script type="text/javascript">
        var grid;
        var addFun = function ($dialog, $grid, $pjq) {
            var dialog = parent.sy.modalDialog({
                title: '添加员工',
                height: 400,
                width:800,
                url: sy.contextPath + '/go?path=storeCenter/workerEdit',
                buttons: [{
                    text: '保存',
                    handler: function () {
                        dialog.find('iframe').get(0).contentWindow.submitForm(dialog, grid, parent.$);
                    }
                }]
            });
        };
        var editFun = function () {
            var rows = grid.datagrid('getSelections');
            if (rows.length != 1) {
                parent.$.messager.w('请选择一条记录进行编辑！');
                return;
            }
            var dialog = parent.sy.modalDialog({
                title: '编辑信息',
                height: 400,
                width:800,
                url: sy.contextPath + '/go?path=storeCenter/workerEdit&id=' + rows[0].id,
                buttons: [{
                    text: '保存',
                    handler: function () {
                        dialog.find('iframe').get(0).contentWindow.submitForm(dialog, grid, parent.$);
                    }
                }]
            });
        };
        var delFun = function () {
            var rows = grid.datagrid('getSelections');
            if (rows.length == 0) {
                parent.$.messager.w('请选择需要删除的记录！');
                return;
            }
            parent.$.messager.confirm('询问', '您确定要删除此记录？', function (r) {
                if (r) {
                    var ids = [];
                    for (var i = 0, l = rows.length; i < l; i++) {
                        var r = rows[i];
                        ids.push(r.id);
                    }
                    var id = ids.join(',');

                    $.post(sy.contextPath + '/storeMember/del', {
                        id: id
                    }, function () {
                        rows.length = 0;//必须，否则有bug
                        grid.datagrid('reload');
                    }, 'json');
                }
            });
        };

        $(function () {
        	var _url = sy.contextPath + '/storeMember/list';
            grid = $('#grid').datagrid({
                url: _url,
                columns: [[
                    {
                        width: $(this).width() * 0.1,
                        title: '工号',
                        field: 'code',
                        align: 'center'
                    },
                    {
                        width: $(this).width() * 0.1,
                        title: '姓名',
                        field: 'name',
                        align: 'center',
                        formatter: function (value, row, index) {
                            return UT.addTitle(value);
                        }
                    },
                    {
                        width: $(this).width() * 0.1,
                        title: '电话',
                        field: 'phone',
                        align: 'center',
                        formatter: function (value, row, index) {
                            return UT.addTitle(value);
                        }
                    },
                    {
                        width: $(this).width() * 0.15,
                        title: '身份证',
                        field: 'idCard',
                        align: 'center',
                        formatter: function (value, row, index) {
                            return UT.addTitle(value);
                        }
                    },
                    {
                        width: $(this).width() * 0.15,
                        title: '地址',
                        field: 'address',
                        align: 'center',
                        formatter: function (value, row, index) {
                            return UT.addTitle(value);
                        }
                    }, {
                        width: $(this).width() * 0.1,
                        title: '性别',
                        field: 'sex',
                        align: 'center',
                        formatter: function (value, row, index) {
                            switch (value) {
                                case 0:
                                    return UT.addLabel("女", "red");
                                case 1:
                                    return UT.addLabel("男", "blue");
                            }
                        }
                    }, {
                        width: $(this).width() * 0.1,
                        title: '状态',
                        field: 'state',
                        align: 'center',
                        formatter: function (value, row, index) {
                            switch (value) {
                                case 0:
                                    return UT.addLabel("离职", "grey");
                                case 1:
                                    return UT.addLabel("在职", "blue");
                            }
                        }
                    },
                    {
                        width: $(this).width() * 0.2,
                        title: '创建时间',
                        field: 'createTime',
                        align: 'center'
                    }
                ]]
            });
        });
    </script>
</head>
<body>
<div id="toolbar">
    <div>
        <a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-plus-circle',plain:true"
           onclick="addFun();">添加</a>
        <a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-pencil',plain:true"
           onclick="editFun();">修改</a>
        <a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-trash',plain:true"
           onclick="delFun();">删除</a>
    </div>
</div>
<table id="grid" data-options="fit:true,border:false"></table>
</body>
</html>