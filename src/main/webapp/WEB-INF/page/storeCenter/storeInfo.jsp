<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/static/admin/jsp/include.jsp"%>
<script type="text/javascript">
	$(function() {
		$.post(sy.contextPath + '/store/findById', {
			id : sy.id
		}, function(result) {
			if (result) {
				for(var key in result){
					if(key == 'type'){
						if(result[key] == 1){
							$("#"+key).html("保养店");
						}else{
							$("#"+key).html("维修店");
						}
					}else if(key == 'openBegin'){
						$("#"+key).html(result["openBegin"].substring(11,16) + " ~ " + result["openEnd"].substring(11,16));
					}else if(key == 'state'){
						$("input[name='state'][value="+result[key]+"]").attr("checked",true); 
						
						$('input:radio[name="state"]').change(function(){
							var state = $("input[name='state']:checked").val();
							var url=sy.contextPath + '/store/save';
							$.post(url, {id:sy.id,state:state}, function(result) {
								$.messager.i("修改成功！");
							}, 'json');
						});  
					}else if(key == 'img' || key == 'imgs'){
						if(result[key]) $("#"+key).html(po.showImg(result[key],30,30));
					}else{
						$("#"+key).html(result[key]);
					}
				}
			}
		}, 'json');
	});
</script>
</head>
<body>
	<form id="form" method="post">
		<div style="padding:15px;font-size: 12px">
			<h3>本店信息：</h3>
			<table style="table-layout:fixed;margin-top: 10px" border="0" cellspacing="0" class="formtable">
				<tr>
					<th style="width:100px;">营业状态：</th>
					<td>
						<input type="radio" name="state" id="state1" value="1"/><label for="state1"> 空闲</label>
						<input type="radio" name="state" id="state2" value="2"/><label for="state2"> 繁忙</label>
						<input type="radio" name="state" id="state3" value="3"/><label for="state3"> 歇业</label>
					</td>
				</tr>
			</table>
			<%--<table style="table-layout:fixed;margin-top: 10px" border="0" cellspacing="0" class="formtable">--%>
				<%--<tr>--%>
					<%--<th style="width:100px;">余额：</th>--%>
					<%--<td id="balance"></td>--%>
				<%--</tr>--%>
			<%--</table>--%>
			<table style="table-layout:fixed;margin-top: 10px" border="0" cellspacing="0" class="formtable">
				<tr>
					<th style="width:100px;">类型：</th>
					<td id="type"></td>
					<th style="width:100px;">账号：</th>
					<td id="account"></td>
				</tr>
				<tr>
					<th style="width:100px;">门店名称：</th>
					<td id="name"></td>
					<th style="width:100px;">别名：</th>
					<td id="nickName"></td>
				</tr>
				<tr>
					<th style="width:100px;">营业时间：</th>
					<td id="openBegin"></td>
					<th style="width:100px;">工位数量：</th>
					<td id="num"></td>
				</tr>
				<tr>
					<th style="width:100px;">地址：</th>
					<td id="address"></td>
					<th style="width:100px;">电话：</th>
					<td id="phone"></td>
				</tr>
				<tr>
					<th style="width:100px;">LOGO：</th>
					<td id="img"></td>
					<th style="width:100px;">组图：</th>
					<td id="imgs"></td>
				</tr>
			</table>
		</div>
	</form>
</body>
</html>