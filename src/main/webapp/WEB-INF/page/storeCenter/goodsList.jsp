<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/static/admin/jsp/include.jsp"%>
<script type="text/javascript">
	var grid;
	//查看商品详情
	var infoFun = function(cont) {
		layer.open({
			type: 2,
		    skin: 'layui-layer-rim', //加上边框
		    area: ['500px', '300px'], //宽高
		    content: sy.contextPath+'/detail?id='+cont+'&type=1' 
		}); 
	};
	var addFun = function(id) {
		layer.prompt({
		  formType: 0,
		  title: '请输入预购数量：',
		}, function(value, index, elem) {
			if (UT.IntNotZero(value)) {
				var url = sy.contextPath + '/storeGoodsCart/save';
				$.post(url, {goodsId:id,num:value}, function() {
					$.messager.i("添加成功！");
				}, 'json');
			}else{
				$.messager.w("请输入大于0的整数！");
			}
			layer.close(index);
		});
	};
	$(function() {
		$('#cateId').combobox({
			textField: 'title',
			valueField: 'id',
			url : sy.contextPath + '/goodsCate/list.do?flag=1',
			onChange: function(data) {
				grid.datagrid('load',sy.serializeObject($('#searchForm')));
			}
		});
		
		grid = $('#grid').datagrid({
			url : sy.contextPath + '/goods/storeList',
			singleSelect : false,
			frozenColumns : [ [{
				width : $(this).width() * 0.2,
				title : '商品名称',
				field : 'name',
				align : 'center',
				formatter: function(value, row, index) {
					return UT.addTitle(value);
				}
			},{
				width : $(this).width() * 0.1,
				title : '商品编号',
				field : 'code',
				align : 'center'
			}
			] ],
			columns : [ [ 
				{
					width : $(this).width() * 0.1,
					title : '商品分类',
					field : 'cateName',
					align : 'center'
				},{
					width : $(this).width() * 0.08,
					title : '本店库存',
					field : 'storeTotal',
					align : 'center',
					formatter : function(value, row, index) {
						if(value>row.warnNum){
							return UT.addLabel(value,"green");
						}else{
							return UT.addLabel(value,"red");
						}
					}
				},{
                    width : $(this).width() * 0.08,
                    title : '总店库存',
                    field : 'total',
                    align : 'center'
                },{
                    width : $(this).width() * 0.08,
                    title : '品牌',
                    field : 'brandName',
                    align : 'center'
                },{
                    width : $(this).width() * 0.08,
                    title : '型号',
                    field : 'model',
                    align : 'center'
                },{
                    width : $(this).width() * 0.08,
                    title : '规格',
                    field : 'standard',
                    align : 'center'
                },{
					width : $(this).width() * 0.08,
					title : '图文详情',
					field : 'detail',
					align : 'center',
					formatter:function(v,r,i){
						return "<a href=\"javascript:void(0)\" style=\"color:blue;\" onclick=\"infoFun(\'"+r.id+"\')\" >详情</a>";
					}
				},{
					width : $(this).width() * 0.1,
					title : '是否上架',
					field : 'isOnline',
					align : 'center',
					formatter : function(value, row, index) {
						switch (value) {
						case 1:
							return UT.addLabel("已上架","green");
						case 0:
							return UT.addLabel("已下架","grey");
						}
					}
				},{
					width : $(this).width() * 0.1,
					title : '操作',
					field : 'op',
					align : 'center',
					formatter:function(value, row, index){
						var html = "";
						html+='<a href="javascript:void(0);" onclick="addFun(\''+row.id+'\')" class="button button-warning" title="加入预购">加入预购</a>';
						return html;
					}
				}
			] ]
		});
	});
</script>
</head>
<body>
	<div id="toolbar">
		<form id="searchForm">
		<div>
			<span>商品分类：</span>
			<input id="cateId" name="cateId" editable="false" style="width:150px;" panelHeight='150'
				data-options="valueField: 'id',textField: 'title'" />
			<span>商品名称：</span>
			<input type="text" class="easyui-textbox" name="name" style="width: 150px" prompt="商品名称"/>
			<span>商品编号：</span>
			<input type="text" class="easyui-textbox" name="code" style="width: 150px" prompt="商品编号"/>
			<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-search'" onclick="grid.datagrid('load',sy.serializeObject($('#searchForm')));">过滤</a> 
			<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-search-minus'" onclick="$('#searchForm input').val('');grid.datagrid('load',{});">重置过滤</a>
		</div>
		</form>
	</div>
	<table id="grid" data-options="fit:true,border:false"></table>
</body>
</html>