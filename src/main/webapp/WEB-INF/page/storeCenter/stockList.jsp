<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/static/admin/jsp/include.jsp"%>
<script type="text/javascript">
	var grid;
	//查看商品详情
	var infoFun = function(cont) {
		layer.open({
			type: 2,
		    skin: 'layui-layer-rim', //加上边框
		    area: ['500px', '300px'], //宽高
		    content: sy.contextPath+'/detail?id='+cont+'&type=1' 
		}); 
	};
	function exportExcel(){
        var obj = sy.serializeObject($('#searchForm'));
        var cateId = obj.cateId?obj.cateId:"";
        var name = obj.name?obj.name:"";
        var code = obj.code?obj.code:"";
	    location.href = sy.basePath + "storeGoods/export?cateId="+cateId+"&code="+code+"&name="+name;
	}
	var setWarn = function(id) {
		layer.prompt({
		  formType: 0,
		  title: '请输入库存预警值：',
		}, function(value, index, elem) {
			if (UT.isInt(value)) {
				var url = sy.contextPath + '/storeGoods/save';
				$.post(url, {goodsId:id,warnNum:value}, function() {
					grid.datagrid('reload');
				}, 'json');
			}else{
				$.messager.w("请输入整数！");
			}
			layer.close(index);
		});
	};
	
	var backStock = function(id) {
		layer.prompt({
		  formType: 0,
		  title: '请输入数量：',
		}, function(value, index, elem) {
			if (UT.IntNotZero(value)) {
				var url = sy.contextPath + '/storeGoods/backStock';
				$.post(url, {goodsId:id,total:value}, function() {
					$.messager.i("提交成功！");
					grid.datagrid('reload');
				}, 'json');
			}else{
				$.messager.w("请输入整数！");
			}
			layer.close(index);
		});
	};
	var logs = function(id) {
		var url = sy.contextPath + '/go?path=storeCenter/stockLogs&goodsId=' + id;
		var dialog = parent.sy.modalDialog({
			title : '采购日志',
			width : 800,
			height : 600,
			url : url
		});
	};
	
	$(function() {
		$('#cateId').combobox({
			textField: 'title',
			valueField: 'id',
			url : sy.contextPath + '/goodsCate/list.do?flag=1',
			onChange: function(data) {
				grid.datagrid('load',sy.serializeObject($('#searchForm')));
			}
		});
		
		grid = $('#grid').datagrid({
			url : sy.contextPath + '/storeGoods/list',
			singleSelect : false,
			frozenColumns : [ [{
				width : $(this).width() * 0.2,
				title : '商品名称',
				field : 'name',
				align : 'center',
				formatter: function(value, row, index) {
					return UT.addTitle(value);
				}
			},{
				width : $(this).width() * 0.1,
				title : '商品编号',
				field : 'code',
				align : 'center'
			}
			] ],
			columns : [ [ 
				{
					width : $(this).width() * 0.1,
					title : '商品分类',
					field : 'cateName',
					align : 'center'
				},{
					width : $(this).width() * 0.08,
					title : '库存',
					field : 'storeTotal',
					align : 'center',
					formatter : function(value, row, index) {
						if(value>row.warnNum){
							return UT.addLabel(value,"green");
						}else{
							return UT.addLabel(value,"red");
						}
					}
				},{
					width : $(this).width() * 0.08,
					title : '库存预警值',
					field : 'storeWarnNum',
					align : 'center'
				},{
					width : $(this).width() * 0.1,
					title : '图文详情',
					field : 'detail',
					align : 'center',
					formatter:function(v,r,i){
						return "<a href=\"javascript:void(0)\" style=\"color:blue;\" onclick=\"infoFun(\'"+r.id+"\')\" >详情</a>";
					}
				},{
					width : $(this).width() * 0.08,
					title : '状态',
					field : 'isOnline',
					align : 'center',
					formatter : function(value, row, index) {
						if(row.isDel == 1){
							return UT.addLabel("已删除","red");
						}
						
						switch (value) {
						case 1:
							return UT.addLabel("已上架","green");
						case 0:
							return UT.addLabel("已下架","grey");
						}
					}
				},{
					width : $(this).width() * 0.25,
					title : '操作',
					field : 'op',
					align : 'center',
					formatter:function(value, row, index){
						var html = "";
						html+='<a href="javascript:void(0);" onclick="setWarn(\''+row.id+'\')" class="button button-info" title="设置预警值">设置预警值</a>';
						html+=' <a href="javascript:void(0);" onclick="logs(\''+row.id+'\')" class="button button-default" title="采购日志">采购日志</a>';
						html+=' <a href="javascript:void(0);" onclick="backStock(\''+row.id+'\')" class="button button-warning" title="退仓">退仓</a>';
						return html;
					}
				}
			] ]
		});
	});
</script>
</head>
<body>
	<div id="toolbar">
		<form id="searchForm">
		<div>
			<span>商品分类：</span>
			<input id="cateId" name="cateId" editable="false" style="width:150px;" panelHeight='150'
				data-options="valueField: 'id',textField: 'title'" />
			<span>商品名称：</span>
			<input type="text" class="easyui-textbox" name="name" style="width: 150px" prompt="商品名称"/>
			<span>商品编号：</span>
			<input type="text" class="easyui-textbox" name="code" style="width: 150px" prompt="商品编号"/>
			<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-search'" onclick="grid.datagrid('load',sy.serializeObject($('#searchForm')));">过滤</a> 
			<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-search-minus'" onclick="$('#searchForm input').val('');grid.datagrid('load',{});">重置过滤</a>
			<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-file-excel-o'" onclick="exportExcel()">导出Excel</a>
		</div>
		</form>
	</div>
	<table id="grid" data-options="fit:true,border:false"></table>
</body>
</html>