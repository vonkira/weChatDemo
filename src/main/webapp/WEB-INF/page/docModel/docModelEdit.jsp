<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/static/admin/jsp/include.jsp"%>
<script type="text/javascript">
	var id = "${id}";
	var submitForm = function($dialog, $grid, $pjq) {
		if ($('form').form('validate')) {
			var obj=sy.serializeObject($('form'));
				obj.state = 0
			var url=sy.contextPath + '/contractPath/save';
			$.post(url, obj, function(result) {
				if (result.code == 0) {
					$grid.datagrid('reload');
					$dialog.dialog('destroy');
				} else {
					$pjq.messager.e('添加失败,'+result.msg);
				}
			}, 'json');
		}
	};
	
	$(function() {
		if (id != '') {
			parent.$.messager.progress({
				text : '数据加载中....'
			});
			$.post(sy.contextPath + '/contractPath/findById', {
				id : id
			}, function(result) {
				if (result) {
					$('form').form('load', result);
                    $('#filePath').setFileId(result.filePath, false, true, true);
				}
				parent.$.messager.progress('close');
			}, 'json');
		} else {
			
		}

        $('#cityId').combobox({
            url:sy.contextPath + '/contractPath/contractList.do',
            valueField:'id',
            textField:'conName',
        });
	});
</script>
</head>
<body>
	<form id="form" method="post">
        <input name="id" type="hidden" />
        <div style="padding:15px;font-size: 12px">
            <table style="table-layout:fixed;" border="0" cellspacing="0" class="formtable">
		    	<tr>
					<th style="width:100px;">合同标题：</th>
					<td>
						<input id="title" class="easyui-textbox" name="title" style="width:100%;" />
					</td>
		    	</tr>
				<tr>
					<th style="width:100px;">上传合同模板附件：</th>
					<td>
						<div id="filePath" multi="false" required="required"  showWidth="40" showHeight="40"  type="file" showImage="true" showBtn="true"
							 fileType="docx" fileSize="2MB" buttonText="上传合同模板"></div>
					</td>
				</tr>
            </table>
        </div>
	</form>
</body>
</html>