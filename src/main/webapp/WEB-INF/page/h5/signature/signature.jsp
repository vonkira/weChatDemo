<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!doctype html>
<html>
<head>
<%@ include file="/static/m/jsp/include.jsp"%>
<meta charset="utf-8">
<title>签名</title>
<meta name="description" content="Signature Pad - HTML5 canvas based smooth signature drawing using variable width spline interpolation.">
  <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=no">
  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-status-bar-style" content="black">
<link rel="stylesheet" href="${path}/static/m/h5/signature.css">
	<script type="text/javascript" src="${path}/static/m/h5/jquery.min.js"></script>

	<script  type="text/javascript">
		var readPath = '${contract.readPath}';
		var id = '${contract.id}';
		$(function(){
		    $("#subBtn").attr("data-id",id);
		});

		function check() {
			window.location.href= sy.contextPath+"/download?id="+ readPath;
        }


	</script>
</head>
<body onselectstart="return false">
	<div id="signature-pad" class="m-signature-pad">
		<div id="signature-pad-title" class="m-signature-pad--title-vi">
			请工整的书写您的姓名&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		</div>
		<div class="m-signature-pad--body">
			<canvas></canvas>
		</div>
		<div id="signature-pad-footer" class="m-signature-pad--footer-vi">
            <button type="button" class="button clear"  id="msg" style="display: none;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;正在保存...&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</button>
			<a onclick="check()" class="btn_primary" data-action="look">查看合同</a>
			<button type="button" class="button clear" data-action="clear" id="clearButton" >清除</button>
 	        <!-- <input type="hidden" value="" id="type">  -->
			<button type="button" class="button save" data-action="save" id="subBtn">保存</button>
			<%--<button type="button"  id="saveSign">保存</button>--%>
		</div>
	</div>
	<script type="text/javascript" src="${path}/static/m/h5/signature_pad.js"></script>
	<script type="text/javascript" src="${path}/static/m/h5/signature.js"></script>

</BODY>
</HTML>