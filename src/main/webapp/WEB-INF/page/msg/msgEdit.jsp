<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/static/admin/jsp/include.jsp"%>
<script type="text/javascript">
	var id = "${id}";
	var submitForm = function($dialog, $grid, $pjq) {
		if ($('form').form('validate')) {
			var obj=sy.serializeObject($('form'));
			var url=sy.contextPath + '/msg/save';
			$.post(url, obj, function(result) {
				if (result.code == 0) {
					$grid.datagrid('reload');
					$dialog.dialog('destroy');
				} else {
					$pjq.messager.e('添加失败,'+result.msg);
				}
			}, 'json');
		}
	};
	
	var importFun = function() {
		var dialog = parent.sy.modalDialog({
			title : '通过模板导入数据',
			width : 600,
			height : 300,
			url : sy.contextPath + '/go?path=car/importData&type=5',
			buttons : [ {
				text : '导入',
				handler : function() {
					dialog.find('iframe').get(0).contentWindow.submitForm(dialog, null, parent.$, writeContent);
				}
			} ]
		});
	};
	
	var writeContent = function(ret) {
		$('#accounts').textbox("setValue", ret);
	};
	
	$(function() {
		$('#type').combobox({   
			onSelect: function(rec){   
				if(rec.code == '2'){
					$('#account').show();
					$('#accounts').textbox({required:true});
				}else {
					$('#account').hide();
					$('#accounts').textbox("reset");
					$('#accounts').textbox({required:false});
				}
	        }
		});
		
		if (id != '') {
			parent.$.messager.progress({
				text : '数据加载中....'
			});
			$.post(sy.contextPath + '/msg/findById', {
				id : id
			}, function(result) {
				if (result) {
					if(result.type != 2){
						$('#account').hide();
						$('#accounts').textbox("reset");
						$('#accounts').textbox({required:false});
					}else{
						$('#account').show();
						$('#accounts').textbox({required:true});
					}
					
					$('form').form('load', result);
				}
				parent.$.messager.progress('close');
			}, 'json');

		}else{
		}
		
		
	});
</script>
</head>
<body>
	<form id="form" method="post">
        <input name="id" type="hidden" />
        <div style="padding:15px;font-size: 12px">
            <table style="table-layout:fixed;" border="0" cellspacing="0" class="formtable">
            	<tr>
            		<th style="width:100px;">类型:</th>
		    		<td>
		    			<select id="type" name="type" class="easyui-combobox" missingMessage="请选择类型" editable="false" panelHeight='auto' 
                    	 	data-options="required:true,valueField: 'code',textField: 'name',data: [{name: '全部',code: '1',selected:true},{name: '个人',code: '2'}]"/>
		    		</td>
		    	</tr>
		    	<tr id="account" style="display: none;">
		    		<th>用户账号：</th>
		    		<td>
		    			<input class="easyui-textbox" id="accounts" name="accounts" type="text" style="width:70%;" missingMessage="请输入用户账号，以英文符,分隔" prompt="请输入用户账号，以英文符,分隔" />
		    			<a id="btn" class="easyui-linkbutton" data-options="iconCls:'ext-icon-search'" onclick="importFun()">导入</a>
		    		</td>
		    	</tr>
		       	<tr>
		       		<th>消息内容:</th>
		    		<td>
		    			<input id="content" name="content" class="easyui-textbox" style="height: 200px"  data-options="required:true,multiline:true,validType:['length[0,50]']" missingMessage="请输入消息内容" prompt="请输入消息内容"></input>
		    			<p style="color: gray;">推送内容长度不要过长，否则有被截断的风险。</p>
		    		</td>
		    	</tr>
            </table>
        </div>
	</form>
</body>
</html>