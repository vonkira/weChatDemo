<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/static/admin/jsp/include.jsp"%>
<script type="text/javascript">
	var grid;
	var id='${id}';
	$(function() {
		grid = $('#grid').datagrid({
			url : sy.contextPath + '/storeGoodsOrderDetail/list?id='+id,
			pagination:false,
			columns : [ [{
				width : $(this).width() * 0.5,
				title : '商品名称',
				field : 'goodsName',
				align : 'center'
			},
			{
				width : $(this).width() * 0.2,
				title : '商品编号',
				field : 'goodsCode',
				align : 'center'
			},
			{
				width : $(this).width() * 0.1,
				title : '成本',
				field : 'cost',
				align : 'center'
			},
			{
				width : $(this).width() * 0.1,
				title : '售价',
				field : 'price',
				align : 'center'
			},
			{
				width : $(this).width() * 0.1,
				title : '数量',
				field : 'num',
				align : 'center'
			}
			] ]
		});
	});
</script>
</head>
<body>
	<table id="grid" data-options="fit:true,border:false"></table>
</body>
</html>