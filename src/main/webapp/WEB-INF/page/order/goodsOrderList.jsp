<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/static/admin/jsp/include.jsp"%>
<script type="text/javascript">
	var grid;
	var logs = function(id) {
		var url = sy.contextPath + '/go?path=order/goodsOrderDetails&id=' + id;
		var dialog = parent.sy.modalDialog({
			title : '订单详情',
			width : 1000,
			height : 600,
			url : url
		});
	};
	var send = function(id) {
		parent.$.messager.confirm('询问', '确定要发货吗？', function(r) {
			if (r) {
				$.post(sy.contextPath + '/storeGoodsOrder/send', {
					id : id
				}, function(ret) {
					if(ret.code != 0){
						$.messager.e(ret.msg);
					}
					grid.datagrid('reload');
				}, 'json');
			}
		});
	};
	var get = function(id) {
		parent.$.messager.confirm('询问', '确定要入库吗？', function(r) {
			if (r) {
				$.post(sy.contextPath + '/storeGoodsOrder/stock', {
					id : id
				}, function(ret) {
					if(ret.code != 0){
						$.messager.e(ret.msg);
					}
					grid.datagrid('reload');
				}, 'json');
			}
		});
	};
	var toPay = function(id,type) {
		$.post(sy.contextPath + '/storeGoodsOrder/toPay', {
			id : id,
			type:type
		}, function(ret) {
			if(ret.code != 0){
				$.messager.e(ret.msg);
			}else{
			    if (type == 2){
                    sy.showImg(sy.basePath+"qr?size=400&msg="+encodeURIComponent(ret.msg),"支付二维码");
				}else if (type == 1){
                    sy.showImg(sy.basePath+"qr?size=400&msg="+encodeURIComponent(ret.msg),"支付二维码");
//			        window.open(ret.msg);
				}

			}
		}, 'json');
	};
	$(function() {
		$('#type').combobox({
			textField: 'title',
			valueField: 'id',
			data:[{id:1,title:"门店采购"},{id:2,title:"第三方销售"}],
			onChange: function(data) {
				grid.datagrid('load',sy.serializeObject($('#searchForm')));
			}
		});
		
		grid = $('#grid').datagrid({
			url : sy.contextPath + '/storeGoodsOrder/list',
			singleSelect : true,
			columns : [ [ 
				{
					width : $(this).width() * 0.1,
					title : '门店',
					field : 'storeName',
					align : 'center',
					hidden:sy.system == 'store'
				},{
					width : $(this).width() * 0.15,
					title : '订单编号',
					field : 'code',
					align : 'center',
					formatter : function(value, row, index) {
						return UT.addTitle(value);
					}
				},{
					width : $(this).width() * 0.08,
					title : '类型',
					field : 'type',
					align : 'center',
					formatter : function(value, row, index) {
						return value == 1?"门店采购":"第三方销售";
					}
				},{
					width : $(this).width() * 0.08,
					title : '总价',
					field : 'total',
					align : 'center'
				},{
					width : $(this).width() * 0.08,
					title : '状态',
					field : 'state',
					align : 'center',
					formatter : function(value, row, index) {
						if(row.type == 1){
							if(value == 1){
								return sy.system == 'admin'?"待发货":"采购中";
							}else if(value == 2){
								return "已发货";
							}else if(value == 3){
								return "已入库";
							}
						}else{
							if(value == 1){
								return "待支付";
							}else if(value == 2){
								return "";
							}else if(value == 3){
								return "已支付";
							}
						}
					}
				},{
					width : $(this).width() * 0.1,
					title : '支付方式',
					field : 'payType',
					align : 'center',
					formatter : function(value, row, index) {
						if(value == 1){
							return "支付宝";
						}else if(value == 2){
							return "微信";
						}
						return "——";
					}
				},{
					width : $(this).width() * 0.1,
					title : '支付流水号',
					field : 'tradeNo',
					align : 'center',
					formatter : function(value, row, index) {
						return value?vlaue:"——";
					}
				},{
					width : $(this).width() * 0.15,
					title : '创建时间',
					field : 'createTime',
					align : 'center'
				},{
					width : $(this).width() * 0.2,
					title : '操作',
					field : 'op',
					align : 'center',
					formatter:function(value, row, index){
						var html = "";
						html+='<a href="javascript:void(0);" onclick="logs(\''+row.id+'\')" class="button button-success" title="查看详情">查看详情</a>';
						if(sy.system == "admin"){
							if(row.type == 1 && row.state == 1){
								html+=' <a href="javascript:void(0);" onclick="send(\''+row.id+'\')" class="button button-info" title="发货">发货</a>';
							}
						}else{
							if(row.type == 1 && row.state == 2){
								html+=' <a href="javascript:void(0);" onclick="get(\''+row.id+'\')" class="button button-info" title="入库">入库</a>';
							}
							if(row.type == 2 && row.state == 1){
//								html+=' <a href="javascript:void(0);" onclick="toPay(\''+row.id+'\',1)" class="button button-info" title="支付宝支付">支付宝支付</a>';
								html+=' <a href="javascript:void(0);" onclick="toPay(\''+row.id+'\',2)" class="button button-info" title="微信支付">微信支付</a>';
							}
						}
						return html;
					}
				}
			] ]
		});
	});
</script>
</head>
<body>
	<div id="toolbar">
		<form id="searchForm">
		<div>
			<span>订单分类：</span>
			<input id="type" name="type" editable="false" style="width:150px;" panelHeight='150' data-options="valueField: 'id',textField: 'title'" />
			<span>门店名称：</span>
			<input type="text" class="easyui-textbox" name="name" style="width: 150px" prompt="门店名称"/>
			<span>订单编号：</span>
			<input type="text" class="easyui-textbox" name="code" style="width: 150px" prompt="订单编号"/>
			<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-search'" onclick="grid.datagrid('load',sy.serializeObject($('#searchForm')));">过滤</a> 
			<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-search-minus'" onclick="$('#searchForm input').val('');grid.datagrid('load',{});">重置过滤</a>
		</div>
		</form>
		<!-- <div class="tbbutton">
			<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-plus-circle',plain:true" onclick="addFun();">添加</a>
			<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-pencil',plain:true" onclick="editFun();">修改</a>
			<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-trash',plain:true" onclick="delFun();">删除</a>
			<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-key',plain:true" onclick="idFun();">查看ID</a>
		</div> -->
	</div>
	<table id="grid" data-options="fit:true,border:false"></table>
</body>
</html>