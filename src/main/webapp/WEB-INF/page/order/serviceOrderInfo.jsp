<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/static/admin/jsp/include.jsp"%>
<script type="text/javascript">
    var grid;
    var array = [];
    var v_id = '${id}';

    var payMethod = {
        1:'支付宝',
		2:'微信APP',
		3:'银联',
		4:'微信公众号'
	};

    $(function() {
        grid = $('#grid').datagrid({
            singleSelect : true,
			pagination:false,
            emptyMsg:'请添加配件信息！',
            showFooter:false,
            columns : [ [
                {
                    width : $(this).width() * 0.1,
                    title : '商品编号',
                    field : 'code',
                    align : 'center'
                },{
                    width : $(this).width() * 0.2,
                    title : '商品名称',
                    field : 'name',
                    align : 'center',
                    formatter: function(value, row, index) {
                        return UT.addTitle(value);
                    }
                },{
                    width : $(this).width() * 0.1,
                    title : '成本',
                    field : 'storeCost',
					hidden:sy.system == 'store',
                    align : 'center'
                },{
                    width : $(this).width() * 0.1,
                    title : '单价',
                    field : 'storePrice',
                    align : 'center'
                },{
                    width : $(this).width() * 0.1,
                    title : '服务费',
                    field : 'service',
                    align : 'center'
                },{
                    width : $(this).width() * 0.1,
                    title : '数量',
                    field : 'num',
                    align : 'center'
                }
            ] ]
        });

        parent.$.messager.progress({
            text : '数据加载中....'
        });
        $.post(sy.contextPath + '/userOrder/detail', {
            id : v_id
        }, function(result) {
            parent.$.messager.progress('close');
            if (result) {
                for(var key in result.order){
                    if(key == 'type'){
                        $("#"+key).html(result.order[key] == 1?'保养单':'维修单');
					}else if(key == 'source'){
                        $("#"+key).html(result.order[key] == 1?'用户下单':('门店派单('+result.sourceStoreName+')'));
					}else if(key == 'payType'){
                        $("#"+key).html(payMethod[result.order[key]]);
					}else{
                        $("#"+key).html(result.order[key]);
                    }
                }
                array = result.array;
                grid.datagrid('loadData',array);
            }
        }, 'json');
    });
</script>
</head>
<body>
	<form id="form" method="post">
		<input name="id" type="hidden" />
		<div style="padding:15px;font-size: 12px">
			<h3>
				基本信息：
			</h3>
			<table style="table-layout:fixed;margin-top: 10px;margin-bottom: 15px" border="0" cellspacing="0" class="formtable">
				<tr>
					<th style="width:100px;">订单编号：</th>
					<td id="code"></td>
					<th style="width:100px;">门店名称：</th>
					<td id="storeName"></td>
				</tr>
				<tr>
					<th style="width:100px;">类型：</th>
					<td id="type"></td>
					<th style="width:100px;">来源：</th>
					<td id="source"></td>
				</tr>
				<tr>
					<th style="width:100px;">金额：</th>
					<td id="price">
					</td>
					<th style="width:100px;">服务费：</th>
					<td id="service">
					</td>
				</tr>
				<tr>
					<th style="width:100px;">优惠券：</th>
					<td id="couponValue">
					</td>
					<th style="width:100px;">优惠券描述：</th>
					<td id="couponInfo">
					</td>
				</tr>
				<tr>
					<th style="width:100px;">支付方式：</th>
					<td id="payType">
					</td>
					<th style="width:100px;">支付流水号：</th>
					<td id="tradeNo">
					</td>
				</tr>
				<tr>
					<th style="width:100px;">创建时间：</th>
					<td id="createTime">
					</td>
					<th style="width:100px;">预约时间：</th>
					<td id="orderTime">
					</td>
				</tr>
				<tr>
					<th style="width:100px;">修改时间：</th>
					<td id="modifyTime">
					</td>
					<th style="width:100px;">支付时间：</th>
					<td id="payTime">
					</td>
				</tr>
			</table>
			<table style="table-layout:fixed;margin-top: 10px;margin-bottom: 15px" border="0" cellspacing="0" class="formtable">
				<tr>
					<th style="width:100px;">车主姓名：</th>
					<td id="carUser"></td>
					<th style="width:100px;">车主电话：</th>
					<td id="carUserPhone"></td>
				</tr>
				<tr>
					<th style="width:100px;">车牌号：</th>
					<td id="carNumber"></td>
					<th style="width:100px;">车型：</th>
					<td id="carType"></td>
				</tr>
				<tr>
					<th style="width:100px;">车架号：</th>
					<td id="carVin">
					</td>
					<th style="width:100px;">发动机编号：</th>
					<td id="carEngineCode">
					</td>
				</tr>
				<tr>
					<th style="width:100px;">接待人：</th>
					<td id="operatorName">
					</td>
					<th style="width:100px;">技师：</th>
					<td id="storeMemberName">
					</td>
				</tr>
				<tr>
					<th style="width:100px;">里程数：</th>
					<td id="carMeter">
					</td>
				</tr>
				<tr>
					<th style="width:100px;">保险公司：</th>
					<td id="insurerFirm">
					</td>
					<th style="width:100px;">保险到期：</th>
					<td id="insurerTime">
					</td>
				</tr>
				<tr>
					<th style="width:100px;">车辆状况描述：</th>
					<td id="carInfo" colspan="3">
					</td>
				</tr>
			</table>
			<h3>配件信息：</h3>
			<table id="grid" data-options="fit:true,border:false"></table>
		</div>
	</form>
</body>
</html>