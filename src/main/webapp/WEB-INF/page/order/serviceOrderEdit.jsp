<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/static/admin/jsp/include.jsp"%>
<script type="text/javascript">
    var grid,selected;
    var array = [];
    var v_id = '${id}';

    Array.prototype.indexOf = function(val) {
        for (var i = 0; i < this.length; i++) {
            if (this[i].id == val) return i;
        }
        return -1;
    };
    Array.prototype.remove = function(val) {
        var index = this.indexOf(val);
        if (index > -1) {
            this.splice(index, 1);
        }
    };

    var del = function(id) {
        array.remove(id);
        grid.datagrid('loadData',array);
    };
    var add = function() {
		if(selected != null){
            var hasInclude = false;
            $.each(array,function(i,o){
                if(o.id == selected.id){
                    hasInclude = true;
                }
            });
            if(!hasInclude) {
                array.push(selected);
                grid.datagrid('loadData',{rows:array,footer:[{id:"footer",name:"合计",storePrice: 1, servicePrice: 2}]});
            }else{
                $.messager.w("该配件已添加");
            }
            $('#keyword').combobox("clear");
            selected = null;
		}
    };
    var numSub = function(id) {
        $.each(array,function(i,o){
            if(o.id == id){
                if(o.num == 1){
                    del(id);
				}else{
                    o.num--;
				}
            }
        });
        grid.datagrid('loadData',array);
    };
    var numAdd = function(id) {
        $.each(array,function(i,o){
            if(o.id == id){
				o.num++;
            }
        });
        grid.datagrid('loadData',array);
    };
    var setServicePrice = function(id) {
        layer.prompt({
            formType: 0,
            title: '请输入金额：',
        }, function(value, index, elem) {
            if (UT.isInt(value) && value > -1) {
                $.each(array,function(i,o){
                    if(o.id == id){
                        o.servicePrice = value;
                    }
                });
                grid.datagrid('loadData',array);
            }else{
                $.messager.w("请输入非负整数！");
            }
            layer.close(index);
        });
    };
    function onComboboxHidePanel() {
        var el = $(this);
        el.combobox('textbox').focus();
        // 检查录入内容是否在数据里
        var opts = el.combobox("options");
        var data = el.combobox("getData");
        var value = el.combobox("getValue");
        // 有高亮选中的项目, 则不进一步处理
        var panel = el.combobox("panel");
        var items = panel.find(".combobox-item-selected");
        if (items.length > 0) {
            var values = el.combobox("getValues");
            el.combobox("setValues", values);
            return;
        }
        var allowInput = opts.allowInput;
        if (allowInput) {
            var idx = data.length;

            data[idx] = [];
            data[idx][opts.textField] = value;
            data[idx][opts.valueField] = value;
            el.combobox("loadData", data);
        } else {
            // 不允许录入任意项, 则清空
            el.combobox("clear");
        }
    }
    function submitForm($dialog, $grid, $pjq) {
        if ($('form').form('Validate')) {
            var obj=sy.serializeObject($('form'));

            if(array.length == 0){
                $.messager.w("请添加配件信息");
                return;
            }

            //配件信息
			obj.details = sy.jsonToString(array);

            $.messager.progress({
                text : '数据提交中....'
            });
            var url=sy.contextPath + '/userOrder/edit';
            $.post(url, obj, function(result) {
                $.messager.progress('close');
                if (result.code == 0) {
                    $grid.datagrid('reload');
                    $dialog.dialog('destroy');
                } else {
                    $.messager.e(result.msg);
                }
            }, 'json');
        }
    }

    $(function() {
        $('#keyword').combobox({
            textField: 'title',
            valueField: 'id',
            url : sy.contextPath + '/goods/storeAll',
            hasDownArrow:false,
            filter: function(q, row){
                var opts = $(this).combobox('options');
                return row[opts.textField].toLowerCase().indexOf(q.toLowerCase()) != -1;
            },
            onSelect:function (record) {
                record.num = 1;
                selected = record;
            },
            onHidePanel: onComboboxHidePanel,
        });

        $('#storeMemberId').combobox({
            textField: 'name',
            valueField: 'id',
            url : sy.contextPath + '/storeMember/all'
        });

        $('#operatorId').combobox({
            textField: 'name',
            valueField: 'id',
            url : sy.contextPath + '/storeMember/allWith',
            onLoadSuccess: function () { //加载完成后,设置选中第一项
                var val = $(this).combobox('getData');
                for (var item in val[0]) {
                    if (item == 'id') {
                        $(this).combobox('select', val[0][item]);
                    }
                }
            }
        });


        grid = $('#grid').datagrid({
            singleSelect : false,
			pagination:false,
            emptyMsg:'请添加配件信息！',
            showFooter:false,
            columns : [ [
                {
                    width : $(this).width() * 0.1,
                    title : '商品编号',
                    field : 'code',
                    align : 'center'
                },{
                    width : $(this).width() * 0.2,
                    title : '商品名称',
                    field : 'name',
                    align : 'center',
                    formatter: function(value, row, index) {
                        return UT.addTitle(value);
                    }
                },{
                    width : $(this).width() * 0.1,
                    title : '单价',
                    field : 'storePrice',
                    align : 'center'
                },{
                    width : $(this).width() * 0.1,
                    title : '服务费',
                    field : 'servicePrice',
                    align : 'center',
                    formatter:function(value, row, index){
                        if(row.id == 'footer')return value;
                        var html = "";
                        html+=(value?value:'')+' ';
                        html+='<a href="javascript:void(0);" onclick="setServicePrice(\''+row.id+'\')" class="button button-info" title="设置">设置</a>';
                        return html;
                    }
                },{
                    width : $(this).width() * 0.1,
                    title : '数量',
                    field : 'num',
                    align : 'center',
                    formatter:function(value, row, index){
                        if(row.id == 'footer')return "";
                        var html = "";
                        html+='<a href="javascript:void(0);" onclick="numSub(\''+row.id+'\')" class="button button-warning" title="减">-</a>';
						html+=' '+value+' ';
                        html+='<a href="javascript:void(0);" onclick="numAdd(\''+row.id+'\')" class="button button-info" title="加">+</a>';
                        return html;
                    }
                },{
                    width : $(this).width() * 0.1,
                    title : '操作',
                    field : 'op',
                    align : 'center',
                    formatter:function(value, row, index){
                        if(row.id == 'footer')return "";
                        var html = "";
                        html+='<a href="javascript:void(0);" onclick="del(\''+row.id+'\')" class="button button-success" title="删除">删除</a>';
                        return html;
                    }
                }
            ] ]
        });

        parent.$.messager.progress({
            text : '数据加载中....'
        });
        $.post(sy.contextPath + '/userOrder/detail', {
            id : v_id
        }, function(result) {
            parent.$.messager.progress('close');
            if (result) {
                for(var key in result.order){
					$("#"+key).html(result.order[key]);
                }
                $('form').form('load', result.order);

                array = result.array;
                grid.datagrid('loadData',array);
            }
        }, 'json');
    });
</script>
</head>
<body>
	<form id="form" method="post">
		<input name="id" type="hidden" />
		<div style="padding:15px;font-size: 12px">
			<h3>
				基本信息：
			</h3>
			<table style="table-layout:fixed;margin-top: 10px;margin-bottom: 15px" border="0" cellspacing="0" class="formtable">
				<tr>
					<th style="width:100px;">车主姓名：</th>
					<td id="carUser"></td>
					<th style="width:100px;">车主电话：</th>
					<td id="carUserPhone"></td>
				</tr>
				<tr>
					<th style="width:100px;">车牌号：</th>
					<td id="carNumber"></td>
					<th style="width:100px;">车型：</th>
					<td id="carType"></td>
				</tr>
				<tr>
					<th style="width:100px;">车架号：</th>
					<td>
						<input class="easyui-textbox" data-options="validType:['length[17,17]']" name="carVin" type="text" missingMessage="请输入车架号" prompt="请输入车架号" id="carVin"  />
					</td>
					<th style="width:100px;">发动机编号：</th>
					<td>
						<input class="easyui-textbox" data-options="validType:['length[0,20]']" name="carEngineCode" type="text" missingMessage="请输入发动机编号" prompt="请输入发动机编号" id="carEngineCode"  />
					</td>
				</tr>
				<tr>
					<th style="width:100px;">接待人：</th>
					<td>
						<select id="operatorId" name="operatorId" editable="false" data-options="required:true" prompt="请选择接待人" missingMessage="请选择接待人" ></select>
					</td>
					<th style="width:100px;">技师：</th>
					<td>
						<select id="storeMemberId" name="storeMemberId" editable="false" data-options="required:true,multiple:true" prompt="请选择技师"/>
					</td>
				</tr>
				<tr>
					<th style="width:100px;">里程数：</th>
					<td>
						<input id="carMeter" name="carMeter" class="easyui-numberbox" data-options="min:0,max:99999999" prompt="请输入里程数" missingMessage="请输入里程数" />
					</td>
				</tr>
				<tr>
					<th style="width:100px;">保险公司：</th>
					<td>
						<input class="easyui-textbox" data-options="validType:['length[0,20]']" name="insurerFirm" type="text" missingMessage="请输入保险公司" prompt="请输入保险公司" id="insurerFirm"  />
					</td>
					<th style="width:100px;">保险到期：</th>
					<td>
						<input  id="insurerTime" name="insurerTime" type= "text" editable="false" class= "easyui-datebox" prompt="请输入保险到期日"> </input>
					</td>
				</tr>
				<tr>
					<th style="width:100px;">车辆状况描述：</th>
					<td>
						<input class="easyui-textbox" data-options="validType:['length[0,150]'],multiline:true" style="height: 80px;" name="carInfo" type="text" missingMessage="请输入车辆状况描述" prompt="请输入车辆状况描述" id="carInfo"  />
					</td>
					<th style="width:100px;"></th>
					<td>
					</td>
				</tr>
			</table>
			<h3>配件信息：</h3>
			<div id="toolbar">
				<div>
					<input id="keyword" editable="true" style="width:500px;" panelHeight='200' prompt="输入编号或名称搜索"/>
					<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-plus-circle'" onclick="add()">添加</a>
					<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-rotate-left'" onclick="resetText()">清空</a>
				</div>
			</div>
			<table id="grid" data-options="fit:true,border:false"></table>
		</div>
	</form>
</body>
</html>