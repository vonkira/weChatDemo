<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/static/admin/jsp/include.jsp"%>
<script type="text/javascript">
	var grid;
    function pushRepair(id){
        var dialog = parent.sy.modalDialog({
            title : '维修订单推送',
			width: 600,
            height : 350,
            url : sy.contextPath + '/go?path=order/pushRepair&id='+id,
            buttons : [ {
                text : '保存',
                handler : function() {
                    dialog.find('iframe').get(0).contentWindow.submitForm(dialog, grid, parent.$);
                }
            } ]
        });
    }
    function edit(id){
        var dialog = parent.sy.modalDialog({
            title : '编辑订单',
			width:1200,
            height : 600,
            url : sy.contextPath + '/go?path=order/serviceOrderEdit&id='+id,
            buttons : [ {
                text : '保存',
                handler : function() {
                    dialog.find('iframe').get(0).contentWindow.submitForm(dialog, grid, parent.$);
                }
            } ]
        });
    }
    function info(id){
        var dialog = parent.sy.modalDialog({
            title : '订单详情',
            width:1200,
            height : 600,
            url : sy.contextPath + '/go?path=order/serviceOrderInfo&id='+id
        });
    }
    function beginOrder(id){
        parent.$.messager.confirm('询问', '车主已到店，即将开始服务？', function(r) {
            if (r) {
                $.post(sy.contextPath + '/userOrder/beginOrder', {
                    id : id
                }, function(ret) {
                    if(ret.code != 0){
                        parent.$.messager.e(ret.msg);
                    }
                    grid.datagrid('reload');
                }, 'json');
            }
        });
    }

	function cofirmOrder(id,type){
        parent.$.messager.confirm('询问', '您确定要开始'+(type == 1?'保养':'维修')+'吗？', function(r) {
            if (r) {
                $.post(sy.contextPath + '/userOrder/cofirmOrder', {
                    id : id
                }, function(ret) {
                    if(ret.code != 0){
                        parent.$.messager.e(ret.msg);
                    }
                    grid.datagrid('reload');
                }, 'json');
            }
        });
	}

    function completeOrder(id,type){
        parent.$.messager.confirm('询问', '您确定要将订单设为待付款吗？', function(r) {
            if (r) {
                $.post(sy.contextPath + '/userOrder/completeOrder', {
                    id : id
                }, function(ret) {
                    if(ret.code != 0){
                        parent.$.messager.e(ret.msg);
					}
                    grid.datagrid('reload');
                }, 'json');
            }
        });
    }

	$(function() {
        $('#state').combobox({
            textField: 'n',
            valueField: 'v',
			data:[{n:"已取消",v:-1},{n:"已预约",v:0},{n:"确认中",v:1},{n:"服务中",v:2},{n:"待付款",v:3},{n:"已完成",v:4}],
            onChange: function(data) {
                grid.datagrid('load',sy.serializeObject($('#searchForm')));
            }
        });
		
		grid = $('#grid').datagrid({
			url : sy.contextPath + '/userOrder/list',
			singleSelect : true,
			columns : [ [ 
				{
					width : $(this).width() * 0.1,
					title : '门店',
					field : 'storeName',
					align : 'center',
					hidden:sy.system == 'store',
					formatter : function(value, row, index) {
						return UT.addTitle(value);
					}
				},{
					width : $(this).width() * 0.2,
					title : '订单编号',
					field : 'code',
					align : 'center',
					formatter : function(value, row, index) {
						return UT.addTitle(value);
					}
				},{
					width : $(this).width() * 0.05,
					title : '类型',
					field : 'type',
					align : 'center',
					formatter : function(value, row, index) {
						return value == 1?"保养":"维修";
					}
				},{
					width : $(this).width() * 0.08,
					title : '总价',
					field : 'price',
					align : 'center',
					formatter : function(value, row, index) {
						return UT.addTitle2(value.add(row.service).sub(row.couponValue),row.couponInfo);
					}
				},{
					width : $(this).width() * 0.1,
					title : '车牌号',
					field : 'carNumber',
					align : 'center',
					formatter : function(value, row, index) {
						return UT.addTitle(value);
					}
				},{
					width : $(this).width() * 0.05,
					title : '状态',
					field : 'state',
					align : 'center',
					formatter : function(value, row, index) {
						if(value == -1){
							return "已取消";
						}else if(value == 0){
							return "预约中";
						}else if(value == 1){
							return "确认中";
						}else if(value == 2){
							return row.type == 1?"保养中":"维修中";
						}else if(value == 3){
							return "待付款";
						}else if(value == 4){
                            return "已完成";
                        }
					}
				},{
					width : $(this).width() * 0.08,
					title : '员工',
					field : 'storeMemberName',
					align : 'center'
				},{
					width : $(this).width() * 0.1,
					title : '预约时间',
					field : 'orderTime',
					align : 'center'
				},{
					width : $(this).width() * 0.15,
					title : '创建时间',
					field : 'createTime',
					align : 'center'
				},{
					width : $(this).width() * 0.15,
					title : '操作',
					field : 'op',
					align : 'center',
					formatter:function(value, row, index){
						var html = "";
						if(sy.system == 'store'){
						    if(row.state > 0 && row.state < 3){
                                html+='<a href="javascript:void(0);" onclick="edit(\''+row.id+'\')" class="button button-info" title="修改订单">修改</a>';
							}

							if(row.state == 0){
                                html+=' <a href="javascript:void(0);" onclick="beginOrder(\''+row.id+'\')" class="button button-warning" title="设为确认中">车主到店</a>';
							}else if(row.state == 1){
							    if(row.type == 1){
                                    html+=' <a href="javascript:void(0);" onclick="cofirmOrder(\''+row.id+'\',1)" class="button button-warning" title="设为保养中">开始保养</a>';
								}else{
                                    html+=' <a href="javascript:void(0);" onclick="cofirmOrder(\''+row.id+'\',2)" class="button button-warning" title="设为维修中">开始维修</a>';
								}
							}else if(row.state == 2){
                                if(row.type == 1){
                                    html+=' <a href="javascript:void(0);" onclick="completeOrder(\''+row.id+'\',1)" class="button button-warning" title="设为待付款">保养完成</a>';
                                }else{
                                    html+=' <a href="javascript:void(0);" onclick="completeOrder(\''+row.id+'\',2)" class="button button-warning" title="设为待付款">维修完成</a>';
                                }
							}else if(row.state > 2){
                                html+='<a href="javascript:void(0);" onclick="info(\''+row.id+'\')" class="button button-default" title="查看详情">查看详情</a>';
							}
							if(row.type == 1 && row.state > 0 && row.state < 5 && row.isPushRepair != 1){
                                html+=' <a href="javascript:void(0);" onclick="pushRepair(\''+row.id+'\')" class="button button-warning" title="推送维修订单">推送维修单</a>';
							}
						}else{
                            if(row.state > 2){
                                html+='<a href="javascript:void(0);" onclick="info(\''+row.id+'\')" class="button button-default" title="查看详情">查看详情</a>';
                            }
						}
						return html;
					}
				}
			] ]
		});
	});
</script>
</head>
<body>
	<div id="toolbar">
		<form id="searchForm">
		<div>
			<span>状态：</span>
			<input id="state" name="state" editable="false" style="width:150px;"/>
			<span>门店名称：</span>
			<input type="text" class="easyui-textbox" name="name" style="width: 150px" prompt="门店名称"/>
			<span>订单编号：</span>
			<input type="text" class="easyui-textbox" name="code" style="width: 150px" prompt="订单编号"/>
			<span>车牌号：</span>
			<input type="text" class="easyui-textbox" name="carNumber" style="width: 150px" prompt="车牌号"/>
			<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-search'" onclick="grid.datagrid('load',sy.serializeObject($('#searchForm')));">过滤</a> 
			<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-search-minus'" onclick="$('#searchForm input').val('');grid.datagrid('load',{});">重置过滤</a>
		</div>
		</form>
	</div>
	<table id="grid" data-options="fit:true,border:false"></table>
</body>
</html>