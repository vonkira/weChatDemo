<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/static/admin/jsp/include.jsp"%>
<script type="text/javascript">
	var submitForm = function($dialog, $grid, $pjq) {
		if ($('form').form('validate')) {
			var obj=sy.serializeObject($('form'));
			var url=sy.contextPath + '/userOrder/pushRepair';
			$.post(url, obj, function(result) {
				if (result.code == 0) {
					$grid.datagrid('reload');
					$dialog.dialog('destroy');
				} else {
					$pjq.messager.e(result.msg);
				}
			}, 'json');
		}
	};
</script>
</head>
<body>
	<form id="form" method="post">
        <input name="id" type="hidden" value="${id}"/>
        <div style="padding:15px;font-size: 12px">
            <table style="table-layout:fixed;" border="0" cellspacing="0" class="formtable">
		       	<tr>
		       		<th style="width: 100px;">车辆状况:</th>
		    		<td>
		    			<input id="carInfo" name="carInfo" class="easyui-textbox" style="height: 200px"  data-options="required:true,multiline:true,validType:['length[0,150]']" missingMessage="请输入车辆状况" prompt="请输入车辆状况"></input>
		    		</td>
		    	</tr>
            </table>
        </div>
	</form>
</body>
</html>