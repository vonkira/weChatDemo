<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/static/admin/jsp/include.jsp"%>
<script type="text/javascript">
    var grid,selected;
    var array = [];
    var carInfo = null;

    Array.prototype.indexOf = function(val) {
        for (var i = 0; i < this.length; i++) {
            if (this[i].id == val) return i;
        }
        return -1;
    };
    Array.prototype.remove = function(val) {
        var index = this.indexOf(val);
        if (index > -1) {
            this.splice(index, 1);
        }
    };

    var del = function(id) {
        array.remove(id);
        grid.datagrid('loadData',array);
    };
    var add = function() {
		if(selected != null){
            var hasInclude = false;
            $.each(array,function(i,o){
                if(o.id == selected.id){
                    hasInclude = true;
                }
            });
            if(!hasInclude) {
                array.push(selected);
                grid.datagrid('loadData',{rows:array,footer:[{id:"footer",name:"合计",storePrice: 1, servicePrice: 2}]});
            }else{
                $.messager.w("该配件已添加");
            }
            $('#keyword').combobox("clear");
            selected = null;
		}
    };
    var numSub = function(id) {
        $.each(array,function(i,o){
            if(o.id == id){
                if(o.num == 1){
                    del(id);
				}else{
                    o.num--;
				}
            }
        });
        grid.datagrid('loadData',array);
    };
    var numAdd = function(id) {
        $.each(array,function(i,o){
            if(o.id == id){
				o.num++;
            }
        });
        grid.datagrid('loadData',array);
    };
    var setServicePrice = function(id) {
        layer.prompt({
            formType: 0,
            title: '请输入金额：',
        }, function(value, index, elem) {
            if (UT.isInt(value) && value > -1) {
                $.each(array,function(i,o){
                    if(o.id == id){
                        o.servicePrice = value;
                    }
                });
                grid.datagrid('loadData',array);
            }else{
                $.messager.w("请输入非负整数！");
            }
            layer.close(index);
        });
    };
    var resetText = function() {
        $('#keyword').combobox("clear");
        selected = null;
    };
    function onComboboxHidePanel() {
        var el = $(this);
        el.combobox('textbox').focus();
        // 检查录入内容是否在数据里
        var opts = el.combobox("options");
        var data = el.combobox("getData");
        var value = el.combobox("getValue");
        // 有高亮选中的项目, 则不进一步处理
        var panel = el.combobox("panel");
        var items = panel.find(".combobox-item-selected");
        if (items.length > 0) {
            var values = el.combobox("getValues");
            el.combobox("setValues", values);
            return;
        }
        var allowInput = opts.allowInput;
        if (allowInput) {
            var idx = data.length;

            data[idx] = [];
            data[idx][opts.textField] = value;
            data[idx][opts.valueField] = value;
            el.combobox("loadData", data);
        } else {
            // 不允许录入任意项, 则清空
            el.combobox("clear");
        }
    }

    function getInfo() {
		var car = $("#car").textbox("getValue");
		if(car){
            var url=sy.contextPath + '/userCars/findByNumber';
            $.post(url, {number:car}, function(result) {
                if (result.code == 0) {
                    carInfo = result.data;
                    for(var key in carInfo){
                        if(key == 'carVin' || key == 'carEngineCode'){
                            $("#"+key).textbox("setValue",carInfo[key]);
						}else{
                            $("#"+key).html(carInfo[key]);
                        }
                    }
                } else {
                    carInfo = null;
                    $.messager.w('未找到相关信息！');
                }
            }, 'json');
		}else{
		    $.messager.w("请先输入车牌号");
		}
    }

    function submitForm() {
        if ($('form').form('Validate')) {
            var obj=sy.serializeObject($('form'));

			if(carInfo == null){
                $.messager.w("请先获取车辆信息");
                return;
			}

			//车辆信息
			obj.userId = carInfo.userId;
			obj.carNumber = carInfo.carNumber;
            obj.carType = carInfo.brand + " " + carInfo.serie + " " + carInfo.typeName;
			obj.carUser = carInfo.name;
            obj.carUserPhone = carInfo.account;

            $.messager.progress({
                text : '数据提交中....'
            });
            var url=sy.contextPath + '/userOrder/add';
            $.post(url, obj, function(result) {
                $.messager.progress('close');
                if (result.code == 0) {
                    layer.alert('提交成功,请至[服务订单列表]查看！', function(index){
                        layer.close(index);
                        location.reload();
                    });
                } else {
                    $.messager.e(result.msg);
                }
            }, 'json');
        }
    }

    $(function() {
        $('#storeMemberId').combobox({
            textField: 'name',
            valueField: 'id',
            url : sy.contextPath + '/storeMember/all'
        });

    });
</script>
</head>
<body>
	<form id="form" method="post">
		<div style="padding:15px;font-size: 12px">
			<h3>
				基本信息：
				<div style="display: inline-block;float: right;">
					<a href="#" class="easyui-linkbutton" onclick="submitForm();" style="width:80px;">提交</a>
				</div>
			</h3>
			<div style="margin-top: 10px">
				<input class="easyui-textbox" id="car" data-options="validType:['isVehicleNumber']" prompt="请输入完整车牌号信息" style="width: 200px"/>
				<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-search'" onclick="getInfo()">获取信息</a>
			</div>
			<table style="table-layout:fixed;margin-top: 10px;margin-bottom: 15px" border="0" cellspacing="0" class="formtable">
				<tr>
					<th style="width:100px;">车主姓名：</th>
					<td id="name"></td>
					<th style="width:100px;">车主电话：</th>
					<td id="account"></td>
				</tr>
				<tr>
					<th style="width:100px;">车牌号：</th>
					<td id="carNumber"></td>
					<th style="width:100px;">品牌：</th>
					<td id="brand"></td>
				</tr>
				<tr>
					<th style="width:100px;">车系：</th>
					<td id="serie"></td>
					<th style="width:100px;">车型：</th>
					<td id="typeName"></td>
				</tr>
				<tr>
					<th style="width:100px;">车架号：</th>
					<td>
						<input class="easyui-textbox" data-options="validType:['length[17,17]']" name="carVin" type="text" missingMessage="请输入车架号" prompt="请输入车架号" id="carVin"  />
					</td>
					<th style="width:100px;">发动机编号：</th>
					<td>
						<input class="easyui-textbox" data-options="validType:['length[0,20]']" name="carEngineCode" type="text" missingMessage="请输入发动机编号" prompt="请输入发动机编号" id="carEngineCode"  />
					</td>
				</tr>
				<c:if test="${sessionScope.sessionUser.roleCode == '4s'}">
				<tr>
					<th style="width:100px;">里程数：</th>
					<td>
						<input id="total" name="total" class="easyui-numberbox" data-options="required:true,min:0,max:99999999" prompt="请输入里程数" missingMessage="请输入里程数" />
					</td>
					<th style="width:100px;">员工：</th>
					<td>
						<select id="storeMemberId" name="storeMemberId" editable="false" data-options="required:true" prompt="请选择员工"/>
					</td>
				</tr>
				</c:if>
				<tr>
					<th style="width:100px;">保险公司：</th>
					<td>
						<input class="easyui-textbox" data-options="validType:['length[0,20]']" name="insurerFirm" type="text" missingMessage="请输入保险公司" prompt="请输入保险公司" id="insurerFirm"  />
					</td>
					<th style="width:100px;">保险到期：</th>
					<td>
						<input  id="insurerTime" name="insurerTime" type= "text" editable="false" class= "easyui-datebox" prompt="请输入保险到期日"> </input>
					</td>
				</tr>
				<tr>
					<th style="width:100px;">车辆状况描述：</th>
					<td>
						<input class="easyui-textbox" data-options="validType:['length[0,150]'],multiline:true" style="height: 80px;" name="carInfo" type="text" missingMessage="请输入车辆状况描述" prompt="请输入车辆状况描述" id="carInfo"  />
					</td>
					<th style="width:100px;"></th>
					<td>
					</td>
				</tr>
			</table>
		</div>
	</form>
</body>
</html>