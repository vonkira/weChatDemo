<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/static/admin/jsp/include.jsp"%>
<script type="text/javascript">
	var grid;
	
	$(function() {
		grid = $('#grid').datagrid({
			url : sy.contextPath + '/storeGoodsOrder/report',
			singleSelect : false,
			frozenColumns : [ [ {
				width : '100',
				checkbox:true,
				field : 'id',
				align : 'center'
			}] ],
			columns : [ [ 
				{
					width : $(this).width() * 0.2,
					title : '门店名称',
					field : 'storeName',
					align : 'center',
					formatter: function(v, r, i) {
						return UT.addTitle(v);
					}
				},{
					width : $(this).width() * 0.15,
					title : '采购类型',
					field : 'type',
					align : 'center',
					formatter: function(v, r, i) {
						if (v == 1) {
							return "采购";
						} else if (v == 2) {
							return "第三方销售";
						} else {
							return "";
						}
					}
				},{
					width : $(this).width() * 0.15,
					title : '总价(￥)',
					field : 'total',
					align : 'center'
				},{
					width : $(this).width() * 0.15,
					title : '成本(￥)',
					field : 'cost',
					align : 'center'
				}
			] ]
		});
		
		$('#type').combobox({
			textField:'text',
			valueField:'value',
			data:[{text:'全部',value:0,selected:true},{text:'采购',value:1},{text:'第三方销售',value:2}],
			onChange: function() {
				grid.datagrid('load',sy.serializeObject($('#searchForm')));
			}
		});
	});
</script>
</head>
<body>
	<div id="toolbar">
		<form id="searchForm">
		<div>
			<span>门店名称：</span>
			<input type="text" class="easyui-textbox" name="storeName" style="width: 150px" prompt="门店名称"/>
			<span>门店类型：</span>
			<input id="type" name="type" editable="false" style="width:150px;" panelHeight='150' />
			<span>开始时间：</span>
			<input class='easyui-datebox' name='beginTime' data-options="width:150,editable:false" prompt="开始时间" />
			<span>结束时间：</span>
			<input class='easyui-datebox' name='endTime' data-options="width:150,editable:false" prompt="结束时间" />
			<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-search'" onclick="grid.datagrid('load',sy.serializeObject($('#searchForm')));">过滤</a> 
			<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-search-minus'" onclick="$('#searchForm input').val('');grid.datagrid('load',{});">重置过滤</a>
		</div>
		</form>
	</div>
	<table id="grid" data-options="fit:true,border:false"></table>
</body>
</html>