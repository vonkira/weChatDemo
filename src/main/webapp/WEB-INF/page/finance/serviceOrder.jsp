<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/static/admin/jsp/include.jsp"%>
<script type="text/javascript">
	var grid;
	
	//导出数据
	var exportFun = function() {
		var obj = sy.serializeObject($('form'));
		var jsonStr = sy.jsonToString(obj);
		window.location = sy.contextPath+'/userOrder/exportExcel?jsonStr=' + encodeURI(jsonStr);
		return;
	};
	
	$(function() {
		grid = $('#grid').datagrid({
			url : sy.contextPath + '/userOrder/report',
			singleSelect : false,
			frozenColumns : [ [ {
				width : '100',
				checkbox:true,
				field : 'id',
				align : 'center'
			}] ],
			columns : [ [ 
				{
					width : $(this).width() * 0.2,
					title : '门店名称',
					field : 'storeName',
					align : 'center',
					formatter: function(v, r, i) {
						return UT.addTitle(v);
					}
				},{
					width : $(this).width() * 0.15,
					title : '门店类型',
					field : 'storeType',
					align : 'center',
					formatter: function(v, r, i) {
						if (v == 1) {
							return "保养店";
						} else if (v == 2) {
							return "维修店";
						} else {
							return "";
						}
					}
				},{
					width : $(this).width() * 0.15,
					title : '订单数量',
					field : 'nums',
					align : 'center'
				},{
					width : $(this).width() * 0.15,
					title : '累计收入(￥)',
					field : 'price',
					align : 'center'
				}
			] ]
		});
		
		$('#storeType').combobox({
			textField:'text',
			valueField:'value',
			data:[{text:'全部',value:0,selected:true},{text:'保养店',value:1},{text:'维修店',value:2}],
			onChange: function() {
				grid.datagrid('load',sy.serializeObject($('#searchForm')));
			}
		});
		
		var date = new Date();
		var year = date.getFullYear();  
		var month = date.getMonth()+1; //月份+1     
		var day = date.getDate(); 
		var begin = "00:00:00";
		var end = "23:59:59";
		$('#beginTime').datetimebox('setValue', day+"/"+month+"/"+year+" "+begin);
		$('#endTime').datetimebox('setValue', day+"/"+month+"/"+year+" "+end);
		grid.datagrid('load',sy.serializeObject($('#searchForm')));
	});
</script>
</head>
<body>
	<div id="toolbar">
		<form id="searchForm">
		<div>
			<span>门店名称：</span>
			<input type="text" class="easyui-textbox" name="storeName" style="width: 150px" prompt="门店名称"/>
			<span>门店类型：</span>
			<input id="storeType" name="storeType" editable="false" style="width:150px;" panelHeight='150' />
			<span>开始时间：</span>
			<input class='easyui-datetimebox' id='beginTime' name='beginTime' data-options="width:180,editable:false" prompt="开始时间" />
			<span>结束时间：</span>
			<input class='easyui-datetimebox' id='endTime' name='endTime' data-options="width:180,editable:false" prompt="结束时间" />
			<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-search'" onclick="grid.datagrid('load',sy.serializeObject($('#searchForm')));">过滤</a> 
			<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-search-minus'" onclick="$('#searchForm input').val('');grid.datagrid('load',{});">重置过滤</a>
			<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-file-excel-o'" onclick="exportFun();">导出</a>
		</div>
		</form>
	</div>
	<table id="grid" data-options="fit:true,border:false"></table>
</body>
</html>