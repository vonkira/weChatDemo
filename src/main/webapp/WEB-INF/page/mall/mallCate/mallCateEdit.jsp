<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/static/admin/jsp/include.jsp"%>
<script type="text/javascript">
	var id = "${id}";
	var editor;
	
	var submitForm = function($dialog, $grid, $pjq) {
		var obj = sy.serializeObject($('form'));
		if ($('form').form('validate')) {
			$.ajax({
				url:sy.contextPath + '/mallCate/save',
				data:obj,
				type:'post',
				dataType:'json',
				success:function(result) {
					if (result.code == 0) {
						$grid.datagrid('reload');
						$dialog.dialog('destroy');
					} else {
						$pjq.messager.e('添加失败 ' + result.msg);
					}
				}
			});
		}
	};

	$(function() {
        if (id != '') {

			parent.$.messager.progress();
			$.post(sy.contextPath + '/mallCate/findById', {
				code : id
			}, function(result) {
				if (result) {
                    $('#img').setFileId(result.img, true, true, true);
					$('form').form('load', result);

				}
				parent.$.messager.progress('close');
			}, 'json');
            $('#code').textbox({novalidate:true});
            $('#code').textbox({readonly:true});
		}
	});
</script>
</head>
<body>
	<form id="form" method="post">
		<input name="id" type="hidden" />
		<div style="padding:15px;font-size: 12px">
		<table style="table-layout:fixed;" border="0" cellspacing="0" class="formtable">
			<tr>
				<th style="width:100px;">分类编号：</th>
				<td>
					<input id="code" name="code" class="easyui-textbox" missingMessage="请输入分类编号" prompt="请输入分类编号" style="width:90%;"
						   data-options="required:true,validType:['length[1,20]','account','unique[\'/mallCate/isCodeExist\']']" />
				</td>
				<tr>
					<th style="width:100px;">图片：</th>
					<td>
						<div id="img" multi="false" fileCountLimit='2'cutWidth="100" cutHeight="100" required="required" type="file" showImage="true" showBtn='true' bestSize='640*320' fileType="jpg,png,gif" fileSize="200MB" buttonText="上传图片"></div>
						<label style="color: red;">*</label>
						<label>建议尺寸：宽度100，高度100，格式JPG,PNG</label>
					</td>
				</tr>
				<th style="width:100px;">分类名称：</th>
				<td>
					<input id="name" name="name" class="easyui-textbox" missingMessage="请输入分类名称" prompt="请输入分类名称" style="width:90%;"
						   data-options="required:true,validType:['length[0,50]']" />
				</td>

			</tr>
			<tr>
				<th>排序</th>
				<td>
					<input id="sortOrder" name="sortOrder" class="easyui-textbox" missingMessage="排序" prompt="排序" style="width:90%;"
						   data-options="required:true,validType:['length[0,10]']" />
				</td>
			</tr>
		</table>
		</div>
	</form>
</body>
</html>