<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <%@ include file="/static/admin/jsp/include.jsp"%>
    <script type="text/javascript">
        var grid;

        var stateStr = {
            1:'待付款',
            2:'待发货',
            3:'待确认',
            4:'已完成'
        };

        var payTypeStr = {
            0:'金币',
            1:'支付宝',
            2:'微信'
        };

        $(function() {
            grid = $('#grid').datagrid({
                url : sy.contextPath + '/mallOrder/list',
                singleSelect : true,
                columns : [ [
                    {
                        width : 80,
                        title : '订单号',
                        field : 'code',
                        align : 'center',
                        formatter:function (v,r) {
                            return UT.addTitle(v);
                        }
                    },{
                        width : 80,
                        title : '用户',
                        field : 'userId',
                        align : 'center',
                        formatter:function (v,r) {
                            return r.nickName+"("+r.account+")"
                        }
                    },{
                        width : 100,
                        title : '商品',
                        field : 'goodsName',
                        align : 'center',
                        formatter:function (v,r) {
                            return UT.addTitle(v);
                        }
                    },{
                        width : 30,
                        title : '数量',
                        field : 'goodsNum',
                        align : 'center',
                        formatter:function (v,r) {
                            return UT.addTitle(v);
                        }
                    },{
                        width : 50,
                        title : '单价',
                        field : 'coin',
                        align : 'center',
                        formatter:function (v,r) {
                            var str = '';
                            if (r.goodsPayType == 1){
                                str += '金币:'+r.goodsCoin;
                            }else if(r.goodsPayType == 2){
                                str += '现金:'+r.goodsPrice;
                            }else{
                                str += '金币:'+r.goodsCoin;
                                str += '/现金:'+r.goodsPrice;
                            }
                            return str;
                        }
                    },{
                        width : 50,
                        title : '合计',
                        field : 'price',
                        align : 'center',
                        formatter:function (v,r) {
                            if (r.state > 1){
                                var str = payTypeStr[r.payType] + ":";
                                if (r.payType == 0){
                                    str += r.goodsNum.mul(r.goodsCoin);
                                }else{
                                    str += r.goodsNum.mul(r.goodsPrice);
                                }
                                return str;
                            }else{
                                var str = '';
                                if (r.goodsPayType == 1){
                                    str += '金币:'+r.goodsCoin.mul(r.goodsNum);
                                }else if(r.goodsPayType == 2){
                                    str += '现金:'+r.goodsPrice.mul(r.goodsNum);
                                }else{
                                    str += '金币:'+r.goodsCoin.mul(r.goodsNum);
                                    str += '/现金:'+r.goodsPrice.mul(r.goodsNum);
                                }
                                return str;
                            }
                        }
                    },{
                        width : 50,
                        title : '支付方式',
                        field : 'payType',
                        align : 'center',
                        formatter:function (v,r) {
                            if (r.state > 1){
                                return payTypeStr[v];
                            }else{
                                return '——';
                            }
                        }
                    },{
                        width : 50,
                        title : '订单状态',
                        field : 'state',
                        align : 'center',
                        formatter:function (v) {
                            return stateStr[v];
                        }
                    },{
                        width : 50,
                        title : '操作',
                        field : 'ids',
                        align : 'center',
                        formatter:function (v,r) {
                            var html = "";
                            if (r.state == 2){
                                html+='<a href="javascript:void(0);" onclick="shipFun(\''+r.id+'\')" class="button button-orange" title="发货">发货</a> ';
                            }
                            return html;
                        }
                    }
                ] ]
            });

            $('#payType').combobox({
                onChange:function () {
                    grid.datagrid('load',sy.serializeObject($('#searchForm')));
                }
            })
            $('#state').combobox({
                onChange:function () {
                    grid.datagrid('load',sy.serializeObject($('#searchForm')));
                }
            })
        });

        function shipFun(id) {
            var dialog = parent.sy.modalDialog({
                title : '发货',
                width : 800,
                height : 600,
                url : sy.contextPath + '/go?path=mall/order/ship&id=' + id,
                buttons : [ {
                    text : '发货',
                    handler : function() {
                        dialog.find('iframe').get(0).contentWindow.submitForm(dialog, grid, parent.$);
                    }
                } ]
            });
        }
    </script>
</head>
<body>
<div id="toolbar">
    <form id="searchForm">
        <div>
            <input type="text" class="easyui-textbox" name="account" style="width: 150px" prompt="用户手机号"/>
            支付方式:
            <select id="payType" name="payType" style="width: 150px" editable="false" panelHeight="auto" missingMessage="支付方式">
                <option value="">全部</option>
                <option value="0">金币</option>
                <option value="1">支付宝</option>
                <option value="2">微信</option>
            </select>
            订单状态:
            <select id="state" name="state" style="width: 150px" editable="false" panelHeight="auto" missingMessage="订单状态">
                <option value="">全部</option>
                <option value="1">待付款</option>
                <option value="2">待发货</option>
                <option value="3">待确认</option>
                <option value="4">已完成</option>
            </select>
            <a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-search'" onclick="grid.datagrid('load',sy.serializeObject($('#searchForm')));">过滤</a>
            <a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-search-minus'" onclick="$('#searchForm input').val('');grid.datagrid('load',{});">重置过滤</a>
        </div>
    </form>
    <div class="tbbutton">
        <a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-trash',plain:true" onclick="delFun();">删除</a>
    </div>
</div>
<table id="grid" data-options="fit:true,border:false"></table>
</body>
</html>