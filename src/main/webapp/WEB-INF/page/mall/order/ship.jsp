<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<%@ include file="/static/admin/jsp/include.jsp"%>
	<script type="text/javascript">
        var id = "${id}";

        var submitForm = function($dialog, $grid, $pjq) {
            var obj = sy.serializeObject($('form'));
            obj.id = id;
            if ($('form').form('validate')) {
                $.ajax({
                    url:sy.contextPath + '/mallOrder/ship',
                    data:obj,
                    type:'post',
                    dataType:'json',
                    success:function(result) {
                        if (result.code == 0) {
                            $grid.datagrid('reload');
                            $dialog.dialog('destroy');
                        } else {
                            $pjq.messager.e('添加失败 ' + result.msg);
                        }
                    }
                });
            }
        };

        $(function() {

            if (id != '') {
                parent.$.messager.progress();
                $.post(sy.contextPath + '/mallOrder/findById', {
                    id : id
                }, function(result) {
                    if (result) {
                        $('#shipUser').html(result.shipUser);
                        $('#shipUserPhone').html(result.shipUserPhone);
                        $('#shipUserAddress').html(result.shipUserAddress);
                    }
                    parent.$.messager.progress('close');
                }, 'json');
            }
        });
	</script>
</head>
<body>
<form id="form" method="post">
	<input name="id" type="hidden" />
	<div style="padding:15px;font-size: 12px">
		<table style="table-layout:fixed;margin-top: 10px;margin-bottom: 15px" border="0" cellspacing="0" class="formtable">
			<tr>
				<th style="width:100px;">收件人：</th>
				<td id="shipUser">
				</td>
				<th style="width:100px;">收件人电话：</th>
				<td id="shipUserPhone">
				</td>
			</tr>
			<tr>
				<th style="width:100px;">收件人地址：</th>
				<td id="shipUserAddress" colspan="3">
				</td>
			</tr>
			<tr>
				<th style="width:100px;">快递名称：</th>
				<td>
					<input class="easyui-textbox" style="width: 90%" data-options="required:true,validType:['length[0,20]']" name="shipExpressName" missingMessage="请输入快递名称" prompt="请输入快递名称" id="shipExpressName"  />
				</td>
				<th style="width:100px;">快递单号：</th>
				<td>
					<input class="easyui-textbox" style="width: 90%" data-options="required:true,validType:['length[0,50]']" name="shipExpressCode" missingMessage="请输入快递单号" prompt="请输入快递单号" id="shipExpressCode"  />
				</td>
			</tr>
		</table>
	</div>
</form>
</body>
</html>