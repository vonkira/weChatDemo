<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/static/admin/jsp/include.jsp"%>
<script type="text/javascript">
	var id = "${id}";
	var mallGoodsId = '${mallGoodsId}';

	var submitForm = function($dialog, $grid, $pjq) {
		var obj = sy.serializeObject($('form'));
		if (mallGoodsId){
		    obj.mallGoodsId = mallGoodsId;
		}
		if ($('form').form('validate')) {
			$.ajax({
				url:sy.contextPath + '/mallGoodsBack/save',
				data:obj,
				type:'post',
				dataType:'json',
				success:function(result) {
					if (result.code == 0) {
						$grid.datagrid('reload');
						$dialog.dialog('destroy');
					} else {
						$pjq.messager.e('添加失败 ' + result.msg);
					}
				}
			});
		}
	};

	$(function() {
	    $("#goodsId").combobox({
				required:true,
                textField: 'name',
                valueField: 'id',
                url : sy.contextPath + '/storeGoods/listByStore',
				filter: function(q, row){
					var opts = $(this).combobox('options');
					return row[opts.textField].toLowerCase().indexOf(q.toLowerCase()) != -1;
				}
        });

        if (id != '') {
			parent.$.messager.progress();
			$.post(sy.contextPath + '/mallGoodsBack/findById', {
				id : id
			}, function(result) {
				if (result) {
					$('form').form('load', result);
				}
				parent.$.messager.progress('close');
			}, 'json');
		}
	});
</script>
</head>
<body>
	<form id="form" method="post">
		<input name="id" type="hidden" />
		<div style="padding:15px;font-size: 12px">
		<table style="table-layout:fixed;" border="0" cellspacing="0" class="formtable">
			<tr>
				<th style="width:100px;">商品：</th>
				<td>
					<select  id="goodsId" name="goodsId"  style="width: 100%"/>
				</td>
			</tr>
			<tr>
				<th class="num">数量</th>
				<td class="num">
					<input id="num" name = "num" style="width: 100%;" class="easyui-numberbox" data-options="min:0,max:999999999" missingMessage="请输入数量" prompt="请输入数量"/>
				</td>
			</tr>
		</table>
		</div>
	</form>
</body>
</html>