<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <%@ include file="/static/admin/jsp/include.jsp"%>
    <script type="text/javascript">
        var _id = '${id}';
        var grid;

        var addFun = function($dialog, $grid, $pjq) {
            var dialog = parent.sy.modalDialog({
                title : '添加商品',
                width : 600,
                height : 400,
                url : sy.contextPath + '/go?path=mall/goods/discount/edit&mallGoodsId='+_id,
                buttons : [ {
                    text : '保存',
                    handler : function() {
                        dialog.find('iframe').get(0).contentWindow.submitForm(dialog, grid, parent.$);
                    }
                } ]
            });
        };

        var editFun = function() {
            var rows = grid.datagrid('getSelections');
            if (rows.length != 1) {
                parent.$.messager.w('请选择一条记录进行编辑！');
                return;
            }
            var dialog = parent.sy.modalDialog({
                title : '编辑商品',
                width : 600,
                height : 400,
                url : sy.contextPath + '/go?path=mall/goods/discount/edit&id=' + rows[0].id,
                buttons : [ {
                    text : '保存',
                    handler : function() {
                        dialog.find('iframe').get(0).contentWindow.submitForm(dialog, grid, parent.$);
                    }
                } ]
            });
        };

        var delFun = function() {
            var rows = grid.datagrid('getSelections');
            if (rows.length == 0) {
                parent.$.messager.w('请选择需要删除的记录！');
                return;
            }
            parent.$.messager.confirm('询问', '您确定要删除此记录？', function(r) {
                if (r) {
                    var ids = [];
                    for ( var i = 0, l = rows.length; i < l; i++) {
                        var r = rows[i];
                        ids.push(r.id);
                    }
                    var id = ids.join(',');

                    $.post(sy.contextPath + '/mallGoodsDiscount/del', {
                        id : id
                    }, function(res) {
                        if (res.code == 0) {
                            rows.length = 0;//必须，否则有bug
                            grid.datagrid('reload');
                        } else {
                            parent.$.messager.w(res.msg);
                        }
                    }, 'json');
                }
            });
        };

        $(function() {
            grid = $('#grid').datagrid({
                url : sy.contextPath + '/mallGoodsDiscount/list?goodsId='+_id,
                singleSelect : false,
                frozenColumns : [ [ {
                    width : '100',
                    checkbox:true,
                    field : 'id',
                    align : 'center'
                }] ],
                columns : [ [
                    {
                        width : 100,
                        title : '商品名称',
                        field : 'goodsName',
                        align : 'center'
                    },{
                        width : 50,
                        title : '抵扣金币',
                        field : 'coin',
                        align : 'center'
                    },{
                        width : 50,
                        title : '抵扣人民币',
                        field : 'price',
                        align : 'center'
                    }
                ] ]
            });
        });
    </script>
</head>
<body>
<div id="toolbar">
    <div class="tbbutton">
        <%--<c:if test="${sessionUser.sys == 'store'}">--%>
            <a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-plus-circle',plain:true" onclick="addFun();">添加</a>
            <a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-pencil',plain:true" onclick="editFun();">修改</a>
        <%--</c:if>--%>
        <a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-trash',plain:true" onclick="delFun();">删除</a>
    </div>
</div>
<table id="grid" data-options="fit:true,border:false"></table>
</body>
</html>