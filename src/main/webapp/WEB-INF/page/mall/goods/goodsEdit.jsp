<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/static/admin/jsp/include.jsp"%>
<script type="text/javascript">
	var id = "${id}";
	var editor;
	
	var submitForm = function($dialog, $grid, $pjq) {
		var obj = sy.serializeObject($('form'));
		if ($('form').form('validate')) {
			$.ajax({
				url:sy.contextPath + '/goodsMall/save',
				data:obj,
				type:'post',
				dataType:'json',
				success:function(result) {
					if (result.code == 0) {
						$grid.datagrid('reload');
						$dialog.dialog('destroy');
					} else {
						$pjq.messager.e('添加失败 ' + result.msg);
					}
				}
			});
		}
	};

	$(function() {
	    $("#goodsId").combobox({
				required:true,
                textField: 'name',
                valueField: 'id',
                url : sy.contextPath + '/storeGoods/listByStore?type=0',
				filter: function(q, row){
					var opts = $(this).combobox('options');
					return row[opts.textField].toLowerCase().indexOf(q.toLowerCase()) != -1;
				}
        });

        $("#cateCode").combobox({
            required:true,
            textField: 'name',
            valueField: 'code',
            url : sy.contextPath + '/mallCate/mallCateList'
        });

        $('#payType').combobox({
			onChange:function (v) {
				if (v == 1){
				    $('.coin').show();
                    $('.price').hide();
                    $('#coin').numberbox({
                        required:true
                    })

                    $('#price').numberbox({
                        required:false
                    })
				}else if(v == 2){
                    $('.price').show();
                    $('.coin').hide();
                    $('#coin').numberbox({
                        required:false
                    })
                    $('#price').numberbox({
                        required:true
                    })
				}else{
                    $('.coin').show();
                    $('.price').show();
				    $('#coin').numberbox({
						required:true
					})
                    $('#price').numberbox({
                        required:true
                    })
				}
            }
		});

        if (id != '') {
			parent.$.messager.progress();
			$.post(sy.contextPath + '/goodsMall/findById', {
				id : id
			}, function(result) {
				if (result) {
                    $('#imgs').setFileId(result.imgs, true, true, true);
					$('form').form('load', result);
				}
				parent.$.messager.progress('close');
			}, 'json');
		}
	});
</script>
</head>
<body>
	<form id="form" method="post">
		<input name="id" type="hidden" />
		<div style="padding:15px;font-size: 12px">
		<table style="table-layout:fixed;" border="0" cellspacing="0" class="formtable">
			<tr>
				<th style="width:100px;">商品：</th>
				<td>
					<select  id="goodsId" name="goodsId"  style="width: 100%"/>
				</td>
				<th style="width:100px;">分类：</th>
				<td>
					<select id="cateCode" name="cateCode"  style="width: 100%" editable="false"/>
				</td>
			</tr>
			<tr>
				<th class="code">商品编码</th>
				<td class="code">
					<input id="code" name = "code" style="width: 100%;" class="easyui-textbox" data-options="required:true,validType:['length[0,50]']" missingMessage="请输入商品编码" prompt="请输入商品编码"/>
				</td>
			</tr>
			<tr>
				<th style="width:100px;">库存：</th>
				<td>
					<input id="total" name="total" style="width: 100%" class="easyui-numberbox" missingMessage="请输入库存" prompt="请输入库存" data-options="required:true,min:0"/>
				</td>
				<th style="width:100px;">支付方式：</th>
				<td>
					<select id="payType" name="payType" editable="false" panelHeight='auto' class="easyui-combobox"
							data-options="valueField: 'value',textField: 'name',data: [{name: '金币',value: '1',selected:true},
							{name: '现金',value: '2'}]," style="width: 100%"/>
					<!--,{name: '金币或现金',value: '3'} -->
				</td>
			</tr>
			<tr>
				<th class="coin">金币</th>
				<td class="coin">
					<input id="coin" name = "coin" style="width: 100%;" class="easyui-numberbox" data-options="min:0,max:999999999" missingMessage="请输入金币" prompt="请输入金币"/>
				</td>
				<th class="price">价格</th>
				<td class="price">
					<input id="price" name = "price" style="width: 100%;" class="easyui-numberbox" data-options="min:0,max:999999999,precision:2" missingMessage="请输入价格" prompt="请输入价格"/>
				</td>
			</tr>
			<tr>
				<th>组图</th>
				<td colspan="3">
					<div id="imgs" multi="true" fileCountLimit="6" showWidth="60" showHeight="40" required="required" type="file" showImage="true" showBtn="true" bestSize="226*226"
						 fileType="jpg,png" fileSize="2MB" buttonText="上传组图"></div>
					<label style="color: red;">*</label>
					<label>建议尺寸：宽度300，高度200，格式JPG,PNG</label>
				</td>
			</tr>
		</table>
		</div>
	</form>
</body>
</html>