<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <%@ include file="/static/admin/jsp/include.jsp"%>
    <script type="text/javascript">
        var grid;

        var addFun = function($dialog, $grid, $pjq) {
            var dialog = parent.sy.modalDialog({
                title : '添加商场商品',
                width : 800,
                height : 600,
                url : sy.contextPath + '/go?path=mall/goods/goodsEdit',
                buttons : [ {
                    text : '保存',
                    handler : function() {
                        dialog.find('iframe').get(0).contentWindow.submitForm(dialog, grid, parent.$);
                    }
                } ]
            });
        };

        var editFun = function() {
            var rows = grid.datagrid('getSelections');
            if (rows.length != 1) {
                parent.$.messager.w('请选择一条记录进行编辑！');
                return;
            }
            var dialog = parent.sy.modalDialog({
                title : '编辑商场商品',
                width : 800,
                height : 600,
                url : sy.contextPath + '/go?path=mall/goods/goodsEdit&id=' + rows[0].id,
                buttons : [ {
                    text : '保存',
                    handler : function() {
                        dialog.find('iframe').get(0).contentWindow.submitForm(dialog, grid, parent.$);
                    }
                } ]
            });
        };

        var delFun = function() {
            var rows = grid.datagrid('getSelections');
            if (rows.length == 0) {
                parent.$.messager.w('请选择需要删除的记录！');
                return;
            }
            parent.$.messager.confirm('询问', '您确定要删除此记录？', function(r) {
                if (r) {
                    var ids = [];
                    for ( var i = 0, l = rows.length; i < l; i++) {
                        var r = rows[i];
                        ids.push(r.id);
                    }
                    var id = ids.join(',');

                    $.post(sy.contextPath + '/goodsMall/del', {
                        id : id
                    }, function(res) {
                        if (res.code == 0) {
                            rows.length = 0;//必须，否则有bug
                            grid.datagrid('reload');
                        } else {
                            parent.$.messager.w(res.msg);
                        }
                    }, 'json');
                }
            });
        };

        //上下架商品事件
        var onlineFun = function(type, id) {
            var title = "";
            if (type == 1) {
                title = "确定上架该商品吗？";
            } else {
                title = "确定下架该商品吗？";
            }
            var data = {id: id, isOnline: type};
            parent.$.messager.confirm("询问", title, function(r) {
                if (r) {
                    saveData(data);
                }
            });
        };

        //保存数据
        var saveData = function(data) {
            var url = sy.contextPath + '/goodsMall/save';
            $.post(url, data, function() {
                grid.datagrid('reload');
            }, 'json');
        };

        $(function() {
            grid = $('#grid').datagrid({
                url : sy.contextPath + '/goodsMall/list',
                singleSelect : false,
                frozenColumns : [ [ {
                    width : '100',
                    checkbox:true,
                    field : 'id',
                    align : 'center'
                }] ],
                columns : [ [
                    {
                        width : 100,
                        title : '商品名称',
                        field : 'stroeGoodsName',
                        align : 'center'
                    },{
                        width : 100,
                        title : '商家名称',
                        field : 'storeName',
                        align : 'center',
                        hidden : sy.system == 'store',
                        formatter:function (v) {
                            return UT.addTitle(v);
                        }
                    },{
                        width : 50,
                        title : '分类名称',
                        field : 'cateName',
                        align : 'center'
                    },{
                        width : 50,
                        title : '支付方式',
                        field : 'payType',
                        align : 'center',
                        formatter : function(value,row,index){
                            if(value == 1){
                                return '金币';
                            }else if(value == 2){
                                return '现金';
                            }else if (value == 3){
                                return "金币或现金";
                            }
                        }
                    },{
                        width : 50,
                        title : '金币',
                        field : 'coin',
                        align : 'center'
                    },{
                        width : 50,
                        title : '价格',
                        field : 'price',
                        align : 'center'
                    },{
                        width : 50,
                        title : '总数',
                        field : 'total',
                        align : 'center'
                    },{
                        width : 50,
                        title : '已售',
                        field : 'sellOut',
                        align : 'center'
                    },{
                        width : 50,
                        title : '组图',
                        field : 'imgs',
                        align : 'center',
                        formatter : function(v,r,i){
                            return po.showImg(v,30,20)
                        }
                    },{
                        width : 50,
                        title : '上架',
                        field : 'isOnline',
                        align : 'center',
                        formatter:function (value,row) {
                            switch (value) {
                                case 0:
                                    return '<a href="javascript:void(0);" onclick="onlineFun(1,\''+row.id+'\')" ><img src="static/admin/images/no.png" title="点击上架" > </img></a>';
                                case 1:
                                    return '<a href="javascript:void(0);" onclick="onlineFun(0,\''+row.id+'\')" ><img src="static/admin/images/yes.png" title="点击下架" > </img></a>';
                            }
                        }
                    },{
                        width : 150,
                        title : '创建时间',
                        field : 'createTime',
                        align : 'center'
                    },
                    {
                        width : 150,
                        title : '操作',
                        field : 'ids',
                        align : 'center',
                        formatter:function (v,r) {
                            var str = '';
                            str += '<a href="javascript:void(0);" onclick="discountList(\''+r.id+'\')" class="button button-info">抵扣物品</a>';
                            str += ' <a href="javascript:void(0);" onclick="backList(\''+r.id+'\')" class="button button-blue">赠品</a>';
                            return str;
                        }
                    }
                ] ]
            });
        });

        //抵扣物品跳转
        var discountList = function (id) {
            parent.sy.modalDialog({
                title : '抵扣物品',
                width : 800,
                height : 600,
                url : sy.contextPath + '/go?path=mall/goods/discount/list&id=' + id
            });
        }

        //抵扣物品跳转
        var backList = function (id) {
            parent.sy.modalDialog({
                title : '赠品',
                width : 800,
                height : 600,
                url : sy.contextPath + '/go?path=mall/goods/back/list&id=' + id
            });
        }
    </script>
</head>
<body>
<div id="toolbar">
    <form id="searchForm">
        <div>
            <input type="text" class="easyui-textbox" name="name" style="width: 150px" prompt="商场名称"/>
            <a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-search'" onclick="grid.datagrid('load',sy.serializeObject($('#searchForm')));">过滤</a>
            <a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-search-minus'" onclick="$('#searchForm input').val('');grid.datagrid('load',{});">重置过滤</a>
        </div>
    </form>
    <div class="tbbutton">
        <%--<c:if test="${sessionUser.sys == 'store'}">--%>
            <a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-plus-circle',plain:true" onclick="addFun();">添加</a>
            <a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-pencil',plain:true" onclick="editFun();">修改</a>
        <%--</c:if>--%>
        <a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-trash',plain:true" onclick="delFun();">删除</a>
    </div>
</div>
<table id="grid" data-options="fit:true,border:false"></table>
</body>
</html>