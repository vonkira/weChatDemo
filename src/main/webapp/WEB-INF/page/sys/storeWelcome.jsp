<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<title>首页</title>
<%@ include file="/static/admin/jsp/include.jsp"%>
<style type="text/css">
.report .panel{display:inline;float:left;margin-right: 10px;margin-bottom: 10px;}  
</style>
<script type="text/javascript">
	$(function() {

	/* var isChrome = window.navigator.userAgent.indexOf("Chrome") !== -1;
	if(!isChrome){
		parent.layer.msg("温馨提示：强烈推荐使用chrome浏览器浏览系统");
	} */
	
	$.post(sy.contextPath + '/userOrder/storeReport', {
		storeId : sy.id
	}, function(ret) {
		$("#today").html(ret.today);
		$("#week").html(ret.week);
		$("#month").html(ret.month);
	}, 'json');
	
	var	storeNotifyGrid = $('#notify').datagrid({
		url : sy.contextPath + '/storeNotify/list',
		toolbar : '#toolbar',
		singleSelect : false,
		frozenColumns : [ [ {
			width : $(this).width() * 0.2,
			title : '标题',
			align : 'center',
			field : 'title',
			formatter:function(v,r,i){
				return UT.addTitle(v);
			}
		}] ],
		columns : [ [ {
			width : $(this).width() * 0.5,
			title : '内容',
			align : 'center',
			field : 'content',
			formatter:function(v,r,i){
				return UT.addTitle(v);
			}
		}, {
			width : $(this).width() * 0.2,
			title : '创建时间',
			field : 'createTime',
			align : 'center'
		}
		]]
	});
});
</script>
</head>
<body>
	<p>欢迎您：${sessionUser.rolename }(${sessionUser.account })</p>
	<br />
	<div class="report">
		<div id="p1" class="easyui-panel" title="今日收入" style="width:300px;height:100px;padding-top: 10px;">
			<h2>￥<span id="today">0</span></h2>
	    </div>
	    <div id="p2" class="easyui-panel" title="本周收入" style="width:300px;height:100px;padding-top: 10px;">
			<h2>￥<span id="week">0</span></h2>
	    </div>
	    <div id="p3" class="easyui-panel" title="本月收入" style="width:300px;height:100px;padding-top: 10px;">
			<h2>￥<span id="month">0</span></h2>
	    </div>
    </div>
    <br />
	<div id="cc" class="easyui-layout" style="width:100%;height:400px;">
		<div region="north" title="我的消息" style="height:100%;" data-options="collapsible:false">
				<table id="notify" data-options="border:false" height="100%"></table>
		</div>
	</div>
</body>
</html>
