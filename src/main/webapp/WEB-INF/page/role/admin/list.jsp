<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/static/admin/jsp/include.jsp"%>
<script type="text/javascript">
	var grid;
	var _id = '${id}'
	var addFun = function($dialog, $grid, $pjq) {
		var dialog = parent.sy.modalDialog({
			title : '添加商家管理员',
			width : 700,
			height : 580,
			url : sy.contextPath + '/go?path=role/admin/edit&storeId='+_id,
			buttons : [ {
				text : '保存',
				handler : function() {
					dialog.find('iframe').get(0).contentWindow.submitForm(dialog, grid, parent.$);
				}
			} ]
		});
	};
	
	var editFun = function() {
		var rows = grid.datagrid('getSelections');
		if (rows.length != 1) {
			parent.$.messager.w('请选择一条记录进行编辑！');
			return;
		}
		var dialog = parent.sy.modalDialog({
			title : '编辑信息',
			width : 700,
			height : 580,
			url : sy.contextPath + '/go?path=role/admin/edit&id=' + rows[0].id,
			buttons : [ {
				text : '保存',
				handler : function() {
					dialog.find('iframe').get(0).contentWindow.submitForm(dialog, grid, parent.$);
				}
			} ]
		});
	};

    var adminFun = function(id) {
        parent.sy.modalDialog({
            title : '商家管理员',
            width : 700,
            height : 580,
            url : sy.contextPath + '/go?path=store/adminList&id=' + id
        });
    };

    function login(id){
        top.location.href = sy.basePath + "store/login?id="+id;
    }
	
	var delFun = function() {
		var rows = grid.datagrid('getSelections');
		if (rows.length == 0) {
			parent.$.messager.w('请选择需要删除的记录！');
			return;
		}
		parent.$.messager.confirm('询问', '您确定要删除此记录？', function(r) {
			if (r) {
				var ids = [];
				for ( var i = 0, l = rows.length; i < l; i++) {
					var r = rows[i];
					ids.push(r.id);
				}
				var id = ids.join(',');
				
				$.post(sy.contextPath + '/admin/del', {
					id : id
				}, function(res) {
					if (res.code == 0) {
						rows.length = 0;//必须，否则有bug
						grid.datagrid('reload');
					} else {
						parent.$.messager.w(res.msg);
					}
				}, 'json');
			}
		});
	};
	
	//查看ID
	var idFun = function() {
		var rows = grid.datagrid('getSelections');
		if (rows.length != 1) {
			parent.$.messager.w('请选择一条记录进行复制！');
			return;
		}
		sy.getId(rows[0].id,'复制');
	};
	
	$(function() {
		grid = $('#grid').datagrid({
			url : sy.contextPath + '/admin/list?type=1&orgId=0',
			singleSelect : true,
			frozenColumns : [ [
				{
				width : '100',
				title : '账号',
				field : 'account',
				align : 'center',
				formatter: function(value, row, index) {
					return UT.addTitle(value);
				}
			},
				{
				width : '100',
				title : '名称',
				field : 'name',
				align : 'center',
				formatter: function(value, row, index) {
					return UT.addTitle(value);
				}
			}
			] ],
			columns : [ [ 
				{
					width : '150',
					title : '创建时间',
					field : 'createTime',
					align : 'center'
				},{
                    width : '50',
                    title : '状态',
                    field : 'state',
                    align : 'center',
                    formatter : function(value, row, index) {
                        switch (value) {
                            case 0:
                                return '<a href="javascript:void(0);" onclick="state(1,\'' + row.id + '\')" ><img src="static/admin/images/no.png" title="点击启用" > </img></a>';
                            case 1:
                                return '<a href="javascript:void(0);" onclick="state(0,\'' + row.id + '\')" ><img src="static/admin/images/yes.png" title="点击禁用" > </img></a>';
                        }
                    }
                },{
                    width : '150',
                    title : '操作',
                    field : 'ids',
                    align : 'center',
                    formatter : function(value, row, index) {
                        var html = "";
//                        html+='<a href="javascript:void(0);" onclick="login(\''+row.id+'\')" class="button button-orange" title="一键登录">一键登录</a> ';
                        html+='<a href="javascript:void(0);" onclick="pwdFun(\''+row.id+'\')" class="button button-info" title="修改密码">修改密码</a>';
//                        html+='<a href="javascript:void(0);" onclick="adminFun(\''+row.id+'\')" class="button button-info" title="管理员">管理员</a>';
                        return html;
                    }
                }
			] ]
		});
	});

    function state(state, id) {
        if (state == 1) {

            parent.$.messager.confirm('询问', "确定启用吗？", function(r) {
                if (r) {

                    var data = {
                        id : id,
                        state : 1
                    };
                    saveData(data);
                }
            });
        } else if (state == 0) {
            parent.$.messager.confirm('询问', "确定禁用吗？", function(r) {
                if (r) {
                    var data = {
                        id : id,
                        state : 0
                    };
                    saveData(data);
                }
            });
        }
    }

    function saveData(data) {
        var url = sy.contextPath + '/admin/save';
        $.post(url, data, function() {
            grid.datagrid('reload');
        }, 'json');
    }

    var pwdFun = function(id) {
        var dialog = parent.sy.modalDialog({
            title : '修改密码',
            width: 400,
            height : 220,
            url : sy.contextPath + '/go.do?path=common/pwdEdit&type=3&id=' + id,
            buttons : [ {
                text : '编辑',
                handler : function() {
                    dialog.find('iframe').get(0).contentWindow.submitForm(dialog, grid, parent.$);
                }
            } ]
        });
    };
</script>
</head>
<body>
	<div id="toolbar">
		<form id="searchForm">
		<div>
			<input type="text" class="easyui-textbox" name="name" style="width: 150px" prompt="账号/名称"/>
			<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-search'" onclick="grid.datagrid('load',sy.serializeObject($('#searchForm')));">过滤</a> 
			<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-search-minus'" onclick="$('#searchForm input').val('');grid.datagrid('load',{});">重置过滤</a>
		</div>
		</form>
		<div class="tbbutton">
			<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-plus-circle',plain:true" onclick="addFun();">添加</a>
			<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-pencil',plain:true" onclick="editFun();">修改</a>
			<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-trash',plain:true" onclick="delFun();">删除</a>
			<%--<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-key',plain:true" onclick="idFun();">查看ID</a>--%>
		</div>
	</div>
	<table id="grid" data-options="fit:true,border:false"></table>
</body>
</html>