<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/static/admin/jsp/include.jsp"%>
<script type="text/javascript">
	var id = "${id}";
	var _storeId = '${storeId}';
	var submitForm = function($dialog, $grid, $pjq) {
		if ($('form').form('validate')) {
			var obj=sy.serializeObject($('form'));
			obj.orgId = _storeId;
            var rids = getValues();
            if (rids){
                obj.rid = rids.join(',');
            }
			var url=sy.contextPath + '/admin/addAccount';
			$.post(url, obj, function(result) {
				if (result.code == 0) {
					$grid.datagrid('reload');
					$dialog.dialog('destroy');
				} else {
					$pjq.messager.e('添加失败,'+result.msg);
				}
			}, 'json');
		}
	};

    var tree;
    var getValues = function(){
        var obj = [];
        var tt = $('#ss');
        var nodes = tt.tree('getChecked');
        if (!nodes) return obj;
        for (var i=0;i<nodes.length;i++){
            obj.push(nodes[i].id);
            getParent(nodes[i],obj);
        }
        return obj;
    };

    var getParent = function (son,obj) {
        var tt = $('#ss');
        var node = tt.tree('getParent',son.target);
        if (node){
            if (!isHave(obj,node.id)){
                obj.push(node.id);
            }
            getParent(node,obj);
        }
    }

    var setValues = function(rid){
        if (!rid) return;
        var array = rid.split(',');
        for (var i=0;i<array.length;i++){
            var node = tree.tree('find',array[i]);
            if (!node) continue;
            tree.tree('check',node.target);
        }
    };

    var isHave = function(obj,node){
        for (var i=0;i<obj.length;i++){
            if (node== obj[i])
                return true;
        }
        return false;
    };


    $(function() {
        tree = $('#ss').tree({
            checkbox:true,
            url:sy.contextPath + '/admin/findRights',
            parentField:'parentId',
            idField:'id',
            textField:'name',
            cascadeCheck:false,
            onLoadSuccess : function(node,data){
                if (id != ''){
                    $.ajax({
                        url: sy.contextPath + '/admin/findById',
                        data : {id:id},
                        type : 'post',
                        cache : true,
                        dataType:'json',
                        success:function(result) {
                            if (result) {
                                $('form').form('load', result);
                                setValues(result.rights);
                            }
                        }
                    });
                    $('#account').textbox({novalidate:true});
                    $('#account').textbox({readonly:true})
                    $('.loginIs2').remove();
//                    $('#code').textbox({novalidate:true,disabled:true});
                }
            }

        });
		
		
	});
</script>
</head>
<body>
	<form id="form" method="post">
		<input name="id" type="hidden" />
        <div style="padding:15px;font-size: 12px">
            <table style="table-layout:fixed;" border="0" cellspacing="0" class="formtable">
		    	<tr>
		    		<th style="width:100px;">名称：</th>
		    		<td>
		    			<input id="name" name="name" class="easyui-textbox" missingMessage="请输入名称" prompt="请输入名称" style="width:90%;"
		    				data-options="required:true,validType:['length[0,10]','sn']" />
		    		</td>
		    	</tr>
				<tr>
					<th style="width:100px;">账号：</th>
					<td>
						<input id="account" name="account" class="easyui-textbox" style="width:90%" data-options="required:true,validType:['length[3,20]','account','unique[\'/role/checkAccount\']']" missingMessage="请输入账号" prompt="请输入账号" />
					</td>
				</tr>
				<tr class="loginIs2">
					<th style="width:100px;">密码：</th>
					<td>
						<input id="password" name="password" type="password" class="easyui-textbox" style="width:90%" required="true" data-options="validType:'length[1,12]'" missingMessage="请输入密码" prompt="请输入密码" />
					</td>
				</tr>
				<tr class="loginIs2">
					<th style="width:100px;">确认密码：</th>
					<td>
						<input type="password" class="easyui-textbox" style="width:90%" data-options="validType:'eqPwd[\'#password\']'" missingMessage="请输入确认密码" prompt="请输入确认密码" />
					</td>
				</tr>
				<tr>
					<th style="width:100px;">授权：</th>
					<td>
						<ul id="ss"></ul>
					</td>
				</tr>
            </table>
        </div>
	</form>
</body>
</html>