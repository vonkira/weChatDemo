<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/static/admin/jsp/include.jsp"%>
<script type="text/javascript">
	var id = "${id}";
	var submitForm = function($dialog, $grid, $pjq) {
		if ($('form').form('validate')) {
			var obj=sy.serializeObject($('form'));
            obj.cityId = $('#cityId').combobox('getValue');
			var url=sy.contextPath + '/point/save';
			$.post(url, obj, function(result) {
				if (result.code == 0) {
					$grid.datagrid('reload');
					$dialog.dialog('destroy');
				} else {
					$pjq.messager.e('添加失败,'+result.msg);
				}
			}, 'json');
		}
	};
	
	$(function() {
		if (id != '') {
			parent.$.messager.progress({
				text : '数据加载中....'
			});
			$.post(sy.contextPath + '/point/findById', {
				id : id
			}, function(result) {
				if (result) {
					$('form').form('load', result);
				}
				parent.$.messager.progress('close');
			}, 'json');
		} else {
			
		}


		//查找出所有的城市列表

        $('#cityId').combobox({
            url:sy.contextPath + '/city/cityList.do',
            valueField:'id',
            textField:'cityName',
        });
		
	});
</script>
</head>
<body>
	<form id="form" method="post">
        <input name="id" type="hidden" />
        <div style="padding:15px;font-size: 12px">
            <table style="table-layout:fixed;" border="0" cellspacing="0" class="formtable">
		    	<tr>
					<th style="width:100px;">城市名称：</th>
					<td>
						<input id="cityId" class="easyui-combobox" name="cityId" style="width:100%;" data-options="editable:false"/>
					</td>

		    		<th style="width:100px;">站点名称：</th>
		    		<td>
		    			<input id="orgName" name="pointName" class="easyui-textbox" missingMessage="请输入站点名称" prompt="请输入站点名称" style="width:90%;"
		    				data-options="required:true,validType:['length[0,50]']" />
		    		</td>
		    	</tr>
            </table>
        </div>
	</form>
</body>
</html>