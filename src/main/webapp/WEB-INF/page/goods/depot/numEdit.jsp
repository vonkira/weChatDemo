<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/static/admin/jsp/include.jsp"%>
<script type="text/javascript">
	var id = "${id}";
	var submitForm = function($dialog, $grid, $pjq) {
		if ($('form').form('validate')) {
			var obj=sy.serializeObject($('form'));
			var url=sy.contextPath + '/storeGoodsPushDepot/saveNum';
			$.post(url, obj, function(result) {
				if (result.code == 0) {
					$grid.datagrid('reload');
					$dialog.dialog('destroy');
				} else {
					$pjq.messager.e('添加失败,'+result.msg);
				}
			}, 'json');
		}
	};
	
	$(function() {
		if (id != '') {
			parent.$.messager.progress({
				text : '数据加载中....'
			});
			$.post(sy.contextPath + '/storeGoodsPushDepot/findById', {
				id : id
			}, function(result) {
				if (result) {
				    result = result.goods;
					$('form').form('load', result);
					var left = result.num-result.userNum;
					$('#leftNum').text(left);
					$('#add').numberbox({
						min:-left,
						max:999999999
					});
				}
				parent.$.messager.progress('close');
			}, 'json');
		} else {
			
		}
		
		
	});
</script>
</head>
<body>
	<form id="form" method="post">
        <input name="id" type="hidden" />
        <div style="padding:15px;font-size: 12px">
            <table style="table-layout:fixed;" border="0" cellspacing="0" class="formtable">
		    	<tr>
		    		<th style="width:100px;" rowspan="2">数量：</th>
		    		<td>
		    			<input id="add" name="add" missingMessage="请输入数量" prompt="请输入数量" style="width:90%;"
		    				data-options="required:true" />
		    		</td>
		    	</tr>
				<tr>
					<td>
						<label style="color: red;">*</label>
						<label>剩余数量<span id="leftNum"></span></label>
					</td>
				</tr>
            </table>
        </div>
	</form>
</body>
</html>