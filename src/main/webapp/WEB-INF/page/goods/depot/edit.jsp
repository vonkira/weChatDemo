<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/static/admin/jsp/include.jsp"%>
<script type="text/javascript">
	var id = "${id}";
	var submitForm = function($dialog, $grid, $pjq) {
		if ($('form').form('validate')) {
			var obj=sy.serializeObject($('form'));
			var url=sy.contextPath + '/storeGoodsPushDepot/save';
			$.post(url, obj, function(result) {
				if (result.code == 0) {
					$grid.datagrid('reload');
					$dialog.dialog('destroy');
				} else {
					$pjq.messager.e('添加失败,'+result.msg);
				}
			}, 'json');
		}
	};
	
	$(function() {
//        if (sy.system == 'store'){
//            $('.store').hide();
//        }
        $('#goodsId').combobox({
            url:sy.contextPath + '/storeGoods/listByStore',
            textField:'name',
            valueField:'id',
            panelMaxHeight:'150',
            filter: function(q, row){
                var opts = $(this).combobox('options');
                return row[opts.textField].toLowerCase().indexOf(q.toLowerCase()) != -1;
            }
        });
		if (id != '') {
			parent.$.messager.progress({
				text : '数据加载中....'
			});
			$.post(sy.contextPath + '/storeGoodsPushDepot/findById', {
				id : id
			}, function(result) {
				if (result) {
//                    if (sy.system == 'store'){
//                        loadGoods(sy.id,function () {
//                            $('#goodsId').combobox("setValue",result.goods.goodsId);
//                        });
//                    }else{
//                        loadStore(result.storeGoods.storeId,function () {
//                            $('#goodsId').combobox("setValue",result.goods.goodsId);
//                        });
//                    }
					$('form').form('load', result.goods);
				}
				parent.$.messager.progress('close');
			}, 'json');
            $('#goodsId').combobox('readonly',true);
            $('#num').numberbox('readonly',true);
		} else {
            if (sy.system == 'store'){
                loadGoods(sy.id);
            }else{
                loadStore();
            }
		}
		
		
	});

    function loadStore(store,func) {
        $('#storeId').combobox({
            url:sy.contextPath + '/store/listStore',
            textField:'name',
            valueField:'id',
            editable:false,
            panelMaxHeight:'150',
            onLoadSuccess:function () {
                $('#storeId').combobox("select",store);
            },
            onSelect:function (v) {
                loadGoods(v.id,func);
            },
            onChange:function (v) {
                if (v){
                    loadGoods(v);
                }

            }
        });
    }
    function loadGoods(v,func) {
        $('#goodsId').combobox({
            url:sy.contextPath + '/storeGoods/listByStore?storeId='+v,
            textField:'name',
            valueField:'id',
            editable:false,
            panelMaxHeight:'150',
            onLoadSuccess:function () {
                if (func){
                    func.call();
                }
            }
        });
    }
</script>
</head>
<body>
	<form id="form" method="post">
        <input name="id" type="hidden" />
        <div style="padding:15px;font-size: 12px">
            <table style="table-layout:fixed;" border="0" cellspacing="0" class="formtable">
		    	<tr>
		    		<th style="width:100px;">名称：</th>
		    		<td>
		    			<input id="name" name="name" class="easyui-textbox" missingMessage="请输入名称" prompt="请输入名称" style="width:90%;"
		    				data-options="required:true,validType:['length[0,50]']" />
		    		</td>
		    	</tr>
				<%--<tr class="store">--%>
					<%--<th style="width:100px;">选择商家：</th>--%>
					<%--<td>--%>
						<%--<select id="storeId"--%>
								<%--editable="false" name="storeId" style="width:182px;"/>--%>
					<%--</td>--%>
				<%--</tr>--%>
				<tr>
					<th style="width:100px;">投放商品：</th>
					<td>
						<input id="goodsId" name="goodsId" style="width:90%;"
							   data-options="required:true" />
					</td>
				</tr>
				<tr>
					<th style="width:100px;">投放数量：</th>
					<td>
						<input id="num" name="num" class="easyui-numberbox" missingMessage="请输入投放数量" prompt="请输入投放数量" style="width:90%;"
							   data-options="required:true,min:0,max:999999999" />
					</td>
				</tr>
				<tr>
					<th style="width:100px;">用户最大获取数量：</th>
					<td>
						<input id="userMax" name="userMax" class="easyui-numberbox" missingMessage="请输入用户最大获取数量" prompt="请输入用户最大获取数量" style="width:90%;" value="-1"
							   data-options="required:true,min:-1,max:999999999" />
					</td>
				</tr>
				<tr>
					<th style="width:100px;">用户每日最大获取数量：</th>
					<td>
						<input id="dayMax" name="dayMax" class="easyui-numberbox" missingMessage="请输入每日最大获取数量" prompt="请输入每日最大获取数量" style="width:90%;" value="-1"
							   data-options="required:true,min:-1,max:999999999" />
					</td>
				</tr>
            </table>
        </div>
	</form>
</body>
</html>