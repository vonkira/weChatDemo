<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/static/admin/jsp/include.jsp"%>
<script type="text/javascript">
	//投放id
	var _id = '${id}';
	var grid;
	
	var addFun = function($dialog, $grid, $pjq) {
		var dialog = parent.sy.modalDialog({
			title : '添加活动',
			width : 500,
			height : 400,
			url : sy.contextPath + '/go?path=goods/depot/actEdit&depotId='+_id,
			buttons : [ {
				text : '保存',
				handler : function() {
					dialog.find('iframe').get(0).contentWindow.submitForm(dialog, grid, parent.$);
				}
			} ]
		});
	};
	
	var delFun = function() {
		var rows = grid.datagrid('getSelections');
		if (rows.length == 0) {
			parent.$.messager.w('请选择需要删除的记录！');
			return;
		}
		parent.$.messager.confirm('询问', '您确定要删除此记录？', function(r) {
			if (r) {
				var ids = [];
				for ( var i = 0, l = rows.length; i < l; i++) {
					var r = rows[i];
					ids.push(r.id);
				}
				var id = ids.join(',');
				
				$.post(sy.contextPath + '/storeGoodsActivity/del', {
					id : id
				}, function(res) {
					rows.length = 0;//必须，否则有bug
					grid.datagrid('reload');
				}, 'json');
			}
		});
	};
	
	//查看商品详情
	var infoFun = function(id) {
        parent.sy.modalDialog({
            title : '触发器商品',
            width : 800,
            height : 600,
            url : sy.contextPath + '/go?path=act/triggerGoodsList&id=' + id
        });
	};
	
	//查看ID
	var idFun = function() {
		var rows = grid.datagrid('getSelections');
		if (rows.length != 1) {
			parent.$.messager.w('请选择一条记录进行复制！');
			return;
		}
		sy.getId(rows[0].id,'复制');
	};
	
	//上下架商品事件
	var onlineFun = function(type, id) {
		var title = "";
		if (type == 1) {
			title = "确定启用该触发器吗？";
		} else {
			title = "确定禁用该触发器吗？";
		}
		var data = {id: id, state: type};
		parent.$.messager.confirm("询问", title, function(r) {
			if (r) {
				saveData(data);
			}
		});
	};

	function triggerFun(id) {
        parent.sy.modalDialog({
            title : '触发器商品',
            width : 800,
            height : 600,
            url : sy.contextPath + '/go?path=act/triggerGoodsList&id=' + id
        });
    }

    //上下架商品事件
    var mapFun = function(type, id) {
        var title = "";
        if (type == 1) {
            title = "确定该活动显示在地图上吗？";
        } else {
            title = "确定该活动不显示在地图上吗？";
        }
        var data = {id: id, isMap: type};
        parent.$.messager.confirm("询问", title, function(r) {
            if (r) {
                saveData(data);
            }
        });
    };

    //保存数据
    var saveData = function(data) {
        var url = sy.contextPath + '/storeActivity/save';
        $.post(url, data, function() {
            grid.datagrid('reload');
        }, 'json');
    };

	$(function() {
		grid = $('#grid').datagrid({
			url : sy.contextPath + '/storeGoodsActivity/list?depotId='+_id,
			singleSelect : false,
//            fitColumns:$(this).width() <= 1310?false:true,
			frozenColumns : [ [ {
				width : '100',
				checkbox:true,
				field : 'id',
				align : 'center'
			},{
				width : 100,
				title : '活动名称',
				field : 'activityName',
				align : 'center',
				formatter: function(value, row, index) {
					return UT.addTitle(value);
				}
			}
			] ],
			columns : [ [
				{
					width : 50,
					title : '已领取数量',
					field : 'userNum',
					align : 'center',
					formatter:function(v,r,i){
                        return UT.addTitle(v);
					}
				},
//				{
//                    width : 50,
//                    title : '状态',
//                    field : 'state',
//                    align : 'center',
//                    formatter : function(value, row, index) {
//                        switch (value) {
//                            case 0:
//                                return '<a href="javascript:void(0);" onclick="onlineFun(1,\''+row.id+'\')" ><img src="static/admin/images/no.png" title="点击启用" > </img></a>';
//                            case 1:
//                                return '<a href="javascript:void(0);" onclick="onlineFun(0,\''+row.id+'\')" ><img src="static/admin/images/yes.png" title="点击禁用" > </img></a>';
//                        }
//                    }
//                },
				{
					width : 140,
					title : '创建时间',
					field : 'createTime',
					align : 'center'
				}
			] ]
		});
	});
</script>
</head>
<body>
	<div id="toolbar">
		<div class="tbbutton">
			<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-plus-circle',plain:true" onclick="addFun();">添加</a>
			<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-trash',plain:true" onclick="delFun();">删除</a>
		</div>
	</div>
	<table id="grid" data-options="fit:true,border:false"></table>
</body>
</html>