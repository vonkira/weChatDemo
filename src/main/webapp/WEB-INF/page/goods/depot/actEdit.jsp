<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/static/admin/jsp/include.jsp"%>
<script type="text/javascript">
	var id = "${id}";
	var _id = '${depotId}'
	var submitForm = function($dialog, $grid, $pjq) {
		if ($('form').form('validate')) {
			var obj=sy.serializeObject($('form'));
			var url=sy.contextPath + '/storeGoodsActivity/save';
			obj.pushDepotId = _id;
			$.post(url, obj, function(result) {
				if (result.code == 0) {
					$grid.datagrid('reload');
					$dialog.dialog('destroy');
				} else {
					$pjq.messager.e('添加失败,'+result.msg);
				}
			}, 'json');
		}
	};
	
	$(function() {
	    $('#acitvityId').combobox({
			url:sy.contextPath + '/storeActivity/listByDepot?depotId='+_id,
			textField:'name',
			valueField:'id',
            formatter:function (row) {
                var opts = $(this).combobox('options');
                if (row.img){
                    return row[opts.textField]+'<img width="18px" height="18px" src="download?id='+row.img+'" />';
				}else{
                    return row[opts.textField];
				}
            }
		});

		if (id != '') {
			parent.$.messager.progress({
				text : '数据加载中....'
			});
			$.post(sy.contextPath + '/storeGoodsActivity/findById', {
				id : id
			}, function(result) {
				if (result) {
					$('form').form('load', result);
				}
				parent.$.messager.progress('close');
			}, 'json');
		} else {
			
		}
		
		
	});
</script>
</head>
<body>
	<form id="form" method="post">
        <input name="id" type="hidden" />
        <div style="padding:15px;font-size: 12px">
            <table style="table-layout:fixed;" border="0" cellspacing="0" class="formtable">
		    	<tr>
		    		<th style="width:100px;">活动：</th>
		    		<td>
		    			<input id="acitvityId" name="acitvityId" missingMessage="请选择活动" prompt="请选择活动" style="width:90%;"
		    				data-options="required:true" />
		    		</td>
		    	</tr>
            </table>
        </div>
	</form>
</body>
</html>