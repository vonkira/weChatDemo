<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/static/admin/jsp/include.jsp"%>
<script type="text/javascript">
	var grid;

	$(function() {
		grid = $('#grid').datagrid({
			url : sy.contextPath + '/goodsCate/list',
			singleSelect : false,
            pagination:false,
			columns : [ [ 
				{
					width : $(this).width() * 0.5,
					title : '分类名称',
					field : 'name',
					align : 'center'
				},{
					width : $(this).width() * 0.5,
					title : '编号',
					field : 'code',
					align : 'center'
				}
			] ]
		});
	});
</script>
</head>
<body>
	<table id="grid" data-options="fit:true,border:false"></table>
</body>
</html>