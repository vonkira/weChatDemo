<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/static/admin/jsp/include.jsp"%>
	<style type="text/css">
		.xjhb{display: none}
	</style>
<script type="text/javascript">
	var id = "${id}";
	var editor;
	
	var submitForm = function($dialog, $grid, $pjq) {
        var obj = sy.serializeObject($('form'));
		obj.detail = editor.getHtml();
		if ($('form').form('validate')) {
			$.ajax({
				url:sy.contextPath + '/storeGoods/update',
				data:obj,
				type:'post',
				dataType:'json',
				success:function(result) {
					if (result.code == 0) {
						$grid.datagrid('reload');
						$dialog.dialog('destroy');
					} else {
						$pjq.messager.e('添加失败 ' + result.msg);
					}
				}
			});
		}
	};

	$(function() {
		editor=new HtmlEditor('#detail');
		
        if (id != '') {
			parent.$.messager.progress();
			$.post(sy.contextPath + '/storeGoods/findById', {
				id : id
			}, function(result) {
				if (result) {
                    $('form').form('load', result);
					editor.setHtml(result.detail);
				}
				parent.$.messager.progress('close');
			}, 'json');
		}
	});
</script>
</head>
<body>
	<form id="form" method="post">
		<input name="id" type="hidden" />
		<table style="table-layout:fixed;" border="0" cellspacing="0"
			   class="formtable">
			<tr>
				<td><textarea id="detail"
										  style="width:98%;height: 400px"></textarea></td>
			</tr>
		</table>
	</form>
</body>
</html>