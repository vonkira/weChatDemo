<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/static/admin/jsp/include.jsp"%>
<script type="text/javascript">
	//导入类型[1:汽车品牌 2:汽车车系 3:汽车车型 4:推荐商品 5:用户消息推送 6:商品信息 7:采购商品]
	var type = "${type}";
	var id = '${id}'
	var submitForm = function($dialog, $grid, $pjq, callback) {
		var template = $('#template').getFileId();
		if (template == '') {
			$pjq.messager.e('请上传文件');
			return;
		}
		$pjq.messager.progress({
			text : '导入中....'
		});
		var url = "";
		if (type == 1) {
			url = sy.contextPath + '/storeGoods/importData';
		} else if (type == 2) {
			url = sy.contextPath + '/carSeries/importData';
		} else if (type == 3) {
			url = sy.contextPath + '/carType/importData';
		} else if (type == 4) {
			url = sy.contextPath + '/carCommend/importData';
		} else if (type == 5) {
			url = sy.contextPath + '/msg/importData';
		} else if (type == 6) {
			url = sy.contextPath + '/goods/importData';
		} else if (type == 7) {
			url = sy.contextPath + '/goodsStock/importData';
		} 
		$.post(url, {fileId: template,id:id}, function(result) {
			if (result.code == 0) {
				$pjq.messager.i('导入成功');
				setTimeout(function() {
					if (type == 5) {
						callback.call($pjq, result.msg);
					} else {
						$grid.datagrid('reload');
					}
					$dialog.dialog('destroy');
				}, 1000);
			} else {
				$pjq.messager.e('导入失败,'+result.msg);
			}
			$pjq.messager.progress('close');
		}, 'json');
	};
	
	var downloadFun = function(){
		window.location = sy.contextPath+'/storeGoods/downloadTemplate?type=' + type;
		return;
	};
</script>
</head>
<body>
	<form id="form" method="post">
        <input name="id" type="hidden" />
        <div style="padding:15px;font-size: 12px">
            <table style="table-layout:fixed;" border="0" cellspacing="0" class="formtable">
		    	<tr>
		    		<th style="width:100px;">导入文件：</th>
		    		<td>
		    			<div id="template" multi="false" required="required" type="file" showBtn="true" fileType="xls,xlsx" fileSize="10MB" buttonText="上传"></div>
		    		</td>
		    	</tr>
		    	<tr>
		    		<td colspan="2" style="text-align: center;">
		    			<a href="javascript:void(0);" class="easyui-linkbutton" style="width:20%;background: antiquewhite;" onclick="downloadFun();">下载模板</a>
		    		</td>
		    	</tr>
            </table>
        </div>
	</form>
</body>
</html>