<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/static/admin/jsp/include.jsp"%>
<script type="text/javascript">
	var grid;
	
	$(function() {
		grid = $('#grid').datagrid({
			url : sy.contextPath + '/userGoodsLog/appList',
			columns : [ [ 
				{
					width : $(this).width() * 0.15,
					title : '商家',
					field : 'storeName',
					align : 'center',
					formatter:function (v) {
						return UT.addTitle(v);
                    }
				},{
                    width : $(this).width() * 0.15,
                    title : '物品名称',
                    field : 'goodsName',
                    align : 'center',
                    formatter:function (v) {
                        return UT.addTitle(v);
                    }
                },{
                    width : $(this).width() * 0.15,
                    title : '用户',
                    field : 'account',
                    align : 'center',
                    formatter:function (v,r) {
                        return UT.addTitle(v+'('+r.nickName+')');
                    }
                },{
                    width : $(this).width() * 0.1,
                    title : '获取时间',
                    field : 'createTime',
                    align : 'center',
                    formatter:function (v) {
                        if (v){
                            return UT.addTitle(v);
                        }else{
                            return '——';
                        }

                    }
                },{
                    width : $(this).width() * 0.1,
                    title : '核销时间',
                    field : 'checkTime',
                    align : 'center',
                    formatter:function (v) {
                        if (v){
                            return UT.addTitle(v);
						}else{
                            return '——';
						}

                    }
                },{
					width : $(this).width() * 0.35,
					title : '描述',
					field : 'info',
					align : 'center',
					formatter:function (v) {
						return UT.addTitle(v);
                    }
				}
			] ]
		});

		$('#storeId').combobox({
			url:sy.contextPath +'/store/listStore',
			textField:'name',
			valueField:'id',
			editable:false
        })
	});
</script>
</head>
<body>
	<div id="toolbar">
		<form id="searchForm">
			<div>
				<input type="text" class="easyui-textbox" name="account" style="width: 150px" prompt="用户账号/昵称"/>
				<select id="storeId" name="storeId" style="width: 150px"></select>
				<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-search'" onclick="grid.datagrid('load',sy.serializeObject($('#searchForm')));">过滤</a>
				<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-search-minus'" onclick="$('#searchForm input').val('');grid.datagrid('load',{});">重置过滤</a>
			</div>
		</form>
	</div>
	<table id="grid" data-options="fit:true,border:false"></table>
</body>
</html>