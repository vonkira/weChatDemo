<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/static/admin/jsp/include.jsp"%>
<script type="text/javascript">
	var id = "${id}";
	var submitForm = function($dialog, $grid, $pjq) {
		if ($('form').form('validate')) {
			var obj=sy.serializeObject($('form'));
            if (!obj.img){
                sy.alert("请上传小图");
                return;
            }
            if (!obj.largeImg){
                $pjq.messager.w("请上传大图");
                return;
            }
            if (!obj.animation){
                $pjq.messager.w("请上传动画");
                return;
            }
			var url=sy.contextPath + '/goodsContainer/save';
			$.post(url, obj, function(result) {
				if (result.code == 0) {
					$grid.datagrid('reload');
					$dialog.dialog('destroy');
				} else {
					$pjq.messager.e('添加失败,'+result.msg);
				}
			}, 'json');
		}
	};
	
	$(function() {
		if (id != '') {
			parent.$.messager.progress({
				text : '数据加载中....'
			});
			$.post(sy.contextPath + '/goodsContainer/findById', {
				id : id
			}, function(result) {
				if (result) {
                    $('#img').setFileId(result.img, false, true, true);
                    $('#largeImg').setFileId(result.largeImg, false, true, true);
                    $('#animation').setFileId(result.animation, false, true, true);
					$('form').form('load', result);
				}
				parent.$.messager.progress('close');
			}, 'json');
		} else {
			
		}
		
		
	});
</script>
</head>
<body>
	<form id="form" method="post">
        <input name="id" type="hidden" />
        <div style="padding:15px;font-size: 12px">
            <table style="table-layout:fixed;" border="0" cellspacing="0" class="formtable">
		    	<tr>
		    		<th style="width:100px;">名称：</th>
		    		<td>
		    			<input id="name" name="name" class="easyui-textbox" missingMessage="请输入名称" prompt="请输入名称" style="width:90%;"
		    				data-options="required:true,validType:['length[0,20]']" />
		    		</td>
		    	</tr>
				<tr>
					<th style="width:100px;">图标(地图上显示)：</th>
					<td >
						<div id="img" multi="false" required="required" type="file" showImage="true" showBtn="true" bestSize="226*226" fileType="jpg,png,gif" fileSize="20MB" buttonText="上传图标"></div>
					</td>
				</tr>
				<tr>
					<th style="width:100px;">摄像头对转扫描效果：</th>
					<td >
						<div id="largeImg" multi="false" required="required" type="file" showImage="true" showBtn="true" bestSize="226*226" fileType="jpg,png,gif" fileSize="20MB" buttonText="上传图标"></div>
					</td>
				</tr>
				<tr>
					<th style="width:100px;">打开动画：</th>
					<td >
						<div id="animation" multi="false" required="required" type="file" showImage="true" showBtn="true" bestSize="226*226" fileType="jpg,png,gif" fileSize="2MB" buttonText="上传动画"></div>
					</td>
				</tr>
            </table>
        </div>
	</form>
</body>
</html>