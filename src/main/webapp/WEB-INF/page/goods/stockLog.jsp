<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/static/admin/jsp/include.jsp"%>
<script type="text/javascript">
	var id = '${id}';
	var grid;
	$(function() {

        grid = $('#grid').datagrid({
            url : sy.contextPath + '/storeGoods/log?id='+id,
            singleSelect : false,
//            fitColumns : false,
            columns : [ [
                {
                    width : 500,
                    title : '说明',
                    field : 'info',
                    align : 'center',
                    formatter: function(value, row, index) {
                        return UT.addTitle(value);
                    }
                },{
                    width : 50,
                    title : '数量',
                    field : 'num',
                    align : 'center'
                },{
                    width : 150,
                    title : '时间',
                    field : 'createTime',
                    align : 'center'
                }
            ] ]
        });
	});
</script>
</head>
<body>
	<table id="grid" data-options="fit:true,border:false"></table>
</body>
</html>