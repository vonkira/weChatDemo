<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/static/admin/jsp/include.jsp"%>
	<style type="text/css">
		.xjhb{display: none}
	</style>
<script type="text/javascript">
	var id = "${id}";
	var editor;
	
	var submitForm = function($dialog, $grid, $pjq) {
		var obj = sy.serializeObject($('form'));
		var detail = editor.getHtml();
		obj.detail = detail;
        if (!obj.imgSmall){
            $pjq.messager.w("请上传小图");
            return;
        }
		if ($('form').form('validate')) {
			$.ajax({
				url:sy.contextPath + '/storeGoods/saveReal',
				data:obj,
				type:'post',
				dataType:'json',
				success:function(result) {
					if (result.code == 0) {
						$grid.datagrid('reload');
						$dialog.dialog('destroy');
					} else {
						$pjq.messager.e('添加失败 ' + result.msg);
					}
				}
			});
		}
	};

	$(function() {
		editor=new HtmlEditor('#detail');
		
        $('#tt').tabs({
            border:false,
            onSelect:function(title){
                if ('图片' == title){
                    sy.initFileUpload();
				}
            }
        });

        if (id != '') {
			parent.$.messager.progress();
			$.post(sy.contextPath + '/storeGoods/findById', {
				id : id
			}, function(result) {
				if (result) {
					$('#imgSmall').setFileId(result.imgSmall, false, true, true);
					$('form').form('load', result);
					editor.setHtml(result.detail);
				}
				parent.$.messager.progress('close');
			}, 'json');
		}
	});
</script>
</head>
<body>
	<form id="form" method="post">
		<input name="id" type="hidden" />
		<div id="tt" class="easyui-tabs" style="width:100%;height:480px;">
			<div title="基础参数" style="padding:20px; ">
				<table style="table-layout:fixed;" border="0" cellspacing="0" class="formtable">
					<tr>
						<th style="width:100px;">商品名称：</th>
						<td colspan="1"><input id="name" name="name"
											   style="width: 90%" class="easyui-textbox"
											   data-options="required:true,validType:['length[0,30]']"
											   missingMessage="请输入名称" prompt="请输入名称" /></td>
					</tr>
					<tr>
						<th style="width:100px;">商品描述：</th>
						<td colspan="1"><input id="info" name="info"
											   style="width: 90%" class="easyui-textbox"
											   data-options="required:true,validType:['length[0,100]']"
											   missingMessage="请输入商品描述" prompt="请输入商品描述" /></td>
					</tr>
				</table>
			</div>
			<div title="图片" style="padding:20px; ">
				<table style="table-layout:fixed;" border="0" cellspacing="0" class="formtable">
					<tr>
						<th style="width:100px;">小图：</th>
						<td colspan="3">
							<div id="imgSmall" multi="false" required="required" cutWidth="100" cutHeight="100" showWidth="40" showHeight="40"  type="file" showImage="true" showBtn="true" bestSize="226*226" fileType="jpg,png" fileSize="2MB" buttonText="上传封面"></div>
							<label style="color: red;">*</label>
							<label>建议尺寸：宽度100，高度100，格式JPG,PNG</label>
						</td>
					</tr>
				</table>
			</div>
			<div title="图文详情" style="padding:20px; ">
				<table style="table-layout:fixed;" border="0" cellspacing="0"
					class="formtable">
					<tr>
						<th style="width:100px;">图文详情：</th>
						<td colspan="3"><textarea id="detail" name="detail"
								style="width:98%;height: 400px"></textarea></td>
					</tr>
				</table>
			</div>
		</div>
	</form>
</body>
</html>