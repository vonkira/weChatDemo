<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/static/admin/jsp/include.jsp"%>
<script type="text/javascript">
	var grid;
	
	var addFun = function($dialog, $grid, $pjq) {
		var dialog = parent.sy.modalDialog({
			title : '添加载体',
			width : 600,
			height : 500,
			url : sy.contextPath + '/go?path=goods/containerEdit',
			buttons : [ {
				text : '保存',
				handler : function() {
					dialog.find('iframe').get(0).contentWindow.submitForm(dialog, grid, parent.$);
				}
			} ]
		});
	};
	
	var editFun = function() {
		var rows = grid.datagrid('getSelections');
		if (rows.length != 1) {
			parent.$.messager.w('请选择一条记录进行编辑！');
			return;
		}
		var dialog = parent.sy.modalDialog({
			title : '编辑载体',
			width : 600,
			height : 500,
			url : sy.contextPath + '/go?path=goods/containerEdit&id=' + rows[0].id,
			buttons : [ {
				text : '保存',
				handler : function() {
					dialog.find('iframe').get(0).contentWindow.submitForm(dialog, grid, parent.$);
				}
			} ]
		});
	};
	
	var delFun = function() {
		var rows = grid.datagrid('getSelections');
		if (rows.length == 0) {
			parent.$.messager.w('请选择需要删除的记录！');
			return;
		}
		parent.$.messager.confirm('询问', '您确定要删除此记录？', function(r) {
			if (r) {
				var ids = [];
				for ( var i = 0, l = rows.length; i < l; i++) {
					var r = rows[i];
					ids.push(r.id);
				}
				var id = ids.join(',');
				
				$.post(sy.contextPath + '/goodsContainer/del', {
					id : id
				}, function(res) {
					if (res.code == 0) {
						rows.length = 0;//必须，否则有bug
						grid.datagrid('reload');
					} else {
						parent.$.messager.w(res.msg);
					}
				}, 'json');
			}
		});
	};
	
	$(function() {
		grid = $('#grid').datagrid({
			url : sy.contextPath + '/goodsContainer/list',
//			singleSelect : false,
//			frozenColumns : [ [ {
//				width : '100',
//				checkbox:true,
//				field : 'id',
//				align : 'center'
//			}] ],
			columns : [ [ 
				{
					width : $(this).width() * 0.2,
					title : '载体名称',
					field : 'name',
					align : 'center'
				},{
                    width : $(this).width() * 0.1,
                    title : '图标',
                    field : 'img',
                    align : 'center',
					formatter:function (v) {
						return po.showImg(v,20,20);
                    }
                },{
                    width : $(this).width() * 0.1,
                    title : '大图标',
                    field : 'largeImg',
                    align : 'center',
                    formatter:function (v) {
                        return po.showImg(v,20,20);
                    }
                },
				{
                    width : $(this).width() * 0.1,
                    title : '动画',
                    field : 'animation',
                    align : 'center',
                    formatter:function (v) {
                        return po.showImg(v,20,20);
                    }
                },
				{
					width : $(this).width() * 0.5,
					title : '创建时间',
					field : 'createTime',
					align : 'center'
				}
			] ]
		});
	});
</script>
</head>
<body>
	<div id="toolbar">
		<div class="tbbutton">
			<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-plus-circle',plain:true" onclick="addFun();">添加</a>
			<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-pencil',plain:true" onclick="editFun();">修改</a>
			<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-trash',plain:true" onclick="delFun();">删除</a>
		</div>
	</div>
	<table id="grid" data-options="fit:true,border:false"></table>
</body>
</html>