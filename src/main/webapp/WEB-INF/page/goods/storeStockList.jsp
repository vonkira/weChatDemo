<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/static/admin/jsp/include.jsp"%>
<script type="text/javascript">
	var storeId = "${storeId}";
	var grid;
	
	//导出数据
	var exportFun = function() {
		var obj = sy.serializeObject($('form'));
		obj.storeId = storeId;
		var jsonStr = sy.jsonToString(obj);
		window.location = sy.contextPath+'/storeGoods/exportExcel.do?jsonStr=' + encodeURI(jsonStr);
		return;
	};
	
	$(function() {
		grid = $('#grid').datagrid({
			url : sy.contextPath + '/storeGoods/goodsList?storeId=' + storeId,
			columns : [ [ 
				{
					width : $(this).width() * 0.2,
					title : '商品名称',
					field : 'goodsName',
					align : 'center',
					formatter: function(v, r, i) {
						return UT.addTitle(v);
					}
				},{
					width : $(this).width() * 0.2,
					title : '商品编号',
					field : 'goodsCode',
					align : 'center',
					formatter: function(v, r, i) {
						return UT.addTitle(v);
					}
				},{
					width : $(this).width() * 0.2,
					title : '商品库存',
					field : 'total',
					align : 'center',
					formatter: function(v, r, i) {
						if (r.total <= r.warnNum) {
							return UT.addLabel(v, 'red');
						} else {
							return v;
						}
					}
				},{
					width : $(this).width() * 0.2,
					title : '创建时间',
					field : 'createTime',
					align : 'center'
				}
			] ]
		});
		
		$('#type').combobox({
			textField:'text',
			valueField:'value',
			data:[{text:'全部',value:0,selected:true},{text:'已预警',value:1},{text:'未预警',value:2}],
			onChange: function(rec) {
				grid.datagrid('load',sy.serializeObject($('#searchForm')));
			}
		});
	});
</script>
</head>
<body>
	<div id="toolbar">
		<form id="searchForm">
		<div>
			<span>商品名称：</span>
			<input type="text" class="easyui-textbox" name="name" style="width: 150px" prompt="商品名称"/>
			<span>商品编号：</span>
			<input type="text" class="easyui-textbox" name="code" style="width: 150px" prompt="商品编号"/>
			<span>预警类型：</span>
			<input id="type" name="type" editable="false" style="width:150px;" panelHeight='150' />
			<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-search'" onclick="grid.datagrid('load',sy.serializeObject($('#searchForm')));">过滤</a> 
			<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-search-minus'" onclick="$('#searchForm input').val('');grid.datagrid('load',{});">重置过滤</a>
		</div>
		</form>
		<div class="tbbutton">
			<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-file-excel-o',plain:true" onclick="exportFun();">导出</a>
		</div>
	</div>
	<table id="grid" data-options="fit:true,border:false"></table>
</body>
</html>