<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/static/admin/jsp/include.jsp"%>
<script type="text/javascript">
	var id = "${id}";
	var editor;
	
	var submitForm = function($dialog, $grid, $pjq) {
		var obj = sy.serializeObject($('form'));
		var detail = editor.getHtml();
		obj.detail = detail;
		if (!obj.imgLarge){
            $pjq.messager.w("请上传大图");
		    return;
		}
        if (!obj.imgMiddle){
            $pjq.messager.w("请上传中图");
            return;
        }
        if (!obj.imgSmall){
            $pjq.messager.w("请上传小图");
            return;
        }
//        if (!obj.animation){
//            $pjq.messager.w("请上传动画");
//            return;
//        }
		if ($('form').form('validate')) {
			$.ajax({
				url:sy.contextPath + '/storeGoods/save',
				data:obj,
				type:'post',
				dataType:'json',
				success:function(result) {
					if (result.code == 0) {
						$grid.datagrid('reload');
						$dialog.dialog('destroy');
					} else {
						$pjq.messager.e('添加失败 ' + result.msg);
					}
				}
			});
		}
	};

	$(function() {
		editor=new HtmlEditor('#detail');
		
		$('#cateCode').combobox({
			textField: 'name',
			valueField: 'code',
			url : sy.contextPath + '/goodsCate/list',
			onChange:function (v) {
				if (v != 'xjhb'){
				    $('.xjhb').hide();
                    $('#price').numberbox('disableValidation');
				}else{
                    $('.xjhb').show();
                    $('#price').numberbox('enableValidation');
				}
                if (v == 'jb'){
                    $('.jb').hide();
                    $('#coinValue').numberbox('disableValidation');
                }else{
                    $('.jb').show();
                    $('#coinValue').numberbox('enableValidation');
				}
				if (v == 'wp' || v == 'jbk' || v == 'jb'){
                    $('.j').hide();
                    //beginTime
                    $('#beginTime').numberbox('disableValidation');
                    $('#endTime').numberbox('disableValidation');
				}else{
                    $('.j').show();
                    $('#beginTime').numberbox('enableValidation');
                    $('#endTime').numberbox('enableValidation');
				}

				if (v == 'jpm'){
                    $('.jpm').show();
                    $('#useUrl').numberbox('enableValidation');
                }else{
                    $('.jpm').hide();
                    $('#useUrl').numberbox('disableValidation');
                }
            }
		});

        $('#tt').tabs({
            border:false,
            onSelect:function(title){
                if ('图片' == title){
                    sy.initFileUpload();
				}
            }
        });

        $('#catchTriggerId').combobox({
            url:sy.contextPath + '/goodsTrigger/listByStore?flag=0',
            textField:'name',
            valueField:'id',
            required:true,
            filter: function(q, row){
                var opts = $(this).combobox('options');
                return row[opts.textField].toLowerCase().indexOf(q.toLowerCase()) != -1;
            }
        });

        $('#pushTriggerId').combobox({
            url:sy.contextPath + '/goodsTrigger/listByStore?flag=0',
            textField:'name',
            valueField:'id',
            required:true,
            filter: function(q, row){
                var opts = $(this).combobox('options');
                return row[opts.textField].toLowerCase().indexOf(q.toLowerCase()) != -1;
            }
        });

        if (id != '') {
            $('#cateCode').combobox("readonly");
			parent.$.messager.progress();
			$.post(sy.contextPath + '/storeGoods/findById', {
				id : id
			}, function(result) {
				if (result) {
					$('#imgLarge').setFileId(result.imgLarge, false, true, true);
					$('#imgMiddle').setFileId(result.imgMiddle, false, true, true);
					$('#imgSmall').setFileId(result.imgSmall, false, true, true);
					$('#imgSmallGray').setFileId(result.imgSmallGray, false, true, true);
					$('#animation').setFileId(result.animation, false, true, true);
					$('form').form('load', result);
					editor.setHtml(result.detail);
				}
				parent.$.messager.progress('close');
			}, 'json');
		}
	});
</script>
</head>
<body>
	<form id="form" method="post">
		<input name="id" type="hidden" />
		<div id="tt" class="easyui-tabs" style="width:100%;height:480px;">
			<div title="基础参数" style="padding:20px; ">
				<table style="table-layout:fixed;" border="0" cellspacing="0" class="formtable">
					<tr>
						<th style="width:100px;">物品分类：</th>
						<td><select id="cateCode" name="cateCode"
									editable="false" style="width: 90%"
									data-options="required:true,valueField: 'id',textField: 'title'" />
						</td>
						<th style="width:100px;">物品名称：</th>
						<td colspan="1"><input id="name" name="name"
											   style="width: 90%" class="easyui-textbox"
											   data-options="required:true,validType:['length[0,30]']"
											   missingMessage="请输入名称" prompt="请输入名称" /></td>
					</tr>
					<tr>
						<th style="width:100px;">物品描述：</th>
						<td colspan="3"><input id="info" name="info"
											   style="width: 90%" class="easyui-textbox"
											   data-options="required:true,validType:['length[0,100]']"
											   missingMessage="请输入物品描述" prompt="请输入物品描述" /></td>
					</tr>
					<tr>
						<th style="width:100px;">物品来源：</th>
						<td colspan="3"><input id="source" name="source"
											   style="width: 90%" class="easyui-textbox"
											   data-options="validType:['length[0,100]']"
											   missingMessage="请输入物品来源" prompt="请输入物品来源" /></td>
					</tr>
					<tr class="jpm">
						<th style="width:100px;">物品使用地址(奖品码专用)：</th>
						<td colspan="3"><input id="useUrl" name="useUrl"
											   style="width: 90%" class="easyui-textbox"
											   data-options="required:true,validType:['url','length[0,255]']"
											   missingMessage="请输入物品使用地址" prompt="请输入物品使用地址" /></td>
					</tr>
					<tr>
						<th style="width:100px;" class="jb">允许用户投放：</th>
						<td class="jb"><select id="isUserPush" name="isUserPush"
									class="easyui-combobox" editable="false" style="width:182px;"
									panelHeight='auto'
									data-options="valueField: 'value',textField: 'name',data: [{name: '否',value: '0',selected:true},{name: '是',value: '1'}]"/>
						</td>
						<th style="width:100px;" class="jb">允许使用：</th>
						<td class="jb"><select id="isUse" name="isUse"
									class="easyui-combobox" editable="false" style="width:182px;"
									panelHeight='auto'
									data-options="valueField: 'value',textField: 'name',data: [{name: '否',value: '0',selected:true},{name: '是',value: '1'}]"/>
						</td>
					</tr>
					<tr>
						<th style="width:100px;">可合成：</th>
						<td><select id="isCompound" name="isCompound"
									class="easyui-combobox" editable="false" style="width:182px;"
									panelHeight='auto'
									data-options="valueField: 'value',textField: 'name',data: [{name: '否',value: '0',selected:true},{name: '是',value: '1'}]"/>
						</td>
						<th style="width:100px;">作为材料：</th>
						<td><select id="isMaterial" name="isMaterial"
									class="easyui-combobox" editable="false" style="width:182px;"
									panelHeight='auto'
									data-options="valueField: 'value',textField: 'name',data: [{name: '否',value: '0',selected:true},{name: '是',value: '1'}]"/>
						</td>
					</tr>
					<tr>
						<th style="width:100px;" class="jb">金币价值：</th>
						<td class="jb"><input id="coinValue" name="coinValue"
								   style="width: 90%" class="easyui-numberbox"
								   groupSeparator=","
								   data-options="required:true,min:0,max:99999999"
								   missingMessage="金币价值" prompt="金币价值" />
						</td>
						<th style="width:100px;" class="xjhb">现金价值(红包)：</th>
						<td class="xjhb"><input id="price" name="price"
												style="width: 90%" class="easyui-numberbox" precision="2"
												groupSeparator="," prefix="￥"
												data-options="required:true,min:0.01,max:99999999"
												missingMessage="请输入物品价值" prompt="请输入物品价值" />
						</td>
					</tr>
					<tr class="j">
						<th style="width:100px;">物品有效期：</th>
						<td colspan="3"><input id="beginTime" name="beginTime"
								   style="width: 40%" class="easyui-datetimebox"
								   data-options="required:true,editable:false"
								   missingMessage="请输入物品有效期" prompt="请输入物品有效期" /> ——
							<input id="endTime" name="endTime"
								   style="width: 40%" class="easyui-datetimebox"
								   data-options="required:true,editable:false"
								   missingMessage="请输入物品有效期" prompt="请输入物品有效期" />
						</td>
					</tr>
					<tr>
						<th style="width:100px;">仓库偷取触发器：</th>
						<td>
							<select id="catchTriggerId" name="catchTriggerId" style="width: 90%"></select>
						</td>
						<th style="width:100px;">仓库投放触发器：</th>
						<td>
							<select id="pushTriggerId" name="pushTriggerId" style="width: 90%"></select>
						</td>
					</tr>
				</table>
			</div>
			<div title="图片" style="padding:20px; ">
				<table style="table-layout:fixed;" border="0" cellspacing="0" class="formtable">
					<tr>
						<th style="width:100px;">大图：</th>
						<td colspan="3">	
							<div id="imgLarge" multi="false" required="required"  showWidth="40" showHeight="40" type="file" showImage="true" showBtn="true" bestSize="226*226" fileType="jpg,png" fileSize="2MB" buttonText="上传封面"></div>
                    		<label style="color: red;">*</label>
                    		<label>建议尺寸：宽度500，高度500，格式JPG,PNG</label>
                    	</td>
					</tr>
					<tr>
						<th style="width:100px;">中图：</th>
						<td colspan="3">
							<div id="imgMiddle" multi="false" required="required" showWidth="40" showHeight="40"  type="file" showImage="true" showBtn="true" bestSize="226*226" fileType="jpg,png" fileSize="2MB" buttonText="上传封面"></div>
							<label style="color: red;">*</label>
							<label>建议尺寸：宽度300，高度300，格式JPG,PNG</label>
						</td>
					</tr>
					<tr>
						<th style="width:100px;">小图：</th>
						<td colspan="3">
							<div id="imgSmall" multi="false" required="required"  showWidth="40" showHeight="40"  type="file" showImage="true" showBtn="true" bestSize="226*226" fileType="jpg,png" fileSize="2MB" buttonText="上传封面"></div>
							<label style="color: red;">*</label>
							<label>建议尺寸：宽度100，高度100，格式JPG,PNG</label>
						</td>
					</tr>
					<tr>
						<th style="width:100px;">灰显图：</th>
						<td colspan="3">
							<div id="imgSmallGray" multi="false" required="required" showWidth="40" showHeight="40"  type="file" showImage="true" showBtn="true" bestSize="226*226" fileType="jpg,png" fileSize="2MB" buttonText="上传封面"></div>
							<label style="color: red;">*</label>
							<label>建议尺寸：宽度100，高度100，格式JPG,PNG</label>
						</td>
					</tr>
					<tr>
						<th style="width:100px;">动画：</th>
						<td colspan="3">
							<div id="animation" multi="false" fileCountLimit="5" showWidth="40" showHeight="40" required="required" type="file" showImage="1" showBtn="true" bestSize="226*226" fileType="gif" fileSize="2MB" buttonText="上传组图"></div>
							<label style="color: red;">*</label>
							<label>建议:格式GIF</label>
						</td>
					</tr>
				</table>
			</div>
			<div title="图文详情" style="padding:20px; ">
				<table style="table-layout:fixed;" border="0" cellspacing="0"
					class="formtable">
					<tr>
						<th style="width:100px;">图文详情：</th>
						<td colspan="3"><textarea id="detail" name="detail"
								style="width:98%;height: 400px"></textarea></td>
					</tr>
				</table>
			</div>
		</div>
	</form>
</body>
</html>