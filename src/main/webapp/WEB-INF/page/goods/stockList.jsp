<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/static/admin/jsp/include.jsp"%>
<script type="text/javascript">
	var grid;
	
	var opFun = function(storeId) {
		var dialog = parent.sy.modalDialog({
			title : '采购商品列表',
			url : sy.contextPath + '/go.do?path=goods/storeStockList&storeId=' + storeId,
			width: 900,
			height: 600,
			buttons : [ {
				text : '关闭',
				handler : function() {
					dialog.dialog('destroy');
				}
			} ]
		});
	};
	
	$(function() {
		grid = $('#grid').datagrid({
			url : sy.contextPath + '/storeGoods/stockList',
			columns : [ [ 
				{
					width : $(this).width() * 0.3,
					title : '门店名称',
					field : 'storeName',
					align : 'center',
					formatter: function(v, r, i) {
						return UT.addTitle(v);
					}
				},{
					width : $(this).width() * 0.2,
					title : '已采购商品种类数量',
					field : 'total',
					align : 'center'
				},{
					width : $(this).width() * 0.2,
					title : '预警商品种类数量',
					field : 'warnNum',
					align : 'center',
					formatter: function(v, r, i) {
						if (v > 0) {
							return UT.addLabel(v, 'red');
						} else {
							return 0;
						}
					}
				},{
					width : $(this).width() * 0.2,
					title : '操作',
					field : 'op',
					align : 'center',
					formatter: function(v, r, i) {
						var html = "";
						html+='<a href="javascript:void(0);" onclick="opFun(\''+r.storeId+'\')" class="button button-info" title="查看">查看</a>';
						return html;
					}
				}
			] ]
		});
	});
</script>
</head>
<body>
	<div id="toolbar">
		<form id="searchForm">
		<div>
			<input type="text" class="easyui-textbox" name="name" style="width: 150px" prompt="门店名称"/>
			<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-search'" onclick="grid.datagrid('load',sy.serializeObject($('#searchForm')));">过滤</a> 
			<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-search-minus'" onclick="$('#searchForm input').val('');grid.datagrid('load',{});">重置过滤</a>
		</div>
		</form>
	</div>
	<table id="grid" data-options="fit:true,border:false"></table>
</body>
</html>