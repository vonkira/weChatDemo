<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/static/admin/jsp/include.jsp"%>
<script type="text/javascript">
	var grid;
	
	var addFun = function($dialog, $grid, $pjq) {
		var dialog = parent.sy.modalDialog({
			title : '添加商品',
			width : 800,
			height : 600,
			url : sy.contextPath + '/go?path=goods/realGoodsEdit',
			buttons : [ {
				text : '保存',
				handler : function() {
					dialog.find('iframe').get(0).contentWindow.submitForm(dialog, grid, parent.$);
				}
			} ]
		});
	};
	
	var editFun = function() {
		var rows = grid.datagrid('getSelections');
		if (rows.length != 1) {
			parent.$.messager.w('请选择一条记录进行编辑！');
			return;
		}
//		if (rows[0].id == -1){
//            parent.$.messager.w('系统金币是特殊物品，不能编辑！');
//            return;
//		}

		var dialog = parent.sy.modalDialog({
			title : '编辑商品',
			width : 800,
			height : 600,
			url : sy.contextPath + '/go?path=goods/realGoodsEdit&id=' + rows[0].id,
			buttons : [ {
				text : '保存',
				handler : function() {
					dialog.find('iframe').get(0).contentWindow.submitForm(dialog, grid, parent.$);
				}
			} ]
		});
	};

    var detailFun = function(id) {
        var dialog = parent.sy.modalDialog({
            title : '编辑详情',
            width : 800,
            height : 600,
            url : sy.contextPath + '/go?path=goods/detailEdit&id=' + id,
            buttons : [ {
                text : '保存',
                handler : function() {
                    dialog.find('iframe').get(0).contentWindow.submitForm(dialog, grid, parent.$);
                }
            } ]
        });
    };
	
	var delFun = function() {
		var rows = grid.datagrid('getSelections');
		if (rows.length == 0) {
			parent.$.messager.w('请选择需要删除的记录！');
			return;
		}
		parent.$.messager.confirm('询问', '您确定要删除此记录？', function(r) {
			if (r) {
				var ids = [];
				for ( var i = 0, l = rows.length; i < l; i++) {
					var r = rows[i];
					ids.push(r.id);
				}
				var id = ids.join(',');
				
				$.post(sy.contextPath + '/storeGoods/del', {
					id : id
				}, function(res) {
					rows.length = 0;//必须，否则有bug
					grid.datagrid('reload');
				}, 'json');
			}
		});
	};
	
	//查看商品详情
	var infoFun = function(cont) {
		layer.open({
			type: 2,
		    skin: 'layui-layer-rim', //加上边框
		    area: ['500px', '300px'], //宽高
		    content: sy.contextPath+'/detail?id='+cont+'&type=1' 
		}); 
	};
	
	//查看ID
	var idFun = function() {
		var rows = grid.datagrid('getSelections');
		if (rows.length != 1) {
			parent.$.messager.w('请选择一条记录进行复制！');
			return;
		}
		sy.getId(rows[0].id,'复制');
	};
	
	//上下架商品事件
	var onlineFun = function(type, id) {
		var title = "";
        if (type == 1) {
            title = "确定使用该商品吗？";
        } else {
            title = "确定关闭该商品吗？";
        }
        var data = {id: id, isSelect: type};
		parent.$.messager.confirm("询问", title, function(r) {
			if (r) {
				saveData(data);
			}
		});
	};
	
	//保存数据
	var saveData = function(data) {
		var url = sy.contextPath + '/storeGoods/save';
		$.post(url, data, function() {
			grid.datagrid('reload');
		}, 'json');
	};
	
	//调整库存
	var opFun = function(id) {
		var dialog = parent.sy.modalDialog({
			title : '调整商品库存',
			width : 400,
			height : 200,
			url : sy.contextPath + '/go?path=goods/goodsTotalEdit&id=' + id,
			buttons : [ {
				text : '确定',
				handler : function() {
					dialog.find('iframe').get(0).contentWindow.submitForm(dialog, grid, parent.$);
				}
			} ]
		});
	};

    //调整库存
    var logFun = function(id) {
        parent.sy.modalDialog({
            title : '物品日志',
            width : 1000,
            height : 600,
            url : sy.contextPath + '/go?path=goods/stockLog&id=' + id
        });
    };
	
	var importFun = function(id) {
		var dialog = parent.sy.modalDialog({
			title : '通过模板导入数据',
			width : 600,
			height : 300,
			url : sy.contextPath + '/go?path=goods/ticket/importData&id='+id+'&type=1',
			buttons : [ {
				text : '导入',
				handler : function() {
					dialog.find('iframe').get(0).contentWindow.submitForm(dialog, grid, parent.$);
				}
			} ]
		});
	};
    var setWarn = function(id) {
        layer.prompt({
            formType: 0,
            title: '请输入库存预警值：',
        }, function(value, index, elem) {
            if (UT.isInt(value)) {
                var url = sy.contextPath + '/goods/save';
                $.post(url, {id:id,warnNum:value}, function() {
                    grid.datagrid('reload');
                }, 'json');
            }else{
                $.messager.w("请输入整数！");
            }
            layer.close(index);
        });
    };
    function exportExcel(){
        var obj = sy.serializeObject($('#searchForm'));
        var cateId = obj.cateId?obj.cateId:"";
        var name = obj.name?obj.name:"";
        var code = obj.code?obj.code:"";
        location.href = sy.basePath + "goods/export?cateId="+cateId+"&code="+code+"&name="+name;
    }
	$(function() {
		$('#cateId').combobox({
			textField: 'name',
			valueField: 'code',
			url : sy.contextPath + '/goodsCate/list.do?flag=1',
			onChange: function(data) {
				grid.datagrid('load',sy.serializeObject($('#searchForm')));
			}
		});
		grid = $('#grid').datagrid({
			url : sy.contextPath + '/storeGoods/list?type=2',
//			singleSelect : false,
            fitColumns:$(this).width() <= 1310?false:true,
			frozenColumns : [ [
//			    {
//				width : '100',
//				checkbox:true,
//				field : 'id',
//				align : 'center'
//			},
				{
				width : 200,
				title : '商品名称',
				field : 'name',
				align : 'center',
				formatter: function(value, row, index) {
					return UT.addTitle(value);
				}
			}
//			,{
//				width : 80,
//				title : '商品编号',
//				field : 'code',
//				align : 'center'
//			}
			] ],
			columns : [ [
                {
                    width : 200,
                    title : '商家',
                    field : 'storeName',
                    align : 'center',
					hidden:sy.system == 'store',
                    formatter: function(value, row, index) {
                        return UT.addTitle(value);
                    }
                },
                {
                    width : 50,
                    title : '图片',
                    field : 'imgSmall',
                    align : 'center',
                    formatter: function(value, row, index) {
                        return po.showImg(value,20,20);
                    }
                },
                /*{
                    width : 100,
                    title : '商品分类',
                    field : 'cateName',
                    align : 'center',
                    formatter: function(value, row, index) {
                        return UT.addTitle(value);
                    }
                },{
                    width : $(this).width() * 0.1,
                    title : '封面',
                    field : 'img',
                    align : 'center',
                    formatter : function(value, row, index) {
                        return po.showImg(value,18,18);
                    }
                },{
                    width : $(this).width() * 0.1,
                    title : '组图',
                    field : 'imgs',
                    align : 'center',
                    formatter : function(value, row, index) {
                        return po.showImg(value,18,18);
                    }
                },{
                    width : 80,
                    title : '库存',
                    field : 'total',
                    align : 'center',
                    formatter : function(value, row, index) {
                        if(!row.warnNum || value>row.warnNum){
                            return UT.addLabel(value,"green");
                        }else{
                            return UT.addLabel(value,"red");
                        }
                    }
                },*/
//				{
//					width : $(this).width() * 0.1,
//					title : '已售',
//					field : 'sellCount',
//					align : 'center',
//					formatter:function(v,r,i){
//						if(r.sellBase) return r.sellBase+r.sellCount;
//						return v;
//					}
//				},
                {
                    width : 50,
                    title : '是否关闭',
                    field : 'isSelect',
                    align : 'center',
                    formatter : function(value, row, index) {
                        switch (value) {
                            case 0:
                                return '<a href="javascript:void(0);" onclick="onlineFun(1,\''+row.id+'\')" ><img src="static/admin/images/no.png" title="点击使用" > </img></a>';
                            case 1:
                                return '<a href="javascript:void(0);" onclick="onlineFun(0,\''+row.id+'\')" ><img src="static/admin/images/yes.png" title="点击关闭" > </img></a>';
                        }
                    }
                },
				{
					width : 60,
					title : '图文详情',
					field : 'detail',
					align : 'center',
					formatter:function(v,r,i){
					    var html = "";
					    html += "<a href=\"javascript:void(0)\" style=\"color:blue;\" onclick=\"infoFun(\'"+r.id+"\')\" >详情</a> ";
					    html += "<a href=\"javascript:void(0)\" style=\"color:orange;\" onclick=\"detailFun(\'"+r.id+"\')\" >编辑</a> ";
					    return html;
					}
				}
//				,{
//                    width : 50,
//                    title : '是否上架',
//                    field : 'isOnline',
//                    align : 'center',
//                    formatter : function(value, row, index) {
//                        switch (value) {
//                            case 0:
//                                return '<a href="javascript:void(0);" onclick="onlineFun(1,\''+row.id+'\')" ><img src="static/admin/images/no.png" title="点击上架" > </img></a>';
//                            case 1:
//                                return '<a href="javascript:void(0);" onclick="onlineFun(0,\''+row.id+'\')" ><img src="static/admin/images/yes.png" title="点击下架" > </img></a>';
//                        }
//                    }
//                }
                ,{
					width : 140,
					title : '创建时间',
					field : 'createTime',
					align : 'center'
				}
/*				,{
                    width : 250,
                    title : '操作',
                    field : 'op',
                    align : 'center',
                    formatter: function(value, row, index) {
                        var html = "";
//                        if (row.cateCode == 'j'){
//                            html+='<a href="javascript:void(0);" onclick="importFun(\''+row.id+'\')" class="button button-orange" title="导入券码">导入券码</a> ';
//                            html+='<a href="javascript:void(0);" onclick="ticketFun(\''+row.id+'\')" class="button button-info" title="查看券码">查看券码</a> ';
//						}
//                        html+='<a href="javascript:void(0);" onclick="opFun(\''+row.id+'\')" class="button button-orange" title="调整库存">调整库存</a>';
//                        html+='<a href="javascript:void(0);" onclick="logFun(\''+row.id+'\')" class="button button-blue" title="日志">日志</a>';
//                        html+=' <a href="javascript:void(0);" onclick="setWarn(\''+row.id+'\')" class="button button-info" title="设置预警值">设置预警值</a>';
                        return html;
                    }
                }*/
			] ]
		});
	});
</script>
</head>
<body>
	<div id="toolbar">
		<form id="searchForm">
		<div>
			<span>商品分类：</span>
			<input id="cateId" name="cateId" editable="false" style="width:150px;" panelHeight='150'
				data-options="valueField: 'id',textField: 'title'" />
			<span>商品名称：</span>
			<input type="text" class="easyui-textbox" name="name" style="width: 150px" prompt="商品名称"/>
			<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-search'" onclick="grid.datagrid('load',sy.serializeObject($('#searchForm')));">过滤</a>
			<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-search-minus'" onclick="$('#searchForm input').val('');grid.datagrid('load',{});">重置过滤</a>
		</div>
		</form>
		<div class="tbbutton">
			<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-plus-circle',plain:true" onclick="addFun();">添加</a>
			<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-pencil',plain:true" onclick="editFun();">修改</a>
			<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-trash',plain:true" onclick="delFun();">删除</a>
			<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-key',plain:true" onclick="idFun();">查看ID</a>
		</div>
	</div>
	<table id="grid" data-options="fit:true,border:false"></table>
</body>
</html>