<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/static/admin/jsp/include.jsp"%>
<script type="text/javascript">
	var id = "${id}";
	var submitForm = function($dialog, $grid, $pjq) {
		var obj=sy.serializeObject($('form'));
//		var img = $('#img').getFileId();
//		if (img == '') {
//			$pjq.messager.e('请上传封面图片 ');
//			return;
//		}
//		var imgs = $('#imgs').getFileId();
//		if (imgs == '') {
//			$pjq.messager.e('请上传组图 ');
//			return;
//		}
//		obj.img = img;
//		obj.imgs = imgs;
		if ($('form').form('validate')) {
			var url=sy.contextPath + '/store/saveBranch';
			if (!obj.lat || !obj.lng || !obj.address){
				$pjq.messager.w('请先打开地图选择一个地址');
				return;
			}
			$.post(url, obj, function(result) {
				if (result.code == 0) {
					$grid.datagrid('reload');
					$dialog.dialog('destroy');
				} else {
					$pjq.messager.e('添加失败,'+result.msg);
				}
			}, 'json');
		}
	};
	
	var openmap = function (ret) {
		if(ret){
			$("#lng").val(ret.lng);
			$("#lat").val(ret.lat);
			$("#address").textbox("setValue",ret.address);
		}
	};
	
	function showmap(){
		var lng =$("input[name='lng']").val();
		var lat=$("input[name='lat']").val();
		sy.amap(openmap,lat,lng);
	}
	
	$(function() {

        $('#tt').tabs({
            border:false,
            onSelect:function(title){
                if ('图片信息' == title){
                    sy.initFileUpload();
                }
            }
        });

        $('#containerId').combobox({
            url:sy.contextPath + '/goodsContainer/listByStore',
            textField:'name',
            valueField:'id',
            editable:false,
            required:true,
            formatter:function (row) {
                var opts = $(this).combobox('options');
                console.log(row);
                return row[opts.textField]+'<img width="18px" height="18px" src="download?id='+row.img+'" />';
            }
        });

        $('#isActivity').combobox({
            onChange:function (value) {
                if (value == 1){
                    $('.activityId').show();
                }else{
                    $('.activityId').hide();
                }
            }
        });

        $('#activityId').combobox({
            url:sy.contextPath + '/storeActivity/listByType?type=10',
            textField:'name',
            valueField:'id',
            editable:false,
            required:true
        });

        $('#cateCode').combobox({
            textField: 'name',
            valueField: 'code',
            url:sy.contextPath + '/storeCate/list'
        });
		if (id != '') {
			parent.$.messager.progress({
				text : '数据加载中....'
			});
			$.post(sy.contextPath + '/store/findById.do', {
				id : id
			}, function(result) {
				if (result) {
					$("#password").val('');
                    $('#imgs').setFileId(result.imgs, true, true, true);
					$('form').form('load', result);
                    $('#logo').setFileId(result.logo, false, true, true);
				}
				parent.$.messager.progress('close');
			}, 'json');
			$('#account').textbox({novalidate:true});
			$('#account').textbox({readonly:true});
			$('#code').textbox({novalidate:true});
			$('#code').textbox({readonly:true});
			$('#loginIs2').remove();
		}
	});
</script>
</head>
<body>
	<form id="form" method="post">
        <input name="id" type="hidden" />
        <input type="hidden" name="lng" id="lng"/>
        <input type="hidden" name="lat" id="lat"/>
		<div id="tt" class="easyui-tabs" style="width:100%;height:460px;">
			<div title="基础信息" style="padding:20px; ">
				<table style="table-layout:fixed;" border="0" cellspacing="0" class="formtable">
					<tr>
						<th style="width:100px;">账号：</th>
						<td>
							 <input id="account" name="account" class="easyui-textbox" style="width:90%" data-options="required:true,validType:['length[3,20]','account','unique[\'/store/isAccountExist\']']" missingMessage="请输入账号" prompt="请输入账号" />
						</td>
						<th style="width:100px;">名称：</th>
						<td>
							<input id="name" name="name" class="easyui-textbox" style="width:90%" data-options="required:true,validType:'length[0,50]'" missingMessage="请输入商家名称" prompt="请输入商家名称" />
						</td>
					</tr>
					<tr id="loginIs2">
						<th style="width:100px;">密码：</th>
						<td>
							<input id="password" name="password" type="password" class="easyui-textbox" style="width:90%" required="true" data-options="validType:'length[1,12]'" missingMessage="请输入密码" prompt="请输入密码" />
						</td>
						<th style="width:100px;">确认密码：</th>
						<td>
							<input type="password" class="easyui-textbox" style="width:90%" data-options="validType:'eqPwd[\'#password\']'" missingMessage="请输入确认密码" prompt="请输入确认密码" />
						</td>
					</tr>
					<tr>
						<th style="width:100px;">联系电话：</th>
						<td>
							<input id="tel" name="tel" class="easyui-textbox" style="width:90%" data-options="required:true,validType:'phone'" missingMessage="请输入联系电话" prompt="请输入联系电话" />
						</td>
					</tr>
					<tr>
						<th style="width:100px;">地址：</th>
						<td colspan="3">
							<input id="address" style="width:80%;" name="address" class="easyui-textbox"  missingMessage="请在地图中选择地址"  data-options="validType:'length[0,50]'" />
							<a id="btn" class="easyui-linkbutton" data-options="iconCls:'ext-icon-search'" onclick="showmap()">地图</a>
						</td>
					</tr>
				</table>
			</div>
			<div title="图片信息" style="padding:20px; ">
				<table style="table-layout:fixed;" border="0" cellspacing="0" class="formtable">
					<tr>
						<th style="width:100px;">LOGO：</th>
						<td colspan="3">
							<div id="logo" multi="false" required="required" cutWidth="100" cutHeight="100" type="file" showImage="true" showBtn="true" bestSize="226*226" fileType="jpg,png" fileSize="2MB" buttonText="上传LOGO"></div>
							<label style="color: red;">*</label>
							<label>建议尺寸：宽度500，高度500，格式JPG,PNG</label>
						</td>
					</tr>
					<tr>
						<th style="width:100px;">地图载体：</th>
						<td colspan="3">
							<select id="containerId" name="containerId" style="width: 90%"></select>
						</td>
					</tr>
					<tr>
						<th style="width:100px;">组图：</th>
						<td colspan="3">
							<div id="imgs" multi="true" fileCountLimit="5" showHeight="50" showWidth="50" required="required" type="file" showImage="true" showBtn="true" bestSize="226*226" fileType="jpg,png" fileSize="2MB" buttonText="上传组图"></div>
							<label style="color: red;">*</label>
							<label>建议尺寸：宽度640，高度254，格式JPG,PNG</label>
						</td>
					</tr>
				</table>
			</div>
			<div title="详细信息" style="padding:20px; ">
				<table style="table-layout:fixed;" border="0" cellspacing="0" class="formtable">
					<tr>
						<th style="width:100px;">商家简介：</th>
						<td colspan="3">
							<input id="info" name="info" class="easyui-textbox" data-options="validType:'length[0,500]'" style="width:90%" missingMessage="请输入商家简介" prompt="请输入商家简介" />
						</td>
					</tr>
					<tr>
						<th style="width:100px;">商家活动：</th>
						<td colspan="3">
							<input id="actUrl" name="actUrl" class="easyui-textbox" data-options="validType:['url','length[0,300]']" style="width:90%" missingMessage="请输入商家活动URL" prompt="请输入商家活动URL" />
						</td>
					</tr>
					<tr>
						<th style="width:100px;">商家全景：</th>
						<td colspan="3">
							<input id="vrUrl" name="vrUrl" data-options="required:true,validType:['url','length[0,300]']" class="easyui-textbox" style="width:90%" missingMessage="请输入商家全景URL" prompt="请输入商家全景URL" />
						</td>
					</tr>
					<tr>
						<th style="width:100px;">商家直播：</th>
						<td colspan="3">
							<input id="liveUrl" name="liveUrl" data-options="required:true,validType:['url','length[0,300]']" class="easyui-textbox" style="width:90%" missingMessage="请输入商家直播URL" prompt="请输入商家直播URL" />
						</td>
					</tr>
					<tr>
						<th style="width:100px;">活动信息：</th>
						<td colspan="3">
							<input id="activityInfo" name="activityInfo" data-options="validType:'length[0,500]'" class="easyui-textbox" style="width:90%" missingMessage="请输入商家活动信息" prompt="请输入商家活动信息" />
						</td>
					</tr>
					<tr>
						<th style="width:100px;">绑定活动：</th>
						<td>
							<select class="easyui-combobox" name="isActivity" id="isActivity" editable="false" style="width: 90%" panelHeight="auto">
								<option value="0">否</option>
								<option value="1" selected>是</option>
							</select>
						</td>
					</tr>
					<tr>
						<th class="activityId">选择活动</th>
						<td class="activityId">
							<select name="activityId" id="activityId" editable="false" style="width: 90%" panelHeight="150px">
							</select>
						</td>
					</tr>
				</table>
			</div>
        </div>
	</form>
</body>
</html>