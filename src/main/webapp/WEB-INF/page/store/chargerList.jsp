<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/static/admin/jsp/include.jsp"%>
<script type="text/javascript">
	var grid;
	
	var addFun = function($dialog, $grid, $pjq) {
		var dialog = parent.sy.modalDialog({
			title : '添加门店负责人',
			width : 600,
			height : 400,
			url : sy.contextPath + '/go?path=store/chargerEdit',
			buttons : [ {
				text : '保存',
				handler : function() {
					dialog.find('iframe').get(0).contentWindow.submitForm(dialog, grid, parent.$);
				}
			} ]
		});
	};
	
	var editFun = function() {
		var rows = grid.datagrid('getSelections');
		if (rows.length != 1) {
			parent.$.messager.w('请选择一条记录进行编辑！');
			return;
		}
		var dialog = parent.sy.modalDialog({
			title : '编辑信息',
			width : 600,
			height : 400,
			url : sy.contextPath + '/go?path=store/chargerEdit&id=' + rows[0].id,
			buttons : [ {
				text : '保存',
				handler : function() {
					dialog.find('iframe').get(0).contentWindow.submitForm(dialog, grid, parent.$);
				}
			} ]
		});
	};
	
	var delFun = function() {
		var rows = grid.datagrid('getSelections');
		if (rows.length == 0) {
			parent.$.messager.w('请选择需要删除的记录！');
			return;
		}
		parent.$.messager.confirm('询问', '您确定要删除此记录？', function(r) {
			if (r) {
				var ids = [];
				for ( var i = 0, l = rows.length; i < l; i++) {
					var r = rows[i];
					ids.push(r.id);
				}
				var id = ids.join(',');
				
				$.post(sy.contextPath + '/storeCharger/del', {
					id : id
				}, function(res) {
					if (res.code == 0) {
						rows.length = 0;//必须，否则有bug
						grid.datagrid('reload');
					} else {
						parent.$.messager.w(res.msg);
					}
				}, 'json');
			}
		});
	};
	
	$(function() {
		grid = $('#grid').datagrid({
			url : sy.contextPath + '/storeCharger/list',
			singleSelect: false,
            frozenColumns: [[{
                width: '100',
                checkbox: true,
                field: 'id',
                align: 'center'
            }]],
            columns: [[
                {
                    width: $(this).width() * 0.1,
                    title: '姓名',
                    field: 'name',
                    align: 'center',
                    formatter: function (value, row, index) {
                        return UT.addTitle(value);
                    }
                },
                {
                    width: $(this).width() * 0.1,
                    title: '电话',
                    field: 'phone',
                    align: 'center',
                    formatter: function (value, row, index) {
                        return UT.addTitle(value);
                    }
                },
                {
                    width: $(this).width() * 0.15,
                    title: '身份证',
                    field: 'idCard',
                    align: 'center',
                    formatter: function (value, row, index) {
                        return UT.addTitle(value);
                    }
                },
                {
                    width: $(this).width() * 0.15,
                    title: '地址',
                    field: 'address',
                    align: 'center',
                    formatter: function (value, row, index) {
                        return UT.addTitle(value);
                    }
                }, {
                    width: $(this).width() * 0.1,
                    title: '性别',
                    field: 'sex',
                    align: 'center',
                    formatter: function (value, row, index) {
                        switch (value) {
                            case 0:
                                return UT.addLabel("女", "red");
                            case 1:
                                return UT.addLabel("男", "blue");
                        }
                    }
                }, {
                    width: $(this).width() * 0.1,
                    title: '状态',
                    field: 'state',
                    align: 'center',
                    formatter: function (value, row, index) {
                        switch (value) {
                            case 0:
                                return UT.addLabel("离职", "grey");
                            case 1:
                                return UT.addLabel("在职", "blue");
                        }
                    }
                },{
                    width: $(this).width() * 0.1,
                    title: '关联门店',
                    field: 'storeName',
                    align: 'center',
                    formatter: function (value, row, index) {
                        return UT.addTitle(value);
                    }
                },
                {
                    width: $(this).width() * 0.2,
                    title: '创建时间',
                    field: 'createTime',
                    align: 'center'
                }
            ]]
		});
	});
</script>
</head>
<body>
	<div id="toolbar">
		<form id="searchForm">
		<div>
			<input type="text" class="easyui-textbox" name="name" style="width: 150px" prompt="名称/联系电话"/>
			<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-search'" onclick="grid.datagrid('load',sy.serializeObject($('#searchForm')));">过滤</a> 
			<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-search-minus'" onclick="$('#searchForm input').val('');grid.datagrid('load',{});">重置过滤</a>
		</div>
		</form>
		<div class="tbbutton">
			<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-plus-circle',plain:true" onclick="addFun();">添加</a>
			<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-pencil',plain:true" onclick="editFun();">修改</a>
			<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-trash',plain:true" onclick="delFun();">删除</a>
		</div>
	</div>
	<table id="grid" data-options="fit:true,border:false"></table>
</body>
</html>