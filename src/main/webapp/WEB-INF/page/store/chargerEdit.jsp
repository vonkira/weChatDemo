<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/static/admin/jsp/include.jsp"%>
<script type="text/javascript">
	var id = "${id}";
	var submitForm = function($dialog, $grid, $pjq) {
		var obj=sy.serializeObject($('form'));
		if ($('form').form('validate')) {
			var url=sy.contextPath + '/storeCharger/save';
			$.post(url, obj, function(result) {
				if (result.code == 0) {
					$grid.datagrid('reload');
					$dialog.dialog('destroy');
				} else {
					$pjq.messager.e('添加失败,'+result.msg);
				}
			}, 'json');
		}
	};
	
	$(function() {
		if (id != '') {
			parent.$.messager.progress({
				text : '数据加载中....'
			});
			$.post(sy.contextPath + '/storeCharger/findById', {
				id : id
			}, function(result) {
				if (result) {
					$('form').form('load', result);
				}
				parent.$.messager.progress('close');
			}, 'json');
		}else{
		}
	});
</script>
</head>
<body>
	<form id="form" method="post">
        <input name="id" type="hidden" />
        <div style="padding:15px;font-size: 12px">
            <table style="table-layout:fixed;" border="0" cellspacing="0" class="formtable">
		    	<tr>
		    		<th style="width:100px;">姓名：</th>
		    		<td>
		    			<input class="easyui-textbox" required="required" style="width: 90%" data-options="validType:['length[0,20]']" name="name" type="text" missingMessage="请输入姓名" prompt="请输入姓名" id="name"  />
		    		</td>
		    	</tr>
		    	<tr>
					<th style="width:100px;">电话：</th>
					<td>
						<input class="easyui-textbox" style="width: 90%" data-options="validType:['mobile']" name="phone" type="text" missingMessage="请输入电话" prompt="请输入电话" id="phone"  />
					</td>
				</tr>
				<tr>
					<th style="width:100px;">身份证：</th>
					<td>
						<input class="easyui-textbox" style="width: 90%" data-options="validType:['length[18,18]']" name="idCard" type="text" missingMessage="请输入身份证" prompt="请输入身份证" id="idCard"  />
					</td>
				</tr>
				<tr>
					<th>性别:</th>
					<td>
						<select id="sex" name="sex" style="width: 90%" class="easyui-combobox" missingMessage="请选择性别" editable="false" panelHeight='auto'
								data-options="required:true,valueField: 'code',textField: 'name',data: [{name: '男',code: '1',selected:true},{name: '女',code: '0'}]"/>
					</td>
				</tr>
				<tr>
					<th style="width:100px;">地址：</th>
					<td>
						<input class="easyui-textbox" style="width: 90%" data-options="validType:['length[0,50]']" name="address" type="text" missingMessage="请输入地址" prompt="请输入地址" id="address"  />
					</td>
				</tr>
		    	<tr>
            		<th>状态:</th>
		    		<td>
		    			<select id="state" name="state" style="width: 90%" class="easyui-combobox" missingMessage="请选择状态" editable="false" panelHeight='auto'
                    	 	data-options="required:true,valueField: 'code',textField: 'name',data: [{name: '在职',code: '1',selected:true},{name: '离职',code: '0'}]"/>
		    		</td>
				</tr>
            </table>
        </div>
	</form>
</body>
</html>