<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/static/admin/jsp/include.jsp"%>
<script type="text/javascript">
	var grid;
	
	var opFun = function(name, id) {
		var dialog = parent.sy.modalDialog({
			title : '门店"' + name + '"--员工列表',
			width : 800,
			height : 600,
			url : sy.contextPath + '/go?path=store/workerList&storeId=' + id,
			buttons : [ {
				text : '关闭',
				handler : function() {
					dialog.dialog('destroy');
				}
			} ]
		});
	};
	
	$(function() {
		grid = $('#grid').datagrid({
			url : sy.contextPath + '/store/myStoreList',
			frozenColumns : [ [ {
				width : '100',
				title : '门店账号',
				field : 'account',
				align : 'center',
				formatter: function(value, row, index) {
					return UT.addTitle(value);
				}
			},{
				width : '150',
				title : '门店名称',
				field : 'name',
				align : 'center',
				formatter: function(value, row, index) {
					return UT.addTitle(value);
				}
			}
			] ],
			columns : [ [ 
				{
					width : '100',
					title : '门店别名',
					field : 'nickName',
					align : 'center',
					formatter: function(value, row, index) {
						return UT.addTitle(value);
					}
				},{
					width : '100',
					title : '门店类型',
					field : 'type',
					align : 'center',
					formatter: function(v, r, i) {
						if (v == 1) {
							return "保养店";
						} else {
							return "维修店";
						}
					}
				},{
					width : '100',
					title : '联系电话',
					field : 'phone',
					align : 'center'
				},{
					width : '100',
					title : '门店状态',
					field : 'state',
					align : 'center',
					formatter: function(v, r, i) {
						switch(v) {
						case 1:
							return "空闲";
						case 2:
							return "繁忙";
						case 3:
							return "歇业";
						default:
							return "";
						}
					}
				},{
					width : '150',
					title : '创建时间',
					field : 'createTime',
					align : 'center'
				},{
					width : '150',
					title : '操作',
					field : 'op',
					align : 'center',
					formatter:function(value, row, index){
						var html = "";
						html+='<a href="javascript:void(0);" onclick="opFun(\''+row.name+'\',\''+row.id+'\')" class="button button-info" title="员工管理">员工管理</a>';
						return html;
					}
				}
			] ]
		});
		
		$('#type').combobox({
			textField: 'text',
			valueField: 'value',
			data:[{text:'全部',value:0,selected:true},{text:'保养店',value:1},{text:'维修店',value:2}],
			onChange: function(data) {
				grid.datagrid('load',sy.serializeObject($('#searchForm')));
			}
		});
	});
</script>
</head>
<body>
	<div id="toolbar">
		<form id="searchForm">
		<div>
			<span>门店类型：</span>
			<input id="type" name="type" editable="false" style="width:150px;" panelHeight='150'
				data-options="valueField: 'value',textField: 'text'" />
			<input type="text" class="easyui-textbox" name="name" style="width: 150px" prompt="账号/名称/别名"/>
			<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-search'" onclick="grid.datagrid('load',sy.serializeObject($('#searchForm')));">过滤</a> 
			<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-search-minus'" onclick="$('#searchForm input').val('');grid.datagrid('load',{});">重置过滤</a>
		</div>
		</form>
	</div>
	<table id="grid" data-options="fit:true,border:false"></table>
</body>
</html>