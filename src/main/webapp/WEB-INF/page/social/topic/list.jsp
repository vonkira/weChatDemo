<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/static/admin/jsp/include.jsp"%>
<script type="text/javascript">
	var grid;
	var index;
	
	var addFun = function($dialog, $grid, $pjq) {
		var dialog = parent.sy.modalDialog({
			title : '添加新用户',
			width : 500,
			height : 400,
			url : sy.contextPath + '/go?path=user/userEdit',
			buttons : [ {
				text : '保存',
				handler : function() {
					dialog.find('iframe').get(0).contentWindow.submitForm(dialog, grid, parent.$);
				}
			} ]
		});
	};
	
	var editFun = function($dialog, $grid, $pjq) {
		var rows = grid.datagrid('getSelections');
		if (rows.length != 1) {
			parent.$.messager.w('请选择一条记录进行编辑！');
			return;
		}
		var dialog = parent.sy.modalDialog({
			title : '修改资料',
			width : 600,
			height : 400,
			url : sy.contextPath + '/go?path=user/userEdit&id=' + rows[0].id,
			buttons : [ {
				text : '保存',
				handler : function() {
					dialog.find('iframe').get(0).contentWindow.submitForm(dialog, grid, parent.$);
				}
			} ]
		});
	};

    var delFun = function() {
        var rows = grid.datagrid('getSelections');
        if (rows.length == 0) {
            parent.$.messager.w('请选择需要删除的记录！');
            return;
        }
        parent.$.messager.confirm('询问', '您确定要删除此记录？', function(r) {
            if (r) {
                var ids = [];
                for ( var i = 0, l = rows.length; i < l; i++) {
                    var r = rows[i];
                    ids.push(r.id);
                }
                var id = ids.join(',');

                $.post(sy.contextPath + '/userTopic/del', {
                    id : id
                }, function(res) {
                    if (res.code == 0) {
                        rows.length = 0;//必须，否则有bug
                        grid.datagrid('reload');
                    } else {
                        parent.$.messager.w(res.msg);
                    }
                }, 'json');
            }
        });
    };
	
	var creditsFun = function() {
		var rows = grid.datagrid('getSelections');
		if (rows.length != 1) {
			parent.$.messager.w('请选择一条记录进行查看！');
			return;
		}
		var nickName = "";
		if (rows[0].nickName != null && rows[0].nickName != "") {
			nickName = rows[0].nickName;
		}
		var url = sy.contextPath + '/go.do?path=user/userCreditLogList&id=' + rows[0].id;
		var dialog = parent.sy.modalDialog({
			title : '积分日志——' + nickName,
			width:sy.width-150,
			height:sy.height-100,
			//width : 800,
		//	height : 620,
			url : url
		});
		index = dialog.selector.substring(12);
		console.log(dialog.selector.substring(12));
	};
	
	function resize(width,height){
		if (index){
			console.log(width);
			console.log(height);
			layer.style(index, {
				width: (width-150)+'px',
				height: (height-100)+'px',
				}); 
		}
		
	}
	
	var pwdFun = function(id) {
		/* var rows = grid.datagrid('getSelections');
		if (rows.length != 1) {
			parent.$.messager.alert('请选择一条记录进行编辑！');
			return;
		} */
		var dialog = parent.sy.modalDialog({
			title : '修改密码',
			width: 400,
			height : 220,
			url : sy.contextPath + '/go.do?path=common/pwdEdit&type=1&id=' + id,
			buttons : [ {
				text : '编辑',
				handler : function() {
					dialog.find('iframe').get(0).contentWindow.submitForm(dialog, grid, parent.$);
				}
			} ]
		});
	};
	
	//用户车辆信息
	var showFun = function(id) {
		/* var rows = grid.datagrid('getSelections');
		if (rows.length != 1) {
			parent.$.messager.w('请选择一条记录进行查看！');
			return;
		} */
		var dialog = parent.sy.modalDialog({
			title : '用户车辆信息列表',
			url : sy.contextPath + '/go.do?path=user/carList&id=' + id,
			width: 900,
			height: 500,
			buttons : [ {
				text : '关闭',
				handler : function() {
					dialog.dialog('destroy');
				}
			} ]
		});
	};
	
	//用户服务日志
	var commentList = function(id) {
		parent.sy.modalDialog({
			title : '评论',
			url : sy.contextPath + '/go.do?path=social/topic/comment/list&id=' + id,
			width: 900,
			height: 600
		});
	};

    //用户服务日志
    var coinLogFun = function(id) {
        var dialog = parent.sy.modalDialog({
            title : '用户金币日志',
            url : sy.contextPath + '/go.do?path=user/coinLogList&id=' + id,
            width: 900,
            height: 600
        });
    };

	var goodsFun = function (id) {
        parent.sy.modalDialog({
            title : '用户仓库',
            url : sy.contextPath + '/go.do?path=user/goodsList&id=' + id,
            width: 900,
            height: 600,
			onClose:function () {
                grid.datagrid('reload');
            }
        });
    }
	
	//查看ID
	var idFun = function() {
		var rows = grid.datagrid('getSelections');
		if (rows.length != 1) {
			parent.$.messager.w('请选择一条记录进行复制！');
			return;
		}
		sy.getId(rows[0].id,'复制');
	};
	
	$(function() {
		grid = $('#grid').datagrid({
			url : sy.contextPath + '/userTopic/list',
			columns : [ [ {
				width : 80,
				title : '账号',
				field : 'account',
				align : 'center',
				formatter: function(v, r, i) {
					return UT.addTitle(v);
				}
			},{
				width : 80,
				title : '昵称',
				field : 'nickName',
				align : 'center',
				formatter: function(v, r, i) {
					return UT.addTitle(v);
				}
			},
			{
				width : 30,
				title : '视频',
				field : 'media',
				align : 'center',
				formatter: function(v, r, i) {
					if (v) {
                        return "<a href='javascript:;' onclick='parent.sy.showVideo(\""+v+"\",\""+r.nickName+"\")'>"+"<img src='${path}/download?w=20&h=20&id="+(v+'_cut')+"'/>"+"</a>";
					}
				}
			},{
                    width : 80,
                    title : '内容',
                    field : 'content',
                    align : 'center',
                    formatter: function(v, r, i) {
                        return UT.addTitle(v);
                    }
                },
			{
				width : 40,
				title : '是否关闭',
				field : 'isDel',
				align : 'center',
				formatter : function(value, row, index) {
					switch (value) {
					case 1:
						return '<a href="javascript:void(0);" onclick="forbidden(0,\'' + row.id + '\')" ><img src="static/admin/images/no.png" title="点击启用" > </img></a>';
					case 0:
						return '<a href="javascript:void(0);" onclick="forbidden(1,\'' + row.id + '\')" ><img src="static/admin/images/yes.png" title="点击禁用" > </img></a>';
					}
				}
			}, {
                    width : 30,
                    title : '评论',
                    field : 'commentCnt',
                    align : 'center',
					formatter:function (v,r) {
                        if (v > 0){
                            return '<a href="javascript:void(0);" onclick="commentList(\'' + r.id + '\')" >'+v+'</a>';
						}else{
                            return '0';
						}
                    }
                }, {
                    width : 30,
                    title : '赞',
                    field : 'praiseCnt',
                    align : 'center'
                }, {
					width : 150,
					title : '发布时间',
					field : 'createTime',
					align : 'center'
			}
			] ]
		});
		

		$('#type').combobox({   
			onSelect: function(rec){   
				grid.datagrid('load',sy.serializeObject($('#searchForm')));
	        } 
		});
	});

	function forbidden(state, id) {
		if (state == 0) {

			parent.$.messager.confirm('询问', "确定恢复吗？", function(r) {
				if (r) {

					var data = {
						id : id,
                        isDel : state
					};
					SaveData(data);
				}
			});
		} else if (state == 1) {
			parent.$.messager.confirm('询问', "确定删除吗？", function(r) {
				if (r) {
					var data = {
						id : id,
                        isDel : state
					};
					SaveData(data);
				}
			});
		}
	}
	function bannedPost(state, id) {
		if (state == 1) {
			parent.$.messager.confirm('询问', "确定将此账号禁言吗？", function(r) {
				if (r) {

					var data = {
						id : id,
						bannedPost : 1
					};
					SaveData(data);
				}
			});
		} else if (state == 0) {
			parent.$.messager.confirm('询问', "确定将此账号取消禁言吗？", function(r) {
				if (r) {
					var data = {
						id : id,
						bannedPost : 0
					};
					SaveData(data);
				}
			});
		}
	}
	function SaveData(data) {
		var url = sy.contextPath + '/userTopic/save';
		$.post(url, data, function() {
			grid.datagrid('reload');
		}, 'json');
	}
</script>
</head>
<body>
	<div id="toolbar">
		<form id="searchForm">
		<div>
			<input type="text" class="easyui-textbox" name="name" style="width: 150px" prompt="手机号"/>
			<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-search'" onclick="grid.datagrid('load',sy.serializeObject($('#searchForm')));">过滤</a> 
			<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-search-minus'" onclick="$('#searchForm input').val('');grid.datagrid('load',{});">重置过滤</a>
		</div>
		</form>
		<div class="tbbutton">
			<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-trash',plain:true" onclick="delFun();">删除</a>
		</div>
	</div>
	<table id="grid" data-options="fit:true,border:false"></table>
</body>
</html>