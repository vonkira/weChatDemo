<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<%@ include file="/static/admin/jsp/include.jsp"%>
	<script type="text/javascript">
        var id = "${id}";

        var submitForm = function($dialog, $grid, $pjq) {
            var obj = sy.serializeObject($('form'));
            if ($('form').form('validate')) {
                $.ajax({
                    url:sy.contextPath + '/storeActivity/saveGame',
                    data:obj,
                    type:'post',
                    dataType:'json',
                    success:function(result) {
                        if (result.code == 0) {
                            $grid.datagrid('reload');
                            $dialog.dialog('destroy');
                        } else {
                            $pjq.messager.e('添加失败 ' + result.msg);
                        }
                    }
                });
            }
        };

        function showamap() {
            var lat = $('#lat').textbox('getValue');
            var lng = $('#lng').textbox('getValue');
            sy.amap(retMap, lat, lng);
        }

        function retMap(ret) {
            if (ret.lat != "") {
                $('#lat').textbox('setValue',ret.lat);
                $('#lng').textbox('setValue',ret.lng);
            }
        }

        $(function() {

            $('#triggerId').combobox({
                url:sy.contextPath + '/goodsTrigger/listByStore',
                textField:'name',
                valueField:'id',
				required:true,
                filter: function(q, row){
                    var opts = $(this).combobox('options');
                    return row[opts.textField].toLowerCase().indexOf(q.toLowerCase()) != -1;
                }
            });

            $('#gameTemplateId').combobox({
                url:sy.contextPath + '/gameTemplate/listAll?type=2',
                textField:'title',
                valueField:'id',
                required:true,
                filter: function(q, row){
                    var opts = $(this).combobox('options');
                    return row[opts.textField].toLowerCase().indexOf(q.toLowerCase()) != -1;
                }
            })

            if (id != '') {
                parent.$.messager.progress();
                $.post(sy.contextPath + '/storeActivity/findById', {
                    id : id
                }, function(result) {
                    if (result) {
                        $('form').form('load', result);
                    }
                    parent.$.messager.progress('close');
                }, 'json');
                $('#code').textbox({novalidate:true});
                $('#code').textbox({readonly:true});
            }
        });
	</script>
</head>
<body>
<form id="form" method="post">
	<input name="id" type="hidden" />
	<div style="padding:15px;font-size: 12px">
		<table style="table-layout:fixed;margin-top: 10px;margin-bottom: 15px" border="0" cellspacing="0" class="formtable">
			<tr>
				<th style="width:100px;">名称：</th>
				<td>
					<input class="easyui-textbox" style="width: 90%" data-options="required:true,validType:['length[0,20]']" name="name" missingMessage="请输入名称" prompt="请输入名称" id="name"  />
				</td>
				<th style="width:100px;">触发器：</th>
				<td>
					<select id="triggerId" name="triggerId" style="width: 90%"></select>
				</td>
			</tr>
			<tr>
				<th style="width:100px;">编号(唯一)：</th>
				<td>
					<input class="easyui-textbox" style="width: 90%" data-options="required:true,validType:['length[0,50]','unique[\'/storeActivity/isCodeExist\']']" name="code" missingMessage="请输入编号" prompt="请输入编号" id="code"  />
				</td>
				<th style="width:100px;">游戏模板：</th>
				<td>
					<input style="width: 90%" data-options="required:true" name="gameTemplateId" missingMessage="请选择模板" prompt="请选择模板" id="gameTemplateId"  />
				</td>
			</tr>
			<tr>
				<th style="width:100px;">开始时间：</th>
				<td><input id="beginTime" name="beginTime"
						   style="width: 90%" class="easyui-datetimebox" style="width: 90%"
						   data-options="required:true"
						   missingMessage="请输入开始时间" prompt="请输入开始时间" /></td>
				<th style="width:100px;">结束时间：</th>
				<td><input id="endTime" name="endTime"
						   style="width: 90%" class="easyui-datetimebox" style="width: 90%"
						   data-options="required:true"
						   missingMessage="请输入结束时间" prompt="请输入结束时间" /></td>
			</tr>
		</table>
	</div>
</form>
</body>
</html>