<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<%@ include file="/static/admin/jsp/include.jsp"%>
	<script type="text/javascript">
        var id = "${id}";

        var submitForm = function($dialog, $grid, $pjq) {
            var obj = sy.serializeObject($('form'));
            if ($('form').form('validate')) {
                $.ajax({
                    url:sy.contextPath + '/gameTemplate/save',
                    data:obj,
                    type:'post',
                    dataType:'json',
                    success:function(result) {
                        if (result.code == 0) {
                            $grid.datagrid('reload');
                            $dialog.dialog('destroy');
                        } else {
                            $pjq.messager.e('添加失败 ' + result.msg);
                        }
                    }
                });
            }
        };

        function showamap() {
            var lat = $('#lat').textbox('getValue');
            var lng = $('#lng').textbox('getValue');
            sy.amap(retMap, lat, lng);
        }

        function retMap(ret) {
            if (ret.lat != "") {
                $('#lat').textbox('setValue',ret.lat);
                $('#lng').textbox('setValue',ret.lng);
            }
        }

        $(function() {

            if (id != '') {
                parent.$.messager.progress();
                $.post(sy.contextPath + '/gameTemplate/findById', {
                    id : id
                }, function(result) {
                    if (result) {
                        $('form').form('load', result);
                    }
                    parent.$.messager.progress('close');
                }, 'json');
            }
        });
	</script>
</head>
<body>
<form id="form" method="post">
	<input name="id" type="hidden" />
	<div style="padding:15px;font-size: 12px">
		<table style="table-layout:fixed;margin-top: 10px;margin-bottom: 15px" border="0" cellspacing="0" class="formtable">
			<tr>
				<th style="width:100px;">类型：</th>
				<td>
					<select class="easyui-combobox" style="width: 90%" data-options="required:true,editable:false" name="type" missingMessage="请选择类型" prompt="请选择类型" id="type"  >
						<option value="1">抽奖</option>
						<option value="2">游戏</option>
					</select>
				</td>
			</tr>
			<tr>
				<th style="width:100px;">模板名称：</th>
				<td>
					<input class="easyui-textbox" style="width: 90%" data-options="required:true,validType:['length[0,30]']" name="title" missingMessage="请输入模板名称" prompt="请输入模板名称" id="title"  />
				</td>
			</tr>
			<tr>
				<th style="width:100px;">模板地址：</th>
				<td>
					<input class="easyui-textbox" style="width: 90%" data-options="required:true,validType:['url']" name="url" missingMessage="请输入模板地址" prompt="请输入模板地址" id="url"  />
				</td>
			</tr>
		</table>
	</div>
</form>
</body>
</html>