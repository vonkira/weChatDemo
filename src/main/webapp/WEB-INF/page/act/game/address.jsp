<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<%@ include file="/static/admin/jsp/include.jsp"%>
	<script type="text/javascript">
        var _code = "${code}";
		var id = '${id}';
        $(function() {
            if (id&&id!=0){
                $.post(sy.contextPath + '/gameTemplate/findById', {
                    id : id
                }, function(result) {
                    if (result) {
                        var v = result.url;
                        var add = v.indexOf('?')!=-1?'&':'?';
                        var url = v+add+"activityCode="+_code;
                        var html = '<a target="_blank" href="'+url+'">'+url+'</a>'
                        $('#url').html(html);
                    }
                }, 'json');
			}else{
                var html = UT.addSpanColor('尚未绑定h5游戏模板,请先绑定','red');
                $('#url').html(html);
			}
        });
	</script>
</head>
<body>
<form id="form" method="post">
	<input name="id" type="hidden" />
	<div style="padding:15px;font-size: 12px">
		<table style="table-layout:fixed;margin-top: 10px;margin-bottom: 15px" border="0" cellspacing="0" class="formtable">
			<tr>
				<th style="width:100px;">游戏部署地址：</th>
				<td id="url">
				</td>
			</tr>
		</table>
	</div>
</form>
</body>
</html>