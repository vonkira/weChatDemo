<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<%@ include file="/static/admin/jsp/include.jsp"%>
	<script type="text/javascript">
        var id = "${id}";
		var _triggerId = '${triggerId}';

        var submitForm = function($dialog, $grid, $pjq) {
            var obj = sy.serializeObject($('form'));
            if (!id){
                obj.goodsTriggerId = _triggerId;
			}
			if(obj.scoreBegin || obj.scoreEnd){
                if (!obj.scoreBegin || !obj.scoreEnd){
                    $pjq.messager.e('积分范围必须两个都填!');
                    return;
				}
			}
            if ($('form').form('validate')) {
                $.ajax({
                    url:sy.contextPath + '/goodsTriggerGoods/save',
                    data:obj,
                    type:'post',
                    dataType:'json',
                    success:function(result) {
                        if (result.code == 0) {
                            $grid.datagrid('reload');
                            $dialog.dialog('destroy');
                        } else {
                            $pjq.messager.e('添加失败 ' + result.msg);
                        }
                    }
                });
            }
        };

        $(function() {

//            if (sy.system == 'store'){
//                $('.store').hide();
//			}

            $('#isTime').combobox({
                onChange:function (value) {
                    if (value == 1){
                        $('.time').show();
                        $('#begin').timespinner('enableValidation');
                        $('#end').timespinner('enableValidation');
                    }else{
                        $('.time').hide();
                        $('#begin').timespinner('disableValidation');
                        $('#end').timespinner('disableValidation');
                    }
                }
            })

            $('#goodsId').combobox({
                url:sy.contextPath + '/storeGoods/listByStore',
                textField:'name',
                valueField:'id',
                panelMaxHeight:'150',
                filter: function(q, row){
                    var opts = $(this).combobox('options');
                    return row[opts.textField].toLowerCase().indexOf(q.toLowerCase()) != -1;
                }
            });

            if (id != '') {
                parent.$.messager.progress();
                $.post(sy.contextPath + '/goodsTriggerGoods/findById', {
                    id : id
                }, function(result) {
                    if (result) {
//                        if (sy.system == 'store'){
//                            loadGoods(sy.id,function () {
//                                $('#goodsId').combobox("setValue",result.goodsTriggerGoods.goodsId);
//                            });
//                        }else{
//                            loadStore(result.storeGoods.storeId,function () {
//                                $('#goodsId').combobox("setValue",result.goodsTriggerGoods.goodsId);
//                            });
//                        }
						if (result.goodsTriggerGoods.beginTime){
                            result.goodsTriggerGoods.begin = result.goodsTriggerGoods.beginTime.substr(10);
						}
                        if (result.goodsTriggerGoods.endTime){
                            result.goodsTriggerGoods.end = result.goodsTriggerGoods.endTime.substr(10);
						}

                        $('form').form('load', result.goodsTriggerGoods);

                    }
                    parent.$.messager.progress('close');
                }, 'json');
            }else{
                if (sy.system == 'store'){
                    loadGoods(sy.id);
                }else{
                    loadStore();
				}

			}
        });

        function loadStore(store,func) {
            $('#storeId').combobox({
                url:sy.contextPath + '/store/listStore',
                textField:'name',
                valueField:'id',
                editable:false,
                panelMaxHeight:'150',
                onLoadSuccess:function () {
                    $('#storeId').combobox("select",store);
                },
                onSelect:function (v) {
                    loadGoods(v.id,func);
                },
				onChange:function (v) {
                    if (v){
                        loadGoods(v);
					}

                }
            });
        }
        function loadGoods(v,func) {
            $('#goodsId').combobox({
                url:sy.contextPath + '/storeGoods/listByStore?storeId='+v,
                textField:'name',
                valueField:'id',
                editable:false,
                panelMaxHeight:'150',
                onLoadSuccess:function () {
                    if (func){
                        func.call();
                    }
                }
            });
        }
	</script>
</head>
<body>
<form id="form" method="post">
	<input name="id" type="hidden" />
	<div style="padding:15px;font-size: 12px">
		<table style="table-layout:fixed;margin-top: 10px;margin-bottom: 15px" border="0" cellspacing="0" class="formtable">
			<%--<tr class="store">--%>
				<%--<th style="width:100px;">选择商家：</th>--%>
				<%--<td>--%>
					<%--<select id="storeId"--%>
							<%--editable="false" name="storeId" style="width:182px;"/>--%>
				<%--</td>--%>
			<%--</tr>--%>
			<tr>
				<th style="width:100px;">选择物品：</th>
				<td>
					<select id="goodsId" name="goodsId"
							 style="width:182px;"/>
				</td>
			</tr>
			<tr>
				<th style="width:100px;">概率(0-100)：</th>
				<td >
					<input class="easyui-numberbox" precision="2" data-options="required:true,min:0,max:100" name="rate"  missingMessage="请输入概率" prompt="请输入概率" id="rate"  />
				</td>
			</tr>
			<tr>
				<th style="width:100px;">最小数量：</th>
				<td >
					<input class="easyui-numberbox" precision="0" data-options="required:true,min:0,max:99999" name="min"  missingMessage="请输入最小数量" prompt="请输入最小数量" id="min"  />
				</td>
			</tr>
			<tr>
				<th style="width:100px;">最大数量：</th>
				<td >
					<input class="easyui-numberbox" precision="0" data-options="required:true,min:0,max:99999" name="max"  missingMessage="请输入最大数量" prompt="请输入最大数量" id="max"  />
				</td>
			</tr>
			<tr>
				<th style="width:100px;">特定时间段：</th>
				<td >
					<select class="easyui-combobox" name="isTime" id="isTime" editable="false" style="width: 90%" panelHeight="auto">
						<option value="0">无</option>
						<option value="1" selected>有</option>
					</select>
				</td>
			</tr>
			<tr class="time">
				<th style="width:100px;">开始时间：</th>
				<td >
					<input class="easyui-timespinner" precision="0" name="begin" data-options="required:true,showSeconds:true"  missingMessage="请输入开始时间" prompt="请输入开始时间" id="begin"  />
				</td>
			</tr>
			<tr class="time">
				<th style="width:100px;">结束时间：</th>
				<td >
					<input class="easyui-timespinner" precision="0" name="end" data-options="required:true,showSeconds:true" missingMessage="请输入结束时间" prompt="请输入结束时间" id="end"  />
				</td>
			</tr>
			<tr>
				<th style="width:100px;">起始积分(积分游戏专用)：</th>
				<td >
					<input class="easyui-numberbox" precision="0" data-options="min:0,max:999999999" name="scoreBegin"  missingMessage="请输入起始积分" prompt="请输入起始积分" id="scoreBegin"  />
				</td>
			</tr>
			<tr>
				<th style="width:100px;">结束积分(积分游戏专用)：</th>
				<td >
					<input class="easyui-numberbox" precision="0" data-options="min:0,max:999999999" name="scoreEnd"  missingMessage="请输入结束积分" prompt="请输入结束积分" id="scoreEnd"  />
				</td>
			</tr>
		</table>
	</div>
</form>
</body>
</html>