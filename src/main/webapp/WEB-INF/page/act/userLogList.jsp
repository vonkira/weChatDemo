<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<%@ include file="/static/admin/jsp/include.jsp"%>
	<script type="text/javascript">
        var userId = "${id}";
        var grid;

        $(function() {
            grid = $('#grid').datagrid({
                url : sy.contextPath + '/userGoodsLog/list?actId=' + userId,
                fitColumns:false,
                columns : [ [
                    {
                        width : 100,
                        title : '用户',
                        field : 'account',
                        align : 'center',
                        formatter:function (v,r) {
                            var u = v+'('+r.nickName+')';
                            return UT.addTitle(u);
                        }
                    },
//                    {
//                        width : $(this).width() * 0.05,
//                        title : '类型',
//                        field : 'type',
//                        align : 'center',
//                        formatter:function (v) {
//                            return v == 1?'获取':'减少';
//                        }
//                    },
					{
                        width : 150,
                        title : '物品名称',
                        field : 'goodsName',
                        align : 'center',
                        formatter:function (v) {
                            return UT.addTitle(v);
                        }
                    },{
                        width : 25,
                        title : '数量',
                        field : 'num',
                        align : 'center',
                        formatter:function (v) {
                            return UT.addTitle(v);
                        }
                    },{
                        width : 50,
                        title : '项目',
                        field : 'item',
                        align : 'center',
                        formatter:function (v) {
                            return UT.addTitle(v);
                        }
                    },{
                        width : 200,
                        title : '描述',
                        field : 'info',
                        align : 'center',
                        formatter:function (v) {
                            return UT.addTitle(v);
                        }
                    },{
                        width : 100,
                        title : '时间',
                        field : 'createTime',
                        align : 'center',
                        formatter:function (v) {
                            return UT.addTitle(v);
                        }
                    }
                ] ]
            });
        });
	</script>
</head>
<body>
<table id="grid" data-options="fit:true,border:false"></table>
</body>
</html>