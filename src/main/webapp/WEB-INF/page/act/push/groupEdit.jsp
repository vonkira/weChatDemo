<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<%@ include file="/static/admin/jsp/include.jsp"%>
	<script type="text/javascript">
        var id = "${id}";
		var _actId = '${actId}';
        var submitForm = function($dialog, $grid, $pjq) {
            var obj = sy.serializeObject($('form'));
            obj.acitvityId = _actId;
            if ($('form').form('validate')) {
                $.ajax({
                    url:sy.contextPath + '/StoreGoodsActivityController/save',
                    data:obj,
                    type:'post',
                    dataType:'json',
                    success:function(result) {
                        if (result.code == 0) {
                            $grid.datagrid('reload');
                            $dialog.dialog('destroy');
                        } else {
                            $pjq.messager.e('添加失败 ' + result.msg);
                        }
                    }
                });
            }
        };


        $(function() {

            $('#pushDepotId').combobox({
                url:sy.contextPath + '/storeGoodsPushDepot/listByActSelect?actId='+_actId,
                textField:'name',
                valueField:'id',
				required:true,
                filter: function(q, row){
                    var opts = $(this).combobox('options');
                    return row[opts.textField].toLowerCase().indexOf(q.toLowerCase()) != -1;
                }
            });

        });
	</script>
</head>
<body>
<form id="form" method="post">
	<input name="id" type="hidden" />
	<div style="padding:15px;font-size: 12px">
		<table style="table-layout:fixed;margin-top: 10px;margin-bottom: 15px" border="0" cellspacing="0" class="formtable">
			<tr>
				<th style="width:100px;">投放库：</th>
				<td>
					<select id="pushDepotId" name="pushDepotId" style="width: 90%"></select>
				</td>
			</tr>
		</table>
	</div>
</form>
</body>
</html>