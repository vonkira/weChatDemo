<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/static/admin/jsp/include.jsp"%>
<script type="text/javascript">
	var grid;
	var _id = '${id}';
	var addFun = function($dialog, $grid, $pjq) {
		var dialog = parent.sy.modalDialog({
			title : '添加投放库',
			width : 600,
			height : 400,
			url : sy.contextPath + '/go?path=act/push/edit&actId='+_id,
			buttons : [ {
				text : '保存',
				handler : function() {
					dialog.find('iframe').get(0).contentWindow.submitForm(dialog, grid, parent.$);
				}
			} ]
		});
	};
	
	var editFun = function() {
		var rows = grid.datagrid('getSelections');
		if (rows.length != 1) {
			parent.$.messager.w('请选择一条记录进行编辑！');
			return;
		}
		var dialog = parent.sy.modalDialog({
			title : '编辑商品',
			width : 600,
			height : 400,
			url : sy.contextPath + '/go?path=goods/depot/edit&id=' + rows[0].id,
			buttons : [ {
				text : '保存',
				handler : function() {
					dialog.find('iframe').get(0).contentWindow.submitForm(dialog, grid, parent.$);
				}
			} ]
		});
	};
	
	var delFun = function() {
		var rows = grid.datagrid('getSelections');
		if (rows.length == 0) {
			parent.$.messager.w('请选择需要删除的记录！');
			return;
		}
		parent.$.messager.confirm('询问', '您确定要删除此记录？', function(r) {
			if (r) {
				var ids = [];
				for ( var i = 0, l = rows.length; i < l; i++) {
					var r = rows[i];
					ids.push(r.id);
				}
				var id = ids.join(',');
				
				$.post(sy.contextPath + '/storeGoodsActivity/del', {
					id : id
				}, function(res) {
					rows.length = 0;//必须，否则有bug
					grid.datagrid('reload');
				}, 'json');
			}
		});
	};
	
	//查看商品详情
	var infoFun = function(cont) {
		layer.open({
			type: 2,
		    skin: 'layui-layer-rim', //加上边框
		    area: ['500px', '300px'], //宽高
		    content: sy.contextPath+'/detail?id='+cont+'&type=1' 
		}); 
	};
	
	//查看ID
	var idFun = function() {
		var rows = grid.datagrid('getSelections');
		if (rows.length != 1) {
			parent.$.messager.w('请选择一条记录进行复制！');
			return;
		}
		sy.getId(rows[0].id,'复制');
	};
	
	//上下架商品事件
	var onlineFun = function(type, id) {
		var title = "";
		if (type == 1) {
			title = "确定上架该商品吗？";
		} else {
			title = "确定下架该商品吗？";
		}
		var data = {id: id, isOnline: type};
		parent.$.messager.confirm("询问", title, function(r) {
			if (r) {
				saveData(data);
			}
		});
	};
	
	//保存数据
	var saveData = function(data) {
		var url = sy.contextPath + '/storeGoodsPushDepot/save';
		$.post(url, data, function() {
			grid.datagrid('reload');
		}, 'json');
	};
	
	//调整库存
	var opFun = function(id) {
		var dialog = parent.sy.modalDialog({
			title : '投放库分配',
			width : 800,
			height : 500,
			url : sy.contextPath + '/go?path=goods/depot/actList&id=' + id
		});
	};

	var numFun = function (id) {
        var dialog = parent.sy.modalDialog({
            title : '投放库数量更新',
            width : 400,
            height : 250,
            url : sy.contextPath + '/go?path=goods/depot/numEdit&id=' + id,
            buttons : [ {
                text : '确定',
                handler : function() {
                    dialog.find('iframe').get(0).contentWindow.submitForm(dialog, grid, parent.$);
                }
            } ]
        });
    }

    //调整库存
    var logFun = function(id) {
        parent.sy.modalDialog({
            title : '用户日志',
            width : 800,
            height : 600,
            url : sy.contextPath + '/go?path=goods/logList&id=' + id
        });
    };
	
	var importFun = function($dialog, $grid, $pjq) {
		var dialog = parent.sy.modalDialog({
			title : '通过模板导入数据',
			width : 600,
			height : 300,
			url : sy.contextPath + '/go?path=car/importData&type=6',
			buttons : [ {
				text : '导入',
				handler : function() {
					dialog.find('iframe').get(0).contentWindow.submitForm(dialog, grid, parent.$);
				}
			} ]
		});
	};

    function exportExcel(){
        var obj = sy.serializeObject($('#searchForm'));
        var cateId = obj.cateId?obj.cateId:"";
        var name = obj.name?obj.name:"";
        var code = obj.code?obj.code:"";
        location.href = sy.basePath + "goods/export?cateId="+cateId+"&code="+code+"&name="+name;
    }
	$(function() {
		grid = $('#grid').datagrid({
			url : sy.contextPath + '/storeGoodsPushDepot/listByAct?actId='+_id,
			singleSelect : true,
            fitColumns:$(this).width() <= 1310?false:true,
			frozenColumns : [ [{
				width : 200,
				title : '投放名称',
				field : 'name',
				align : 'center',
				formatter: function(value, row, index) {
					return UT.addTitle(value);
				}
			}
			] ],
			columns : [ [
                {
                    width : 180,
                    title : '投放编号',
                    field : 'code',
                    align : 'center',
                    formatter: function(value, row, index) {
                        return UT.addTitle(value);
                    }
                },
				{
					width : 150,
					title : '投放商品',
					field : 'goodsName',
					align : 'center',
                    formatter: function(value, row, index) {
                        return UT.addTitle(value);
                    }
				},{
                    width : 80,
                    title : '总数',
                    field : 'num',
                    align : 'center',
                    formatter: function(value, row, index) {
                        return UT.addTitle(value);
                    }
                },{
                    width : 80,
                    title : '已领取',
                    field : 'userNum',
                    align : 'center',
                    formatter: function(value, row, index) {
                        return UT.addTitle(value);
                    }
                },
                {
                    width : 50,
                    title : '用户限制',
                    field : 'userMax',
                    align : 'center',
                    formatter: function(value, row, index) {
                        return value != -1?value:'不限';
                    }
                },{
                    width : 80,
                    title : '用户每日限制',
                    field : 'dayMax',
                    align : 'center',
                    formatter: function(value, row, index) {
                        return value != -1?value:'不限';
                    }
                },{
					width : 140,
					title : '创建时间',
					field : 'createTime',
					align : 'center'
				}
			] ]
		});
	});

    function forbidden(state, id) {
        if (state == 1) {

            parent.$.messager.confirm('询问', "确定启用吗？", function(r) {
                if (r) {

                    var data = {
                        id : id,
                        state : 1
                    };
                    saveData(data);
                }
            });
        } else if (state == 0) {
            parent.$.messager.confirm('询问', "确定禁用吗？", function(r) {
                if (r) {
                    var data = {
                        id : id,
                        state : 0
                    };
                    saveData(data);
                }
            });
        }
    }
</script>
</head>
<body>
	<div id="toolbar">
		<%--<form id="searchForm">--%>
		<%--<div>--%>
			<%--<span>投放名称：</span>--%>
			<%--<input type="text" class="easyui-textbox" name="name" style="width: 150px" prompt="投放名称"/>--%>
			<%--<span>商品名称：</span>--%>
			<%--<input type="text" class="easyui-textbox" name="goodsName" style="width: 150px" prompt="商品名称"/>--%>
			<%--<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-search'" onclick="grid.datagrid('load',sy.serializeObject($('#searchForm')));">过滤</a>--%>
			<%--<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-search-minus'" onclick="$('#searchForm input').val('');grid.datagrid('load',{});">重置过滤</a>--%>
		<%--</div>--%>
		<%--</form>--%>
		<div class="tbbutton">
			<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-plus-circle',plain:true" onclick="addFun();">添加</a>
			<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-trash',plain:true" onclick="delFun();">删除</a>
		</div>
	</div>
	<table id="grid" data-options="fit:true,border:false"></table>
</body>
</html>