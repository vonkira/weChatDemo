<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/static/admin/jsp/include.jsp"%>
<script type="text/javascript">
	var id = "${id}";

	var submitForm = function($dialog, $grid, $pjq) {
		var obj = sy.serializeObject($('form'));
		var detail = editor.getHtml();
		obj.detail = detail;
		if ($('form').form('validate')) {
			$.ajax({
				url:sy.contextPath + '/goodsTrigger/save',
				data:obj,
				type:'post',
				dataType:'json',
				success:function(result) {
					if (result.code == 0) {
						$grid.datagrid('reload');
						$dialog.dialog('destroy');
					} else {
						$pjq.messager.e('添加失败 ' + result.msg);
					}
				}
			});
		}
	};

	$(function() {
	    $('#resourceGoodsId').combobox({
			url:sy.contextPath + '/storeGoods/listByStore',
            textField:'name',
            valueField:'id',
			editable:false
		});

        if (id != '') {
			parent.$.messager.progress();
			$.post(sy.contextPath + '/goodsTrigger/findById', {
				id : id
			}, function(result) {
				if (result) {
					$('form').form('load', result);
				}
				parent.$.messager.progress('close');
			}, 'json');
		}
	});
</script>
</head>
<body>
	<form id="form" method="post">
		<div style="padding:15px;font-size: 12px">
			<table style="table-layout:fixed;margin-top: 10px;margin-bottom: 15px" border="0" cellspacing="0" class="formtable">
				<tr>
					<th style="width:100px;">名称：</th>
					<td>
						<input class="easyui-textbox" data-options="validType:['length[17,17]']" name="name" type="text" missingMessage="请输入名称" prompt="请输入名称" id="name"  />
					</td>
					<th style="width:100px;">资源类型：</th>
					<td><select id="resourceType" name="resourceType"
								class="easyui-combobox" editable="false" style="width:182px;"
								panelHeight='auto'
								data-options="valueField: 'value',textField: 'name',data: [{name: '无',value: '0',selected:true},{name: '体力',value: '1'},{name: '金币',value: '2'},{name: '物品',value: '3'}]"/>
				</tr>
				<tr>
					<th style="width:100px;">资源数量：</th>
					<td >
						<input class="easyui-numberbox" data-options="validType:['length[17,17]']" name="resourceNum"  missingMessage="请输入资源数量" prompt="请输入资源数量" id="resourceNum"  />
					</td>
					<th style="width:100px;">选择物品：</th>
					<td>
						<select id="resourceGoodsId" name="resourceGoodsId"
								 editable="false" style="width:182px;"/>
					</td>
				</tr>
				<tr>
					<th style="width:100px;">开始时间：</th>
					<td><input id="beginTime" name="beginTime"
							   style="width: 90%" class="easyui-datetimebox"
							   data-options="required:false"
							   missingMessage="请输入开始时间" prompt="请输入开始时间" /></td>
					<th style="width:100px;">结束时间：</th>
					<td><input id="endTime" name="endTime"
							   style="width: 90%" class="easyui-datetimebox"
							   data-options="required:false"
							   missingMessage="请输入结束时间" prompt="请输入结束时间" /></td>
				</tr>
			</table>
			<%--<h3>物品信息：</h3>--%>
			<%--<div id="toolbar">--%>
				<%--<div>--%>
					<%--<input id="keyword" editable="true" style="width:500px;" panelHeight='200' prompt="输入物品名称搜索"/>--%>
					<%--<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-plus-circle'" onclick="add()">添加</a>--%>
					<%--<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-rotate-left'" onclick="resetText()">清空</a>--%>
				<%--</div>--%>
			<%--</div>--%>
			<%--<table id="grid" data-options="fit:true,border:false"></table>--%>
		</div>
	</form>
</body>
</html>