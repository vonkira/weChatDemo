<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/static/admin/jsp/include.jsp"%>
<script type="text/javascript">
	var grid;
	var _id = '${id}';
	var addFun = function($dialog, $grid, $pjq) {
		var dialog = parent.sy.modalDialog({
			title : '添加触发器商品',
			width : 600,
			height : 400,
			url : sy.contextPath + '/go?path=act/triggerGoodsEdit&triggerId='+_id,
			buttons : [ {
				text : '保存',
				handler : function() {
					dialog.find('iframe').get(0).contentWindow.submitForm(dialog, grid, parent.$);
				}
			} ]
		});
	};
	
	var editFun = function() {
		var rows = grid.datagrid('getSelections');
		if (rows.length != 1) {
			parent.$.messager.w('请选择一条记录进行编辑！');
			return;
		}
		var dialog = parent.sy.modalDialog({
			title : '编辑触发器' +
			'商品',
			width : 600,
			height : 400,
			url : sy.contextPath + '/go?path=act/triggerGoodsEdit&id=' + rows[0].id,
			buttons : [ {
				text : '保存',
				handler : function() {
					dialog.find('iframe').get(0).contentWindow.submitForm(dialog, grid, parent.$);
				}
			} ]
		});
	};
	
	var delFun = function() {
		var rows = grid.datagrid('getSelections');
		if (rows.length == 0) {
			parent.$.messager.w('请选择需要删除的记录！');
			return;
		}
		parent.$.messager.confirm('询问', '您确定要删除此记录？', function(r) {
			if (r) {
				var ids = [];
				for ( var i = 0, l = rows.length; i < l; i++) {
					var r = rows[i];
					ids.push(r.id);
				}
				var id = ids.join(',');
				
				$.post(sy.contextPath + '/goodsTriggerGoods/del', {
					id : id
				}, function(res) {
					rows.length = 0;//必须，否则有bug
					grid.datagrid('reload');
				}, 'json');
			}
		});
	};

	$(function() {
		grid = $('#grid').datagrid({
			url : sy.contextPath + '/goodsTriggerGoods/listById?id='+_id,
			singleSelect : false,
            pagination:false,
            fitColumns:$(this).width() <= 1310?false:true,
			frozenColumns : [ [ {
				width : '100',
				checkbox:true,
				field : 'id',
				align : 'center'
			},{
				width : 100,
				title : '商品名称',
				field : 'goodsName',
				align : 'center',
				formatter: function(value, row, index) {
					return UT.addTitle(value);
				}
			}
			] ],
			columns : [ [ 
				{
					width : 100,
					title : '概率',
					field : 'rate',
					align : 'center',
                    formatter: function(value, row, index) {
                        return UT.addTitle(value+"%");
                    }
				},{
                    width : 80,
                    title : '最小数量',
                    field : 'min',
                    align : 'center',
                    formatter : function(value, row, index) {
                        return UT.addTitle(value);
                    }
                },
				{
					width : 80,
					title : '最大数量',
					field : 'max',
					align : 'center',
					formatter:function(v,r,i){
                        return UT.addTitle(v);
					}
				},
                {
                    width : 80,
                    title : '开始',
                    field : 'beginTime',
                    align : 'center',
                    formatter:function(v,r,i){
                        if (r.isTime == 1){
                            return v?v.substr(10):'';
                        }else{
                            return '——';
                        }

                    }
                },
                {
                    width : 80,
                    title : '结束',
                    field : 'endTime',
                    align : 'center',
                    formatter:function(v,r,i){
                        if (r.isTime == 1){
                            return v?v.substr(10):'';
                        }else{
                            return '——';
                        }
                    }
                },{
                    width : 80,
                    title : '起始积分',
                    field : 'scoreBegin',
                    align : 'center',
                    formatter : function(value, row, index) {
                        if (value){
                            return UT.addTitle(value);
                        }else{
                            return '——';
                        }
                    }
                },
                {
                    width : 80,
                    title : '结束积分',
                    field : 'scoreEnd',
                    align : 'center',
                    formatter:function(v,r,i){
                        if (v){
                            return UT.addTitle(v);
                        }else{
                            return '——';
                        }
                    }
                }
			] ]
		});
	});
</script>
</head>
<body>
	<table id="grid" data-options="fit:true,border:false"></table>
</body>
</html>