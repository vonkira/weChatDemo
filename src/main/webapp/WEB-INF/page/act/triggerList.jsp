<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/static/admin/jsp/include.jsp"%>
<script type="text/javascript">
	var grid;
	
	var addFun = function($dialog, $grid, $pjq) {
		var dialog = parent.sy.modalDialog({
			title : '添加触发器',
			width : 800,
			height : 600,
			url : sy.contextPath + '/go?path=act/triggerEdit',
			buttons : [ {
				text : '保存',
				handler : function() {
					dialog.find('iframe').get(0).contentWindow.submitForm(dialog, grid, parent.$);
				}
			} ]
		});
	};
	
	var editFun = function() {
		var rows = grid.datagrid('getSelections');
		if (rows.length != 1) {
			parent.$.messager.w('请选择一条记录进行编辑！');
			return;
		}
		var dialog = parent.sy.modalDialog({
			title : '编辑触发器',
			width : 800,
			height : 600,
			url : sy.contextPath + '/go?path=act/triggerEdit&id=' + rows[0].id,
			buttons : [ {
				text : '保存',
				handler : function() {
					dialog.find('iframe').get(0).contentWindow.submitForm(dialog, grid, parent.$);
				}
			} ]
		});
	};
	
	var delFun = function() {
		var rows = grid.datagrid('getSelections');
		if (rows.length == 0) {
			parent.$.messager.w('请选择需要删除的记录！');
			return;
		}
		parent.$.messager.confirm('询问', '您确定要删除此记录？', function(r) {
			if (r) {
				var ids = [];
				for ( var i = 0, l = rows.length; i < l; i++) {
					var r = rows[i];
					ids.push(r.id);
				}
				var id = ids.join(',');
				
				$.post(sy.contextPath + '/goodsTrigger/del', {
					id : id
				}, function(res) {
					rows.length = 0;//必须，否则有bug
					grid.datagrid('reload');
				}, 'json');
			}
		});
	};
	
	//查看商品详情
	var infoFun = function(id) {
        parent.sy.modalDialog({
            title : '触发器商品',
            width : 800,
            height : 600,
            url : sy.contextPath + '/go?path=act/triggerGoodsList&id=' + id
        });
	};
	
	//查看ID
	var idFun = function() {
		var rows = grid.datagrid('getSelections');
		if (rows.length != 1) {
			parent.$.messager.w('请选择一条记录进行复制！');
			return;
		}
		sy.getId(rows[0].id,'复制');
	};
	
	//上下架商品事件
	var onlineFun = function(type, id) {
		var title = "";
		if (type == 1) {
			title = "确定启用该触发器吗？";
		} else {
			title = "确定禁用该触发器吗？";
		}
		var data = {id: id, state: type};
		parent.$.messager.confirm("询问", title, function(r) {
			if (r) {
				saveData(data);
			}
		});
	};

    //保存数据
    var saveData = function(data) {
        var url = sy.contextPath + '/goodsTrigger/save';
        $.post(url, data, function() {
            grid.datagrid('reload');
        }, 'json');
    };

    var resourceTypeStr = {
        0:'无',
		1:'体力',
		2:'金币',
		3:'物品'
	};

	$(function() {
		grid = $('#grid').datagrid({
			url : sy.contextPath + '/goodsTrigger/list',
			singleSelect : true,
            fitColumns:$(this).width() <= 1310?false:true,
			columns : [ [
                {
                    width : 180,
                    title : '名称',
                    field : 'name',
                    align : 'center',
                    formatter: function(value, row, index) {
                        return UT.addTitle(value);
                    }
                },{
					width : 50,
					title : '消耗体力',
					field : 'isStrength',
					align : 'center',
                    formatter: function(value, row, index) {
                        return value==1?'是x'+row.strengthNum:'否';
                    }
				},{
                    width : 50,
                    title : '消耗金币',
                    field : 'isCoin',
                    align : 'center',
                    formatter : function(value, row, index) {
                        return value==1?'是x'+row.coinNum:'否';
                    }
                },{
                    width : 50,
                    title : '消耗物品',
                    field : 'isGoods',
                    align : 'center',
                    formatter : function(value, row, index) {
                        return value==1?'是x'+row.goodsNum:'否';
                    }
                },
				{
					width : 280,
					title : '描述',
					field : 'goodsName',
					align : 'center',
					formatter:function(v,r,i){
                        return UT.addTitle(v);
					}
				},
//				{
//					width : 80,
//					title : '开始时间',
//					field : 'beginTime',
//					align : 'center'
//				},{
//                    width : 80,
//                    title : '结束时间',
//                    field : 'endTime',
//                    align : 'center'
//                },
//				{
//                    width : 50,
//                    title : '状态',
//                    field : 'state',
//                    align : 'center',
//                    formatter : function(value, row, index) {
//                        switch (value) {
//                            case 0:
//                                return '<a href="javascript:void(0);" onclick="onlineFun(1,\''+row.id+'\')" ><img src="static/admin/images/no.png" title="点击启用" > </img></a>';
//                            case 1:
//                                return '<a href="javascript:void(0);" onclick="onlineFun(0,\''+row.id+'\')" ><img src="static/admin/images/yes.png" title="点击禁用" > </img></a>';
//                        }
//                    }
//                },
				{
					width : 120,
					title : '创建时间',
					field : 'createTime',
					align : 'center'
				}
				,{
                    width : 100,
                    title : '操作',
                    field : 'op',
                    align : 'center',
                    formatter: function(value, row, index) {
                        var html = "";
                        html+='<a href="javascript:void(0);" onclick="infoFun(\''+row.id+'\')" class="button button-orange" title="明细">明细</a>';
                        return html;
                    }
                }
			] ]
		});
	});
</script>
</head>
<body>
	<div id="toolbar">
		<form id="searchForm">
		<div>
			<span>触发器名称：</span>
			<input type="text" class="easyui-textbox" name="name" style="width: 150px" prompt="触发器名称"/>
			<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-search'" onclick="grid.datagrid('load',sy.serializeObject($('#searchForm')));">过滤</a>
			<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-search-minus'" onclick="$('#searchForm input').val('');grid.datagrid('load',{});">重置过滤</a>
		</div>
		</form>
		<div class="tbbutton">
			<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-plus-circle',plain:true" onclick="addFun();">添加</a>
			<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-pencil',plain:true" onclick="editFun();">修改</a>
			<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-trash',plain:true" onclick="delFun();">删除</a>
			<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-key',plain:true" onclick="idFun();">查看ID</a>
		</div>
	</div>
	<table id="grid" data-options="fit:true,border:false"></table>
</body>
</html>