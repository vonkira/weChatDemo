<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<%@ include file="/static/admin/jsp/include.jsp"%>
	<script type="text/javascript">
        var id = "${id}";

        var submitForm = function($dialog, $grid, $pjq) {
            var obj = sy.serializeObject($('form'));
            obj.beginTime += ' 00:00:00';
            obj.endTime += ' 00:00:00';
            if ($('form').form('validate')) {
                $.ajax({
                    url:sy.contextPath + '/storeActivity/saveVr',
                    data:obj,
                    type:'post',
                    dataType:'json',
                    success:function(result) {
                        if (result.code == 0) {
                            $grid.datagrid('reload');
                            $dialog.dialog('destroy');
                        } else {
                            $pjq.messager.e('添加失败 ' + result.msg);
                        }
                    }
                });
            }
        };

        $(function() {
            $('#containerId').combobox({
                url:sy.contextPath + '/goodsContainer/listByStore',
                textField:'name',
                valueField:'id',
                editable:false,
                required:true,
                formatter:function (row) {
                    var opts = $(this).combobox('options');
                    console.log(row);
                    return row[opts.textField]+'<img width="18px" height="18px" src="download?id='+row.img+'" />';
                }
            });

            $('#triggerId').combobox({
                url:sy.contextPath + '/goodsTrigger/listByStore',
                textField:'name',
                valueField:'id',
                required:true,
				panelHeight:'150px',
                filter: function(q, row){
                    var opts = $(this).combobox('options');
                    return row[opts.textField].toLowerCase().indexOf(q.toLowerCase()) != -1;
                }
            });

            if (id != '') {
                parent.$.messager.progress();
                $.post(sy.contextPath + '/storeActivity/findById', {
                    id : id
                }, function(result) {
                    if (result) {
                        $('form').form('load', result);
                    }
                    parent.$.messager.progress('close');
                }, 'json');
            }
        });

        function showamap() {
            var lat = $('#lat').textbox('getValue');
            var lng = $('#lng').textbox('getValue');
            sy.amap(retMap, lat, lng);
        }

        function retMap(ret) {
            if (ret.lat != "") {
                $('#lat').textbox('setValue',ret.lat);
                $('#lng').textbox('setValue',ret.lng);
            }
        }

	</script>
</head>
<body>
<form id="form" method="post">
	<input name="id" type="hidden" />
	<div style="padding:15px;font-size: 12px">
		<table style="table-layout:fixed;margin-top: 10px;margin-bottom: 15px" border="0" cellspacing="0" class="formtable">
			<tr>
				<th style="width:100px;">名称：</th>
				<td>
					<input class="easyui-textbox" style="width: 90%" data-options="required:true,validType:['length[0,20]']" name="name" type="text" missingMessage="请输入名称" prompt="请输入名称" id="name"  />
				</td>
			</tr>
			<tr>
				<th style="width:100px;">载体：</th>
				<td>
					<select id="containerId" name="containerId" style="width: 90%"></select>
				</td>
			</tr>
			<%--<tr>--%>
				<%--<th style="width:100px;">触发器：</th>--%>
				<%--<td>--%>
					<%--<select id="triggerId" name="triggerId" style="width: 90%"></select>--%>
				<%--</td>--%>
			<%--</tr>--%>
			<tr>
				<th style="width:100px;">描述：</th>
				<td>
					<input class="easyui-textbox" style="width: 90%" data-options="validType:['length[0,100]']" name="info" type="text" missingMessage="请输入活动描述" prompt="请输入活动描述" id="info"  />
				</td>
			</tr>
			<tr>
				<th style="width:100px;">VR地址：</th>
				<td>
					<input class="easyui-textbox" style="width: 90%" data-options="required:true,validType:['url','length[0,100]']" name="vrUrl" type="text" missingMessage="请输入VR地址" prompt="请输入VR地址" id="vrUrl"  />
				</td>
			</tr>
			<tr>
				<th style="width:100px;">描点：</th>
				<td>
					<a class="btn btn-white btn-bitbucket" title="地图锚点" onclick="showamap()"><i class="fa fa-user-md"></i></a>
				</td>
			</tr>
			<tr>
				<th style="width:100px;">lat：</th>
				<td><input class="easyui-textbox" style="width: 90%" data-options="required:true,validType:['length[0,20]']" name="lat" missingMessage="请输入纬度" prompt="请输入纬度" id="lat"  /></td>
			</tr>
			<tr>
				<th style="width:100px;">lng：</th>
				<td><input class="easyui-textbox" style="width: 90%" data-options="required:true,validType:['length[0,20]']" name="lng"  missingMessage="请输入经度" prompt="请输入经度" id="lng"  /></td>
			</tr>
			<tr>
				<th style="width:100px;">范围(米)：</th>
				<td><input class="easyui-numberbox" style="width: 90%" data-options="required:true,min:0,max:9999999999" name="scope" missingMessage="请输入范围" prompt="请输入范围" id="scope"  /></td>
			</tr>
			<tr>
				<th style="width:100px;">开始时间：</th>
				<td><input id="beginTime" name="beginTime"
						   style="width: 90%" class="easyui-datebox" style="width: 90%"
						   data-options="required:true"
						   missingMessage="请输入开始时间" prompt="请输入开始时间" /></td>
			</tr>
			<tr>
				<th style="width:100px;">结束时间：</th>
				<td><input id="endTime" name="endTime"
						   style="width: 90%" class="easyui-datebox" style="width: 90%"
						   data-options="required:true"
						   missingMessage="请输入结束时间" prompt="请输入结束时间" /></td>
			</tr>
		</table>
	</div>
</form>
</body>
</html>