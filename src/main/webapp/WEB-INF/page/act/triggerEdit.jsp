<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<%@ include file="/static/admin/jsp/include.jsp"%>
	<script type="text/javascript">
        var id = "${id}";

        var submitForm = function($dialog, $grid, $pjq) {
            var obj = sy.serializeObject($('form'));
            if ($('form').form('validate')) {
                $.ajax({
                    url:sy.contextPath + '/goodsTrigger/save',
                    data:obj,
                    type:'post',
                    dataType:'json',
                    success:function(result) {
                        if (result.code == 0) {
                            $grid.datagrid('reload');
                            $dialog.dialog('destroy');
                        } else {
                            $pjq.messager.e('添加失败 ' + result.msg);
                        }
                    }
                });
            }
        };

        $(function() {
            $('#resourceGoodsId').combobox({
                url:sy.contextPath + '/storeGoods/listByStore?coin=1',
                textField:'name',
                valueField:'id',
				required:true
            });

//            $('#resourceType').combobox({
//				onChange:function (v) {
//					if (v == 3){
//					    $('.resourceGoodsId').show();
//					}else{
//                        $('.resourceGoodsId').hide();
//					}
//                }
//			})

			$('#isStrength').combobox({
				onChange:function (v) {
					if (v == 1){
					    $('#strengthNum').numberbox('enableValidation');
					}else{
                        $('#strengthNum').numberbox('disableValidation');
					}
                }
			})
			$('#isCoin').combobox({
				onChange:function (v) {
					if (v == 1){
					    $('#coinNum').numberbox('enableValidation');
					}else{
                        $('#coinNum').numberbox('disableValidation');
					}
                }
			})
			$('#isGoods').combobox({
				onChange:function (v) {
					if (v == 1){
					    $('#goodsNum').numberbox('enableValidation');
					    $('#resourceGoodsId').combobox('enableValidation');
					}else{
                        $('#goodsNum').numberbox('disableValidation');
                        $('#resourceGoodsId').combobox('disableValidation');
					}
                }
			})

            if (id != '') {
                parent.$.messager.progress();
                $.post(sy.contextPath + '/goodsTrigger/findById', {
                    id : id
                }, function(result) {
                    if (result) {
                        $('form').form('load', result);
                    }
                    parent.$.messager.progress('close');
                }, 'json');
            }
        });
	</script>
</head>
<body>
<form id="form" method="post">
	<input name="id" type="hidden" />
	<div style="padding:15px;font-size: 12px">
		<table style="table-layout:fixed;margin-top: 10px;margin-bottom: 15px" border="0" cellspacing="0" class="formtable">
			<tr>
				<th style="width:100px;">触发器名称：</th>
				<td >
					<input class="easyui-textbox" style="width: 90%" data-options="required:true,validType:['length[0,20]']" name="name" type="text" missingMessage="请输入名称" prompt="请输入名称" id="name"  />
				</td>
				<th style="width:100px;">最多获取奖励：</th>
				<td>
					<input class="easyui-numberbox" style="width: 90%" data-options="required:true,min:1,max:99999999" name="maxNum" type="text" missingMessage="请输入最多获取奖励" prompt="请输入最多获取奖励" id="maxNum"  />
				</td>
			</tr>
			<tr>
				<th style="width:100px;">消耗体力：</th>
				<td><select id="isStrength" name="isStrength" style="width: 90%"
							class="easyui-combobox"
							editable="false"
							panelHeight='auto'
							data-options="valueField: 'value',textField: 'name',data: [{name: '否',value: '0',selected:true},{name: '是',value: '1'}]"/>
				</td>
				<th style="width:100px;">体力数量：</th>
				<td >
					<input class="easyui-numberbox" style="width: 90%"  name="strengthNum" data-options='required:true'  missingMessage="请输入体力数量" prompt="请输入体力数量" id="strengthNum"  />
				</td>
			</tr>
			<tr>
				<th style="width:100px;">消耗金币：</th>
				<td><select id="isCoin" name="isCoin" style="width: 90%"
							class="easyui-combobox"
							editable="false"
							panelHeight='auto'
							data-options="valueField: 'value',textField: 'name',data: [{name: '否',value: '0',selected:true},{name: '是',value: '1'}]"/>
				</td>
				<th style="width:100px;">金币数量：</th>
				<td >
					<input class="easyui-numberbox" style="width: 90%"  name="coinNum" data-options='required:true'  missingMessage="请输入金币数量" prompt="请输入金币数量" id="coinNum"  />
				</td>
			</tr>
			<tr>
				<th style="width:100px;">消耗物品：</th>
				<td><select id="isGoods" name="isGoods" style="width: 90%"
							class="easyui-combobox"
							panelHeight='auto'
							editable="false"
							data-options="valueField: 'value',textField: 'name',data: [{name: '否',value: '0',selected:true},{name: '是',value: '1'}]"/>
				</td>
			</tr>
			<tr>
				<th style="width:100px;">物品数量：</th>
				<td >
					<input class="easyui-numberbox" style="width: 90%"  name="goodsNum" data-options='required:true' missingMessage="请输入物品数量" prompt="请输入物品数量" id="goodsNum"  />
				</td>
				<th style="width:100px;" class="resourceGoodsId">选择物品：</th>
				<td class="resourceGoodsId">
					<input id="resourceGoodsId" name="resourceGoodsId" style="width: 90%" data-options='required:true'/>
				</td>
			</tr>

			<%--<tr>--%>
				<%--<th style="width:100px;">开始时间：</th>--%>
				<%--<td><input id="beginTime" name="beginTime"--%>
						   <%--style="width: 90%" class="easyui-datetimebox" style="width: 90%"--%>
						   <%--data-options="required:false"--%>
						   <%--missingMessage="请输入开始时间" prompt="请输入开始时间" /></td>--%>
				<%--<th style="width:100px;">结束时间：</th>--%>
				<%--<td><input id="endTime" name="endTime"--%>
						   <%--style="width: 90%" class="easyui-datetimebox" style="width: 90%"--%>
						   <%--data-options="required:false"--%>
						   <%--missingMessage="请输入结束时间" prompt="请输入结束时间" /></td>--%>
			<%--</tr>--%>
		</table>
		<%--<h3>物品信息：</h3>--%>
		<%--<div id="toolbar">--%>
		<%--<div>--%>
		<%--<input id="keyword" editable="true" style="width:500px;" panelHeight='200' prompt="输入物品名称搜索"/>--%>
		<%--<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-plus-circle'" onclick="add()">添加</a>--%>
		<%--<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-rotate-left'" onclick="resetText()">清空</a>--%>
		<%--</div>--%>
		<%--</div>--%>
		<%--<table id="grid" data-options="fit:true,border:false"></table>--%>
	</div>
</form>
</body>
</html>