<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<%@ include file="/static/admin/jsp/include.jsp"%>
	<script type="text/javascript">
        var id = "${id}";
		var _activityId = '${activityId}';
		var detail;
        var submitForm = function($dialog, $grid, $pjq) {
            var obj = sy.serializeObject($('form'));
            if (!id){
                obj.activityId = _activityId;
			}
			obj.info = detail.getHtml();
            if ($('form').form('validate')) {
                $.ajax({
                    url:sy.contextPath + '/goodsGroup/save',
                    data:obj,
                    type:'post',
                    dataType:'json',
                    success:function(result) {
                        if (result.code == 0) {
                            $grid.datagrid('reload');
                            $dialog.dialog('destroy');
                        } else {
                            $pjq.messager.e('添加失败 ' + result.msg);
                        }
                    }
                });
            }
        };

        $(function() {
            detail = new HtmlEditor('#info');
//            if (sy.system == 'store'){
//                $('.store').hide();
//            }
//            if (sy.system == 'store'){
//                loadGoods(sy.id);
//            }else{
//                loadStore();
//            }

            $('#goodsId').combobox({
                url:sy.contextPath + '/storeGoods/listByStore?compound=1',
                textField:'name',
                valueField:'id',
                panelMaxHeight:'150',
                filter: function(q, row){
                    var opts = $(this).combobox('options');
                    return row[opts.textField].toLowerCase().indexOf(q.toLowerCase()) != -1;
                }
            });

            if (id != '') {
                parent.$.messager.progress();
                $.post(sy.contextPath + '/goodsGroup/findById', {
                    id : id
                }, function(result) {
                    if (result) {
                        $('form').form('load', result);
                        detail.setHtml(result.info);
                    }
                    parent.$.messager.progress('close');
                }, 'json');
            }
        });

        function loadStore(store,func) {
            $('#storeId').combobox({
                url:sy.contextPath + '/store/listStore',
                textField:'name',
                valueField:'id',
                editable:false,
                panelMaxHeight:'150',
                onLoadSuccess:function () {
                    $('#storeId').combobox("select",store);
                },
                onSelect:function (v) {
                    loadGoods(v.id,func);
                },
                onChange:function (v) {
                    if (v){
                        loadGoods(v);
                    }

                }
            });
        }
        function loadGoods(v,func) {
            $('#goodsId').combobox({
                url:sy.contextPath + '/storeGoods/listByStore?compound=1&storeId='+v,
                textField:'name',
                valueField:'id',
                editable:false,
                panelMaxHeight:'150',
                onLoadSuccess:function () {
                    if (func){
                        func.call();
                    }
                }
            });
        }
	</script>
</head>
<body>
<form id="form" method="post">
	<input name="id" type="hidden" />
	<div style="padding:15px;font-size: 12px">
		<table style="table-layout:fixed;margin-top: 10px;margin-bottom: 15px" border="0" cellspacing="0" class="formtable">
			<tr>
				<th style="width:100px;">选择物品：</th>
				<td>
					<select id="goodsId" name="goodsId"
							 style="width:182px;"/>
				</td>
			</tr>
			<tr>
				<th style="width:100px;">数量：</th>
				<td>
					<input class="easyui-numberbox" name="num" data-options="required:true,min:1,max:9999999999" style="width:182px;"/>
				</td>
			</tr>
			<tr>
				<th style="width:100px;">活动规则：</th>
				<td><textarea id="info" name="info"
										  style="width:98%;height: 400px"></textarea></td>
			</tr>
		</table>
	</div>
</form>
</body>
</html>