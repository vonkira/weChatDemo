<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/static/admin/jsp/include.jsp"%>
<script type="text/javascript">
	var _id = '${id}';
	var grid;
	
	var addFun = function($dialog, $grid, $pjq) {
		var dialog = parent.sy.modalDialog({
			title : '添加合成物品',
			width : 800,
			height : 600,
			url : sy.contextPath + '/go?path=act/group/goodsEdit&activityId='+_id,
			buttons : [ {
				text : '保存',
				handler : function() {
					dialog.find('iframe').get(0).contentWindow.submitForm(dialog, grid, parent.$);
				}
			} ]
		});
	};

    var editFun = function($dialog, $grid, $pjq) {
        var rows = grid.datagrid('getSelections');
        if (rows.length != 1) {
            parent.$.messager.w('请选择需要编辑的记录！');
            return;
        }
        var dialog = parent.sy.modalDialog({
            title : '编辑合成物品',
            width : 800,
            height : 600,
            url : sy.contextPath + '/go?path=act/group/goodsEdit&id='+rows[0].id,
            buttons : [ {
                text : '保存',
                handler : function() {
                    dialog.find('iframe').get(0).contentWindow.submitForm(dialog, grid, parent.$);
                }
            } ]
        });
    };
	
	var delFun = function() {
		var rows = grid.datagrid('getSelections');
		if (rows.length == 0) {
			parent.$.messager.w('请选择需要删除的记录！');
			return;
		}
		parent.$.messager.confirm('询问', '您确定要删除此记录？', function(r) {
			if (r) {
				var ids = [];
				for ( var i = 0, l = rows.length; i < l; i++) {
					var r = rows[i];
					ids.push(r.id);
				}
				var id = ids.join(',');
				
				$.post(sy.contextPath + '/goodsGroup/del', {
					id : id
				}, function(res) {
					rows.length = 0;//必须，否则有bug
					grid.datagrid('reload');
				}, 'json');
			}
		});
	};
	
	//查看ID
	var idFun = function() {
		var rows = grid.datagrid('getSelections');
		if (rows.length != 1) {
			parent.$.messager.w('请选择一条记录进行复制！');
			return;
		}
		sy.getId(rows[0].id,'复制');
	};
	
    function triggerFun(id) {
        parent.sy.modalDialog({
            title : '合成材料',
            width : 450,
            height : 600,
            url : sy.contextPath + '/go?path=act/group/materialList&id=' + id
        });
    }

	$(function() {
		grid = $('#grid').datagrid({
			url : sy.contextPath + '/goodsGroup/list?activityId='+_id,
			singleSelect : false,
            fitColumns:$(this).width() <= 1310?false:true,
			frozenColumns : [ [ {
				width : '100',
				checkbox:true,
				field : 'id',
				align : 'center'
			}
			] ],
			columns : [ [
                {
                    width : 150,
                    title : '物品名称',
                    field : 'goodsName',
                    align : 'center',
                    formatter: function(value, row, index) {
                        return UT.addTitle(value);
                    }
                },{
                    width : 50,
                    title : '物品数量',
                    field : 'num',
                    align : 'center',
                    formatter: function(value, row, index) {
                        return UT.addTitle(value);
                    }
				}
				,{

                    width : 100,
                    title : '操作',
                    field : 'op',
                    align : 'center',
                    formatter: function(value, row, index) {
                        var html = "";
                        html+='<a href="javascript:void(0);" onclick="triggerFun(\''+row.id+'\')" class="button button-orange" title="组成材料">组成材料</a>';
                        return html;
                    }
                }
			] ]
		});
	});
</script>
</head>
<body>
	<div id="toolbar">
		<div class="tbbutton">
			<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-plus-circle',plain:true" onclick="addFun();">添加</a>
			<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-plus-circle',plain:true" onclick="editFun();">编辑</a>
			<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-trash',plain:true" onclick="delFun();">删除</a>
		</div>
	</div>
	<table id="grid" data-options="fit:true,border:false"></table>
</body>
</html>