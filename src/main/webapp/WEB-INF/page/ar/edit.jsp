<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<%@ include file="/static/admin/jsp/include.jsp"%>
	<script type="text/javascript">
        var id = "${id}";

        var submitForm = function($dialog, $grid, $pjq) {
            var obj = sy.serializeObject($('form'));
//            if (!obj.img){
//                $pjq.messager.w("请上传图片");
//                return;
//			}

			if(obj.arResultType != -1){
                if (!obj.arResult){
                    $pjq.messager.w("请上传内容");
                    return;
                }
			}

            if ($('form').form('validate')) {
                $.ajax({
                    url:sy.contextPath + '/arConfig/save',
                    data:obj,
                    type:'post',
                    dataType:'json',
                    success:function(result) {
                        if (result.code == 0) {
                            $grid.datagrid('reload');
                            $dialog.dialog('destroy');
                        } else {
                            $pjq.messager.e('添加失败 ' + result.msg);
                        }
                    }
                });
            }
        };


        $(function() {
			$('#isScope').combobox({
				onChange:function (value) {
					if (value == 1){
					    $('.scope').show();
					}else{
                        $('.scope').hide();
					}
                }
			});
			$('#isActivity').combobox({
				onChange:function (value) {
					if (value == 1){
					    $('.activityId').show();
					}else{
                        $('.activityId').hide();
					}
                }
			});

			$('#activityId').combobox({
                url:sy.contextPath + '/storeActivity/listByType?type=8',
                textField:'name',
                valueField:'id',
                editable:false,
                required:true
			});

            if (id) {
                parent.$.messager.progress();
                $.post(sy.contextPath + '/arConfig/findById', {
                    id : id
                }, function(result) {
                    if (result) {
//                        $('#img').setFileId(result.img,false,true,true);
                        if (result.arResultType == 1){
                            $('#arResult').setFileId(result.arResult,false,2,true);
                        }else if(result.arResultType == 0){
                            $('#arResult').setFileId(result.arResult,false,1,true);
                        }
                        $('form').form('load', result);
                    }
                    parent.$.messager.progress('close');
                }, 'json');
            }
        });
	</script>
</head>
<body>
<form id="form" method="post">
	<input name="id" type="hidden" />
	<div style="padding:15px;font-size: 12px">
		<table style="table-layout:fixed;margin-top: 10px;margin-bottom: 15px" border="0" cellspacing="0" class="formtable">
			<tr>
				<th style="width:100px;">名称：</th>
				<td>
					<input class="easyui-textbox" style="width: 90%" data-options="required:true,validType:['length[0,20]']" name="name" missingMessage="请输入名称" prompt="请输入名称" id="name"  />
				</td>
				<th style="width:100px;">类型：</th>
				<td>
					<select class="easyui-combobox" name="arResultType" id="arResultType" editable="false" style="width: 90%" panelHeight="auto">
						<option value="-1">无</option>
						<option value="0">图片</option>
						<option value="1" selected>视频</option>
					</select>
				</td>
			</tr>
			<tr>
				<%--<th style="width:100px;">AR图片：</th>--%>
				<%--<td>--%>
					<%--<div id="img" multi="false" fileCountLimit='2' required="required" type="file" showImage="true" showBtn='true' bestSize='640*320' fileType="jpg,png,mp4" fileSize="200MB" buttonText="上传AR图片"></div>--%>
					<%--<label style="color: red;">*</label>--%>
					<%--<label>建议尺寸：宽度640，高度320，格式JPG,PNG</label>--%>
				<%--</td>--%>
				<th style="width:100px;">AR内容：</th>
				<td>
					<div id="arResult" multi="false" fileCountLimit='2' required="required" type="file" showImage="true" showBtn='true' bestSize='640*320' fileType="jpg,png,mp4" fileSize="200MB" buttonText="上传AR内容"></div>
					<label style="color: red;">*</label>
					<label>建议尺寸：宽度640，高度320，格式JPG,PNG</label>
				</td>
			</tr>
			<tr>
				<th style="width:100px;">跳转URL：</th>
				<td colspan="3"><input class="easyui-textbox" style="width: 90%" data-options="validType:['url','length[0,200]']" name="arUrl" missingMessage="请输入跳转URL" prompt="请输入跳转URL" id="arUrl"  /></td>
			</tr>
			<tr>
				<th style="width:100px;">绑定活动：</th>
				<td>
					<select class="easyui-combobox" name="isActivity" id="isActivity" editable="false" style="width: 90%" panelHeight="auto">
						<option value="0">否</option>
						<option value="1" selected>是</option>
					</select>
				</td>
				<th class="activityId">选择活动</th>
				<td class="activityId">
					<select name="activityId" id="activityId" editable="false" style="width: 90%" panelHeight="150px">
					</select>
				</td>
			</tr>
		</table>
	</div>
</form>
</body>
</html>