<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<%@ include file="/static/admin/jsp/include.jsp"%>
	<script type="text/javascript">
        var id = "${id}";
		var _arConfigId = '${arConfigId}';
        var submitForm = function($dialog, $grid, $pjq) {
            var obj = sy.serializeObject($('form'));
            if (!obj.img){
                $pjq.messager.w("请上传目标图");
                return;
			}
			if (!id){
                obj.arConfigId = _arConfigId;
			}
            if ($('form').form('validate')) {
                $.ajax({
                    url:sy.contextPath + '/arConfigImg/save',
                    data:obj,
                    type:'post',
                    dataType:'json',
                    success:function(result) {
                        if (result.code == 0) {
                            $grid.datagrid('reload');
                            $dialog.dialog('destroy');
                        } else {
                            $pjq.messager.e('添加失败 ' + result.msg);
                        }
                    }
                });
            }
        };


        $(function() {

            $('#isScope').combobox({
                panelHeight:'auto',
                onChange:function (value) {
                    if (value == 1){
                        $('.scope').show();
                        $('#lat').textbox("enableValidation");
                        $('#lng').textbox("enableValidation");
//                        $('#scope').textbox("enableValidation");
                    }else{
                        $('.scope').hide();
                        $('#lat').textbox("disableValidation");
                        $('#lng').textbox("disableValidation");
//                        $('#scope').textbox("disableValidation");
                    }
                }
            });

            if (id != '') {
                parent.$.messager.progress();
                $.post(sy.contextPath + '/arConfigImg/findById', {
                    id : id
                }, function(result) {
                    if (result) {
                        $('#img').setFileId(result.img,false,true,true);
                        $('form').form('load', result);
                    }
                    parent.$.messager.progress('close');
                }, 'json');
            }
        });
        function showamap() {
            var lat = $('#lat').textbox('getValue');
            var lng = $('#lng').textbox('getValue');
            sy.amap(retMap, lat, lng);
        }

        function retMap(ret) {
            if (ret.lat != "") {
                $('#lat').textbox('setValue',ret.lat);
                $('#lng').textbox('setValue',ret.lng);
            }
        }
	</script>
</head>
<body>
<form id="form" method="post">
	<input name="id" type="hidden" />
	<input name="arConfigId" type="hidden" />
	<div style="padding:15px;font-size: 12px">
		<table style="table-layout:fixed;margin-top: 10px;margin-bottom: 15px" border="0" cellspacing="0" class="formtable">
			<tr>
				<th style="width:100px;">目标图：</th>
				<td>
					<div id="img" multi="false" fileCountLimit='2' required="required" type="file" showImage="true" showBtn='true' bestSize='640*320' fileType="jpg,png,mp4" fileSize="200MB" buttonText="上传AR图片"></div>
				</td>
			</tr>
			<tr>
				<th style="width:100px;">次数：</th>
				<td>
					<input class="easyui-numberbox" style="width: 90%" data-options="required:true,min:-1,max:99999" name="num" missingMessage="请输入次数" prompt="请输入次数" id="num"  />
				</td>
			</tr>
			<tr>
				<th style="width:100px;">范围：</th>
				<td>
					<select class="easyui-combobox" name="isScope" id="isScope" editable="false" style="width: 90%">
						<option value="0">无</option>
						<option value="1" selected>有</option>
					</select>
				</td>
			</tr>
			<tr class="scope">
				<th style="width:100px;">纬度：</th>
				<td>
					<input class="easyui-textbox" style="width: 90%" data-options="required:true,validType:['length[0,20]']" name="lat" missingMessage="请输入纬度" prompt="请输入纬度" id="lat"  />
				</td>
			</tr>
			<tr class="scope">
				<th style="width:100px;">经度：</th>
				<td>
					<input class="easyui-textbox" style="width: 90%" data-options="required:true,validType:['length[0,20]']" name="lng" missingMessage="请输入经度" prompt="请输入经度" id="lng"  />
				</td>
			</tr>
			<tr class="scope">
				<th style="width:100px;">描点：</th>
				<td>
					<a class="btn btn-white btn-bitbucket" title="地图锚点" onclick="showamap()"><i class="fa fa-user-md"></i></a>
				</td>
			</tr>
		</table>
	</div>
</form>
</body>
</html>