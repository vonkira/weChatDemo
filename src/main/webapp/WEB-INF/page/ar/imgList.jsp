<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/static/admin/jsp/include.jsp"%>
<script type="text/javascript">
	var grid;
	var _id = '${id}';
    var addFun = function($dialog, $grid, $pjq) {
        var dialog = parent.sy.modalDialog({
            title : '添加AR图',
            width : 600,
            height : 500,
            url : sy.contextPath + '/go?path=ar/imgEdit&arConfigId='+_id,
            buttons : [ {
                text : '保存',
                handler : function() {
                    dialog.find('iframe').get(0).contentWindow.submitForm(dialog, grid, parent.$);
                }
            } ]
        });
    };

    var editFun = function() {
        var rows = grid.datagrid('getSelections');
        if (rows.length != 1) {
            parent.$.messager.w('请选择一条记录进行编辑！');
            return;
        }

        var dialog = parent.sy.modalDialog({
            title : '编辑AR图',
            width : 600,
            height : 500,
            url : sy.contextPath + '/go?path=ar/imgEdit&id=' + rows[0].id,
            buttons : [ {
                text : '保存',
                handler : function() {
                    dialog.find('iframe').get(0).contentWindow.submitForm(dialog, grid, parent.$);
                }
            } ]
        });
    };

    var delFun = function() {
        var rows = grid.datagrid('getSelections');
        if (rows.length == 0) {
            parent.$.messager.w('请选择需要删除的记录！');
            return;
        }
        parent.$.messager.confirm('询问', '您确定要删除此记录？', function(r) {
            if (r) {
                var ids = [];
                for ( var i = 0, l = rows.length; i < l; i++) {
                    var r = rows[i];
                    ids.push(r.id);
                }
                var id = ids.join(',');

                $.post(sy.contextPath + '/arConfigImg/del', {
                    id : id
                }, function(res) {
                    rows.length = 0;//必须，否则有bug
                    grid.datagrid('reload');
                }, 'json');
            }
        });
    };
	var typeStr = {
	    0:'图片',
	    1:'视频',
	    2:'语音',
	    3:'3D模型'
	};
	$(function() {
		grid = $('#grid').datagrid({
			url : sy.contextPath + '/arConfigImg/list?id='+_id,
			singleSelect : true,
			columns : [ [
				{
					width : $(this).width() * 0.5,
					title : '图片',
					field : 'img',
					align : 'center',
					formatter:function (v) {
						return po.showImg(v,20,20);
                    }
				},{
                    width : $(this).width() * 0.2,
                    title : '次数',
                    field : 'num',
                    align : 'center'
                },{
                    width : $(this).width() * 0.1,
                    title : '范围',
                    field : 'isScope',
                    align : 'center',
                    formatter:function (v) {
                        return v == 1?'有':'无';
                    }
                }
			] ]
		});
	});
    function forbidden(state, id) {
        if (state == 1) {

            parent.$.messager.confirm('询问', "确定启用此AR吗？", function(r) {
                if (r) {

                    var data = {
                        id : id,
                        state : 1
                    };
                    SaveData(data);
                }
            });
        } else if (state == 0) {
            parent.$.messager.confirm('询问', "确定禁用此AR吗？", function(r) {
                if (r) {
                    var data = {
                        id : id,
                        state : 0
                    };
                    SaveData(data);
                }
            });
        }
    }
    function SaveData(data) {
        var url = sy.contextPath + '/arConfig/save';
        $.post(url, data, function() {
            grid.datagrid('reload');
        }, 'json');
    }
</script>
</head>
<body>
	<div id="toolbar">
		<div class="tbbutton">
			<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-plus-circle',plain:true" onclick="addFun();">添加</a>
			<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-pencil',plain:true" onclick="editFun();">修改</a>
			<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-trash',plain:true" onclick="delFun();">删除</a>
		</div>
	</div>
	<table id="grid" data-options="fit:true,border:false"></table>
</body>
</html>