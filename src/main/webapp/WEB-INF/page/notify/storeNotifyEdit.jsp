<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/static/admin/jsp/include.jsp"%>
<script type="text/javascript">
	var id = "${id}";
	var submitForm = function($dialog, $grid, $pjq) {
		if ($('form').form('validate')) {
			var obj=sy.serializeObject($('form'));
			var url=sy.contextPath + '/storeNotify/save';
			$.post(url, obj, function(result) {
				if (result.code == 0) {
					$grid.datagrid('load');
					$dialog.dialog('destroy');
				} else {
					$pjq.messager.e('添加失败,'+result.msg);
				}
			}, 'json');
		}
	};
	
	$(function() {
		$('#type').combobox({   
			onSelect: function(rec){   
				if(rec.code == '1'){
					$('#account').hide();
					$('#storeId').textbox("reset");
					$('#storeId').textbox({required:false});
				}else{
					$('#account').show();
					$('#storeId').textbox({required:true});
				}
	        }
		});
		
		if (id != '') {
			parent.$.messager.progress({
				text : '数据加载中....'
			});
			$.post(sy.contextPath + '/storeNotify/findById', {
				id : id
			}, function(result) {
				if (result) {
					//系统消息
					var type = 1;
					if(result.storeId == null || result.storeId ==''){
						$('#account').hide();
						$('#storeId').textbox("reset");
						$('#storeId').textbox({required:false});
					}else{
						//个人消息
						type = 2;
						$('#account').show();
						$('#storeId').textbox({required:true});
					}
					$('#type').combobox('setValue', type);
					$('form').form('load', result);
				}
				parent.$.messager.progress('close');
			}, 'json');
		}
	});
</script>
</head>
<body>
	<form id="form" method="post">
        <input name="id" type="hidden" />
        <div style="padding:15px;font-size: 12px">
            <table style="table-layout:fixed;" border="0" cellspacing="0" class="formtable">
            	<tr>
            		<th style="width:100px;">类型:</th>
		    		<td>
		    			<select id="type" class="easyui-combobox" missingMessage="请选择类型" editable="false" panelHeight='auto' 
                    	 	data-options="required:true,valueField: 'code',textField: 'name',data: [{name: '系统消息',code: '1',selected:true},{name: '个人消息',code: '2'}]"/>
		    		</td>
		    	</tr>
		    	<tr id="account" style="display: none;">
		    		<th>门店ID：</th>
		    		<td>
		    			<input class="easyui-textbox" data-options="validType:['length[0,50]','exist[\'/store/isExist\']']" name="storeId" type="text" missingMessage="请输入门店ID" prompt="请输入门店ID" id="storeId"  />
		    		</td>
		    	</tr>
		    	<tr>
		    		<th>标题：</th>
		    		<td>
		    			<input class="easyui-textbox" id="title" class="easyui-validatebox" data-options="required:true,validType:['length[0,50]']" name="title" type="text" missingMessage="请输入标题" prompt="请输入标题"/>
		    		</td>
		    	</tr>
		       	<tr>
		       		<th>消息内容:</th>
		    		<td>
		    			<input id="content" name="content" class="easyui-textbox" style="height: 200px"  data-options="required:true,multiline:true,validType:['length[0,500]']" missingMessage="请输入消息内容" prompt="请输入消息内容"></input>
		    		</td>
		    	</tr>
            </table>
        </div>
	</form>
</body>
</html>