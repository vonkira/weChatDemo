<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/static/admin/jsp/include.jsp"%>
<script type="text/javascript">
	var id = "${id}";
	var submitForm = function($dialog, $grid, $pjq) {
		if ($('form').form('validate')) {
			var obj=sy.serializeObject($('form'));
			var url=sy.contextPath + '/coupon/save';
			$.post(url, obj, function(result) {
				if (result.code == 0) {
					$grid.datagrid('reload');
					$dialog.dialog('destroy');
				} else {
					$pjq.messager.e('添加失败,'+result.msg);
				}
			}, 'json');
		}
	};
	
	$(function() {
		if (id != '') {
			parent.$.messager.progress({
				text : '数据加载中....'
			});
			$.post(sy.contextPath + '/coupon/findById', {
				id : id
			}, function(result) {
				if (result) {
					$('form').form('load', result);
				}
				parent.$.messager.progress('close');
			}, 'json');

		}else{
		}
	});
</script>
</head>
<body>
	<form id="form" method="post">
        <input name="id" type="hidden" />
        <div style="padding:15px;font-size: 12px">
            <table style="table-layout:fixed;" border="0" cellspacing="0" class="formtable">
		    	<tr>
		    		<th style="width:100px;">满：</th>
					<td><input id="full" name="full" style="width: 90%"
						class="easyui-numberbox" precision="2" groupSeparator=","
						prefix="￥" data-options="required:true,min:0,max:99999999"
						missingMessage="请输入" prompt="请输入" /></td>
		    	</tr>
		    	<tr>
		    		<th style="width:100px;">立减：</th>
					<td><input id="value" name="value" style="width: 90%"
						class="easyui-numberbox" precision="2" groupSeparator=","
						prefix="￥" data-options="required:true,min:0.01,max:99999999"
						missingMessage="请输入立减" prompt="请输入立减" /></td>
		    	</tr>
		    	<tr>
		    		<th style="width:100px;">领取后有效天数：</th>
					<td><input id="days" name="days" style="width: 90%"
						class="easyui-numberbox" groupSeparator="," data-options="required:true,min:1,max:99999999"
						missingMessage="请输入有效天数" prompt="请输入有效天数" /></td>
		    	</tr>
            </table>
        </div>
	</form>
</body>
</html>