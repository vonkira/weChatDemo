<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/static/admin/jsp/include.jsp"%>
<script type="text/javascript">
	var grid;
	
	//下单操作
	var createOrder = function($dialog, $grid, $pjq) {
		var rows = grid.datagrid('getSelections');
		if (rows.length == 0) {
			parent.$.messager.w('请至少选择一条记录！');
			return;
		}
        var obj = sy.serializeObject($('#form'));
        if ($('#form').form('validate')) {
            parent.$.messager.confirm('询问', '您确定要下单采购吗？', function(r) {
                if (r) {
                    var idArray = [];
                    for ( var i = 0, l = rows.length; i < l; i++) {
                        var r = rows[i];
                        idArray.push(r.id);
                    }
                    var ids = idArray.join(',');
					obj.ids = ids;
                    $.post(sy.contextPath + '/goodsStock/saveOrder', obj, function(res) {
                        if (res.code == 0){
                            $grid.datagrid('reload');
                            $dialog.dialog('destroy');
						}else{
                            parent.$.messager.w(res.msg);
						}

                    }, 'json');
                }
            });
        }
	};
	
	//加入商品
	var addFun = function($dialog, $grid, $pjq) {
		var dialog = parent.sy.modalDialog({
			title : '加入商品',
			width : 1000,
			height : 600,
			url : sy.contextPath + '/go?path=car/buy/goodsList',
			buttons : [ {
				text : '关闭',
				handler : function() {
					dialog.dialog('destroy');
					//grid.datagrid('reload');
				}
			} ],
            onClose : function(){
				grid.datagrid('reload');
            }
		});
	};

	var editFun = function () {
        var rows = grid.datagrid('getSelections');
        if (rows.length != 1) {
            parent.$.messager.w('请选择一条记录进行编辑！');
            return;
        }
        var dialog = parent.sy.modalDialog({
            title : '编辑',
            width : 500,
            height : 300,
            url : sy.contextPath + '/go?path=car/buy/goodsEdit&id='+ rows[0].id,
            buttons : [ {
                text : '保存',
                handler : function() {
                    dialog.find('iframe').get(0).contentWindow.submitForm(dialog, grid, parent.$);
                }
            } ]
        });
    }
	
	var delFun = function() {
		var rows = grid.datagrid('getSelections');
		if (rows.length == 0) {
			parent.$.messager.w('请选择需要删除的记录！');
			return;
		}
		parent.$.messager.confirm('询问', '您确定要删除此记录？', function(r) {
			if (r) {
				var idArray = [];
				for ( var i = 0, l = rows.length; i < l; i++) {
					var r = rows[i];
					idArray.push(r.id);
				}
				var ids = idArray.join(',');
				
				$.post(sy.contextPath + '/goodsStock/delGoods', {
					ids : ids
				}, function(res) {
					rows.length = 0;//必须，否则有bug
					grid.datagrid('reload');
				}, 'json');
			}
		});
	};
	
	var importFun = function($dialog, $grid, $pjq) {
		var dialog = parent.sy.modalDialog({
			title : '通过模板导入数据',
			width : 600,
			height : 300,
			url : sy.contextPath + '/go?path=car/importData&type=7',
			buttons : [ {
				text : '导入',
				handler : function() {
					dialog.find('iframe').get(0).contentWindow.submitForm(dialog, grid, parent.$);
				}
			} ]
		});
	};
	
	$(function() {
		grid = $('#grid').datagrid({
			url : sy.contextPath + '/goodsStock/cartGoodsList',
			singleSelect : false,
            fitColumns : false,
			frozenColumns : [ [ {
				width : 100,
				checkbox:true,
				field : 'id',
				align : 'center'
			},{
                width : 100,
                title : '商品名称',
                field : 'goodsName',
                align : 'center',
                formatter: function(value, row, index) {
                    return UT.addTitle(value);
                }
            }
			] ],
			columns : [ [ 
				{
					width : 50,
					title : '商品编号',
					field : 'goodsCode',
					align : 'center',
                    formatter: function(value, row, index) {
                        return UT.addTitle(value);
                    }
				},{
					width : 50,
					title : '采购数量',
					field : 'num',
					align : 'center'
				},{
                    width : 50,
                    title : '单位',
                    field : 'unit',
                    align : 'center',
                    formatter: function(value, row, index) {
                        return UT.addTitle(value);
                    }
                },{
                    width : 50,
                    title : '采购成本',
                    field : 'purchaseCost',
                    align : 'center'
                },{
                    width : 50,
                    title : '税',
                    field : 'isTax',
                    align : 'center',
                    formatter:function (v) {
                        return v == 1?'是':'否';
                    }
                },{
                    width : 100,
                    title : '备注',
                    field : 'info',
                    align : 'center',
                    formatter: function(value, row, index) {
                        return UT.addTitle(value);
                    }
                },{
                    width : 100,
                    title : '供应商',
                    field : 'provide',
                    align : 'center',
                    formatter: function(value, row, index) {
                        return UT.addTitle(value);
                    }
                },{
                    width : 100,
                    title : '品牌',
                    field : 'brand',
                    align : 'center',
                    formatter: function(value, row, index) {
                        return UT.addTitle(value);
                    }
                },{
                    width : 100,
                    title : '规格',
                    field : 'standard',
                    align : 'center',
                    formatter: function(value, row, index) {
                        return UT.addTitle(value);
                    }
                },{
                    width : 100,
                    title : '型号',
                    field : 'model',
                    align : 'center',
                    formatter: function(value, row, index) {
                        return UT.addTitle(value);
                    }
                }
			] ]
		});
	});
</script>
</head>
<body class="easyui-layout">
		<div data-options="region:'north',border:false" style="height: 110px;">
			<form id="form" method="post"  >
				<div style="padding:15px;font-size: 12px">
					<table style="table-layout:fixed;" border="0" cellspacing="0" class="formtable">
						<tr>
							<th style="width:100px;">经办人：</th>
							<td>
								<input id="userName" name="userName" class="easyui-textbox" data-options="required:true,validType:['length[0,5]']" missingMessage="请输入经办人" style="width:90%;" prompt="请输入经办人"/>
							</td>
							<th style="width:100px;">结算方式：</th>
							<td>
								<input id="payType" name="payType" class="easyui-textbox" data-options="required:true,validType:['length[0,10]']" missingMessage="请输入结算方式" style="width:90%;" prompt="请输入结算方式"/>
							</td>
						</tr>
						<tr>
							<th style="width:100px;">备注：</th>
							<td colspan="3">
								<input id="info" name="info" class="easyui-textbox" data-options="validType:['length[0,100]']" missingMessage="请输入备注" style="width:90%;" prompt="请输入备注"/>
							</td>
						</tr>
					</table>
				</div>
			</form>
		</div>

		<div data-options="region:'center'">
			<div id="toolbar">
				<form id="searchForm">
					<div>
						<input type="text" class="easyui-textbox" name="name" style="width: 150px" prompt="商品名称/商品编号"/>
						<input type="text" class="easyui-textbox" name="brand" style="width: 150px" prompt="品牌"/>
						<input type="text" class="easyui-textbox" name="standard" style="width: 150px" prompt="规格"/>
						<input type="text" class="easyui-textbox" name="model" style="width: 150px" prompt="型号"/>
						<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-search'" onclick="grid.datagrid('load',sy.serializeObject($('#searchForm')));">过滤</a>
						<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-search-minus'" onclick="$('#searchForm input').val('');grid.datagrid('load',{});">重置过滤</a>
					</div>
				</form>
				<div class="tbbutton">
					<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-plus-circle',plain:true" onclick="addFun();">加入商品</a>
					<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-edit',plain:true" onclick="editFun();">编辑</a>
					<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-trash',plain:true" onclick="delFun();">删除</a>
					<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-file',plain:true" onclick="importFun();">导入</a>
				</div>
			</div>
			<table id="grid" data-options="fit:true,border:false"></table>
		</div>
</body>
</html>