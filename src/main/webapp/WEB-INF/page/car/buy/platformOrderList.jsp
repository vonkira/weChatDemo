<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/static/admin/jsp/include.jsp"%>
<script type="text/javascript">
	var grid;
	
	//平台采购车
	var addFun = function($dialog, $grid, $pjq) {
		var dialog = parent.sy.modalDialog({
			title : '采购商品',
			width : 1000,
			height : 600,
			url : sy.contextPath + '/go?path=car/buy/shoppingList',
			buttons : [ {
				text : '下单',
				handler : function() {
					dialog.find('iframe').get(0).contentWindow.createOrder(dialog, grid, parent.$);
				}
			} ]
		});
	};
	
	//取消订单、确认入库、查看订单
	var opFun = function(id, type) {
		if (type == 3) {
			var dialog = parent.sy.modalDialog({
				title : '查看采购订单--商品列表',
				width : 1000,
				height : 600,
				url : sy.contextPath + '/go?path=car/buy/detailList&orderId=' + id,
				buttons : [ {
					text : '关闭',
					handler : function() {
						dialog.dialog('destroy');
					}
				} ]
			});
		} else {
			var title;
			if (type == 1) {
				title = "您确定要取消该订单吗？";
			} else {
				title = "您确定要入库采购的商品吗？";
			}
			
			parent.$.messager.confirm('询问', title, function(r) {
				if (r) {
					$.post(sy.contextPath + '/goodsStock/editOrder', {
						id: id,
						type: type
					}, function(res) {
						if (res.code == 0) {
							grid.datagrid('reload');
						} else {
							parent.$.messager.e(res.msg);
						}
					}, 'json');
				}
			});
		}
	};
	
	$(function() {
        $('#provider').combobox({
            textField: 'title',
            valueField: 'id',
            url : sy.contextPath + '/goodsProvider/list.do?flag=1',
            onChange: function(data) {
                grid.datagrid('load',sy.serializeObject($('#searchForm')));
            }
        });
		grid = $('#grid').datagrid({
			url : sy.contextPath + '/goodsStock/list',
			columns : [ [
                {
                    width : 120,
                    title : '采购批次号',
                    field : 'code',
                    align : 'center',
                    formatter: function(value, row, index) {
                        return UT.addTitle(value);
                    }
                },
                {
                    width : 50,
                    title : '经办人',
                    field : 'userName',
                    align : 'center'
                },{
                    width : 50,
                    title : '结算方式',
                    field : 'payType',
                    align : 'center'
                },
                {
                    width : 50,
                    title : '供应商',
                    field : 'providerName',
                    align : 'center',
                    formatter: function(value, row, index) {
                        return UT.addTitle(value);
                    }
                },
				{
					width : 50,
					title : '采购商品数量',
					field : 'num',
					align : 'center'
				},{
                    width : 50,
                    title : '金额',
                    field : 'price',
                    align : 'center'
                },{
					width : 80,
					title : '订单状态',
					field : 'state',
					align : 'center',
					formatter: function(v, r, i) {
						switch(v) {
						case -1:
							return UT.addLabel('已取消', 'grey');
						case 1:
							return UT.addLabel('采购中', 'orange');
						case 2:
							return UT.addLabel('已入库', 'green');
						default:
							return "";
						}
					}
				},{
                    width : 150,
                    title : '备注',
                    field : 'info',
                    align : 'center',
                    formatter: function(value, row, index) {
                        return UT.addTitle(value);
                    }
                },{
					width : 120,
					title : '创建时间',
					field : 'createTime',
					align : 'center'
				},{
					width : 150,
					title : '操作',
					field : 'op',
					align : 'center',
					formatter:function(value, row, index){
						var html = "";
						html+='<a href="javascript:void(0);" onclick="opFun(\''+row.id+'\', 3)" class="button button-orange" title="查看">查看</a>';
						if (row.state == 1) {
							html+=' <a href="javascript:void(0);" onclick="opFun(\''+row.id+'\', 1)" class="button button-grey" title="取消订单">取消订单</a>';
							html+=' <a href="javascript:void(0);" onclick="opFun(\''+row.id+'\', 2)" class="button button-green" title="确认入库">确认入库</a>';
						}
						return html;
					}
				}
			] ]
		});
		
		$('#state').combobox({
			textField:'text',
			valueField:'value',
			data:[{text:'全部',value:0,selected:true},{text:'已取消',value:-1},{text:'采购中',value:1},{text:'已入库',value:2}],
			onChange: function() {
				grid.datagrid('load',sy.serializeObject($('#searchForm')));
			}
		});
	});
</script>
</head>
<body>
	<div id="toolbar">
		<form id="searchForm">
		<div>
			<span>订单状态：</span>
			<input id="state" name="state" editable="false" style="width:150px;" panelHeight='150' />
			<span>结算方式：</span>
			<input id="payType" name="payType"  style="width:150px;" class="easyui-textbox" />
			<span>供应商：</span>
			<input type="text" id="provider" editable="false" panelHeight='150'  name="provider" style="width: 150px" prompt="供应商"/>
			<span>开始时间：</span>
			<input class='easyui-datebox' id='beginTime' name='beginTime' data-options="width:180,editable:false" prompt="开始时间" />
			<span>结束时间：</span>
			<input class='easyui-datebox' id='endTime' name='endTime' data-options="width:180,editable:false" prompt="结束时间" />
			<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-search'" onclick="grid.datagrid('load',sy.serializeObject($('#searchForm')));">过滤</a> 
			<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-search-minus'" onclick="$('#searchForm input').val('');grid.datagrid('load',{});">重置过滤</a>
		</div>
		</form>
		<div class="tbbutton">
			<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-shopping-cart',plain:true" onclick="addFun();">采购车</a>
		</div>
	</div>
	<table id="grid" data-options="fit:true,border:false"></table>
</body>
</html>