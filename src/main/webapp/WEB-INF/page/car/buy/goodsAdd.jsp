<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/static/admin/jsp/include.jsp"%>
<script type="text/javascript">
	var id = "${id}";
	
	var submitForm = function($dialog, $grid, $pjq) {
		var obj = sy.serializeObject($('form'));
		if ($('form').form('validate')) {
            var url = sy.contextPath + '/goodsStock/saveGoods';
            $.post(url, obj, function() {
                $grid.datagrid('reload');
                $dialog.dialog('destroy');
                parent.$.messager.i("操作成功！");
            }, 'json');
		}
	};
	
	$(function() {
        $('#ids').val(id);
	});
</script>
</head>
<body>
	<form id="form" method="post">
        <input id="ids" name="ids" type="hidden" />
        <div style="padding:15px;font-size: 12px">
            <table style="table-layout:fixed;" border="0" cellspacing="0" class="formtable">
		    	<tr>
                    <th style="width:100px;">请输入采购数量：</th>
                    <td>
                    	 <input id="num" name="num" class="easyui-numberbox" data-options="required:true,min:1" missingMessage="请输入" style="width:90%;" prompt="请输入"/>
                    </td>
                </tr>
                <tr>
                    <th style="width:100px;">请输入采购成本：</th>
                    <td>
                    	 <input id="purchaseCost" name="purchaseCost" class="easyui-numberbox" data-options="required:true,precision:2" missingMessage="请输入" style="width:90%;" prompt="请输入"/>
                    </td>
                </tr>
                <tr>
                    <th style="width:100px;">是否含税：</th>
                    <td>
                        <input id="isTax" name="isTax" class="easyui-combobox" editable="false" panelHeight='auto'
                               data-options="required:true,valueField: 'code',textField: 'name',data: [{name: '不含税',code: '0',selected:true},{name: '含税',code: '1'}]"
                               missingMessage="请输入" style="width:90%;" prompt="请输入"/>
                    </td>
                </tr>
                <tr>
                    <th style="width:100px;">备注：</th>
                    <td>
                        <input id="info" name="info" class="easyui-textbox" data-options="validType:['length[0,100]']" missingMessage="请输入" style="width:90%;" prompt="请输入"/>
                    </td>
                </tr>
            </table>
        </div>
	</form>
</body>
</html>