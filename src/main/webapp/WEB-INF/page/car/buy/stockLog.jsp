<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/static/admin/jsp/include.jsp"%>
<script type="text/javascript">
	var id = '${id}';
	var grid;
	$(function() {

        grid = $('#grid').datagrid({
            url : sy.contextPath + '/goodsStockDetail/listByGoods?goodsId='+id,
            singleSelect : false,
            fitColumns : false,
            frozenColumns : [ [ {
                width : '150',
                title : '批次号',
                field : 'code',
                align : 'center'
            },{
                width : 80,
                title : '状态',
                field : 'state',
                align : 'center',
                formatter: function(v, r, i) {
                    switch(v) {
                        case -1:
                            return UT.addLabel('已取消', 'grey');
                        case 1:
                            return UT.addLabel('采购中', 'orange');
                        case 2:
                            return UT.addLabel('已入库', 'green');
                        default:
                            return "";
                    }
                }
            }
//            ,{
//                width : 100,
//                title : '商品名称',
//                field : 'goodsName',
//                align : 'center',
//                formatter: function(value, row, index) {
//                    return UT.addTitle(value);
//                }
//            }
            ] ],
            columns : [ [
                {
                    width : 50,
                    title : '商品编号',
                    field : 'goodsCode',
                    align : 'center',
                    formatter: function(value, row, index) {
                        return UT.addTitle(value);
                    }
                },{
                    width : 50,
                    title : '采购数量',
                    field : 'num',
                    align : 'center'
                },{
                    width : 50,
                    title : '单位',
                    field : 'unit',
                    align : 'center',
                    formatter: function(value, row, index) {
                        return UT.addTitle(value);
                    }
                },{
                    width : 50,
                    title : '采购成本',
                    field : 'purchaseCost',
                    align : 'center'
                },{
                    width : 50,
                    title : '税',
                    field : 'isTax',
                    align : 'center',
					formatter:function (v) {
						return v == 1?'是':'否';
                    }
                },{
                    width : 100,
                    title : '备注',
                    field : 'info',
                    align : 'center',
                    formatter: function(value, row, index) {
                        return UT.addTitle(value);
                    }
                },{
                    width : 100,
                    title : '供应商',
                    field : 'provide',
                    align : 'center',
                    formatter: function(value, row, index) {
                        return UT.addTitle(value);
                    }
                },{
                    width : 100,
                    title : '品牌',
                    field : 'brand',
                    align : 'center',
                    formatter: function(value, row, index) {
                        return UT.addTitle(value);
                    }
                },{
                    width : 100,
                    title : '规格',
                    field : 'standard',
                    align : 'center',
                    formatter: function(value, row, index) {
                        return UT.addTitle(value);
                    }
                },{
                    width : 100,
                    title : '型号',
                    field : 'model',
                    align : 'center',
                    formatter: function(value, row, index) {
                        return UT.addTitle(value);
                    }
                }
            ] ]
        });
	});
</script>
</head>
<body>
	<table id="grid" data-options="fit:true,border:false"></table>
</body>
</html>