<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/static/admin/jsp/include.jsp"%>
<script type="text/javascript">
	var orderId = "${orderId}";
	var grid;
	
	$(function() {
		grid = $('#grid').datagrid({
			url : sy.contextPath + '/goodsStock/queryOrder?orderId=' + orderId,
            fitColumns:false,
            frozenColumns:[
                [
                    {
                        width : 80,
                        title : '商品名称',
                        field : 'goodsName',
                        align : 'center',
                        formatter: function(value, row, index) {
                            return UT.addTitle(value);
                        }
                    },{
                    width : 80,
                    title : '商品编号',
                    field : 'goodsCode',
                    align : 'center'
                },{
                    width : 50,
                    title : '采购数量',
                    field : 'num',
                    align : 'center'
                },{
                    width : 30,
                    title : '单位',
                    field : 'unit',
                    align : 'center',
                    formatter: function(value, row, index) {
                        return UT.addTitle(value);
                    }
                }
				]
			],
			columns : [ [
                {
                    width : 50,
                    title : '采购成本',
                    field : 'purchaseCost',
                    align : 'center'
                },{
                    width : 50,
                    title : '税',
                    field : 'isTax',
                    align : 'center',
                    formatter:function (v) {
                        return v == 1?'是':'否';
                    }
                },{
                    width : 100,
                    title : '备注',
                    field : 'info',
                    align : 'center',
                    formatter: function(value, row, index) {
                        return UT.addTitle(value);
                    }
                },{
                    width : 80,
                    title : '供应商',
                    field : 'provide',
                    align : 'center',
                    formatter: function(value, row, index) {
                        return UT.addTitle(value);
                    }
                },
				{
                    width : 100,
                    title : '品牌',
                    field : 'brand',
                    align : 'center',
                    formatter: function(value, row, index) {
                        return UT.addTitle(value);
                    }
                },{
                    width : 100,
                    title : '规格',
                    field : 'standard',
                    align : 'center',
                    formatter: function(value, row, index) {
                        return UT.addTitle(value);
                    }
                },{
                    width : 100,
                    title : '型号',
                    field : 'model',
                    align : 'center',
                    formatter: function(value, row, index) {
                        return UT.addTitle(value);
                    }
                }
			] ]
		});
	});
</script>
</head>
<body>
	<div id="toolbar">
		<form id="searchForm">
		<div>
			<input type="text" class="easyui-textbox" name="name" style="width: 150px" prompt="商品名称/商品编号"/>
			<input type="text" class="easyui-textbox" name="brand" style="width: 150px" prompt="品牌"/>
			<input type="text" class="easyui-textbox" name="standard" style="width: 150px" prompt="规格"/>
			<input type="text" class="easyui-textbox" name="model" style="width: 150px" prompt="型号"/>
			<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-search'" onclick="grid.datagrid('load',sy.serializeObject($('#searchForm')));">过滤</a> 
			<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-search-minus'" onclick="$('#searchForm input').val('');grid.datagrid('load',{});">重置过滤</a>
		</div>
		</form>
	</div>
	<table id="grid" data-options="fit:true,border:false"></table>
</body>
</html>