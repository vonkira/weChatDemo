<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/static/admin/jsp/include.jsp"%>
<script type="text/javascript">
	var grid;
	
	//采购操作
	var buyFun = function(id) {
		var idArray = [];
		if (id == undefined || id == '') {
			var rows = grid.datagrid('getSelections');
			if (rows.length == 0) {
				parent.$.messager.w('请至少选择一条记录！');
				return;
			}
			for ( var i = 0, l = rows.length; i < l; i++) {
				var r = rows[i];
				idArray.push(r.id);
			}
		} else {
			idArray.push(id);
		}
		var ids = idArray.join(",");
		parent.$.messager.prompt(false, "请输入采购数量：", function(value){
			if (UT.IntNotZero(value)) {
				var url = sy.contextPath + '/goodsStock/saveGoods';
				$.post(url, {ids: ids, num: value}, function() {
					parent.$.messager.i("操作成功！");
				}, 'json');
			} else {
				parent.$.messager.w("请输入大于0的整数！");
			}
		});
	};

    //采购操作
    var buyFunSingle = function(id) {
        var dialog = parent.sy.modalDialog({
            title : '采购商品',
            width : 500,
            height : 300,
            url : sy.contextPath + '/go?path=car/buy/goodsAdd&id=' + id,
            buttons : [ {
                text : '确定',
                handler : function() {
                    dialog.find('iframe').get(0).contentWindow.submitForm(dialog, grid, parent.$);
                }
            } ]
        });
    };
	
	//查看商品详情
	var infoFun = function(cont) {
		layer.open({
			type: 2,
		    skin: 'layui-layer-rim', //加上边框
		    area: ['500px', '300px'], //宽高
		    content: sy.contextPath+'/detail?id='+cont+'&type=1' 
		}); 
	};
	
	$(function() {
		$('#cateId').combobox({
			textField: 'title',
			valueField: 'id',
			url : sy.contextPath + '/goodsCate/list.do?flag=1',
			onChange: function(data) {
				grid.datagrid('load',sy.serializeObject($('#searchForm')));
			}
		});
        $('#provider').combobox({
            textField: 'title',
            valueField: 'id',
            url : sy.contextPath + '/goodsProvider/list.do?flag=1',
            onChange: function(data) {
                grid.datagrid('load',sy.serializeObject($('#searchForm')));
            }
        });
		
		grid = $('#grid').datagrid({
			url : sy.contextPath + '/goods/list',
			singleSelect : false,
			frozenColumns : [ [ {
				width : '100',
				checkbox:true,
				field : 'id',
				align : 'center'
			},{
				width : 100,
				title : '商品名称',
				field : 'name',
				align : 'center',
				formatter: function(value, row, index) {
					return UT.addTitle(value);
				}
			},{
				width : 50,
				title : '商品编号',
				field : 'code',
				align : 'center',
                formatter: function(value, row, index) {
                    return UT.addTitle(value);
                }
			}
			] ],
			columns : [ [ 
				{
					width : $(this).width() * 0.1,
					title : '商品分类',
					field : 'cateName',
					align : 'center'
				},{
                    width : 100,
                    title : '供应商',
                    field : 'providerName',
                    align : 'center',
                    formatter: function(value, row, index) {
                        return UT.addTitle(value);
                    }
                },{
                    width : 100,
                    title : '品牌',
                    field : 'brandName',
                    align : 'center',
                    formatter: function(value, row, index) {
                        return UT.addTitle(value);
                    }
                },{
                    width : 100,
                    title : '规格',
                    field : 'standard',
                    align : 'center',
                    formatter: function(value, row, index) {
                        return UT.addTitle(value);
                    }
                },{
                    width : 100,
                    title : '型号',
                    field : 'model',
                    align : 'center',
                    formatter: function(value, row, index) {
                        return UT.addTitle(value);
                    }
                },{
                    width : 50,
                    title : '单位',
                    field : 'unit',
                    align : 'center',
                    formatter: function(value, row, index) {
                        return UT.addTitle(value);
                    }
                }
//                ,{
//					width : $(this).width() * 0.1,
//					title : '封面',
//					field : 'img',
//					align : 'center',
//					formatter : function(value, row, index) {
//						return po.showImg(value,18,18);
//					}
//				},{
//					width : $(this).width() * 0.2,
//					title : '组图',
//					field : 'imgs',
//					align : 'center',
//					formatter : function(value, row, index) {
//						return po.showImg(value,18,18);
//					}
//				},{
//					width : $(this).width() * 0.1,
//					title : '图文详情',
//					field : 'detail',
//					align : 'center',
//					formatter:function(v,r,i){
//						return "<a href=\"javascript:void(0)\" style=\"color:blue;\" onclick=\"infoFun(\'"+r.id+"\')\" >详情</a>";
//					}
//				},{
//					width : $(this).width() * 0.2,
//					title : '创建时间',
//					field : 'createTime',
//					align : 'center'
//				}
				,{
					width : $(this).width() * 0.15,
					title : '操作',
					field : 'op',
					align : 'center',
					formatter:function(v,r,i){
						var html = "";
						html+='<a href="javascript:void(0);" onclick="buyFunSingle(\''+r.id+'\')" class="button button-orange" title="加入采购">加入采购</a>';
						return html;
					}
				}
			] ]
		});
	});
</script>
</head>
<body>
	<div id="toolbar">
		<form id="searchForm">
		<div>
			<span>商品分类：</span>
			<input id="cateId" name="cateId" editable="false" style="width:150px;" panelHeight='150'
				data-options="valueField: 'id',textField: 'title'" />
			<span>商品名称：</span>
			<input type="text" class="easyui-textbox" name="name" style="width: 150px" prompt="商品名称"/>
			<span>商品编号：</span>
			<input type="text" class="easyui-textbox" name="code" style="width: 150px" prompt="商品编号"/>
		</div>
			<div style="margin-top: 5px;">
				<span>供应商：</span>
				<input type="text" id="provider" editable="false" panelHeight='150'  name="provider" style="width: 150px" prompt="供应商"/>
				<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-search'" onclick="grid.datagrid('load',sy.serializeObject($('#searchForm')));">过滤</a>
				<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-search-minus'" onclick="$('#searchForm input').val('');grid.datagrid('load',{});">重置过滤</a>
			</div>
		</form>
		<div class="tbbutton">
			<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-inbox',plain:true" onclick="buyFun();">批量采购</a>
		</div>
	</div>
	<table id="grid" data-options="fit:true,border:false"></table>
</body>
</html>