<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/static/admin/jsp/include.jsp"%>
<script type="text/javascript">
	var grid;
	
	var editFun = function() {
		var rows = grid.datagrid('getSelections');
		if (rows.length != 1) {
			parent.$.messager.w('请选择一条记录进行编辑！');
			return;
		}
		var dialog = parent.sy.modalDialog({
			title : '编辑信息',
			width : 500,
			height : 300,
			url : sy.contextPath + '/go?path=car/recommEdit&id=' + rows[0].id + '&goodsCode=' + rows[0].goodsCode,
			buttons : [ {
				text : '保存',
				handler : function() {
					dialog.find('iframe').get(0).contentWindow.submitForm(dialog, grid, parent.$);
				}
			} ]
		});
	};
	
	var delFun = function() {
		var rows = grid.datagrid('getSelections');
		if (rows.length == 0) {
			parent.$.messager.w('请选择需要删除的记录！');
			return;
		}
		parent.$.messager.confirm('询问', '您确定要删除此记录？', function(r) {
			if (r) {
				var ids = [];
				for ( var i = 0, l = rows.length; i < l; i++) {
					var r = rows[i];
					ids.push(r.id);
				}
				var id = ids.join(',');
				
				$.post(sy.contextPath + '/carCommend/del', {
					id : id
				}, function(res) {
					rows.length = 0;//必须，否则有bug
					grid.datagrid('reload');
				}, 'json');
			}
		});
	};
	
	var importFun = function($dialog, $grid, $pjq) {
		var dialog = parent.sy.modalDialog({
			title : '通过模板导入数据',
			width : 600,
			height : 300,
			url : sy.contextPath + '/go?path=car/importData&type=4',
			buttons : [ {
				text : '导入',
				handler : function() {
					dialog.find('iframe').get(0).contentWindow.submitForm(dialog, grid, parent.$);
				}
			} ]
		});
	};
	
	$(function() {
		grid = $('#grid').datagrid({
			url : sy.contextPath + '/carCommend/recommList',
			singleSelect : false,
			frozenColumns : [ [ {
				width : '100',
				checkbox:true,
				field : 'id',
				align : 'center'
			}
			] ],
			columns : [ [ 
				{
					width : $(this).width() * 0.2,
					title : '车型',
					field : 'typeName',
					align : 'center'
				},{
					width : $(this).width() * 0.2,
					title : '商品名称',
					field : 'goodsName',
					align : 'center',
					formatter: function(value, row, index) {
						return UT.addTitle(value);
					}
				},{
					width : $(this).width() * 0.2,
					title : '商品编号',
					field : 'goodsCode',
					align : 'center'
				},{
					width : $(this).width() * 0.1,
					title : '商品图片',
					field : 'goodsImg',
					align : 'center',
					formatter : function(value, row, index) {
						return po.showImg(value,18,18);
					}
				},{
					width : $(this).width() * 0.2,
					title : '保养类型',
					field : 'type',
					align : 'center',
					formatter: function(v, r, i) {
						if (v == 1) {
							return "小保养";
						} else if (v == 2) {
							return "大保养";
						}
					}
				},{
					width : $(this).width() * 0.4,
					title : '创建时间',
					field : 'createTime',
					align : 'center'
				}
			] ]
		});
		
		$('#type').combobox({
			textField:'text',
			valueField:'value',
			data:[{text:'全部',value:0,selected:true},{text:'小保养',value:1},{text:'大保养',value:2}],
			onChange: function(rec) {
				grid.datagrid('load',sy.serializeObject($('#searchForm')));
			}
		});
		$('#brandId').combobox({
			textField: 'name',
			valueField: 'id',
			groupField: 'firstLetter',
			url : sy.contextPath + '/carBrand/list.do?flag=1',
			onSelect: function(rec) {
				$('#serieId').combobox({
					disabled: false,
					url : sy.contextPath + '/carSeries/seriesList.do?brandId=' + rec.id
				});
			}
		});
		$('#serieId').combobox({
			textField: 'name',
			valueField: 'id',
			groupField: 'factory',
			disabled: true
		});
	});
</script>
</head>
<body>
	<div id="toolbar">
		<form id="searchForm">
		<div>
			<span>保养类型：</span>
			<input id="type" name="type" editable="false" style="width:150px;" panelHeight='150' />
			<span>汽车品牌：</span>
			<input id="brandId" name="brandId" style="width:150px;" panelHeight='150' />
			<span>汽车车系：</span>
			<input id="serieId" name="serieId" style="width:150px;" panelHeight='150' />
			<span>车型：</span>
			<input type="text" class="easyui-textbox" name="name" style="width: 150px" prompt="车型名称"/>
			<span>商品编号：</span>
			<input type="text" class="easyui-textbox" name="goodsCode" style="width: 150px" prompt="商品编号"/>
			<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-search'" onclick="grid.datagrid('load',sy.serializeObject($('#searchForm')));">过滤</a> 
			<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-search-minus'" onclick="$('#searchForm input').val('');grid.datagrid('load',{});">重置过滤</a>
		</div>
		</form>
		<div class="tbbutton">
			<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-pencil',plain:true" onclick="editFun();">修改</a>
			<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-trash',plain:true" onclick="delFun();">删除</a>
			<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-file',plain:true" onclick="importFun();">导入</a>
			<!-- <a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-key',plain:true" onclick="downloadFun();">下载导入模板</a> -->
		</div>
	</div>
	<table id="grid" data-options="fit:true,border:false"></table>
</body>
</html>