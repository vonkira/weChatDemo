<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/static/admin/jsp/include.jsp"%>
<script type="text/javascript">
	var id = "${id}";
	
	var submitForm = function($dialog, $grid, $pjq) {
		if ($('form').form('validate')) {
			var obj = sy.serializeObject($('form'));
			var url = sy.contextPath + '/carSeries/save.do';
			$.post(url, obj, function(result) {
				if (result.code == 0) {
					$grid.datagrid('reload');
					$dialog.dialog('destroy');
				} else {
					$pjq.messager.e('操作失败');
				}
			}, 'json');
		}
	};
	
	$(function() {
		$('#brandId').combobox({
			textField: 'name',
			valueField: 'id',
			url : sy.contextPath + '/carBrand/list.do?flag=1'
		});
		
		if (id != '') {
			parent.$.messager.progress({
				text : '数据加载中....'
			});
			$.post(sy.contextPath + '/carSeries/findById', {
				id: id
			}, function(result) {
				if (result) {
					$('form').form('load', result);
				}
				parent.$.messager.progress('close');
			}, 'json');
		}
	});
</script>
</head>
<body>
	<form id="form" method="post">
        <input name="id" type="hidden" />
        <div style="padding:15px;font-size: 12px">
            <table style="table-layout:fixed;" border="0" cellspacing="0" class="formtable">
            	<tr>
                    <th style="width:100px;">汽车品牌：</th>
                    <td>
                    	 <input id="brandId" name="brandId" class="easyui-combobox" data-options="panelHeight:'150',editable:false,required:true" style="width: 90%" />
                    </td>
                </tr>
		    	<tr>
                    <th style="width:100px;">首字母索引：</th>
                    <td>
                    	 <input id="firstLetter" name="firstLetter" class="easyui-textbox" data-options="required:true,validType:['length[1,1]']" missingMessage="请输入" style="width:90%;" prompt="请输入"/>
                    </td>
                </tr>
                <tr>
                    <th style="width:100px;">车系名称：</th>
                    <td>
                    	 <input id="name" name="name" class="easyui-textbox" data-options="required:true,validType:['length[0,20]']" missingMessage="请输入" style="width:90%;" prompt="请输入"/>
                    </td>
                </tr>
                <tr>
                    <th style="width:100px;">厂商：</th>
                    <td>
                    	 <input id="factory" name="factory" class="easyui-textbox" data-options="required:true,validType:['length[0,20]']" missingMessage="请输入" style="width:90%;" prompt="请输入"/>
                    </td>
                </tr>
            </table>
        </div>
	</form>
</body>
</html>