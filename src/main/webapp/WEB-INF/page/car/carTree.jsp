<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/static/admin/jsp/include.jsp"%>
<script type="text/javascript">
	var grid;
	var addFun = function($dialog, $grid, $pjq) {
		var rows = grid.datagrid('getSelections');
		if (rows.length != 1) {
			parent.$.messager.w('请选择一个父节点！');
			return;
		}
		if (rows[0].level > 3) {
			parent.$.messager.w('最多添加到第四级');
			return;
		}
		var dialog = parent.sy.modalDialog({
			title : '添加汽车品牌信息',
			width : 600,
			height : 400,
			url : sy.contextPath + '/go.do?path=car/brandEdit&parentCode='+rows[0].code,
			buttons : [ {
				text : '添加',
				handler : function() {
					dialog.find('iframe').get(0).contentWindow.submitForm(dialog, grid, parent.$);
				}
			} ]
		});
	};
	
	var editFun = function() {
		var rows = grid.datagrid('getSelections');
		if (rows.length != 1) {
			parent.$.messager.w('请选择一条记录进行编辑！');
			return;
		}
		if(rows[0].code == '0'){
			parent.$.messager.i('根节点无法编辑！');
			return;
		}
		var dialog = parent.sy.modalDialog({
			title : '编辑信息',
			width : 600,
			height : 400,
			url : sy.contextPath + '/go.do?path=car/brandEdit&code=' + rows[0].code,
			buttons : [ {
				text : '编辑',
				handler : function() {
					dialog.find('iframe').get(0).contentWindow.submitForm(dialog, grid, parent.$);
				}
			} ]
		});
	};
	
	var delFun = function() {
		var rows = grid.datagrid('getSelections');
		if (rows.length == 0) {
			parent.$.messager.w('请选择需要删除的记录！');
			return;
		}
		if(rows[0].code == '0'){
			parent.$.messager.w('根节点无法删除！');
			return;
		}
		parent.$.messager.confirm('询问', '您确定要删除此记录？', function(r) {
			if (r) {
				var ids = [];
				for ( var i = 0, l = rows.length; i < l; i++) {
					var r = rows[i];
					ids.push(r.code);
				}
				var id = ids.join(',');
				
				$.post(sy.contextPath + '/car/del.do', {
					code : id
				}, function() {
					grid.treegrid('reload',r.parent);
				}, 'json');
			}
		});
	};
	
	
	$(function() {
		grid = $('#grid').treegrid({
			url : sy.contextPath + '/car/tree',
			idField : 'code',
			treeField : 'title',
			parentField : 'parent',
			toolbar : '#toolbar',
			pagination : false,
			onLoadSuccess : function(row){
			},
			columns : [ [{
				width : $(this).width() * 0.2,
				title : '名称',
				align : 'left',
				field : 'title'
			},{
				width : $(this).width() * 0.2,
				title : '级别',
				align : 'center',
				field : 'level',
				formatter : function(value, row, index) {
					if(value == 1){
						return "品牌";
					}else if(value == 2){
						return "厂商";
					}else if(value == 3){
						return "车系";
					}else if(value == 4){
						return "车型";
					}
				}
			},{
				width : $(this).width() * 0.2,
				title : '编号',
				align : 'center',
				field : 'code'
			},{
				width : $(this).width() * 0.2,
				title : '图标',
				align : 'center',
				field : 'logo',
				formatter : function(value, row, index) {
					if(value!=null){
						return po.showImg(value,20,20);
					}else{
						return null;
					}
				}
			},{
				width : $(this).width() * 0.2,
				title : '排序号(倒序)',
				align : 'center',
				field : 'sortOrder'
			}
			]]
		});
	});
</script>
</head>
<body>
	<div id="toolbar">
		<div>
			<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-plus-circle',plain:true" onclick="addFun();">添加</a>
			<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-pencil',plain:true" onclick="editFun();">修改</a>
			<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-trash',plain:true" onclick="delFun();">删除</a>
		</div>
	</div>
	<table id="grid" data-options="fit:true,border:false"></table>
</body>
</html>