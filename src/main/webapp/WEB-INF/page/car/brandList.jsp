<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/static/admin/jsp/include.jsp"%>
<script type="text/javascript">
	var grid;
	
	var addFun = function($dialog, $grid, $pjq) {
		var dialog = parent.sy.modalDialog({
			title : '添加汽车品牌',
			width : 600,
			height : 400,
			url : sy.contextPath + '/go?path=car/brandEdit',
			buttons : [ {
				text : '保存',
				handler : function() {
					dialog.find('iframe').get(0).contentWindow.submitForm(dialog, grid, parent.$);
				}
			} ]
		});
	};
	
	var editFun = function() {
		var rows = grid.datagrid('getSelections');
		if (rows.length != 1) {
			parent.$.messager.w('请选择一条记录进行编辑！');
			return;
		}
		var dialog = parent.sy.modalDialog({
			title : '编辑信息',
			width : 600,
			height : 400,
			url : sy.contextPath + '/go?path=car/brandEdit&id=' + rows[0].id,
			buttons : [ {
				text : '保存',
				handler : function() {
					dialog.find('iframe').get(0).contentWindow.submitForm(dialog, grid, parent.$);
				}
			} ]
		});
	};
	
	var delFun = function() {
		var rows = grid.datagrid('getSelections');
		if (rows.length == 0) {
			parent.$.messager.w('请选择需要删除的记录！');
			return;
		}
		parent.$.messager.confirm('询问', '您确定要删除此记录？', function(r) {
			if (r) {
				var ids = [];
				for ( var i = 0, l = rows.length; i < l; i++) {
					var r = rows[i];
					ids.push(r.id);
				}
				var id = ids.join(',');
				
				$.post(sy.contextPath + '/carBrand/del', {
					id : id
				}, function(res) {
					rows.length = 0;//必须，否则有bug
					grid.datagrid('reload');
				}, 'json');
			}
		});
	};
	
	var importFun = function($dialog, $grid, $pjq) {
		var dialog = parent.sy.modalDialog({
			title : '通过模板导入数据',
			width : 600,
			height : 300,
			url : sy.contextPath + '/go?path=car/importData&type=1',
			buttons : [ {
				text : '导入',
				handler : function() {
					dialog.find('iframe').get(0).contentWindow.submitForm(dialog, grid, parent.$);
				}
			} ]
		});
	};
	
	var jumpFun = function(id) {
		window.location = sy.contextPath + '/go.do?path=car/seriesList&brandId=' + id;
	};
	
	$(function() {
		grid = $('#grid').datagrid({
			url : sy.contextPath + '/carBrand/list',
			singleSelect : false,
			frozenColumns : [ [ {
				width : '100',
				checkbox:true,
				field : 'id',
				align : 'center'
			}
			] ],
			columns : [ [ 
				{
					width : $(this).width() * 0.2,
					title : '首字母索引',
					field : 'firstLetter',
					align : 'center'
				},{
					width : $(this).width() * 0.2,
					title : '品牌名称',
					field : 'name',
					align : 'center',
					formatter: function(value, row, index) {
						var html = "";
						html+='<a href="javascript:void(0);" onclick="jumpFun(\''+row.id+'\')" style="color:blue;" title="'+value+'">'+value+'</a>';
						return html;
					}
				},{
					width : $(this).width() * 0.2,
					title : 'logo',
					field : 'logo',
					align : 'center',
					formatter : function(value, row, index) {
						return po.showImg(value,18,18);
					}
				},{
					width : $(this).width() * 0.3,
					title : '创建时间',
					field : 'createTime',
					align : 'center'
				}
			] ]
		});
	});
</script>
</head>
<body>
	<div id="toolbar">
		<form id="searchForm">
		<div>
			<span>首字母索引：</span>
			<input type="text" class="easyui-textbox" name="letter" style="width: 150px" prompt="首字母索引"/>
			<span>品牌名称：</span>
			<input type="text" class="easyui-textbox" name="name" style="width: 150px" prompt="品牌名称"/>
			<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-search'" onclick="grid.datagrid('load',sy.serializeObject($('#searchForm')));">过滤</a> 
			<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-search-minus'" onclick="$('#searchForm input').val('');grid.datagrid('load',{});">重置过滤</a>
		</div>
		</form>
		<div class="tbbutton">
			<!-- <a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-plus-circle',plain:true" onclick="addFun();">添加</a>
			<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-pencil',plain:true" onclick="editFun();">修改</a> -->
			<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-trash',plain:true" onclick="delFun();">删除</a>
			<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-file',plain:true" onclick="importFun();">导入</a>
			<!-- <a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-key',plain:true" onclick="downloadFun();">下载品牌导入模板</a> -->
		</div>
	</div>
	<table id="grid" data-options="fit:true,border:false"></table>
</body>
</html>