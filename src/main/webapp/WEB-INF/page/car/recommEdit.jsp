<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/static/admin/jsp/include.jsp"%>
<script type="text/javascript">
	var id = "${id}";
	var goodsCode = "${goodsCode}";
	
	var submitForm = function($dialog, $grid, $pjq) {
		var obj = sy.serializeObject($('form'));
		if ($('form').form('validate')) {
			var url = sy.contextPath + '/carCommend/update.do';
			$.post(url, obj, function(result) {
				if (result.code == 0) {
					$grid.datagrid('reload');
					$dialog.dialog('destroy');
				} else {
					$pjq.messager.e('操作失败');
				}
			}, 'json');
		}
	};
	
	$(function() {
		$('#goodsCode').combobox({
			textField: 'name',
			valueField: 'code',
			url : sy.contextPath + '/goods/selectRecommList.do?code=' + goodsCode
		});
		
		if (id != '') {
			parent.$.messager.progress({
				text : '数据加载中....'
			});
			$.post(sy.contextPath + '/carCommend/findById', {
				id : id
			}, function(result) {
				if (result) {
					$('form').form('load', result);
				}
				parent.$.messager.progress('close');
			}, 'json');
		}
	});
</script>
</head>
<body>
	<form id="form" method="post">
        <input name="id" type="hidden" />
        <div style="padding:15px;font-size: 12px">
            <table style="table-layout:fixed;" border="0" cellspacing="0" class="formtable">
		    	<tr>
                	<th style="width:100px;">选择商品：</th>
                	<td>
	                	<input id="goodsCode" name="goodsCode" class="easyui-combobox" style="width:90%" data-options="required:true,editable:false,panelHeight:'150'" />
	                </td>
                </tr>
            </table>
        </div>
	</form>
</body>
</html>