<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/static/admin/jsp/include.jsp"%>
<script type="text/javascript">
	var grid;
	var carType = "${carType}";
	var type = "${type}";
	
	var saveGoods = function($dialog, $grid, $pjq) {
		var rows = grid.datagrid('getSelections');
		if (rows.length == 0) {
			parent.$.messager.w('请选择需要添加的商品！');
			return;
		}
		var codeArray = [];
		for ( var i = 0, l = rows.length; i < l; i++) {
			var row = rows[i];
			codeArray.push(row.code);
		}
		var codes = codeArray.join(',');
		$.post(sy.contextPath + '/carCommend/saveGoods.do', {carType: carType, type: type, codes: codes}, function(result) {
			if (result.code == 0) {
				$grid.datagrid('reload');
				$dialog.dialog('destroy');
			} else {
				$pjq.messager.e('添加失败 ' + result.msg);
			}
		}, 'json');
	};
	
	$(function() {
		grid = $('#grid').datagrid({
			url : sy.contextPath + '/carCommend/goodsList?carType=' + carType + '&type=' + type,
			singleSelect : false,
			frozenColumns : [ [ {
				width : '100',
				checkbox:true,
				field : 'id',
				align : 'center'
			}] ],
			columns : [ [ 
				{
					width : $(this).width() * 0.1,
					title : '商品名称',
					field : 'name',
					align : 'center',
					formatter: function(value, row, index) {
						return UT.addTitle(value);
					}
				},{
					width : $(this).width() * 0.1,
					title : '商品编号',
					field : 'code',
					align : 'center'
				},{
					width : $(this).width() * 0.1,
					title : '封面',
					field : 'img',
					align : 'center',
					formatter : function(value, row, index) {
						return po.showImg(value,18,18);
					}
				},{
					width : $(this).width() * 0.2,
					title : '组图',
					field : 'imgs',
					align : 'center',
					formatter : function(value, row, index) {
						return po.showImg(value,18,18);
					}
				},{
					width : $(this).width() * 0.1,
					title : '库存',
					field : 'total',
					align : 'center'
				},{
					width : $(this).width() * 0.1,
					title : '已售',
					field : 'sellCount',
					align : 'center',
					formatter:function(v,r,i){
						if(r.sellBase) return r.sellBase+r.sellCount;
						return v;
					}
				},{
					width : $(this).width() * 0.4,
					title : '创建时间',
					field : 'createTime',
					align : 'center'
				}
			] ]
		});
	});
</script>
</head>
<body>
	<div id="toolbar">
		<form id="searchForm">
		<div>
			<span>商品名称：</span>
			<input type="text" class="easyui-textbox" name="name" style="width: 150px" prompt="商品名称"/>
			<span>商品编号：</span>
			<input type="text" class="easyui-textbox" name="code" style="width: 150px" prompt="商品编号"/>
			<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-search'" onclick="grid.datagrid('load',sy.serializeObject($('#searchForm')));">过滤</a> 
			<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-search-minus'" onclick="$('#searchForm input').val('');grid.datagrid('load',{});">重置过滤</a>
		</div>
		</form>
	</div>
	<table id="grid" data-options="fit:true,border:false"></table>
</body>
</html>