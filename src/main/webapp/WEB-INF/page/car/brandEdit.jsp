<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/static/admin/jsp/include.jsp"%>
<script type="text/javascript">
	var id = "${id}";
	
	var submitForm = function($dialog, $grid, $pjq) {
		var obj = sy.serializeObject($('form'));
		var logo = $('#logo').getFileId();
		if (logo == '') {
			$pjq.messager.e('请上传图标');
			return;
		}
		obj.logo = logo;
		if ($('form').form('validate')) {
			var url = sy.contextPath + '/carBrand/save.do';
			$.post(url, obj, function(result) {
				if (result.code == 0) {
					$grid.datagrid('reload');
					$dialog.dialog('destroy');
				} else {
					$pjq.messager.e('操作失败');
				}
			}, 'json');
		}
	};
	
	$(function() {
		if (id != '') {
			parent.$.messager.progress({
				text : '数据加载中....'
			});
			$.post(sy.contextPath + '/carBrand/findById', {
				id: id
			}, function(result) {
				if (result) {
					$("#logo").setFileId(result.logo,false,true,true);
					$('form').form('load', result);
				}
				parent.$.messager.progress('close');
			}, 'json');
		}
	});
</script>
</head>
<body>
	<form id="form" method="post">
        <input name="id" type="hidden" />
        <div style="padding:15px;font-size: 12px">
            <table style="table-layout:fixed;" border="0" cellspacing="0" class="formtable">
		    	<tr>
                    <th style="width:100px;">首字母索引：</th>
                    <td>
                    	 <input id="firstLetter" name="firstLetter" class="easyui-textbox" data-options="required:true,validType:['length[1,1]']" missingMessage="请输入" style="width:90%;" prompt="请输入"/>
                    </td>
                </tr>
                <tr>
                    <th style="width:100px;">品牌名称：</th>
                    <td>
                    	 <input id="name" name="name" class="easyui-textbox" data-options="required:true,validType:['length[0,20]']" missingMessage="请输入" style="width:90%;" prompt="请输入"/>
                    </td>
                </tr>
                <tr>
					<th style="width:100px;">图标：</th>
					<td>
						<div id="logo" multi="false" type="file" showImage="true" showBtn="true" bestSize="150*150" fileType="jpg,png" fileSize="2MB" buttonText="上传图标"></div>
                    	<span><span style="color: red;" >*</span>155*155</span>
					</td>
				</tr>
            </table>
        </div>
	</form>
</body>
</html>