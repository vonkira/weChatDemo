<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/static/admin/jsp/include.jsp"%>
<script type="text/javascript">
	var grid;

	var addFun = function($dialog, $grid, $pjq) {
		var carType = $("#carType").combotree("getValue");
		var type = $("#type").combobox("getValue");
		if (carType == "") {
			parent.$.messager.w('请选择车型！');
			return;
		}
		if (type == "" || type == 0) {
			parent.$.messager.w('请选择保养类型！');
			return;
		}
		var desc = (type == 1 ? "小保养" : "中保养");
		var dialog = parent.sy.modalDialog({
			title : '添加推荐产品--' + desc,
			width : 800,
			height : 500,
			url : sy.contextPath + '/go?path=car/goodsChooseList&carType=' + carType + "&type=" + type,
			buttons : [ {
				text : '添加',
				handler : function() {
					dialog.find('iframe').get(0).contentWindow.saveGoods(dialog, grid, parent.$);
				}
			} ]
		});
	};
	
	var delFun = function() {
		var rows = grid.datagrid('getSelections');
		if (rows.length == 0) {
			parent.$.messager.w('请选择需要删除的记录！');
			return;
		}
		parent.$.messager.confirm('询问', '您确定要删除此记录？', function(r) {
			if (r) {
				var ids = [];
				for ( var i = 0, l = rows.length; i < l; i++) {
					var r = rows[i];
					ids.push(r.id);
				}
				var id = ids.join(',');
				
				$.post(sy.contextPath + '/carCommend/del', {
					id : id
				}, function(res) {
					if (res.code == 0) {
						rows.length = 0;//必须，否则有bug
						grid.datagrid('reload');
					} else {
						parent.$.messager.w(res.msg);
					}
				}, 'json');
			}
		});
	};
	
	$(function() {
		grid = $('#grid').datagrid({
			url : sy.contextPath + '/carCommend/list',
			singleSelect : false,
			frozenColumns : [ [ {
				width : '100',
				checkbox:true,
				field : 'id',
				align : 'center'
			}] ],
			columns : [ [ 
				{
					width : $(this).width() * 0.2,
					title : '车型',
					field : 'title',
					align : 'center'
				},{
					width : $(this).width() * 0.2,
					title : '商品名称',
					field : 'goodsName',
					align : 'center'
				},{
					width : $(this).width() * 0.2,
					title : '保养类型',
					field : 'type',
					align : 'center',
					formatter: function(v, r, i) {
						if (v == 1) {
							return "小保养";
						} else if (v == 2) {
							return "大保养";
						}
					}
				},{
					width : $(this).width() * 0.4,
					title : '创建时间',
					field : 'createTime',
					align : 'center'
				}
			] ]
		});
		
		$('#carType').combotree({
			url:'car/list.do',
			idField : 'code',
			textField : 'title',
			parentField : 'parent',
			onBeforeSelect:function(rec){
				if(rec.level != 4){
					return false;
				}
			},
			onChange:function(newVal, oldVal){
				grid.datagrid('load',sy.serializeObject($('#searchForm')));
			}
		});
		$('#type').combobox({
			textField:'text',
			valueField:'value',
			data:[{text:'全部',value:0,selected:true},{text:'小保养',value:1},{text:'大保养',value:2}],
			onChange: function(rec) {
				grid.datagrid('load',sy.serializeObject($('#searchForm')));
			}
		});
	});
</script>
</head>
<body>
	<div id="toolbar">
		<form id="searchForm">
		<div>
			<span>车型：</span>
			<input id="carType" name="carType" editable="false" style="width:150px;" />
			<span>保养类型：</span>
			<input id="type" name="type" editable="false" style="width:150px;" panelHeight='150' />
			<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-search'" onclick="grid.datagrid('load',sy.serializeObject($('#searchForm')));">过滤</a> 
			<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-search-minus'" onclick="$('#searchForm input').val('');grid.datagrid('load',{});">重置过滤</a>
		</div>
		</form>
		<div class="tbbutton">
			<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-plus-circle',plain:true" onclick="addFun();">添加</a>
			<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-trash',plain:true" onclick="delFun();">删除</a>
			<!-- <a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-file-excel-o',plain:true" onclick="importFun();">导入</a> -->
		</div>
	</div>
	<table id="grid" data-options="fit:true,border:false"></table>
</body>
</html>