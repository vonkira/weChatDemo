<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/static/admin/jsp/include.jsp"%>
<script type="text/javascript">
	var code = "${code}";
	var parentCode = "${parentCode}";
	
	var submitForm = function($dialog, $grid, $pjq) {
		var obj = sy.serializeObject($('form'));
		if (parentCode.length == 9) {
			var logo = $('#logo').getFileId();
			if (logo == '') {
				$pjq.messager.e('请上传图标');
				return;
			}
			obj.logo = logo;
		}
		if ($('form').form('validate')) {
			var url = sy.contextPath + '/car/save.do';
			if(parentCode != ""){
				obj.parent = parentCode;
			}
			$.post(url, obj, function(result) {
				if (result.code == 0) {
					$grid.treegrid('reload', parentCode);
					$dialog.dialog('destroy');
				} else {
					$pjq.messager.e('操作失败');
				}
			}, 'json');
		}
	};
	
	$(function() {
		if (code != '') {
			parent.$.messager.progress({
				text : '数据加载中....'
			});
			$.post(sy.contextPath + '/car/findById', {
				code : code
			}, function(result) {
				if (result) {
					parentCode = result.parent;
					if(result.level == 4){
						$("#trLogo").show();
						$("#logo").setFileId(result.logo,false,true,true,"150x150");
					}
					$('form').form('load', result);
				}
				parent.$.messager.progress('close');
			}, 'json');
		}else{
			if(parentCode.length == 9){
				$("#trLogo").show();
			}
		}
	});
</script>
</head>
<body>
	<form id="form" method="post">
        <input name="code" type="hidden" />
        <div style="padding:15px;font-size: 12px">
            <table style="table-layout:fixed;" border="0" cellspacing="0" class="formtable">
		    	<tr>
                    <th style="width:100px;">名称：</th>
                    <td>
                    	 <input id="title" name="title" class="easyui-textbox" data-options="required:true,validType:['length[0,20]']" missingMessage="请输入名称" style="width:90%;" prompt="请输入名称"/>
                    </td>
                </tr>
                <tr>
                    <th style="width:100px;">排序号(倒序)：</th>
                    <td>   
                    	 <input id="sortOrder" name="sortOrder" class="easyui-numberbox" data-options="required:true,min:0,max:9999" value="" missingMessage="请输入排序号" prompt="请输入排序号" style="width:90%;"/>
                    </td>
                </tr>
                <tr id="trLogo" style="display: none;">
					<th style="width:100px;">图标：</th>
					<td>
						<div id="logo" multi="false" type="file" showImage="true" showBtn="true" bestSize="150*150" fileType="jpg,png" fileSize="2MB" buttonText="上传图标"></div>
                    	<span><span style="color: red;" >*</span>155*155</span>
					</td>
				</tr>
            </table>
        </div>
	</form>
</body>
</html>