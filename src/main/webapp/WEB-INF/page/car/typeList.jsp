<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/static/admin/jsp/include.jsp"%>
<script type="text/javascript">
	var brandId = "${brandId}";
	var serieId = "${serieId}";
	var firstFlag = true;
	var grid;
	
	var addFun = function($dialog, $grid, $pjq) {
		var dialog = parent.sy.modalDialog({
			title : '添加汽车车型',
			width : 500,
			height : 400,
			url : sy.contextPath + '/go?path=car/typeEdit',
			buttons : [ {
				text : '保存',
				handler : function() {
					dialog.find('iframe').get(0).contentWindow.submitForm(dialog, grid, parent.$);
				}
			} ]
		});
	};
	
	var editFun = function() {
		var rows = grid.datagrid('getSelections');
		if (rows.length != 1) {
			parent.$.messager.w('请选择一条记录进行编辑！');
			return;
		}
		var dialog = parent.sy.modalDialog({
			title : '编辑信息',
			width : 500,
			height : 400,
			url : sy.contextPath + '/go?path=car/typeEdit&id=' + rows[0].id,
			buttons : [ {
				text : '保存',
				handler : function() {
					dialog.find('iframe').get(0).contentWindow.submitForm(dialog, grid, parent.$);
				}
			} ]
		});
	};
	
	var delFun = function() {
		var rows = grid.datagrid('getSelections');
		if (rows.length == 0) {
			parent.$.messager.w('请选择需要删除的记录！');
			return;
		}
		parent.$.messager.confirm('询问', '您确定要删除此记录？', function(r) {
			if (r) {
				var ids = [];
				for ( var i = 0, l = rows.length; i < l; i++) {
					var r = rows[i];
					ids.push(r.id);
				}
				var id = ids.join(',');
				
				$.post(sy.contextPath + '/carType/del', {
					id : id
				}, function(res) {
					rows.length = 0;//必须，否则有bug
					grid.datagrid('reload');
				}, 'json');
			}
		});
	};
	
	var importFun = function($dialog, $grid, $pjq) {
		var dialog = parent.sy.modalDialog({
			title : '通过模板导入数据',
			width : 600,
			height : 300,
			url : sy.contextPath + '/go?path=car/importData&type=3',
			buttons : [ {
				text : '导入',
				handler : function() {
					dialog.find('iframe').get(0).contentWindow.submitForm(dialog, grid, parent.$);
				}
			} ]
		});
	};
	
	$(function() {
		$('#brandId').combobox({
			textField: 'name',
			valueField: 'id',
			groupField: 'firstLetter',
			url : sy.contextPath + '/carBrand/list.do?flag=1',
			onSelect: function(rec) {
				$('#serieId').combobox({
					disabled: false,
					url : sy.contextPath + '/carSeries/seriesList.do?brandId=' + rec.id,
					onLoadSuccess:function(data) {
						if (serieId != '' && firstFlag) {
							firstFlag = false;
							$('#serieId').combobox('setValue', serieId);
							grid.datagrid('load',sy.serializeObject($('#searchForm')));
						}
					}
				});
			}
		});
		$('#serieId').combobox({
			textField: 'name',
			valueField: 'id',
			groupField: 'factory',
			disabled: true
		});
		
		grid = $('#grid').datagrid({
			url : sy.contextPath + '/carType/list',
			singleSelect : false,
			frozenColumns : [ [ {
				width : '100',
				checkbox:true,
				field : 'id',
				align : 'center'
			},{
				width : '100',
				title : '汽车品牌',
				field : 'brand',
				align : 'center',
				formatter: function(value, row, index) {
					return UT.addTitle(value);
				}
			},{
				width : '100',
				title : '车系',
				field : 'serie',
				align : 'center',
				formatter: function(value, row, index) {
					return UT.addTitle(value);
				}
			}
			] ],
			columns : [ [ 
				/* {
					width : '100',
					title : '汽车厂商',
					field : 'facName',
					align : 'center',
					formatter: function(value, row, index) {
						return UT.addTitle(value);
					}
				}, */
				{
					width : '100',
					title : '车型',
					field : 'name',
					align : 'center',
					formatter: function(value, row, index) {
						return UT.addTitle(value);
					}
				},{
					width : '100',
					title : '汽车排量',
					field : 'engine',
					align : 'center',
					formatter: function(value, row, index) {
						return UT.addTitle(value);
					}
				},{
					width : '100',
					title : '年份',
					field : 'year',
					align : 'center',
					formatter: function(value, row, index) {
						return UT.addTitle(value);
					}
				},{
					width : '100',
					title : '创建时间',
					field : 'createTime',
					align : 'center'
				}
			] ]
		});
		
		if (brandId != '') {
			$('#brandId').combobox('setValue', brandId);
		}
	});
</script>
</head>
<body>
	<div id="toolbar">
		<form id="searchForm">
		<div>
			<span>汽车品牌：</span>
			<input id="brandId" name="brandId" style="width:150px;" panelHeight='150' />
			<span>汽车车系：</span>
			<input id="serieId" name="serieId" style="width:150px;" panelHeight='150' />
			<input type="text" class="easyui-textbox" name="name" style="width: 150px" prompt="车型名称/排量/年份"/>
			<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-search'" onclick="grid.datagrid('load',sy.serializeObject($('#searchForm')));">过滤</a> 
			<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-search-minus'" onclick="$('#searchForm input').val('');grid.datagrid('load',{});">重置过滤</a>
		</div>
		</form>
		<div class="tbbutton">
			<!-- <a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-plus-circle',plain:true" onclick="addFun();">添加</a>
			<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-pencil',plain:true" onclick="editFun();">修改</a> -->
			<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-trash',plain:true" onclick="delFun();">删除</a>
			<a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-file',plain:true" onclick="importFun();">导入</a>
			<!-- <a href="javascript:void(0);" class="easyui-linkbutton" data-options="iconCls:'fa-key',plain:true" onclick="downloadFun();">下载车型导入模板</a> -->
		</div>
	</div>
	<table id="grid" data-options="fit:true,border:false"></table>
</body>
</html>