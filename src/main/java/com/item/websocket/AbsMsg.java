package com.item.websocket;

public abstract class AbsMsg {

    protected String type;

    public String getType() {
        return setType();
    }

    public abstract String setType();
}
