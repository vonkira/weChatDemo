package com.item.websocket;

import com.base.util.JSONUtils;

public class TextMsg extends AbsMsg{

    public TextMsg(){};

    public TextMsg(String msg){
        this.msg = msg;
    }

    private String msg;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    @Override
    public String toString() {
        return JSONUtils.serialize(this);
    }

    @Override
    public String setType() {
        return "TEXT";
    }
}
