package com.item;

import com.base.CoreConstants;
import com.base.support.PropertySupport;

import java.math.BigDecimal;

public class ConstantsCode extends CoreConstants{

	public static final String WELCOME_IMG = "welcome@sys";//欢迎页
	public static final String OAUTH_LOGIN = "oauthLogin@sys";//第三方登录开关
	public static final String ICON_URL = SERVER_URL + "download?id=";
	public static final String SERVICE_CACHE = "serviceCache";
	public static final String PROJECT_NAME = PropertySupport.getProperty("project.name");
	public static final boolean MULTI_LOGIN = "true".equals(PropertySupport.getProperty("api.login.multi"))?true:false;

	public static final boolean DEBUG = "true".equals(PropertySupport.getProperty("api.debug"))?true:false;
	public static final boolean AJAX_DEBUG = "true".equals(PropertySupport.getProperty("api.ajax.debug"))?true:false;
	public static final String ENCRYPT = PropertySupport.getProperty("api.encrypt","DES");

	public static final BigDecimal min = new BigDecimal("0.01");
	public static final String MKM_ACT_CACHE_NAME = "fictitiousCache";
	public static final String API_SIGN_CACHE_NAME = "apiSignCache";

	public static final String SUCCESS = "SUCCESS";
}
