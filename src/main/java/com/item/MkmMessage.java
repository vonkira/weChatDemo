package com.item;

/**
 * 拍当网消息模板
 */
public enum MkmMessage {
    SYS_NOTIFY(1,"系统消息","{}",0),
    SHARE_ACT_NOTIFY(1,"系统消息","您已通过分享得到{}",0),
    H5_GAME_ACT_NOTIFY(1,"系统消息","您在游戏排名{},获得{}x{}",0),
    GET_FRIEND_GOODS_NOTIFY(2,"夺取提醒","您的宝贝{}被{}夺取!",0),
    ADD_FRIEND_NOTIFY(3,"添加提醒","用户{}({})想添加您为好友",0),
    DEL_FRIEND_NOTIFY(3,"解除提醒","用户{}({})与您解除好友",0),
    GET_GOODS_MESSAGE(2,"弹幕消息","恭喜{}获得{}x{}",0),
    ;

    /**
     * 消息类型1:系统通知
     */
    private int type;
    /**
     * 标题
     */
    private String title;
    /**
     * 内容模板
     */
    private String template;
    /**
     * 消息跳转类型
     */
    private int redirectType;

    MkmMessage(int type, String title, String template, int redirectType){
        this.type = type;
        this.title = title;
        this.template = template;
        this.redirectType = redirectType;
    }

    public int getRedirectType() {
        return redirectType;
    }

    public String getTemplate() {
        return template;
    }

    public String getTitle() {
        return title;
    }

    public int getType() {
        return type;
    }
}

