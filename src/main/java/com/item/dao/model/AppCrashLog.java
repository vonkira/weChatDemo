package com.item.dao.model;



/**
 *
 */
public class AppCrashLog {

	/**
	 *
	 */
	private Integer id;

	/**
	 *品牌
	 */
	private String brand;

	/**
	 *设备型号
	 */
	private String model;

	/**
	 *手机串码
	 */
	private String imei;

	/**
	 *错误信息
	 */
	private String error;

	/**
	 *
	 */
	private java.util.Date createTime;

	public void setId(Integer id) {
		this.id=id;
	}

	public Integer getId() {
		return id;
	}

	public void setBrand(String brand) {
		this.brand=brand == null ? brand : brand.trim();
	}

	public String getBrand() {
		return brand;
	}

	public void setModel(String model) {
		this.model=model == null ? model : model.trim();
	}

	public String getModel() {
		return model;
	}

	public void setImei(String imei) {
		this.imei=imei == null ? imei : imei.trim();
	}

	public String getImei() {
		return imei;
	}

	public void setError(String error) {
		this.error=error == null ? error : error.trim();
	}

	public String getError() {
		return error;
	}

	public void setCreateTime(java.util.Date createTime) {
		this.createTime=createTime;
	}

	public java.util.Date getCreateTime() {
		return createTime;
	}

}
