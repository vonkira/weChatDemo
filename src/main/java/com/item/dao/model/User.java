package com.item.dao.model;



/**
 *
 */
public class User {

	/**
	 *
	 */
	private Integer id;

	/**
	 *账户
	 */
	private String account;

	/**
	 *密码
	 */
	private String password;

	/**
	 *昵称
	 */
	private String nickName;

	/**
	 *头像
	 */
	private String headImg;

	/**
	 *性别 0:女 1:男 2:未知
	 */
	private Integer sex;

	/**
	 *金币
	 */
	private Integer coin;

	/**
	 *体力
	 */
	private Integer physicalStrength;

	/**
	 *最新体力消耗或恢复时间
	 */
	private java.util.Date lastStrengthTime;

	/**
	 *状态 1:启用 0:禁用
	 */
	private Integer state;

	/**
	 *上次登录时间
	 */
	private java.util.Date loginTime;

	/**
	 *是否开启附件的人1开启0关闭
	 */
	private Integer isNear;

	/**
	 *lat
	 */
	private String lat;

	/**
	 *lng
	 */
	private String lng;

	/**
	 *最近位置
	 */
	private String lastAddress;

	/**
	 *最近位置更新时间
	 */
	private java.util.Date lastPositionTime;

	/**
	 *最近开启宝箱的时间
	 */
	private java.util.Date lastGoodsTime;

	/**
	 *1已完善资料0否
	 */
	private Integer isComplete;

	/**
	 *邮箱
	 */
	private String email;

	/**
	 *城市
	 */
	private String city;

	/**
	 *生日
	 */
	private java.util.Date birthday;

	/**
	 *爱好
	 */
	private String interest;

	/**
	 *注册时间
	 */
	private java.util.Date createTime;

	/**
	 *更新时间
	 */
	private java.util.Date modifyTime;

	/**
	 *1已绑定
	 */
	private Integer isBindQq;

	/**
	 *1
	 */
	private Integer isBindWeixin;

	/**
	 *1
	 */
	private Integer isBindWeibo;

	/**
	 *积分（小号金币获得）
	 */
	private Integer credit;

	public void setId(Integer id) {
		this.id=id;
	}

	public Integer getId() {
		return id;
	}

	public void setAccount(String account) {
		this.account=account == null ? account : account.trim();
	}

	public String getAccount() {
		return account;
	}

	public void setPassword(String password) {
		this.password=password == null ? password : password.trim();
	}

	public String getPassword() {
		return password;
	}

	public void setNickName(String nickName) {
		this.nickName=nickName == null ? nickName : nickName.trim();
	}

	public String getNickName() {
		return nickName;
	}

	public void setHeadImg(String headImg) {
		this.headImg=headImg == null ? headImg : headImg.trim();
	}

	public String getHeadImg() {
		return headImg;
	}

	public void setSex(Integer sex) {
		this.sex=sex;
	}

	public Integer getSex() {
		return sex;
	}

	public void setCoin(Integer coin) {
		this.coin=coin;
	}

	public Integer getCoin() {
		return coin;
	}

	public void setPhysicalStrength(Integer physicalStrength) {
		this.physicalStrength=physicalStrength;
	}

	public Integer getPhysicalStrength() {
		return physicalStrength;
	}

	public void setLastStrengthTime(java.util.Date lastStrengthTime) {
		this.lastStrengthTime=lastStrengthTime;
	}

	public java.util.Date getLastStrengthTime() {
		return lastStrengthTime;
	}

	public void setState(Integer state) {
		this.state=state;
	}

	public Integer getState() {
		return state;
	}

	public void setLoginTime(java.util.Date loginTime) {
		this.loginTime=loginTime;
	}

	public java.util.Date getLoginTime() {
		return loginTime;
	}

	public void setIsNear(Integer isNear) {
		this.isNear=isNear;
	}

	public Integer getIsNear() {
		return isNear;
	}

	public void setLat(String lat) {
		this.lat=lat == null ? lat : lat.trim();
	}

	public String getLat() {
		return lat;
	}

	public void setLng(String lng) {
		this.lng=lng == null ? lng : lng.trim();
	}

	public String getLng() {
		return lng;
	}

	public void setLastAddress(String lastAddress) {
		this.lastAddress=lastAddress == null ? lastAddress : lastAddress.trim();
	}

	public String getLastAddress() {
		return lastAddress;
	}

	public void setLastPositionTime(java.util.Date lastPositionTime) {
		this.lastPositionTime=lastPositionTime;
	}

	public java.util.Date getLastPositionTime() {
		return lastPositionTime;
	}

	public void setLastGoodsTime(java.util.Date lastGoodsTime) {
		this.lastGoodsTime=lastGoodsTime;
	}

	public java.util.Date getLastGoodsTime() {
		return lastGoodsTime;
	}

	public void setIsComplete(Integer isComplete) {
		this.isComplete=isComplete;
	}

	public Integer getIsComplete() {
		return isComplete;
	}

	public void setEmail(String email) {
		this.email=email == null ? email : email.trim();
	}

	public String getEmail() {
		return email;
	}

	public void setCity(String city) {
		this.city=city == null ? city : city.trim();
	}

	public String getCity() {
		return city;
	}

	public void setBirthday(java.util.Date birthday) {
		this.birthday=birthday;
	}

	public java.util.Date getBirthday() {
		return birthday;
	}

	public void setInterest(String interest) {
		this.interest=interest == null ? interest : interest.trim();
	}

	public String getInterest() {
		return interest;
	}

	public void setCreateTime(java.util.Date createTime) {
		this.createTime=createTime;
	}

	public java.util.Date getCreateTime() {
		return createTime;
	}

	public void setModifyTime(java.util.Date modifyTime) {
		this.modifyTime=modifyTime;
	}

	public java.util.Date getModifyTime() {
		return modifyTime;
	}

	public void setIsBindQq(Integer isBindQq) {
		this.isBindQq=isBindQq;
	}

	public Integer getIsBindQq() {
		return isBindQq;
	}

	public void setIsBindWeixin(Integer isBindWeixin) {
		this.isBindWeixin=isBindWeixin;
	}

	public Integer getIsBindWeixin() {
		return isBindWeixin;
	}

	public void setIsBindWeibo(Integer isBindWeibo) {
		this.isBindWeibo=isBindWeibo;
	}

	public Integer getIsBindWeibo() {
		return isBindWeibo;
	}

	public void setCredit(Integer credit) {
		this.credit=credit;
	}

	public Integer getCredit() {
		return credit;
	}

}
