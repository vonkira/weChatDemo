package com.item.dao.model;



/**
 *
 */
public class Code {

	/**
	 *编号
	 */
	private String code;

	/**
	 *组
	 */
	private String type;

	/**
	 *值
	 */
	private String value;

	/**
	 *备注
	 */
	private String remark;

	public void setCode(String code) {
		this.code=code == null ? code : code.trim();
	}

	public String getCode() {
		return code;
	}

	public void setType(String type) {
		this.type=type == null ? type : type.trim();
	}

	public String getType() {
		return type;
	}

	public void setValue(String value) {
		this.value=value == null ? value : value.trim();
	}

	public String getValue() {
		return value;
	}

	public void setRemark(String remark) {
		this.remark=remark == null ? remark : remark.trim();
	}

	public String getRemark() {
		return remark;
	}

}
