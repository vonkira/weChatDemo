package com.item.dao.model;



/**
 *
 */
public class Admin {

	/**
	 *
	 */
	private Integer id;

	/**
	 *角色id
	 */
	private String roleCode;

	/**
	 *用户名
	 */
	private String account;

	/**
	 *密码
	 */
	private String password;

	/**
	 *状态:0:禁用1启用
	 */
	private Integer state;

	/**
	 *1管理员2代理商3商家
	 */
	private Integer type;

	/**
	 *更新时间
	 */
	private java.util.Date modifyTime;

	/**
	 *创建时间
	 */
	private java.util.Date createTime;

	/**
	 *用户权限表
	 */
	private String rights;

	/**
	 *
	 */
	private String name;

	/**
	 *组织id
	 */
	private Integer orgId;

	/**
	 *地区编号
	 */
	private String areaCode;

	/**
	 *
	 */
	private String phone;

	public void setId(Integer id) {
		this.id=id;
	}

	public Integer getId() {
		return id;
	}

	public void setRoleCode(String roleCode) {
		this.roleCode=roleCode == null ? roleCode : roleCode.trim();
	}

	public String getRoleCode() {
		return roleCode;
	}

	public void setAccount(String account) {
		this.account=account == null ? account : account.trim();
	}

	public String getAccount() {
		return account;
	}

	public void setPassword(String password) {
		this.password=password == null ? password : password.trim();
	}

	public String getPassword() {
		return password;
	}

	public void setState(Integer state) {
		this.state=state;
	}

	public Integer getState() {
		return state;
	}

	public void setType(Integer type) {
		this.type=type;
	}

	public Integer getType() {
		return type;
	}

	public void setModifyTime(java.util.Date modifyTime) {
		this.modifyTime=modifyTime;
	}

	public java.util.Date getModifyTime() {
		return modifyTime;
	}

	public void setCreateTime(java.util.Date createTime) {
		this.createTime=createTime;
	}

	public java.util.Date getCreateTime() {
		return createTime;
	}

	public void setRights(String rights) {
		this.rights=rights == null ? rights : rights.trim();
	}

	public String getRights() {
		return rights;
	}

	public void setName(String name) {
		this.name=name == null ? name : name.trim();
	}

	public String getName() {
		return name;
	}

	public void setOrgId(Integer orgId) {
		this.orgId=orgId;
	}

	public Integer getOrgId() {
		return orgId;
	}

	public void setAreaCode(String areaCode) {
		this.areaCode=areaCode == null ? areaCode : areaCode.trim();
	}

	public String getAreaCode() {
		return areaCode;
	}

	public void setPhone(String phone) {
		this.phone=phone == null ? phone : phone.trim();
	}

	public String getPhone() {
		return phone;
	}

}
