package com.item.dao.model;

import java.util.List;
import java.util.ArrayList;

public class UserExample {
	protected String orderByClause;

	protected boolean distinct;

	protected List<Criteria> oredCriteria;

	public UserExample(){
		oredCriteria = new ArrayList<Criteria>();
	}

	public void setOrderByClause(String orderByClause) {
		this.orderByClause = orderByClause;
	}

	public String getOrderByClause() {
		return orderByClause;
	}

	public void setDistinct(boolean distinct) {
		this.distinct = distinct;
	}

	public boolean isDistinct() {
		return distinct;
	}

	public List<Criteria> getOredCriteria() {
		return oredCriteria;
	}

	public void or(Criteria criteria) {
		oredCriteria.add(criteria);
	}

	public Criteria or() {
		Criteria criteria = createCriteriaInternal();
		oredCriteria.add(criteria);
		return criteria;
	}

	public Criteria createCriteria() {
		Criteria criteria = createCriteriaInternal();
		if (oredCriteria.size() == 0) {
			oredCriteria.add(criteria);
		}
		return criteria;
	}

	protected Criteria createCriteriaInternal() {
		Criteria criteria = new Criteria();
		return criteria;
	}

	public void clear() {
		oredCriteria.clear();
		orderByClause = null;
		distinct = false;
	}

	protected abstract static class GeneratedCriteria {
		protected List<Criterion> criteria;

		protected GeneratedCriteria() {
			super();
			criteria = new ArrayList<Criterion>();
		}

		public boolean isValid() {
			return criteria.size() > 0;
		}

		public List<Criterion> getAllCriteria() {
			return criteria;
		}

		public List<Criterion> getCriteria() {
			return criteria;
		}

		protected void addCriterion(String condition) {
			if (condition == null) {
				throw new RuntimeException("Value for condition cannot be null");
			}
			criteria.add(new Criterion(condition));
		}

		protected void addCriterion(String condition, Object value, String property) {
			if (value == null) {
				throw new RuntimeException("Value for " + property + " cannot be null");
			}
			criteria.add(new Criterion(condition, value));
		}

		protected void addCriterion(String condition, Object value1, Object value2, String property) {
			if (value1 == null || value2 == null) {
				throw new RuntimeException("Between values for " + property + " cannot be null");
			}
			criteria.add(new Criterion(condition, value1, value2));
		}

		public Criteria andIdIsNull() {
			addCriterion("id is null");
			return (Criteria) this;
		}

		public Criteria andIdIsNotNull() {
			addCriterion("id is not null");
			return (Criteria) this;
		}

		public Criteria andIdEqualTo(Integer value) {
			addCriterion("id =", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotEqualTo(Integer value) {
			addCriterion("id <>", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdGreaterThan(Integer value) {
			addCriterion("id >", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdGreaterThanOrEqualTo(Integer value) {
			addCriterion("id >=", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdLessThan(Integer value) {
			addCriterion("id <", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdLessThanOrEqualTo(Integer value) {
			addCriterion("id <=", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdIn(List<Integer> values) {
			addCriterion("id in", values, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotIn(List<Integer> values) {
			addCriterion("id not in", values, "id");
			return (Criteria) this;
		}

		public Criteria andIdBetween(Integer value1, Integer value2) {
			addCriterion("id between", value1, value2, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotBetween(Integer value1, Integer value2) {
			addCriterion("id not between", value1, value2, "id");
			return (Criteria) this;
		}

		public Criteria andAccountIsNull() {
			addCriterion("account is null");
			return (Criteria) this;
		}

		public Criteria andAccountIsNotNull() {
			addCriterion("account is not null");
			return (Criteria) this;
		}

		public Criteria andAccountEqualTo(String value) {
			addCriterion("account =", value, "account");
			return (Criteria) this;
		}

		public Criteria andAccountNotEqualTo(String value) {
			addCriterion("account <>", value, "account");
			return (Criteria) this;
		}

		public Criteria andAccountGreaterThan(String value) {
			addCriterion("account >", value, "account");
			return (Criteria) this;
		}

		public Criteria andAccountGreaterThanOrEqualTo(String value) {
			addCriterion("account >=", value, "account");
			return (Criteria) this;
		}

		public Criteria andAccountLessThan(String value) {
			addCriterion("account <", value, "account");
			return (Criteria) this;
		}

		public Criteria andAccountLessThanOrEqualTo(String value) {
			addCriterion("account <=", value, "account");
			return (Criteria) this;
		}

		public Criteria andAccountLike(String value) {
			addCriterion("account like", value, "account");
			return (Criteria) this;
		}

		public Criteria andAccountNotLike(String value) {
			addCriterion("account not like", value, "account");
			return (Criteria) this;
		}

		public Criteria andAccountIn(List<String> values) {
			addCriterion("account in", values, "account");
			return (Criteria) this;
		}

		public Criteria andAccountNotIn(List<String> values) {
			addCriterion("account not in", values, "account");
			return (Criteria) this;
		}

		public Criteria andAccountBetween(String value1, String value2) {
			addCriterion("account between", value1, value2, "account");
			return (Criteria) this;
		}

		public Criteria andAccountNotBetween(String value1, String value2) {
			addCriterion("account not between", value1, value2, "account");
			return (Criteria) this;
		}

		public Criteria andPasswordIsNull() {
			addCriterion("password is null");
			return (Criteria) this;
		}

		public Criteria andPasswordIsNotNull() {
			addCriterion("password is not null");
			return (Criteria) this;
		}

		public Criteria andPasswordEqualTo(String value) {
			addCriterion("password =", value, "password");
			return (Criteria) this;
		}

		public Criteria andPasswordNotEqualTo(String value) {
			addCriterion("password <>", value, "password");
			return (Criteria) this;
		}

		public Criteria andPasswordGreaterThan(String value) {
			addCriterion("password >", value, "password");
			return (Criteria) this;
		}

		public Criteria andPasswordGreaterThanOrEqualTo(String value) {
			addCriterion("password >=", value, "password");
			return (Criteria) this;
		}

		public Criteria andPasswordLessThan(String value) {
			addCriterion("password <", value, "password");
			return (Criteria) this;
		}

		public Criteria andPasswordLessThanOrEqualTo(String value) {
			addCriterion("password <=", value, "password");
			return (Criteria) this;
		}

		public Criteria andPasswordLike(String value) {
			addCriterion("password like", value, "password");
			return (Criteria) this;
		}

		public Criteria andPasswordNotLike(String value) {
			addCriterion("password not like", value, "password");
			return (Criteria) this;
		}

		public Criteria andPasswordIn(List<String> values) {
			addCriterion("password in", values, "password");
			return (Criteria) this;
		}

		public Criteria andPasswordNotIn(List<String> values) {
			addCriterion("password not in", values, "password");
			return (Criteria) this;
		}

		public Criteria andPasswordBetween(String value1, String value2) {
			addCriterion("password between", value1, value2, "password");
			return (Criteria) this;
		}

		public Criteria andPasswordNotBetween(String value1, String value2) {
			addCriterion("password not between", value1, value2, "password");
			return (Criteria) this;
		}

		public Criteria andNickNameIsNull() {
			addCriterion("nick_name is null");
			return (Criteria) this;
		}

		public Criteria andNickNameIsNotNull() {
			addCriterion("nick_name is not null");
			return (Criteria) this;
		}

		public Criteria andNickNameEqualTo(String value) {
			addCriterion("nick_name =", value, "nickName");
			return (Criteria) this;
		}

		public Criteria andNickNameNotEqualTo(String value) {
			addCriterion("nick_name <>", value, "nickName");
			return (Criteria) this;
		}

		public Criteria andNickNameGreaterThan(String value) {
			addCriterion("nick_name >", value, "nickName");
			return (Criteria) this;
		}

		public Criteria andNickNameGreaterThanOrEqualTo(String value) {
			addCriterion("nick_name >=", value, "nickName");
			return (Criteria) this;
		}

		public Criteria andNickNameLessThan(String value) {
			addCriterion("nick_name <", value, "nickName");
			return (Criteria) this;
		}

		public Criteria andNickNameLessThanOrEqualTo(String value) {
			addCriterion("nick_name <=", value, "nickName");
			return (Criteria) this;
		}

		public Criteria andNickNameLike(String value) {
			addCriterion("nick_name like", value, "nickName");
			return (Criteria) this;
		}

		public Criteria andNickNameNotLike(String value) {
			addCriterion("nick_name not like", value, "nickName");
			return (Criteria) this;
		}

		public Criteria andNickNameIn(List<String> values) {
			addCriterion("nick_name in", values, "nickName");
			return (Criteria) this;
		}

		public Criteria andNickNameNotIn(List<String> values) {
			addCriterion("nick_name not in", values, "nickName");
			return (Criteria) this;
		}

		public Criteria andNickNameBetween(String value1, String value2) {
			addCriterion("nick_name between", value1, value2, "nickName");
			return (Criteria) this;
		}

		public Criteria andNickNameNotBetween(String value1, String value2) {
			addCriterion("nick_name not between", value1, value2, "nickName");
			return (Criteria) this;
		}

		public Criteria andHeadImgIsNull() {
			addCriterion("head_img is null");
			return (Criteria) this;
		}

		public Criteria andHeadImgIsNotNull() {
			addCriterion("head_img is not null");
			return (Criteria) this;
		}

		public Criteria andHeadImgEqualTo(String value) {
			addCriterion("head_img =", value, "headImg");
			return (Criteria) this;
		}

		public Criteria andHeadImgNotEqualTo(String value) {
			addCriterion("head_img <>", value, "headImg");
			return (Criteria) this;
		}

		public Criteria andHeadImgGreaterThan(String value) {
			addCriterion("head_img >", value, "headImg");
			return (Criteria) this;
		}

		public Criteria andHeadImgGreaterThanOrEqualTo(String value) {
			addCriterion("head_img >=", value, "headImg");
			return (Criteria) this;
		}

		public Criteria andHeadImgLessThan(String value) {
			addCriterion("head_img <", value, "headImg");
			return (Criteria) this;
		}

		public Criteria andHeadImgLessThanOrEqualTo(String value) {
			addCriterion("head_img <=", value, "headImg");
			return (Criteria) this;
		}

		public Criteria andHeadImgLike(String value) {
			addCriterion("head_img like", value, "headImg");
			return (Criteria) this;
		}

		public Criteria andHeadImgNotLike(String value) {
			addCriterion("head_img not like", value, "headImg");
			return (Criteria) this;
		}

		public Criteria andHeadImgIn(List<String> values) {
			addCriterion("head_img in", values, "headImg");
			return (Criteria) this;
		}

		public Criteria andHeadImgNotIn(List<String> values) {
			addCriterion("head_img not in", values, "headImg");
			return (Criteria) this;
		}

		public Criteria andHeadImgBetween(String value1, String value2) {
			addCriterion("head_img between", value1, value2, "headImg");
			return (Criteria) this;
		}

		public Criteria andHeadImgNotBetween(String value1, String value2) {
			addCriterion("head_img not between", value1, value2, "headImg");
			return (Criteria) this;
		}

		public Criteria andSexIsNull() {
			addCriterion("sex is null");
			return (Criteria) this;
		}

		public Criteria andSexIsNotNull() {
			addCriterion("sex is not null");
			return (Criteria) this;
		}

		public Criteria andSexEqualTo(Integer value) {
			addCriterion("sex =", value, "sex");
			return (Criteria) this;
		}

		public Criteria andSexNotEqualTo(Integer value) {
			addCriterion("sex <>", value, "sex");
			return (Criteria) this;
		}

		public Criteria andSexGreaterThan(Integer value) {
			addCriterion("sex >", value, "sex");
			return (Criteria) this;
		}

		public Criteria andSexGreaterThanOrEqualTo(Integer value) {
			addCriterion("sex >=", value, "sex");
			return (Criteria) this;
		}

		public Criteria andSexLessThan(Integer value) {
			addCriterion("sex <", value, "sex");
			return (Criteria) this;
		}

		public Criteria andSexLessThanOrEqualTo(Integer value) {
			addCriterion("sex <=", value, "sex");
			return (Criteria) this;
		}

		public Criteria andSexIn(List<Integer> values) {
			addCriterion("sex in", values, "sex");
			return (Criteria) this;
		}

		public Criteria andSexNotIn(List<Integer> values) {
			addCriterion("sex not in", values, "sex");
			return (Criteria) this;
		}

		public Criteria andSexBetween(Integer value1, Integer value2) {
			addCriterion("sex between", value1, value2, "sex");
			return (Criteria) this;
		}

		public Criteria andSexNotBetween(Integer value1, Integer value2) {
			addCriterion("sex not between", value1, value2, "sex");
			return (Criteria) this;
		}

		public Criteria andCoinIsNull() {
			addCriterion("coin is null");
			return (Criteria) this;
		}

		public Criteria andCoinIsNotNull() {
			addCriterion("coin is not null");
			return (Criteria) this;
		}

		public Criteria andCoinEqualTo(Integer value) {
			addCriterion("coin =", value, "coin");
			return (Criteria) this;
		}

		public Criteria andCoinNotEqualTo(Integer value) {
			addCriterion("coin <>", value, "coin");
			return (Criteria) this;
		}

		public Criteria andCoinGreaterThan(Integer value) {
			addCriterion("coin >", value, "coin");
			return (Criteria) this;
		}

		public Criteria andCoinGreaterThanOrEqualTo(Integer value) {
			addCriterion("coin >=", value, "coin");
			return (Criteria) this;
		}

		public Criteria andCoinLessThan(Integer value) {
			addCriterion("coin <", value, "coin");
			return (Criteria) this;
		}

		public Criteria andCoinLessThanOrEqualTo(Integer value) {
			addCriterion("coin <=", value, "coin");
			return (Criteria) this;
		}

		public Criteria andCoinIn(List<Integer> values) {
			addCriterion("coin in", values, "coin");
			return (Criteria) this;
		}

		public Criteria andCoinNotIn(List<Integer> values) {
			addCriterion("coin not in", values, "coin");
			return (Criteria) this;
		}

		public Criteria andCoinBetween(Integer value1, Integer value2) {
			addCriterion("coin between", value1, value2, "coin");
			return (Criteria) this;
		}

		public Criteria andCoinNotBetween(Integer value1, Integer value2) {
			addCriterion("coin not between", value1, value2, "coin");
			return (Criteria) this;
		}

		public Criteria andPhysicalStrengthIsNull() {
			addCriterion("physical_strength is null");
			return (Criteria) this;
		}

		public Criteria andPhysicalStrengthIsNotNull() {
			addCriterion("physical_strength is not null");
			return (Criteria) this;
		}

		public Criteria andPhysicalStrengthEqualTo(Integer value) {
			addCriterion("physical_strength =", value, "physicalStrength");
			return (Criteria) this;
		}

		public Criteria andPhysicalStrengthNotEqualTo(Integer value) {
			addCriterion("physical_strength <>", value, "physicalStrength");
			return (Criteria) this;
		}

		public Criteria andPhysicalStrengthGreaterThan(Integer value) {
			addCriterion("physical_strength >", value, "physicalStrength");
			return (Criteria) this;
		}

		public Criteria andPhysicalStrengthGreaterThanOrEqualTo(Integer value) {
			addCriterion("physical_strength >=", value, "physicalStrength");
			return (Criteria) this;
		}

		public Criteria andPhysicalStrengthLessThan(Integer value) {
			addCriterion("physical_strength <", value, "physicalStrength");
			return (Criteria) this;
		}

		public Criteria andPhysicalStrengthLessThanOrEqualTo(Integer value) {
			addCriterion("physical_strength <=", value, "physicalStrength");
			return (Criteria) this;
		}

		public Criteria andPhysicalStrengthIn(List<Integer> values) {
			addCriterion("physical_strength in", values, "physicalStrength");
			return (Criteria) this;
		}

		public Criteria andPhysicalStrengthNotIn(List<Integer> values) {
			addCriterion("physical_strength not in", values, "physicalStrength");
			return (Criteria) this;
		}

		public Criteria andPhysicalStrengthBetween(Integer value1, Integer value2) {
			addCriterion("physical_strength between", value1, value2, "physicalStrength");
			return (Criteria) this;
		}

		public Criteria andPhysicalStrengthNotBetween(Integer value1, Integer value2) {
			addCriterion("physical_strength not between", value1, value2, "physicalStrength");
			return (Criteria) this;
		}

		public Criteria andLastStrengthTimeIsNull() {
			addCriterion("last_strength_time is null");
			return (Criteria) this;
		}

		public Criteria andLastStrengthTimeIsNotNull() {
			addCriterion("last_strength_time is not null");
			return (Criteria) this;
		}

		public Criteria andLastStrengthTimeEqualTo(java.util.Date value) {
			addCriterion("last_strength_time =", value, "lastStrengthTime");
			return (Criteria) this;
		}

		public Criteria andLastStrengthTimeNotEqualTo(java.util.Date value) {
			addCriterion("last_strength_time <>", value, "lastStrengthTime");
			return (Criteria) this;
		}

		public Criteria andLastStrengthTimeGreaterThan(java.util.Date value) {
			addCriterion("last_strength_time >", value, "lastStrengthTime");
			return (Criteria) this;
		}

		public Criteria andLastStrengthTimeGreaterThanOrEqualTo(java.util.Date value) {
			addCriterion("last_strength_time >=", value, "lastStrengthTime");
			return (Criteria) this;
		}

		public Criteria andLastStrengthTimeLessThan(java.util.Date value) {
			addCriterion("last_strength_time <", value, "lastStrengthTime");
			return (Criteria) this;
		}

		public Criteria andLastStrengthTimeLessThanOrEqualTo(java.util.Date value) {
			addCriterion("last_strength_time <=", value, "lastStrengthTime");
			return (Criteria) this;
		}

		public Criteria andLastStrengthTimeIn(List<java.util.Date> values) {
			addCriterion("last_strength_time in", values, "lastStrengthTime");
			return (Criteria) this;
		}

		public Criteria andLastStrengthTimeNotIn(List<java.util.Date> values) {
			addCriterion("last_strength_time not in", values, "lastStrengthTime");
			return (Criteria) this;
		}

		public Criteria andLastStrengthTimeBetween(java.util.Date value1, java.util.Date value2) {
			addCriterion("last_strength_time between", value1, value2, "lastStrengthTime");
			return (Criteria) this;
		}

		public Criteria andLastStrengthTimeNotBetween(java.util.Date value1, java.util.Date value2) {
			addCriterion("last_strength_time not between", value1, value2, "lastStrengthTime");
			return (Criteria) this;
		}

		public Criteria andStateIsNull() {
			addCriterion("state is null");
			return (Criteria) this;
		}

		public Criteria andStateIsNotNull() {
			addCriterion("state is not null");
			return (Criteria) this;
		}

		public Criteria andStateEqualTo(Integer value) {
			addCriterion("state =", value, "state");
			return (Criteria) this;
		}

		public Criteria andStateNotEqualTo(Integer value) {
			addCriterion("state <>", value, "state");
			return (Criteria) this;
		}

		public Criteria andStateGreaterThan(Integer value) {
			addCriterion("state >", value, "state");
			return (Criteria) this;
		}

		public Criteria andStateGreaterThanOrEqualTo(Integer value) {
			addCriterion("state >=", value, "state");
			return (Criteria) this;
		}

		public Criteria andStateLessThan(Integer value) {
			addCriterion("state <", value, "state");
			return (Criteria) this;
		}

		public Criteria andStateLessThanOrEqualTo(Integer value) {
			addCriterion("state <=", value, "state");
			return (Criteria) this;
		}

		public Criteria andStateIn(List<Integer> values) {
			addCriterion("state in", values, "state");
			return (Criteria) this;
		}

		public Criteria andStateNotIn(List<Integer> values) {
			addCriterion("state not in", values, "state");
			return (Criteria) this;
		}

		public Criteria andStateBetween(Integer value1, Integer value2) {
			addCriterion("state between", value1, value2, "state");
			return (Criteria) this;
		}

		public Criteria andStateNotBetween(Integer value1, Integer value2) {
			addCriterion("state not between", value1, value2, "state");
			return (Criteria) this;
		}

		public Criteria andLoginTimeIsNull() {
			addCriterion("login_time is null");
			return (Criteria) this;
		}

		public Criteria andLoginTimeIsNotNull() {
			addCriterion("login_time is not null");
			return (Criteria) this;
		}

		public Criteria andLoginTimeEqualTo(java.util.Date value) {
			addCriterion("login_time =", value, "loginTime");
			return (Criteria) this;
		}

		public Criteria andLoginTimeNotEqualTo(java.util.Date value) {
			addCriterion("login_time <>", value, "loginTime");
			return (Criteria) this;
		}

		public Criteria andLoginTimeGreaterThan(java.util.Date value) {
			addCriterion("login_time >", value, "loginTime");
			return (Criteria) this;
		}

		public Criteria andLoginTimeGreaterThanOrEqualTo(java.util.Date value) {
			addCriterion("login_time >=", value, "loginTime");
			return (Criteria) this;
		}

		public Criteria andLoginTimeLessThan(java.util.Date value) {
			addCriterion("login_time <", value, "loginTime");
			return (Criteria) this;
		}

		public Criteria andLoginTimeLessThanOrEqualTo(java.util.Date value) {
			addCriterion("login_time <=", value, "loginTime");
			return (Criteria) this;
		}

		public Criteria andLoginTimeIn(List<java.util.Date> values) {
			addCriterion("login_time in", values, "loginTime");
			return (Criteria) this;
		}

		public Criteria andLoginTimeNotIn(List<java.util.Date> values) {
			addCriterion("login_time not in", values, "loginTime");
			return (Criteria) this;
		}

		public Criteria andLoginTimeBetween(java.util.Date value1, java.util.Date value2) {
			addCriterion("login_time between", value1, value2, "loginTime");
			return (Criteria) this;
		}

		public Criteria andLoginTimeNotBetween(java.util.Date value1, java.util.Date value2) {
			addCriterion("login_time not between", value1, value2, "loginTime");
			return (Criteria) this;
		}

		public Criteria andIsNearIsNull() {
			addCriterion("is_near is null");
			return (Criteria) this;
		}

		public Criteria andIsNearIsNotNull() {
			addCriterion("is_near is not null");
			return (Criteria) this;
		}

		public Criteria andIsNearEqualTo(Integer value) {
			addCriterion("is_near =", value, "isNear");
			return (Criteria) this;
		}

		public Criteria andIsNearNotEqualTo(Integer value) {
			addCriterion("is_near <>", value, "isNear");
			return (Criteria) this;
		}

		public Criteria andIsNearGreaterThan(Integer value) {
			addCriterion("is_near >", value, "isNear");
			return (Criteria) this;
		}

		public Criteria andIsNearGreaterThanOrEqualTo(Integer value) {
			addCriterion("is_near >=", value, "isNear");
			return (Criteria) this;
		}

		public Criteria andIsNearLessThan(Integer value) {
			addCriterion("is_near <", value, "isNear");
			return (Criteria) this;
		}

		public Criteria andIsNearLessThanOrEqualTo(Integer value) {
			addCriterion("is_near <=", value, "isNear");
			return (Criteria) this;
		}

		public Criteria andIsNearIn(List<Integer> values) {
			addCriterion("is_near in", values, "isNear");
			return (Criteria) this;
		}

		public Criteria andIsNearNotIn(List<Integer> values) {
			addCriterion("is_near not in", values, "isNear");
			return (Criteria) this;
		}

		public Criteria andIsNearBetween(Integer value1, Integer value2) {
			addCriterion("is_near between", value1, value2, "isNear");
			return (Criteria) this;
		}

		public Criteria andIsNearNotBetween(Integer value1, Integer value2) {
			addCriterion("is_near not between", value1, value2, "isNear");
			return (Criteria) this;
		}

		public Criteria andLatIsNull() {
			addCriterion("lat is null");
			return (Criteria) this;
		}

		public Criteria andLatIsNotNull() {
			addCriterion("lat is not null");
			return (Criteria) this;
		}

		public Criteria andLatEqualTo(String value) {
			addCriterion("lat =", value, "lat");
			return (Criteria) this;
		}

		public Criteria andLatNotEqualTo(String value) {
			addCriterion("lat <>", value, "lat");
			return (Criteria) this;
		}

		public Criteria andLatGreaterThan(String value) {
			addCriterion("lat >", value, "lat");
			return (Criteria) this;
		}

		public Criteria andLatGreaterThanOrEqualTo(String value) {
			addCriterion("lat >=", value, "lat");
			return (Criteria) this;
		}

		public Criteria andLatLessThan(String value) {
			addCriterion("lat <", value, "lat");
			return (Criteria) this;
		}

		public Criteria andLatLessThanOrEqualTo(String value) {
			addCriterion("lat <=", value, "lat");
			return (Criteria) this;
		}

		public Criteria andLatLike(String value) {
			addCriterion("lat like", value, "lat");
			return (Criteria) this;
		}

		public Criteria andLatNotLike(String value) {
			addCriterion("lat not like", value, "lat");
			return (Criteria) this;
		}

		public Criteria andLatIn(List<String> values) {
			addCriterion("lat in", values, "lat");
			return (Criteria) this;
		}

		public Criteria andLatNotIn(List<String> values) {
			addCriterion("lat not in", values, "lat");
			return (Criteria) this;
		}

		public Criteria andLatBetween(String value1, String value2) {
			addCriterion("lat between", value1, value2, "lat");
			return (Criteria) this;
		}

		public Criteria andLatNotBetween(String value1, String value2) {
			addCriterion("lat not between", value1, value2, "lat");
			return (Criteria) this;
		}

		public Criteria andLngIsNull() {
			addCriterion("lng is null");
			return (Criteria) this;
		}

		public Criteria andLngIsNotNull() {
			addCriterion("lng is not null");
			return (Criteria) this;
		}

		public Criteria andLngEqualTo(String value) {
			addCriterion("lng =", value, "lng");
			return (Criteria) this;
		}

		public Criteria andLngNotEqualTo(String value) {
			addCriterion("lng <>", value, "lng");
			return (Criteria) this;
		}

		public Criteria andLngGreaterThan(String value) {
			addCriterion("lng >", value, "lng");
			return (Criteria) this;
		}

		public Criteria andLngGreaterThanOrEqualTo(String value) {
			addCriterion("lng >=", value, "lng");
			return (Criteria) this;
		}

		public Criteria andLngLessThan(String value) {
			addCriterion("lng <", value, "lng");
			return (Criteria) this;
		}

		public Criteria andLngLessThanOrEqualTo(String value) {
			addCriterion("lng <=", value, "lng");
			return (Criteria) this;
		}

		public Criteria andLngLike(String value) {
			addCriterion("lng like", value, "lng");
			return (Criteria) this;
		}

		public Criteria andLngNotLike(String value) {
			addCriterion("lng not like", value, "lng");
			return (Criteria) this;
		}

		public Criteria andLngIn(List<String> values) {
			addCriterion("lng in", values, "lng");
			return (Criteria) this;
		}

		public Criteria andLngNotIn(List<String> values) {
			addCriterion("lng not in", values, "lng");
			return (Criteria) this;
		}

		public Criteria andLngBetween(String value1, String value2) {
			addCriterion("lng between", value1, value2, "lng");
			return (Criteria) this;
		}

		public Criteria andLngNotBetween(String value1, String value2) {
			addCriterion("lng not between", value1, value2, "lng");
			return (Criteria) this;
		}

		public Criteria andLastAddressIsNull() {
			addCriterion("last_address is null");
			return (Criteria) this;
		}

		public Criteria andLastAddressIsNotNull() {
			addCriterion("last_address is not null");
			return (Criteria) this;
		}

		public Criteria andLastAddressEqualTo(String value) {
			addCriterion("last_address =", value, "lastAddress");
			return (Criteria) this;
		}

		public Criteria andLastAddressNotEqualTo(String value) {
			addCriterion("last_address <>", value, "lastAddress");
			return (Criteria) this;
		}

		public Criteria andLastAddressGreaterThan(String value) {
			addCriterion("last_address >", value, "lastAddress");
			return (Criteria) this;
		}

		public Criteria andLastAddressGreaterThanOrEqualTo(String value) {
			addCriterion("last_address >=", value, "lastAddress");
			return (Criteria) this;
		}

		public Criteria andLastAddressLessThan(String value) {
			addCriterion("last_address <", value, "lastAddress");
			return (Criteria) this;
		}

		public Criteria andLastAddressLessThanOrEqualTo(String value) {
			addCriterion("last_address <=", value, "lastAddress");
			return (Criteria) this;
		}

		public Criteria andLastAddressLike(String value) {
			addCriterion("last_address like", value, "lastAddress");
			return (Criteria) this;
		}

		public Criteria andLastAddressNotLike(String value) {
			addCriterion("last_address not like", value, "lastAddress");
			return (Criteria) this;
		}

		public Criteria andLastAddressIn(List<String> values) {
			addCriterion("last_address in", values, "lastAddress");
			return (Criteria) this;
		}

		public Criteria andLastAddressNotIn(List<String> values) {
			addCriterion("last_address not in", values, "lastAddress");
			return (Criteria) this;
		}

		public Criteria andLastAddressBetween(String value1, String value2) {
			addCriterion("last_address between", value1, value2, "lastAddress");
			return (Criteria) this;
		}

		public Criteria andLastAddressNotBetween(String value1, String value2) {
			addCriterion("last_address not between", value1, value2, "lastAddress");
			return (Criteria) this;
		}

		public Criteria andLastPositionTimeIsNull() {
			addCriterion("last_position_time is null");
			return (Criteria) this;
		}

		public Criteria andLastPositionTimeIsNotNull() {
			addCriterion("last_position_time is not null");
			return (Criteria) this;
		}

		public Criteria andLastPositionTimeEqualTo(java.util.Date value) {
			addCriterion("last_position_time =", value, "lastPositionTime");
			return (Criteria) this;
		}

		public Criteria andLastPositionTimeNotEqualTo(java.util.Date value) {
			addCriterion("last_position_time <>", value, "lastPositionTime");
			return (Criteria) this;
		}

		public Criteria andLastPositionTimeGreaterThan(java.util.Date value) {
			addCriterion("last_position_time >", value, "lastPositionTime");
			return (Criteria) this;
		}

		public Criteria andLastPositionTimeGreaterThanOrEqualTo(java.util.Date value) {
			addCriterion("last_position_time >=", value, "lastPositionTime");
			return (Criteria) this;
		}

		public Criteria andLastPositionTimeLessThan(java.util.Date value) {
			addCriterion("last_position_time <", value, "lastPositionTime");
			return (Criteria) this;
		}

		public Criteria andLastPositionTimeLessThanOrEqualTo(java.util.Date value) {
			addCriterion("last_position_time <=", value, "lastPositionTime");
			return (Criteria) this;
		}

		public Criteria andLastPositionTimeIn(List<java.util.Date> values) {
			addCriterion("last_position_time in", values, "lastPositionTime");
			return (Criteria) this;
		}

		public Criteria andLastPositionTimeNotIn(List<java.util.Date> values) {
			addCriterion("last_position_time not in", values, "lastPositionTime");
			return (Criteria) this;
		}

		public Criteria andLastPositionTimeBetween(java.util.Date value1, java.util.Date value2) {
			addCriterion("last_position_time between", value1, value2, "lastPositionTime");
			return (Criteria) this;
		}

		public Criteria andLastPositionTimeNotBetween(java.util.Date value1, java.util.Date value2) {
			addCriterion("last_position_time not between", value1, value2, "lastPositionTime");
			return (Criteria) this;
		}

		public Criteria andLastGoodsTimeIsNull() {
			addCriterion("last_goods_time is null");
			return (Criteria) this;
		}

		public Criteria andLastGoodsTimeIsNotNull() {
			addCriterion("last_goods_time is not null");
			return (Criteria) this;
		}

		public Criteria andLastGoodsTimeEqualTo(java.util.Date value) {
			addCriterion("last_goods_time =", value, "lastGoodsTime");
			return (Criteria) this;
		}

		public Criteria andLastGoodsTimeNotEqualTo(java.util.Date value) {
			addCriterion("last_goods_time <>", value, "lastGoodsTime");
			return (Criteria) this;
		}

		public Criteria andLastGoodsTimeGreaterThan(java.util.Date value) {
			addCriterion("last_goods_time >", value, "lastGoodsTime");
			return (Criteria) this;
		}

		public Criteria andLastGoodsTimeGreaterThanOrEqualTo(java.util.Date value) {
			addCriterion("last_goods_time >=", value, "lastGoodsTime");
			return (Criteria) this;
		}

		public Criteria andLastGoodsTimeLessThan(java.util.Date value) {
			addCriterion("last_goods_time <", value, "lastGoodsTime");
			return (Criteria) this;
		}

		public Criteria andLastGoodsTimeLessThanOrEqualTo(java.util.Date value) {
			addCriterion("last_goods_time <=", value, "lastGoodsTime");
			return (Criteria) this;
		}

		public Criteria andLastGoodsTimeIn(List<java.util.Date> values) {
			addCriterion("last_goods_time in", values, "lastGoodsTime");
			return (Criteria) this;
		}

		public Criteria andLastGoodsTimeNotIn(List<java.util.Date> values) {
			addCriterion("last_goods_time not in", values, "lastGoodsTime");
			return (Criteria) this;
		}

		public Criteria andLastGoodsTimeBetween(java.util.Date value1, java.util.Date value2) {
			addCriterion("last_goods_time between", value1, value2, "lastGoodsTime");
			return (Criteria) this;
		}

		public Criteria andLastGoodsTimeNotBetween(java.util.Date value1, java.util.Date value2) {
			addCriterion("last_goods_time not between", value1, value2, "lastGoodsTime");
			return (Criteria) this;
		}

		public Criteria andIsCompleteIsNull() {
			addCriterion("is_complete is null");
			return (Criteria) this;
		}

		public Criteria andIsCompleteIsNotNull() {
			addCriterion("is_complete is not null");
			return (Criteria) this;
		}

		public Criteria andIsCompleteEqualTo(Integer value) {
			addCriterion("is_complete =", value, "isComplete");
			return (Criteria) this;
		}

		public Criteria andIsCompleteNotEqualTo(Integer value) {
			addCriterion("is_complete <>", value, "isComplete");
			return (Criteria) this;
		}

		public Criteria andIsCompleteGreaterThan(Integer value) {
			addCriterion("is_complete >", value, "isComplete");
			return (Criteria) this;
		}

		public Criteria andIsCompleteGreaterThanOrEqualTo(Integer value) {
			addCriterion("is_complete >=", value, "isComplete");
			return (Criteria) this;
		}

		public Criteria andIsCompleteLessThan(Integer value) {
			addCriterion("is_complete <", value, "isComplete");
			return (Criteria) this;
		}

		public Criteria andIsCompleteLessThanOrEqualTo(Integer value) {
			addCriterion("is_complete <=", value, "isComplete");
			return (Criteria) this;
		}

		public Criteria andIsCompleteIn(List<Integer> values) {
			addCriterion("is_complete in", values, "isComplete");
			return (Criteria) this;
		}

		public Criteria andIsCompleteNotIn(List<Integer> values) {
			addCriterion("is_complete not in", values, "isComplete");
			return (Criteria) this;
		}

		public Criteria andIsCompleteBetween(Integer value1, Integer value2) {
			addCriterion("is_complete between", value1, value2, "isComplete");
			return (Criteria) this;
		}

		public Criteria andIsCompleteNotBetween(Integer value1, Integer value2) {
			addCriterion("is_complete not between", value1, value2, "isComplete");
			return (Criteria) this;
		}

		public Criteria andEmailIsNull() {
			addCriterion("email is null");
			return (Criteria) this;
		}

		public Criteria andEmailIsNotNull() {
			addCriterion("email is not null");
			return (Criteria) this;
		}

		public Criteria andEmailEqualTo(String value) {
			addCriterion("email =", value, "email");
			return (Criteria) this;
		}

		public Criteria andEmailNotEqualTo(String value) {
			addCriterion("email <>", value, "email");
			return (Criteria) this;
		}

		public Criteria andEmailGreaterThan(String value) {
			addCriterion("email >", value, "email");
			return (Criteria) this;
		}

		public Criteria andEmailGreaterThanOrEqualTo(String value) {
			addCriterion("email >=", value, "email");
			return (Criteria) this;
		}

		public Criteria andEmailLessThan(String value) {
			addCriterion("email <", value, "email");
			return (Criteria) this;
		}

		public Criteria andEmailLessThanOrEqualTo(String value) {
			addCriterion("email <=", value, "email");
			return (Criteria) this;
		}

		public Criteria andEmailLike(String value) {
			addCriterion("email like", value, "email");
			return (Criteria) this;
		}

		public Criteria andEmailNotLike(String value) {
			addCriterion("email not like", value, "email");
			return (Criteria) this;
		}

		public Criteria andEmailIn(List<String> values) {
			addCriterion("email in", values, "email");
			return (Criteria) this;
		}

		public Criteria andEmailNotIn(List<String> values) {
			addCriterion("email not in", values, "email");
			return (Criteria) this;
		}

		public Criteria andEmailBetween(String value1, String value2) {
			addCriterion("email between", value1, value2, "email");
			return (Criteria) this;
		}

		public Criteria andEmailNotBetween(String value1, String value2) {
			addCriterion("email not between", value1, value2, "email");
			return (Criteria) this;
		}

		public Criteria andCityIsNull() {
			addCriterion("city is null");
			return (Criteria) this;
		}

		public Criteria andCityIsNotNull() {
			addCriterion("city is not null");
			return (Criteria) this;
		}

		public Criteria andCityEqualTo(String value) {
			addCriterion("city =", value, "city");
			return (Criteria) this;
		}

		public Criteria andCityNotEqualTo(String value) {
			addCriterion("city <>", value, "city");
			return (Criteria) this;
		}

		public Criteria andCityGreaterThan(String value) {
			addCriterion("city >", value, "city");
			return (Criteria) this;
		}

		public Criteria andCityGreaterThanOrEqualTo(String value) {
			addCriterion("city >=", value, "city");
			return (Criteria) this;
		}

		public Criteria andCityLessThan(String value) {
			addCriterion("city <", value, "city");
			return (Criteria) this;
		}

		public Criteria andCityLessThanOrEqualTo(String value) {
			addCriterion("city <=", value, "city");
			return (Criteria) this;
		}

		public Criteria andCityLike(String value) {
			addCriterion("city like", value, "city");
			return (Criteria) this;
		}

		public Criteria andCityNotLike(String value) {
			addCriterion("city not like", value, "city");
			return (Criteria) this;
		}

		public Criteria andCityIn(List<String> values) {
			addCriterion("city in", values, "city");
			return (Criteria) this;
		}

		public Criteria andCityNotIn(List<String> values) {
			addCriterion("city not in", values, "city");
			return (Criteria) this;
		}

		public Criteria andCityBetween(String value1, String value2) {
			addCriterion("city between", value1, value2, "city");
			return (Criteria) this;
		}

		public Criteria andCityNotBetween(String value1, String value2) {
			addCriterion("city not between", value1, value2, "city");
			return (Criteria) this;
		}

		public Criteria andBirthdayIsNull() {
			addCriterion("birthday is null");
			return (Criteria) this;
		}

		public Criteria andBirthdayIsNotNull() {
			addCriterion("birthday is not null");
			return (Criteria) this;
		}

		public Criteria andBirthdayEqualTo(java.util.Date value) {
			addCriterion("birthday =", value, "birthday");
			return (Criteria) this;
		}

		public Criteria andBirthdayNotEqualTo(java.util.Date value) {
			addCriterion("birthday <>", value, "birthday");
			return (Criteria) this;
		}

		public Criteria andBirthdayGreaterThan(java.util.Date value) {
			addCriterion("birthday >", value, "birthday");
			return (Criteria) this;
		}

		public Criteria andBirthdayGreaterThanOrEqualTo(java.util.Date value) {
			addCriterion("birthday >=", value, "birthday");
			return (Criteria) this;
		}

		public Criteria andBirthdayLessThan(java.util.Date value) {
			addCriterion("birthday <", value, "birthday");
			return (Criteria) this;
		}

		public Criteria andBirthdayLessThanOrEqualTo(java.util.Date value) {
			addCriterion("birthday <=", value, "birthday");
			return (Criteria) this;
		}

		public Criteria andBirthdayIn(List<java.util.Date> values) {
			addCriterion("birthday in", values, "birthday");
			return (Criteria) this;
		}

		public Criteria andBirthdayNotIn(List<java.util.Date> values) {
			addCriterion("birthday not in", values, "birthday");
			return (Criteria) this;
		}

		public Criteria andBirthdayBetween(java.util.Date value1, java.util.Date value2) {
			addCriterion("birthday between", value1, value2, "birthday");
			return (Criteria) this;
		}

		public Criteria andBirthdayNotBetween(java.util.Date value1, java.util.Date value2) {
			addCriterion("birthday not between", value1, value2, "birthday");
			return (Criteria) this;
		}

		public Criteria andInterestIsNull() {
			addCriterion("interest is null");
			return (Criteria) this;
		}

		public Criteria andInterestIsNotNull() {
			addCriterion("interest is not null");
			return (Criteria) this;
		}

		public Criteria andInterestEqualTo(String value) {
			addCriterion("interest =", value, "interest");
			return (Criteria) this;
		}

		public Criteria andInterestNotEqualTo(String value) {
			addCriterion("interest <>", value, "interest");
			return (Criteria) this;
		}

		public Criteria andInterestGreaterThan(String value) {
			addCriterion("interest >", value, "interest");
			return (Criteria) this;
		}

		public Criteria andInterestGreaterThanOrEqualTo(String value) {
			addCriterion("interest >=", value, "interest");
			return (Criteria) this;
		}

		public Criteria andInterestLessThan(String value) {
			addCriterion("interest <", value, "interest");
			return (Criteria) this;
		}

		public Criteria andInterestLessThanOrEqualTo(String value) {
			addCriterion("interest <=", value, "interest");
			return (Criteria) this;
		}

		public Criteria andInterestLike(String value) {
			addCriterion("interest like", value, "interest");
			return (Criteria) this;
		}

		public Criteria andInterestNotLike(String value) {
			addCriterion("interest not like", value, "interest");
			return (Criteria) this;
		}

		public Criteria andInterestIn(List<String> values) {
			addCriterion("interest in", values, "interest");
			return (Criteria) this;
		}

		public Criteria andInterestNotIn(List<String> values) {
			addCriterion("interest not in", values, "interest");
			return (Criteria) this;
		}

		public Criteria andInterestBetween(String value1, String value2) {
			addCriterion("interest between", value1, value2, "interest");
			return (Criteria) this;
		}

		public Criteria andInterestNotBetween(String value1, String value2) {
			addCriterion("interest not between", value1, value2, "interest");
			return (Criteria) this;
		}

		public Criteria andCreateTimeIsNull() {
			addCriterion("create_time is null");
			return (Criteria) this;
		}

		public Criteria andCreateTimeIsNotNull() {
			addCriterion("create_time is not null");
			return (Criteria) this;
		}

		public Criteria andCreateTimeEqualTo(java.util.Date value) {
			addCriterion("create_time =", value, "createTime");
			return (Criteria) this;
		}

		public Criteria andCreateTimeNotEqualTo(java.util.Date value) {
			addCriterion("create_time <>", value, "createTime");
			return (Criteria) this;
		}

		public Criteria andCreateTimeGreaterThan(java.util.Date value) {
			addCriterion("create_time >", value, "createTime");
			return (Criteria) this;
		}

		public Criteria andCreateTimeGreaterThanOrEqualTo(java.util.Date value) {
			addCriterion("create_time >=", value, "createTime");
			return (Criteria) this;
		}

		public Criteria andCreateTimeLessThan(java.util.Date value) {
			addCriterion("create_time <", value, "createTime");
			return (Criteria) this;
		}

		public Criteria andCreateTimeLessThanOrEqualTo(java.util.Date value) {
			addCriterion("create_time <=", value, "createTime");
			return (Criteria) this;
		}

		public Criteria andCreateTimeIn(List<java.util.Date> values) {
			addCriterion("create_time in", values, "createTime");
			return (Criteria) this;
		}

		public Criteria andCreateTimeNotIn(List<java.util.Date> values) {
			addCriterion("create_time not in", values, "createTime");
			return (Criteria) this;
		}

		public Criteria andCreateTimeBetween(java.util.Date value1, java.util.Date value2) {
			addCriterion("create_time between", value1, value2, "createTime");
			return (Criteria) this;
		}

		public Criteria andCreateTimeNotBetween(java.util.Date value1, java.util.Date value2) {
			addCriterion("create_time not between", value1, value2, "createTime");
			return (Criteria) this;
		}

		public Criteria andModifyTimeIsNull() {
			addCriterion("modify_time is null");
			return (Criteria) this;
		}

		public Criteria andModifyTimeIsNotNull() {
			addCriterion("modify_time is not null");
			return (Criteria) this;
		}

		public Criteria andModifyTimeEqualTo(java.util.Date value) {
			addCriterion("modify_time =", value, "modifyTime");
			return (Criteria) this;
		}

		public Criteria andModifyTimeNotEqualTo(java.util.Date value) {
			addCriterion("modify_time <>", value, "modifyTime");
			return (Criteria) this;
		}

		public Criteria andModifyTimeGreaterThan(java.util.Date value) {
			addCriterion("modify_time >", value, "modifyTime");
			return (Criteria) this;
		}

		public Criteria andModifyTimeGreaterThanOrEqualTo(java.util.Date value) {
			addCriterion("modify_time >=", value, "modifyTime");
			return (Criteria) this;
		}

		public Criteria andModifyTimeLessThan(java.util.Date value) {
			addCriterion("modify_time <", value, "modifyTime");
			return (Criteria) this;
		}

		public Criteria andModifyTimeLessThanOrEqualTo(java.util.Date value) {
			addCriterion("modify_time <=", value, "modifyTime");
			return (Criteria) this;
		}

		public Criteria andModifyTimeIn(List<java.util.Date> values) {
			addCriterion("modify_time in", values, "modifyTime");
			return (Criteria) this;
		}

		public Criteria andModifyTimeNotIn(List<java.util.Date> values) {
			addCriterion("modify_time not in", values, "modifyTime");
			return (Criteria) this;
		}

		public Criteria andModifyTimeBetween(java.util.Date value1, java.util.Date value2) {
			addCriterion("modify_time between", value1, value2, "modifyTime");
			return (Criteria) this;
		}

		public Criteria andModifyTimeNotBetween(java.util.Date value1, java.util.Date value2) {
			addCriterion("modify_time not between", value1, value2, "modifyTime");
			return (Criteria) this;
		}

		public Criteria andIsBindQqIsNull() {
			addCriterion("is_bind_qq is null");
			return (Criteria) this;
		}

		public Criteria andIsBindQqIsNotNull() {
			addCriterion("is_bind_qq is not null");
			return (Criteria) this;
		}

		public Criteria andIsBindQqEqualTo(Integer value) {
			addCriterion("is_bind_qq =", value, "isBindQq");
			return (Criteria) this;
		}

		public Criteria andIsBindQqNotEqualTo(Integer value) {
			addCriterion("is_bind_qq <>", value, "isBindQq");
			return (Criteria) this;
		}

		public Criteria andIsBindQqGreaterThan(Integer value) {
			addCriterion("is_bind_qq >", value, "isBindQq");
			return (Criteria) this;
		}

		public Criteria andIsBindQqGreaterThanOrEqualTo(Integer value) {
			addCriterion("is_bind_qq >=", value, "isBindQq");
			return (Criteria) this;
		}

		public Criteria andIsBindQqLessThan(Integer value) {
			addCriterion("is_bind_qq <", value, "isBindQq");
			return (Criteria) this;
		}

		public Criteria andIsBindQqLessThanOrEqualTo(Integer value) {
			addCriterion("is_bind_qq <=", value, "isBindQq");
			return (Criteria) this;
		}

		public Criteria andIsBindQqIn(List<Integer> values) {
			addCriterion("is_bind_qq in", values, "isBindQq");
			return (Criteria) this;
		}

		public Criteria andIsBindQqNotIn(List<Integer> values) {
			addCriterion("is_bind_qq not in", values, "isBindQq");
			return (Criteria) this;
		}

		public Criteria andIsBindQqBetween(Integer value1, Integer value2) {
			addCriterion("is_bind_qq between", value1, value2, "isBindQq");
			return (Criteria) this;
		}

		public Criteria andIsBindQqNotBetween(Integer value1, Integer value2) {
			addCriterion("is_bind_qq not between", value1, value2, "isBindQq");
			return (Criteria) this;
		}

		public Criteria andIsBindWeixinIsNull() {
			addCriterion("is_bind_weixin is null");
			return (Criteria) this;
		}

		public Criteria andIsBindWeixinIsNotNull() {
			addCriterion("is_bind_weixin is not null");
			return (Criteria) this;
		}

		public Criteria andIsBindWeixinEqualTo(Integer value) {
			addCriterion("is_bind_weixin =", value, "isBindWeixin");
			return (Criteria) this;
		}

		public Criteria andIsBindWeixinNotEqualTo(Integer value) {
			addCriterion("is_bind_weixin <>", value, "isBindWeixin");
			return (Criteria) this;
		}

		public Criteria andIsBindWeixinGreaterThan(Integer value) {
			addCriterion("is_bind_weixin >", value, "isBindWeixin");
			return (Criteria) this;
		}

		public Criteria andIsBindWeixinGreaterThanOrEqualTo(Integer value) {
			addCriterion("is_bind_weixin >=", value, "isBindWeixin");
			return (Criteria) this;
		}

		public Criteria andIsBindWeixinLessThan(Integer value) {
			addCriterion("is_bind_weixin <", value, "isBindWeixin");
			return (Criteria) this;
		}

		public Criteria andIsBindWeixinLessThanOrEqualTo(Integer value) {
			addCriterion("is_bind_weixin <=", value, "isBindWeixin");
			return (Criteria) this;
		}

		public Criteria andIsBindWeixinIn(List<Integer> values) {
			addCriterion("is_bind_weixin in", values, "isBindWeixin");
			return (Criteria) this;
		}

		public Criteria andIsBindWeixinNotIn(List<Integer> values) {
			addCriterion("is_bind_weixin not in", values, "isBindWeixin");
			return (Criteria) this;
		}

		public Criteria andIsBindWeixinBetween(Integer value1, Integer value2) {
			addCriterion("is_bind_weixin between", value1, value2, "isBindWeixin");
			return (Criteria) this;
		}

		public Criteria andIsBindWeixinNotBetween(Integer value1, Integer value2) {
			addCriterion("is_bind_weixin not between", value1, value2, "isBindWeixin");
			return (Criteria) this;
		}

		public Criteria andIsBindWeiboIsNull() {
			addCriterion("is_bind_weibo is null");
			return (Criteria) this;
		}

		public Criteria andIsBindWeiboIsNotNull() {
			addCriterion("is_bind_weibo is not null");
			return (Criteria) this;
		}

		public Criteria andIsBindWeiboEqualTo(Integer value) {
			addCriterion("is_bind_weibo =", value, "isBindWeibo");
			return (Criteria) this;
		}

		public Criteria andIsBindWeiboNotEqualTo(Integer value) {
			addCriterion("is_bind_weibo <>", value, "isBindWeibo");
			return (Criteria) this;
		}

		public Criteria andIsBindWeiboGreaterThan(Integer value) {
			addCriterion("is_bind_weibo >", value, "isBindWeibo");
			return (Criteria) this;
		}

		public Criteria andIsBindWeiboGreaterThanOrEqualTo(Integer value) {
			addCriterion("is_bind_weibo >=", value, "isBindWeibo");
			return (Criteria) this;
		}

		public Criteria andIsBindWeiboLessThan(Integer value) {
			addCriterion("is_bind_weibo <", value, "isBindWeibo");
			return (Criteria) this;
		}

		public Criteria andIsBindWeiboLessThanOrEqualTo(Integer value) {
			addCriterion("is_bind_weibo <=", value, "isBindWeibo");
			return (Criteria) this;
		}

		public Criteria andIsBindWeiboIn(List<Integer> values) {
			addCriterion("is_bind_weibo in", values, "isBindWeibo");
			return (Criteria) this;
		}

		public Criteria andIsBindWeiboNotIn(List<Integer> values) {
			addCriterion("is_bind_weibo not in", values, "isBindWeibo");
			return (Criteria) this;
		}

		public Criteria andIsBindWeiboBetween(Integer value1, Integer value2) {
			addCriterion("is_bind_weibo between", value1, value2, "isBindWeibo");
			return (Criteria) this;
		}

		public Criteria andIsBindWeiboNotBetween(Integer value1, Integer value2) {
			addCriterion("is_bind_weibo not between", value1, value2, "isBindWeibo");
			return (Criteria) this;
		}

		public Criteria andCreditIsNull() {
			addCriterion("credit is null");
			return (Criteria) this;
		}

		public Criteria andCreditIsNotNull() {
			addCriterion("credit is not null");
			return (Criteria) this;
		}

		public Criteria andCreditEqualTo(Integer value) {
			addCriterion("credit =", value, "credit");
			return (Criteria) this;
		}

		public Criteria andCreditNotEqualTo(Integer value) {
			addCriterion("credit <>", value, "credit");
			return (Criteria) this;
		}

		public Criteria andCreditGreaterThan(Integer value) {
			addCriterion("credit >", value, "credit");
			return (Criteria) this;
		}

		public Criteria andCreditGreaterThanOrEqualTo(Integer value) {
			addCriterion("credit >=", value, "credit");
			return (Criteria) this;
		}

		public Criteria andCreditLessThan(Integer value) {
			addCriterion("credit <", value, "credit");
			return (Criteria) this;
		}

		public Criteria andCreditLessThanOrEqualTo(Integer value) {
			addCriterion("credit <=", value, "credit");
			return (Criteria) this;
		}

		public Criteria andCreditIn(List<Integer> values) {
			addCriterion("credit in", values, "credit");
			return (Criteria) this;
		}

		public Criteria andCreditNotIn(List<Integer> values) {
			addCriterion("credit not in", values, "credit");
			return (Criteria) this;
		}

		public Criteria andCreditBetween(Integer value1, Integer value2) {
			addCriterion("credit between", value1, value2, "credit");
			return (Criteria) this;
		}

		public Criteria andCreditNotBetween(Integer value1, Integer value2) {
			addCriterion("credit not between", value1, value2, "credit");
			return (Criteria) this;
		}

}

	public static class Criteria extends GeneratedCriteria {

		protected Criteria() {
			super();
		}
	}

	public static class Criterion {
		private String condition;

		private Object value;

		private Object secondValue;

		private boolean noValue;

		private boolean singleValue;

		private boolean betweenValue;

		private boolean listValue;

		private String typeHandler;

		public String getCondition() {
			return condition;
		}

		public Object getValue() {
			return value;
		}

		public Object getSecondValue() {
			return secondValue;
		}

		public boolean isNoValue() {
		return noValue;
		}

		public boolean isSingleValue() {
			return singleValue;
		}

		public boolean isBetweenValue() {
			return betweenValue;
		}

		public boolean isListValue() {
			return listValue;
		}

		public String getTypeHandler() {
			return typeHandler;
		}

		protected Criterion(String condition) {
			super();
			this.condition = condition;
			this.typeHandler = null;
			this.noValue = true;
		}

		protected Criterion(String condition, Object value, String typeHandler) {
			super();
			this.condition = condition;
			this.value = value;
			this.typeHandler = typeHandler;
			if (value instanceof List<?>) {
				this.listValue = true;
			} else {
				this.singleValue = true;
			}
		}

		protected Criterion(String condition, Object value) {
			this(condition, value, null);
		}

		protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
			super();
			this.condition = condition;
			this.value = value;
			this.secondValue = secondValue;
			this.typeHandler = typeHandler;
			this.betweenValue = true;
		}

		protected Criterion(String condition, Object value, Object secondValue) {
			this(condition, value, secondValue, null);
		}
	}
}