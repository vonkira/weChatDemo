package com.item.dao.model;



/**
 *
 */
public class Message {

	/**
	 *id
	 */
	private Integer id;

	/**
	 *1:全体 2:个人 3:tag
	 */
	private Integer type;

	/**
	 *用户账号集合，以,分隔
	 */
	private String accounts;

	/**
	 *对象id集合，以,分隔
	 */
	private String target;

	/**
	 *内容
	 */
	private String content;

	/**
	 *1:打开应用
	 */
	private Integer redirectType;

	/**
	 *跳转内容
	 */
	private String redirectContent;

	/**
	 *1:未发送 2:已发送
	 */
	private Integer state;

	/**
	 *人数
	 */
	private Integer num;

	/**
	 *创建时间
	 */
	private java.util.Date createTime;

	public void setId(Integer id) {
		this.id=id;
	}

	public Integer getId() {
		return id;
	}

	public void setType(Integer type) {
		this.type=type;
	}

	public Integer getType() {
		return type;
	}

	public void setAccounts(String accounts) {
		this.accounts=accounts == null ? accounts : accounts.trim();
	}

	public String getAccounts() {
		return accounts;
	}

	public void setTarget(String target) {
		this.target=target == null ? target : target.trim();
	}

	public String getTarget() {
		return target;
	}

	public void setContent(String content) {
		this.content=content == null ? content : content.trim();
	}

	public String getContent() {
		return content;
	}

	public void setRedirectType(Integer redirectType) {
		this.redirectType=redirectType;
	}

	public Integer getRedirectType() {
		return redirectType;
	}

	public void setRedirectContent(String redirectContent) {
		this.redirectContent=redirectContent == null ? redirectContent : redirectContent.trim();
	}

	public String getRedirectContent() {
		return redirectContent;
	}

	public void setState(Integer state) {
		this.state=state;
	}

	public Integer getState() {
		return state;
	}

	public void setNum(Integer num) {
		this.num=num;
	}

	public Integer getNum() {
		return num;
	}

	public void setCreateTime(java.util.Date createTime) {
		this.createTime=createTime;
	}

	public java.util.Date getCreateTime() {
		return createTime;
	}

}
