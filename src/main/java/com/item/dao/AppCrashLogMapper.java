package com.item.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.item.dao.model.AppCrashLog;
import com.item.dao.model.AppCrashLogExample;

public interface AppCrashLogMapper {
	int countByExample(AppCrashLogExample example);

	int deleteByExample(AppCrashLogExample example);

	int deleteByPrimaryKey(Integer id);

	int insert(AppCrashLog record);

	int insertSelective(AppCrashLog record);

	List<AppCrashLog> selectByExample(AppCrashLogExample example);

	List<AppCrashLog> selectByExampleWithBLOBs(AppCrashLogExample example);

	AppCrashLog selectByPrimaryKey(Integer id);

	int updateByExampleSelective(@Param("record") AppCrashLog record,@Param("example") AppCrashLogExample example);

	int updateByExampleWithBLOBs(@Param("record") AppCrashLog record, @Param("example") AppCrashLogExample example);

	int updateByExample(@Param("record") AppCrashLog record,@Param("example") AppCrashLogExample example);

	int updateByPrimaryKeySelective(AppCrashLog record);

	int updateByPrimaryKeyWithBLOBs(AppCrashLog record);

	int updateByPrimaryKey(AppCrashLog record);

}
