package com.item.action;

import com.api.util.MathUtil;
import com.base.action.CoreController;
import com.base.des.Md5;
import com.base.dialect.PaginationSupport;
import com.base.util.CollectionUtil;
import com.base.util.StringUtil;
import com.base.web.annotation.LoginFilter;
import com.item.dao.model.Admin;
import com.item.dao.model.AdminExample;
import com.item.dao.model.Right;
import com.item.service.AdminService;
import com.item.service.RightService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

/**
@author sun
*/
@RequestMapping("admin")
@Controller
@LoginFilter
public class AdminController extends CoreController{

    @Autowired
    private AdminService adminService;
    @Autowired
	private RightService rightService;

    @RequestMapping("/list")
	@ResponseBody
    public String list(Integer page, Integer rows,Integer orgId,Integer type){
    	PaginationSupport.byPage(page, rows);
    	AdminExample example = new AdminExample();
		AdminExample.Criteria criteria = example.createCriteria();
		if (orgId != null){
			criteria.andOrgIdEqualTo(orgId);
		}
		if (type != null){
			criteria.andTypeEqualTo(type);
		}
		example.setOrderByClause("create_time desc");
    	List<Admin> list = adminService.selectByExample(example);
      	return page(list);
    }

    @RequestMapping("/save")
	@ResponseBody
    public String save(Admin admin){
		if (StringUtil.isNotBlank(admin.getPassword())){
			admin.setPassword(Md5.mD5(admin.getPassword()));
		}
    	if (admin.getId() == null){
    		adminService.insert(admin);
    	}else{
    		adminService.updateByPrimaryKeySelective(admin);
    	}
       	return ok();
    }

	@RequestMapping("/addAccount")
	@ResponseBody
	public String addAccount(Admin admin, String rid){
		if (StringUtil.isNotBlank(admin.getPassword())){
			admin.setPassword(Md5.mD5(admin.getPassword()));
		}
		if (!StringUtils.isBlank(rid)){
			rid = MathUtil.str2f(rid);
			admin.setRights(rid);
		}
		if (admin.getId() == null){
			admin.setType(1);
			admin.setState(1);
			admin.setRights(rid);
			admin.setOrgId(0);
			adminService.insert(admin);
		}else{
			adminService.updateByPrimaryKeySelective(admin);
		}
		return ok();
	}

//	@RequestMapping("/findRights")
//	@ResponseBody
//	public String findRights(){
//		List<Right> list = RoleUtil.getRightsByCode(MkmRole.ADMIN.getGroupCode(),MkmRole.ADMIN.getRoleCode());
//		return ok(list);
//	}

    @RequestMapping("/findById")
	@ResponseBody
    public String find(Integer id){
    	Admin admin = adminService.selectByPrimaryKey(id);
    	if (admin != null){
			List<Right> list = rightService.selectByUser(id);
			List<Integer> rids = new ArrayList<>();
			for (Right right : list){
				if(right==null){
					continue;
				}
				rids.remove(right.getParentId());
				rids.add(right.getId());
			}
    		admin.setRights(CollectionUtil.join(rids,","));
		}
       	return ok(admin);
    }

    @RequestMapping("/del")
	@ResponseBody
    public String del(String id){
    	String[] ids = id.split(",");
    	for (String str : ids){
    		adminService.deleteByPrimaryKey(Integer.parseInt(str));
    	}
       	return ok();
    }


}