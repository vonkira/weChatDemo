package com.item.action;

import com.base.action.CoreController;
import com.item.dao.model.Focus;
import com.item.dao.model.Notify;
import com.item.dao.model.SinglePage;
import com.item.service.FocusService;
import com.item.service.NotifyService;
import com.item.service.SinglePageService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.HashMap;
import java.util.Map;

@Controller
public class SystemController extends CoreController {
	@Autowired
	private SinglePageService singlePageService;
	@Autowired
	private FocusService focusService;
	@Autowired
	private NotifyService notifyService;

	/**
	 * webview
	 * @param id
	 * @param type 类型
	 */
	@RequestMapping("/detail")
	public String detail(Integer id, Integer type, ModelMap map) throws Exception {
		if (id == null || type == null) {
			return null;
		}
		Map<String,Object> ret = new HashMap<String,Object>();
		switch (type) {
		case 1:
			break;
		case 2:
			// type==2 焦点图
			Focus focus = focusService.selectByPrimaryKey(id);
			if (focus != null) {
				ret.put("content", focus.getContent());
				map.put("obj", ret);
			} else {
				return "common/404";
			}
			break;
		case 3:
			// type==3 微信详情页
			break;
		case 4:
			break;
		case 5:
			// type==5 用户信息详情
			Notify notify = notifyService.selectByPrimaryKey(id);
			if (notify != null) {
				ret.put("content", notify.getContent());
				map.put("obj", ret);
			} else {
				return "common/404";
			}
			break;
		case 8:
			// type==8 新闻
			break;
		case 9:
			break;
		default:
			break;
		}
		return "common/detail";
	}

	@RequestMapping("/singlePage/{code}")
	public String detail(@PathVariable("code")String code,ModelMap map) throws Exception {
		if (StringUtils.isBlank(code)) {
			return null;
		}
		SinglePage page = singlePageService.selectByPrimaryKey(code);
		if(page == null) page = new SinglePage();
		map.put("obj", page);
		return "common/detail";
	}
}