package com.item.action;

import com.base.action.CoreController;
import com.base.dialect.PaginationSupport;
import com.base.util.StringUtil;
import com.base.web.annotation.LoginFilter;
import com.item.dao.model.AppCrashLog;
import com.item.dao.model.AppCrashLogExample;
import com.item.service.AppCrashLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
@author sun
*/
@RequestMapping("appCrashLog")
@Controller
@LoginFilter
public class AppCrashLogController extends CoreController{

    @Autowired
    private AppCrashLogService appCrashLogService;

    @RequestMapping("/list")
	@ResponseBody
    public String list(Integer page, Integer rows,String model,String brand){
    	PaginationSupport.byPage(page, rows);
    	AppCrashLogExample example = new AppCrashLogExample();
		AppCrashLogExample.Criteria criteria = example.createCriteria();
		if (StringUtil.isNotBlank(model)){
			criteria.andModelLike("%"+model+"%");
		}
		if (StringUtil.isNotBlank(brand)){
			criteria.andBrandLike("%"+brand+"%");
		}
		example.setOrderByClause("create_time desc");
    	List<AppCrashLog> list = appCrashLogService.selectByExampleWithBLOBs(example);
      	return page(list);
    }

//	@RequestMapping("/brand")
//	@ResponseBody
//	public String brandList(){
//		QueryParams.Builder builder = new QueryParams.Builder();
//		builder.put("type","brand");
//		List<String> list = appCrashLogService.selectStrList(builder);
//		List<StoreCate> array = new ArrayList<>();
//		for (String str : list){
//			StoreCate mini = new StoreCate();
//			mini.setName(str);
//			mini.setCode(str);
//			array.add(mini);
//		}
//		return ok(array);
//	}
//
//	@RequestMapping("/model")
//	@ResponseBody
//	public String modelList(String brand){
//		QueryParams.Builder builder = new QueryParams.Builder();
//		builder.put("type","model");
//		builder.put("brand",brand);
//		List<String> list = appCrashLogService.selectStrList(builder);
//		List<StoreCate> array = new ArrayList<>();
//		for (String str : list){
//			StoreCate mini = new StoreCate();
//			mini.setName(str);
//			mini.setCode(str);
//			array.add(mini);
//		}
//		return ok(array);
//	}

    @RequestMapping("/save")
	@ResponseBody
    public String save(AppCrashLog appCrashLog){
    	if (appCrashLog.getId() == null){
    		appCrashLogService.insert(appCrashLog);
    	}else{
    		appCrashLogService.updateByPrimaryKeySelective(appCrashLog);
    	}
       	return ok();
    }

    @RequestMapping("/findById")
	@ResponseBody
    public String find(Integer id){
    	AppCrashLog appCrashLog = appCrashLogService.selectByPrimaryKey(id);
       	return ok(appCrashLog);
    }

    @RequestMapping("/del")
	@ResponseBody
    public String del(String id){
    	String[] ids = id.split(",");
    	for (String str : ids){
    		appCrashLogService.deleteByPrimaryKey(Integer.parseInt(str));
    	}
       	return ok();
    }


}