package com.item.action;

import com.api.view.common.AppVersion;
import com.api.view.common.LoadingImg;
import com.base.CoreConstants;
import com.base.action.CoreController;
import com.base.dao.model.Ret;
import com.base.dialect.PaginationSupport;
import com.base.io.IoUtil;
import com.base.util.DFA;
import com.base.util.JSONUtils;
import com.base.util.RequestKit;
import com.base.util.StringUtil;
import com.base.web.annotation.LoginFilter;
import com.item.dao.model.Code;
import com.item.dao.model.CodeExample;
import com.item.dao.model.CodeExample.Criteria;
import com.item.service.CodeService;
import com.item.service.UserService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.List;
import java.util.Map;

@RequestMapping("code")
@Controller
@LoginFilter
public class CodeController extends CoreController{
	@Autowired
	private CodeService codeService;
	@Autowired
	private UserService userService;
	
	@RequestMapping("/list")
	@ResponseBody
	public String list(Integer page,Integer rows,String code,String group){
		PaginationSupport.byPage(page, rows);
		CodeExample example = new CodeExample();
		Criteria criteria = example.createCriteria();//去除单独维护的参数
		if(StringUtil.isNotBlank(code)){
			criteria.andCodeEqualTo(code);
		}
		if (StringUtil.isNotBlank(group)){
			criteria.andTypeEqualTo(group);
		}
		List<Code> list = codeService.selectByExample(example);
		return page(list);
	}

	@RequestMapping("/listNoPage")
	@ResponseBody
	public String list(String code,String group){
		CodeExample example = new CodeExample();
		Criteria criteria = example.createCriteria();//去除单独维护的参数
		if(StringUtil.isNotBlank(code)){
			criteria.andCodeEqualTo(code);
		}
		if (StringUtil.isNotBlank(group)){
			criteria.andTypeEqualTo(group);
		}
		List<Code> list = codeService.selectByExample(example);
		return ok(list);
	}

	@RequestMapping("findById")
	@ResponseBody
	public String findById(String code){
		Code codes = codeService.getByCode(code);
		return ok(codes);
	}
	
	@RequestMapping("saveVersion")
	@ResponseBody
	public String saveVersion(String code,AppVersion appVersion){
		Code record = new Code();
		record.setCode(code);
		record.setValue(JSONUtils.serialize(appVersion));
		int cnt = codeService.updateByPrimaryKeySelective(record);
		if (cnt == 0){
			record.setType("VERSION");
			codeService.insertSelective(record);
		}
		return ok();
	}
	
	@RequestMapping("saveLoading")
	@ResponseBody
	public String saveLoading(String code,LoadingImg appVersion){
		Code record = new Code();
		record.setCode(code);
		record.setValue(JSONUtils.serialize(appVersion));
		codeService.updateByPrimaryKeySelective(record);
		return ok();
	}
	
	@RequestMapping("save")
	@ResponseBody
	public String save(Code code){
		int i = this.codeService.updateByPrimaryKeySelective(code);
		if(i == 0){
			codeService.insertSelective(code);
		}
		return ok();
	}

	@RequestMapping("saveAll")
	@ResponseBody
	public String saveAll(HttpServletRequest request){
//		String userStrength = codeService.getCode(MkmConst.USER_STRENGTH_MAX);
		Map<String,String> map = RequestKit.getFromRequest(request);
		Code code = new Code();
		for (Map.Entry<String,String> entry : map.entrySet()){
			code.setCode(entry.getKey());
			code.setValue(entry.getValue());
			int update = codeService.updateByPrimaryKeySelective(code);
			if (update == 0){
				codeService.insertSelective(code);
			}
//			if (MkmConst.USER_STRENGTH_MAX.equals(entry.getKey())){
//				//用户最大体力是否改变
//				if (!entry.getValue().equals(userStrength)){
//					//已改变
//					User user = new User();
//					user.setPhysicalStrength(Integer.parseInt(entry.getValue()));
//					userService.updateByExampleSelective(user,new UserExample());
//				}
//			}
		}
		return ok();
	}

	@RequestMapping("/del")
	@ResponseBody
	public String del(String code)throws Exception{
		String[] ids = code.split(",");
		for (String c : ids) {
			codeService.deleteByPrimaryKey(c);
		}
		return ok();
	}

	@RequestMapping("/checkExists")
	@ResponseBody
	public String checkExists(String value) throws Exception {
		CodeExample example = new CodeExample();
		example.createCriteria().andCodeEqualTo(value);
		int cnt = codeService.countByExample(example);
		if (cnt > 0) return msg(1);
		return ok();
	}
	
	@RequestMapping("/loadSensitiveWords")
	@ResponseBody
	public String loadSensitiveWords()throws Exception{
		String txt = "";
		String filePath = CoreConstants.getProperty("social.sensitive_word_filepath");
		if(StringUtils.isNotBlank(filePath)){
			File file = new File(filePath);
			if (!(file.isFile() && file.exists())){
				file.createNewFile();
			}
			if(file.isFile() && file.exists()){
				try {
					txt = IoUtil.read(new FileInputStream(file), "utf-8");
				} catch (Exception e) {
					e.printStackTrace();
					return JSONUtils.serialize(new Ret(1,"敏感词加载失败"));
				}
			}
		}
		return JSONUtils.serialize(new Ret(0,txt));
	}
	
	@RequestMapping("/saveSensitiveWords")
	@ResponseBody
	public String saveSensitiveWords(String content)throws Exception{
		String filePath = CoreConstants.getProperty("social.sensitive_word_filepath");
		if(StringUtils.isNotBlank(filePath)){
			File file = new File(filePath);
			if(file.isFile() && file.exists()){
				try {
					IoUtil.write(new FileOutputStream(file), true, content.getBytes("utf-8"));
					if(DFA.keyMap != null)
						DFA.reloadData();
				} catch (Exception e) {
					e.printStackTrace();
					return JSONUtils.serialize(new Ret(1,"敏感词保存失败"));
				}
			}
		}
		return JSONUtils.serialize(new Ret(0));
	}
}
