package com.item.task;

import com.hr.dao.model.Contract;
import com.hr.dao.model.ContractExample;
import com.hr.service.ContractService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.base.spring.SpringContextUtil;
import com.item.service.UserService;

import java.util.Date;
import java.util.List;

@Component
public class TestCron{

	@Autowired
	private UserService userService;
	@Autowired
	private ContractService contractService;
	

	public void test(){
		System.out.println("test定时任务开始");
//		userService = SpringContextUtil.getBean(UserService.class);
		ContractExample example = new ContractExample();
		example.createCriteria().andEndTimeLessThanOrEqualTo(new Date());
		List<Contract> list = contractService.selectByExample(example);

		for(Contract contract : list){
			contract.setType(2);
			contractService.updateByPrimaryKeySelective(contract);
		}
		System.out.println("test定时任务结束");
	}
}
