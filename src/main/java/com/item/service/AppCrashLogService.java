package com.item.service;

import java.util.List;

import com.base.entity.QueryParams;
import com.item.daoEx.AppCrashLogMapperEx;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.item.dao.AppCrashLogMapper;
import com.item.dao.model.AppCrashLog;
import com.item.dao.model.AppCrashLogExample;

@Service
public class AppCrashLogService {
	@Autowired
	private AppCrashLogMapper appCrashLogMapper;
	@Autowired
	private AppCrashLogMapperEx appCrashLogMapperEx;

	public int countByExample(AppCrashLogExample example) {
		return this.appCrashLogMapper.countByExample(example);
	}

	public AppCrashLog selectByPrimaryKey(Integer id) {
		return this.appCrashLogMapper.selectByPrimaryKey(id);
	}

	public List<AppCrashLog> selectByExample(AppCrashLogExample example) {
		return this.appCrashLogMapper.selectByExample(example);
	}

	public List<AppCrashLog> selectByExampleWithBLOBs(AppCrashLogExample example) {
		return this.appCrashLogMapper.selectByExampleWithBLOBs(example);
	}

	public int deleteByPrimaryKey(Integer id) {
		return this.appCrashLogMapper.deleteByPrimaryKey(id);
	}

	public int updateByPrimaryKeySelective(AppCrashLog record) {
		return this.appCrashLogMapper.updateByPrimaryKeySelective(record);
	}

	public int updateByPrimaryKeyWithBLOBs(AppCrashLog record) {
		return this.appCrashLogMapper.updateByPrimaryKeyWithBLOBs(record);
	}

	public int updateByPrimaryKey(AppCrashLog record) {
		return this.appCrashLogMapper.updateByPrimaryKey(record);
	}

	public int deleteByExample(AppCrashLogExample example) {
		return this.appCrashLogMapper.deleteByExample(example);
	}

	public int updateByExampleSelective(AppCrashLog record, AppCrashLogExample example) {
		return this.appCrashLogMapper.updateByExampleSelective(record, example);
	}

	public int updateByExampleWithBLOBs(AppCrashLog record, AppCrashLogExample example) {
		return this.appCrashLogMapper.updateByExampleWithBLOBs(record, example);
	}

	public int updateByExample(AppCrashLog record, AppCrashLogExample example) {
		return this.appCrashLogMapper.updateByExample(record, example);
	}

	public int insert(AppCrashLog record) {
		return this.appCrashLogMapper.insert(record);
	}

	public int insertSelective(AppCrashLog record) {
		return this.appCrashLogMapper.insertSelective(record);
	}

	public List<String> selectStrList(QueryParams.Builder builder){
		return this.appCrashLogMapperEx.selectStrList(builder);
	}
}
