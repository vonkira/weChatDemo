package com.item.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.base.support.PropertySupport;
import com.base.util.JPushUtil;
import com.item.dao.MessageMapper;
import com.item.dao.model.Message;
import com.item.dao.model.MessageExample;

@Service
public class MessageService {
    @Autowired
    private MessageMapper messageMapper;
    @Autowired
    private MobileVerifyService verifyService;
    @Autowired
    private UserService userService;

    public int countByExample(MessageExample example) {
        return this.messageMapper.countByExample(example);
    }

    public Message selectByPrimaryKey(Integer id) {
        return this.messageMapper.selectByPrimaryKey(id);
    }

    public List<Message> selectByExample(MessageExample example) {
        return this.messageMapper.selectByExample(example);
    }

    public int deleteByPrimaryKey(Integer id) {
        return this.messageMapper.deleteByPrimaryKey(id);
    }

    public int updateByPrimaryKeySelective(Message record) {
        return this.messageMapper.updateByPrimaryKeySelective(record);
    }

    public int updateByPrimaryKey(Message record) {
        return this.messageMapper.updateByPrimaryKey(record);
    }
    
    public int updateByPrimaryKeyWithBLOBs(Message record) {
    	return this.messageMapper.updateByPrimaryKeyWithBLOBs(record);
    }

    public int deleteByExample(MessageExample example) {
        return this.messageMapper.deleteByExample(example);
    }

    public int updateByExampleSelective(Message record, MessageExample example) {
        return this.messageMapper.updateByExampleSelective(record, example);
    }

    public int updateByExample(Message record, MessageExample example) {
        return this.messageMapper.updateByExample(record, example);
    }

    public int insert(Message record) {
        return this.messageMapper.insert(record);
    }

    public int insertSelective(Message record) {
        return this.messageMapper.insertSelective(record);
    }
    
    public String pushToApp(String msg,Integer redirectType,String redirectContent){
    	if(StringUtils.isBlank(PropertySupport.getProperty("jpush.appkey")))return null;
		Map<String, String> params = new HashMap<String, String>();
    	params.put("redirectType", redirectType+"");
    	params.put("redirectContent", redirectContent);
		params.put("msg", msg);
		return JPushUtil.pushMessageToApp(params, msg);
    }
    
    public String pushToList(List<Integer> userId, String msg,Integer redirectType,String redirectContent){
    	if(StringUtils.isBlank(PropertySupport.getProperty("jpush.appkey")))return null;
		Map<String, String> params = new HashMap<String, String>();
    	params.put("redirectType", redirectType+"");
    	params.put("redirectContent", redirectContent);
		params.put("msg", msg);
		List<String> cids = verifyService.getCid(userId);
        if (cids == null){
            return null;
        }
		if(cids.size() == 0) return null;
		return JPushUtil.pushMessageToList(params, msg, cids);
    }
    
    public String pushToList(List<Integer> userId, String content,Integer redirectType,Map<String, String> extra){
    	if(StringUtils.isBlank(PropertySupport.getProperty("jpush.appkey")))return null;
		Map<String, String> params = new HashMap<String, String>();
    	params.put("redirectType", redirectType+"");
		params.put("msg", content);
		if (extra != null){
			params.putAll(extra);
		}
		List<String> cids = verifyService.getCid(userId);
        if (cids == null){
            return null;
        }
        if(cids.size() == 0) return null;
		return JPushUtil.pushMessageToList(params, content, cids);
    }
    
    public String pushToSingleAccount(String account, String msg,Integer redirectType,String redirectContent){
    	if(StringUtils.isBlank(PropertySupport.getProperty("jpush.appkey")))return null;
    	Map<String, String> params = new HashMap<String, String>();
    	params.put("redirectType", redirectType+"");
		params.put("msg", msg);
		params.put("redirectContent", redirectContent);
		List<String> cids = verifyService.getCid(userService.selectIdByAccount(account));
        if (cids == null){
            return null;
        }
        if(cids.size() == 0) return null;
		return JPushUtil.pushMessageToList(params, msg, cids);
    }
    
    public String pushToSingle(Integer userId, String msg,Integer redirectType,String redirectContent){
    	if(StringUtils.isBlank(PropertySupport.getProperty("jpush.appkey")))return null;
    	Map<String, String> params = new HashMap<String, String>();
    	params.put("redirectType", redirectType+"");
		params.put("msg", msg);
		params.put("redirectContent", redirectContent);
		List<String> cids = verifyService.getCid(userId);
		if (cids == null){
		    return null;
        }
        if(cids.size() == 0) return null;
		return JPushUtil.pushMessageToList(params, msg, cids);
    }
    
    public String pushToSingle(Integer userId, String msg,Integer redirectType,Map<String, String> extra){
    	if(StringUtils.isBlank(PropertySupport.getProperty("jpush.appkey")))return null;
    	Map<String, String> params = new HashMap<String, String>();
    	params.put("redirectType", redirectType+"");
		params.put("msg", msg);
		if (extra != null){
			params.putAll(extra);
		}
		List<String> cids = verifyService.getCid(userId);
        if (cids == null){
            return null;
        }
        if(cids.size() == 0) return null;
		return JPushUtil.pushMessageToList(params, msg, cids);
    }
}