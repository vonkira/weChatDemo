package com.item.service;

import java.util.List;

import com.base.api.MobileInfo;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.item.dao.UserOauthMapper;
import com.item.dao.model.UserOauth;
import com.item.dao.model.UserOauthExample;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserOauthService {
    @Autowired
    private UserOauthMapper userOauthMapper;

    private static final Logger logger = Logger.getLogger(UserOauthService.class);

    public int countByExample(UserOauthExample example) {
        return this.userOauthMapper.countByExample(example);
    }

    public UserOauth selectByPrimaryKey(Integer id) {
        return this.userOauthMapper.selectByPrimaryKey(id);
    }

    public List<UserOauth> selectByExample(UserOauthExample example) {
        return this.userOauthMapper.selectByExample(example);
    }

    public int deleteByPrimaryKey(Integer id) {
        return this.userOauthMapper.deleteByPrimaryKey(id);
    }

    public int updateByPrimaryKeySelective(UserOauth record) {
        return this.userOauthMapper.updateByPrimaryKeySelective(record);
    }

    public int updateByPrimaryKey(UserOauth record) {
        return this.userOauthMapper.updateByPrimaryKey(record);
    }

    public int deleteByExample(UserOauthExample example) {
        return this.userOauthMapper.deleteByExample(example);
    }

    public int updateByExampleSelective(UserOauth record, UserOauthExample example) {
        return this.userOauthMapper.updateByExampleSelective(record, example);
    }

    public int updateByExample(UserOauth record, UserOauthExample example) {
        return this.userOauthMapper.updateByExample(record, example);
    }

    public int insert(UserOauth record) {
        return this.userOauthMapper.insert(record);
    }

    public int insertSelective(UserOauth record) {
        return this.userOauthMapper.insertSelective(record);
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public Integer bindOpenId(MobileInfo mobileInfo, String openId){
        UserOauthExample oauthExample = new UserOauthExample();
        if (mobileInfo.getUserid() == null){
            oauthExample.createCriteria().andTypeEqualTo(4).andOpenIdEqualTo(openId);
            List<UserOauth> userOauths = userOauthMapper.selectByExample(oauthExample);
            if (userOauths.size() > 0){
                UserOauth oauth = userOauths.get(0);
                mobileInfo.setUserid(oauth.getUserId());
                return oauth.getUserId();
            }
        }else{
            oauthExample.createCriteria().andTypeEqualTo(4).andUserIdEqualTo(mobileInfo.getUserid());
            List<UserOauth> userOauths = userOauthMapper.selectByExample(oauthExample);
            if (userOauths.size() > 0){
                UserOauth oauth = userOauths.get(0);
                if (!openId.equals(oauth.getOpenId())){
                    oauth.setOpenId(openId);
                    userOauthMapper.updateByPrimaryKey(oauth);
                }
            }else{
                UserOauth oauth = new UserOauth();
                oauth.setOpenId(openId);
                oauth.setType(4);
                oauth.setUserId(mobileInfo.getUserid());
                userOauthMapper.insert(oauth);
            }
            return mobileInfo.getUserid();
        }
        return null;
    }
}