package com.item.service;

import com.base.api.MobileInfo;
import com.base.cache.CacheSupport;
import com.base.cache.CacheUtil;
import com.base.util.StringUtil;
import com.item.ConstantsCode;
import com.item.dao.MobileVerifyMapper;
import com.item.dao.model.MobileVerify;
import com.item.dao.model.MobileVerifyExample;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class MobileVerifyService {
    @Autowired
    private MobileVerifyMapper mobileVerifyMapper;

    public int countByExample(MobileVerifyExample example) {
        return this.mobileVerifyMapper.countByExample(example);
    }

    public MobileVerify selectByPrimaryKey(String deviceId) {
        return this.mobileVerifyMapper.selectByPrimaryKey(deviceId);
    }

    public List<MobileVerify> selectByExample(MobileVerifyExample example) {
        return this.mobileVerifyMapper.selectByExample(example);
    }

    public int deleteByPrimaryKey(String deviceId) {
        return this.mobileVerifyMapper.deleteByPrimaryKey(deviceId);
    }

    public int updateByPrimaryKeySelective(MobileVerify record) {
        return this.mobileVerifyMapper.updateByPrimaryKeySelective(record);
    }

    public int updateByPrimaryKey(MobileVerify record) {
        return this.mobileVerifyMapper.updateByPrimaryKey(record);
    }

    public int deleteByExample(MobileVerifyExample example) {
        return this.mobileVerifyMapper.deleteByExample(example);
    }

    public int updateByExampleSelective(MobileVerify record, MobileVerifyExample example) {
        return this.mobileVerifyMapper.updateByExampleSelective(record, example);
    }

    public int updateByExample(MobileVerify record, MobileVerifyExample example) {
        return this.mobileVerifyMapper.updateByExample(record, example);
    }

    public int insert(MobileVerify record) {
        return this.mobileVerifyMapper.insert(record);
    }

    public int insertSelective(MobileVerify record) {
        return this.mobileVerifyMapper.insertSelective(record);
    }
    
    /**
     * 更新登录信息
     * @param mobileInfo 
     * @param deviceType 设备类型 ios or android
     * @param cid 个推cid
     * @return verify
     */
    public String updateMobileVerify(MobileInfo mobileInfo,Integer deviceType,String cid){
		if (!mobileInfo.isMultiLogin()){
			mobileInfo.setMultiLogin(ConstantsCode.MULTI_LOGIN);
		}
		if(!mobileInfo.isMultiLogin()){
			//单设备登录删除其他设备的登录信息
			if (deviceType == 0){
				deleteOther(mobileInfo.getUserid(),mobileInfo.getDeviceid(),deviceType);
			}else{
				deleteOther(mobileInfo.getUserid(),mobileInfo.getDeviceid());
			}
		}

		//更新当前设备登录信息
		MobileVerify mobileVerify = new MobileVerify();
		mobileVerify.setVerify(StringUtil.getRandomUUID());
		if(deviceType == 0){
			mobileVerify.setDeviceId(StringUtil.getRandomUUID());
		}else{
			mobileVerify.setDeviceId(mobileInfo.getDeviceid());
		}
		mobileVerify.setUserId(mobileInfo.getUserid());
		mobileVerify.setDeviceType(deviceType);
		if(StringUtils.isNotBlank(cid))mobileVerify.setCid(cid);
		int count = mobileVerifyMapper.updateByPrimaryKeySelective(mobileVerify);

		if(count == 0){//未更新则新增
			mobileVerify.setCreateTime(new Date());
			mobileVerifyMapper.insert(mobileVerify);
		}
		return mobileVerify.getVerify();
	}
    
    public void logout(MobileInfo mobileInfo){
    	MobileVerify mobileVerify = mobileVerifyMapper.selectByPrimaryKey(mobileInfo.getDeviceid());
    	if(mobileVerify == null){
    		return;
    	}
    	CacheSupport.remove(CacheUtil.MOBILE_VERIFY_CACHE, mobileVerify.getVerify());
    	mobileVerifyMapper.deleteByPrimaryKey(mobileInfo.getDeviceid());
    }

	/**
	 * 删除app端的登陆token
	 * @param userid
	 * @param deviceid
	 */
	public void deleteOther(Integer userid,String deviceid) {
		MobileVerifyExample example = new MobileVerifyExample();
		MobileVerifyExample.Criteria criteria = example.createCriteria().andDeviceTypeBetween(1,2);
		if (deviceid != null){
			criteria.andUserIdEqualTo(userid).andDeviceIdNotEqualTo(deviceid);
		}else{
			criteria.andUserIdEqualTo(userid);
		}
		List<MobileVerify> verifies = mobileVerifyMapper.selectByExample(example);
		for (MobileVerify verify : verifies){
			CacheSupport.remove(CacheUtil.MOBILE_VERIFY_CACHE, verify.getVerify());
		}
		mobileVerifyMapper.deleteByExample(example);
	}

	/**
	 * 删除指定端的登陆token
	 * @param userid
	 * @param deviceid
	 * @param deviceType 0 web,1 ,2
	 */
	public void deleteOther(Integer userid,String deviceid, Integer deviceType){
		MobileVerifyExample example = new MobileVerifyExample();
		MobileVerifyExample.Criteria criteria = example.createCriteria().andDeviceTypeEqualTo(deviceType);
		if (deviceid != null){
			criteria.andUserIdEqualTo(userid).andDeviceIdNotEqualTo(deviceid);
		}else{
			criteria.andUserIdEqualTo(userid);
		}
		List<MobileVerify> verifies = mobileVerifyMapper.selectByExample(example);
		for (MobileVerify verify : verifies){
			CacheSupport.remove(CacheUtil.MOBILE_VERIFY_CACHE, verify.getVerify());
		}
		mobileVerifyMapper.deleteByExample(example);
	}
    
	public void deleteOther(MobileInfo mobileInfo) {
    	deleteOther(mobileInfo.getUserid(),mobileInfo.getDeviceid());
	}
	
    public List<String> getCid(Integer userId){
    	MobileVerifyExample example = new MobileVerifyExample();
    	example.createCriteria().andUserIdEqualTo(userId).andDeviceTypeBetween(1,2);
    	List<MobileVerify> list = mobileVerifyMapper.selectByExample(example);
    	List<String> cids = new ArrayList<String>();
    	for (MobileVerify verify : list){
    		if (StringUtils.isNotBlank(verify.getCid())&& !cids.contains(verify.getCid())){
    			cids.add(verify.getCid());
    		}
    	}
    	if (cids.size() > 0){
    		return cids;
    	}
    	return null;
    }
    
    public List<String> getCid(List<Integer> userIds){
    	MobileVerifyExample example = new MobileVerifyExample();
    	example.createCriteria().andUserIdIn(userIds).andDeviceTypeBetween(1,2);
    	List<MobileVerify> list = mobileVerifyMapper.selectByExample(example);
    	List<String> cids = new ArrayList<String>();
    	for (MobileVerify verify : list){
    		if (StringUtils.isNotBlank(verify.getCid()) && !cids.contains(verify.getCid())){
    			cids.add(verify.getCid());
    		}
    	}
    	if (cids.size() > 0){
    		return cids;
    	}
    	return null;
    }

	public MobileVerify queryByToken(String token) {
		MobileVerifyExample example = new MobileVerifyExample();
		example.createCriteria().andVerifyEqualTo(token);
		List<MobileVerify> list = selectByExample(example);
		if(list.size()>0)return list.get(0);
		return null;
	}
}