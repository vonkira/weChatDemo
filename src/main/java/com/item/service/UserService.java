package com.item.service;

import com.base.entity.QueryParams;
import com.base.web.annotation.LoginMethod;
import com.item.dao.MobileVerifyMapper;
import com.item.dao.UserMapper;
import com.item.dao.model.User;
import com.item.dao.model.UserExample;
import com.item.daoEx.UserMapperEx;
import com.item.daoEx.model.UserEx;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class UserService {
	@Autowired
	private UserMapper userMapper;
	@Autowired
	private UserMapperEx userMapperEx;
	@Autowired
	private MobileVerifyMapper verifyMapper;
	@Autowired
	private NotifyService notifyService;
	@Autowired
	private BaseService baseService;

	public int countByExample(UserExample example) {
		return this.userMapper.countByExample(example);
	}

	public User selectByPrimaryKey(Integer id) {
		return this.userMapper.selectByPrimaryKey(id);
	}

	public List<User> selectByExample(UserExample example) {
		return this.userMapper.selectByExample(example);
	}

	public int deleteByPrimaryKey(Integer id) {
		return this.userMapper.deleteByPrimaryKey(id);
	}

	public int updateByPrimaryKeySelective(User record) {
		return this.userMapper.updateByPrimaryKeySelective(record);
	}

	public int updateByPrimaryKey(User record) {
		return this.userMapper.updateByPrimaryKey(record);
	}

	public int deleteByExample(UserExample example) {
		return this.userMapper.deleteByExample(example);
	}

	public int updateByExampleSelective(User record, UserExample example) {
		return this.userMapper.updateByExampleSelective(record, example);
	}

	public int updateByExample(User record, UserExample example) {
		return this.userMapper.updateByExample(record, example);
	}

	public int insert(User record) {
		int cnt = this.userMapper.insert(record);
		notifyService.insertNewUser(record.getId());
		return cnt;
	}

	public int insertSelective(User record) {
		int cnt =  this.userMapper.insertSelective(record);
		notifyService.insertNewUser(record.getId());
		return cnt;
	}

	public void updateBalance(String userId, BigDecimal goodsTotal) {
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("id", userId);
		map.put("num", goodsTotal);
		userMapperEx.updateBalance(map);
	}

	@Transactional(propagation = Propagation.NOT_SUPPORTED)
	public int updateStrength(QueryParams.Builder builder){
		return this.userMapperEx.updateStrength(builder);
	}

	public List<UserEx> selectList(Map<String, Object> map) {
		return userMapperEx.selectList(map);
	}
	
	public List<Integer> selectIdByAccounts(List<String> accounts){
		return userMapperEx.selectIdByAccounts(accounts);
	}
	
	public Integer selectIdByAccount(String account){
		return userMapperEx.selectIdByAccount(account);
	}
	
	public String selectAccountById(String id){
		return userMapperEx.selectAccountById(id);
	}
	
    public boolean autoH5Login(HttpServletRequest request,HttpServletResponse response){
		Cookie[] cookies = request.getCookies();
		Cookie c = null;
		String account = "";
		String pwd = "";
		for (int i = 0; cookies!=null && i < cookies.length; i++){
			c = cookies[i];
			if (c.getName().equals("account")){
				account = c.getValue();
			}else if(c.getName().equals("pwd")){
				pwd = c.getValue();
			}
		}
		if(account.length()>0 && pwd.length()>0){
			UserExample example = new UserExample();
			example.createCriteria().andAccountEqualTo(account).andPasswordEqualTo(pwd).andStateEqualTo(1);
			List<User> list = this.selectByExample(example);
			if(list.size()>0){
				User user = list.get(0);
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("id", user.getId());
				map.put("account", user.getAccount());
				request.getSession().setAttribute(LoginMethod.PHONE.getName(), map);
				return true;
			}
		}
    	return false;
    }

    public List<UserEx> selectNear(QueryParams.Builder builder){
    	return this.userMapperEx.selectNear(builder);
	}

	public List<UserEx> selectAppList(QueryParams.Builder builder){
    	return this.userMapperEx.selectAppList(builder);
	}

	public List<UserEx> selectMyFriend(Integer userId){
		return this.userMapperEx.selectMyFriend(userId);
	}


	@Transactional(propagation = Propagation.NOT_SUPPORTED)
	public int updateUserStrength(QueryParams.Builder builder){
		return this.userMapperEx.updateUserStrength(builder);
	}

	public Integer selectCoinSort(QueryParams.Builder builder){
		return this.userMapperEx.selectCoinSort(builder);
	}

	public Integer selectGoodsSort(QueryParams.Builder builder){
		return this.userMapperEx.selectGoodsSort(builder);
	}

	public List<UserEx> selectGoodsSortUser(QueryParams.Builder builder){
		return this.userMapperEx.selectGoodsSortUser(builder);
	}

	public List<UserEx> selectCoinList(QueryParams.Builder builder){
		return this.userMapperEx.selectCoinList(builder);
	}


	public int updateStrengthByUser(QueryParams.Builder builder){
		return this.userMapperEx.updateStrengthByUser(builder);
	}
}