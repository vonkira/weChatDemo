package com.item.daoEx;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.item.daoEx.model.AdminEx;

public interface AdminMapperEx {

	AdminEx selectUser(String id);
	
	List<AdminEx> selectAdmins(@Param("group")Integer groupId, @Param("role")Integer roleId);

    List<AdminEx> selectList(Map<String, Object> map);
}
