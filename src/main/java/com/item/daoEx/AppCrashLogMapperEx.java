package com.item.daoEx;

import com.base.entity.QueryParams;

import java.util.List;

/**
@author sun
*/
public interface AppCrashLogMapperEx {
    List<String> selectStrList(QueryParams.Builder builder);
}
