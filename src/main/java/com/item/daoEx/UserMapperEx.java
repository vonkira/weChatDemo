package com.item.daoEx;

import java.util.List;
import java.util.Map;

import com.base.entity.QueryParams;
import org.apache.ibatis.annotations.Param;

import com.item.daoEx.model.UserEx;


public interface UserMapperEx{

	List<UserEx> selectList(Map<String, Object> map);

	void updateBalance(Map<String, Object> map);

	List<Integer> selectIdByAccounts(@Param("accounts")List<String> accounts);
	
	Integer selectIdByAccount(String account);
	
	String selectAccountById(String id);

	List<UserEx> selectNear(QueryParams.Builder builder);

	List<UserEx> selectAppList(QueryParams.Builder builder);

	List<UserEx> selectMyFriend(Integer userId);

	int updateUserStrength(QueryParams.Builder builder);

	Integer selectCoinSort(QueryParams.Builder builder);

	Integer selectGoodsSort(QueryParams.Builder builder);

	List<UserEx> selectGoodsSortUser(QueryParams.Builder builder);

	int updateStrength(QueryParams.Builder builder);

	List<UserEx> selectCoinList(QueryParams.Builder builder);

	int updateStrengthByUser(QueryParams.Builder builder);
}