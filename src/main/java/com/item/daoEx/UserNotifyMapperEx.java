package com.item.daoEx;

import com.item.dao.model.UserNotify;

import java.util.List;


public interface UserNotifyMapperEx{

    int insertBatch(List<UserNotify> list);
}