package com.item;

/**
 * 用户物品来源
 */
public enum MkmGoodsSource {
    ZHUCE("注册","{}"),      //注册
    DIGNSHI("定时宝箱","{}"), //定时宝箱
    DANMU("弹幕",""),  //弹幕
    OTHER("其他",""),  //其他
    XUNI("地图","{}"),   //虚拟
    ZUHE("组合","{}"),   //组合
    AR("AR","{}"),     //AR
    H5("H5","{}"),     //AR
    STORE("商家活动","{}"),     //AR
    REGULAR("固定",""),     //AR
    SYSTEM("系统","{}通过后台添加"),
    DUOQU("夺取","从好友{}({})的仓库中夺取{}x{}"),
    BEIDUOQU("被夺取","被好友{}({})夺取{}x{}"),
    USE("使用",""),
    USE_JBK("使用金币卡","使用金币卡"),
    USE_ADD("使用获得","使用{}"),
    CHECK("商家核销","券{}于{}在商家{}({})被核销"),
    MALL("商城消费","商城购物"),
    USER_PUSH_CYCLE("用户投放","无人拾取,退回仓库"),
    USER_PUSH("用户投放","拾取用户{}({})投放的{}x{}"),
    TRIGGER_SUB("参与活动","触发器扣除物品:{}x{}"),
    USER_PUSH_LOG("用户投放","投放{}x{}"),
    USER_DEL("删除物品","用户{}删除{}x{}"),
    GOODS_EXPIRE("物品过期","物品已过期{}天,系统自动删除"),
    HB_USE("使用红包","现金红包于{}使用,金额:{}"),
    ORDER_CANCEL("订单取消","订单{}超时自动取消，物品退回仓库"),
    MALL_ORDER_GIFT("商城赠品","随订单:{}附赠"),
    ;
    private String item;
    private String template;
    MkmGoodsSource(String item, String template){
        this.item = item;
        this.template = template;
    }

    public String getItem() {
        return item;
    }

    public String getTemplate() {
        return template;
    }

    public void setTemplate(String template) {
        this.template = template;
    }
}
