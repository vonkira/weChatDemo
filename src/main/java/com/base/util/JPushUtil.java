package com.base.util;

import java.util.List;
import java.util.Map;

import com.base.push.JpushClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cn.jiguang.common.ClientConfig;
import cn.jiguang.common.ServiceHelper;
import cn.jiguang.common.connection.NativeHttpClient;
import cn.jiguang.common.resp.APIConnectionException;
import cn.jiguang.common.resp.APIRequestException;
import cn.jpush.api.JPushClient;
import cn.jpush.api.push.PushResult;
import cn.jpush.api.push.model.Message;
import cn.jpush.api.push.model.Options;
import cn.jpush.api.push.model.Platform;
import cn.jpush.api.push.model.PushPayload;
import cn.jpush.api.push.model.audience.Audience;
import cn.jpush.api.push.model.notification.AndroidNotification;
import cn.jpush.api.push.model.notification.IosNotification;
import cn.jpush.api.push.model.notification.Notification;

import com.base.CoreConstants;

public class JPushUtil {

	protected static final Logger LOG = LoggerFactory
			.getLogger(JPushUtil.class);

	private static String APPKEY = CoreConstants.getProperty("jpush.appkey");
	private static String SECRET = CoreConstants.getProperty("jpush.secret");
	private static boolean PRODUCT = "true".equals(CoreConstants.getProperty("jpush.product"))?true:false;
	private static JpushClient jpushClient;
	static {
		jpushClient = new JpushClient(APPKEY,SECRET,PRODUCT);
	}

	public static String pushMessageToApp(Map<String,String> params,String iosMsg) {
		return jpushClient.pushMessageToApp(params,iosMsg);
	}
	
	public static String pushMessageToSingle(Map<String,String> params,String iosMsg,String cid){
		return jpushClient.pushMessageToSingle(params,iosMsg,cid);
	}
	
	public static String pushMessageToList(Map<String,String> params,String iosMsg,List<String> cid){
		return jpushClient.pushMessageToList(params,iosMsg,cid);
	}

	public static String pushOnlyMessageToApp(Map<String,String> params,String iosMsg) {
		return jpushClient.pushMessageToApp(params,iosMsg,false,true);
	}

}
