package com.base.api;

import com.base.exception.IMError;

public class ErrorResult {
    private int errorCode;
    private String errorMsg;

    public ErrorResult(IMError error) {
        this.errorCode = error.getErrorCode();
        this.errorMsg = error.getErrorMsg();
    }

    public ErrorResult(int errorCode) {
        this.errorCode = errorCode;
    }

    public ErrorResult(int errorCode, String errorMsg) {
        this.errorCode = errorCode;
        this.errorMsg = errorMsg;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }
}
