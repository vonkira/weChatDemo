package com.base.api;

import com.base.CoreConstants;
import com.base.api.ApiException;
import com.base.dao.model.Result;
import com.base.dao.model.Ret;
import com.base.util.ResponseUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ApiBaseController {
	protected Logger logger = LoggerFactory.getLogger(getClass());

	@ExceptionHandler(Exception.class)
	@ResponseStatus(value = HttpStatus.OK)
	public void handleException(Exception ex, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		ErrorResult ret;
		if (ex instanceof ApiException) {
			ApiException se = (ApiException) ex;
			ret = new ErrorResult(se.getCode(), ex.getMessage());
		} else {
			logger.error(ex.getMessage(), ex);
			ret = new ErrorResult(500, ex.getMessage());
		}
		ResponseUtils.renderJson(response, ret);
	}

	@InitBinder
	public void initBinder(WebDataBinder binder) {
		SimpleDateFormat dateFormat = new SimpleDateFormat(
				"yyyy-MM-dd HH:mm:ss");
		dateFormat.setLenient(false);
		binder.registerCustomEditor(Date.class, new CustomDateEditor(
				dateFormat, true));
	}

	protected String getUrl(String fileid) {
		return getUrl(fileid, null, null);
	}

	protected String getUrl(String fileid, Integer width, Integer height) {
		if (StringUtils.isNotBlank(fileid)) {
			String url = CoreConstants.SERVER_URL + "download?id=" + fileid;
			if (width != null)
				url += "&w=" + width;
			if (height != null)
				url += "&h=" + height;
			return url;
		} else {
			return "";
		}
	}

	protected String getPage(String id, Integer type) {
		return CoreConstants.SERVER_URL + "detail?id=" + id + "&type=" + type;
	}
	
	protected Ret ok(){
		return new Ret();
	}
	
	protected Ret msg(int code, String msg){
		return new Ret(code,msg);
	}
	
	protected Ret msg(String msg){
		return msg(0, msg);
	}

}
