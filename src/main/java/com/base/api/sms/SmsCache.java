package com.base.api.sms;

import com.base.cache.redis.ReditClient;

/**
 * 短信缓存工具类
 */
public class SmsCache {
    private static final String SMS_PRE = "mkm:sms:";

    public static void putCache(String key,String value){
        ReditClient.set(getKey(key),value,5*60L);
    }

    public static String getCache(String key){
        return ReditClient.get(getKey(key),String.class);
    }

    public static void delCache(String key){
        ReditClient.delete(getKey(key));
    }

    private static String getKey(String key){
        return SMS_PRE+key;
    }
}
