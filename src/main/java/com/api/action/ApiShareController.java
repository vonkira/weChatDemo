package com.api.action;

import com.api.view.common.AppShare;
import com.base.CoreConstants;
import com.base.api.ApiBaseController;
import com.base.api.MobileInfo;
import com.base.api.annotation.ApiMethod;
import com.base.util.StringUtil;
import com.item.service.CodeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

@RestController
@RequestMapping(value="/api/share", produces = {"application/json;charset=UTF-8"})
@Api(tags = "分享接口")
public class ApiShareController extends ApiBaseController{
    @Autowired
    private CodeService codeService;

    private static final String SHARE = "m/share/";

    private enum shareEnum{
        INVITE("m/share/invite/{}"),
        GOODS("m/share/goods/{}?num={}"),
        VIDEO("m/share/video/{}");
        String url;
        shareEnum(String url){
            this.url = url;
        }
    }

    @ApiMethod(isLogin = true)
    @ApiOperation(value = "分享物品", notes = "")
    @RequestMapping(value = "/goods",method = {RequestMethod.POST})
    public AppShare shareGoods(MobileInfo mobileInfo,
                               @ApiParam("物品id,不是仓库id") Integer goodsId,
                               @ApiParam("物品数量") Integer num){
        AppShare share = new AppShare();
        Map<String,String> map = new HashMap<>();
        map.put("id",goodsId+"");
        map.put("num",num+"");
        map.put("type","1");
        map.put("uid",mobileInfo.getUserid().toString());
//        share.setUrl(buildUrlWithQueryString(codeService.getShareUrl(),map));
//        share.setUrl(CoreConstants.SERVER_URL+StringUtil.format(shareEnum.GOODS.url,goodsId,num));
        return share;
    }

    @ApiMethod(isLogin = true)
    @ApiOperation(value = "分享邀请", notes = "")
    @RequestMapping(value = "/invite",method = {RequestMethod.POST})
    public AppShare shareinvite(MobileInfo mobileInfo){
        AppShare share = new AppShare();
        Map<String,String> map = new HashMap<>();
        map.put("id",mobileInfo.getUserid().toString());
        map.put("type","3");
        map.put("uid",mobileInfo.getUserid().toString());
//        share.setUrl(buildUrlWithQueryString(codeService.getShareUrl(),map));
//        share.setUrl(CoreConstants.SERVER_URL+StringUtil.format(shareEnum.INVITE.url,mobileInfo.getUserid()));
        return share;
    }

    @ApiMethod(isLogin = true)
    @ApiOperation(value = "分享视频", notes = "")
    @RequestMapping(value = "/video",method = {RequestMethod.POST})
    public AppShare shareVideo(MobileInfo mobileInfo,
                               @ApiParam("视频的id") String fileId){
        AppShare share = new AppShare();
        Map<String,String> map = new HashMap<>();
        map.put("id",fileId);
        map.put("type","2");
        map.put("uid",mobileInfo.getUserid().toString());
//        share.setUrl(buildUrlWithQueryString(codeService.getShareUrl(),map));
//        share.setUrl(CoreConstants.SERVER_URL+StringUtil.format(shareEnum.VIDEO.url,fileId));
        return share;
    }

    private static String buildUrlWithQueryString(String url, Map<String, String> queryParas) {
        if (queryParas != null && !queryParas.isEmpty()) {
            StringBuilder sb = new StringBuilder(url);
            boolean isFirst;
            if (!url.contains("?")) {
                isFirst = true;
                sb.append("?");
            } else {
                isFirst = false;
            }

            String key;
            String value;
            for(Iterator var4 = queryParas.entrySet().iterator(); var4.hasNext(); sb.append(key).append("=").append(value)) {
                Map.Entry<String, String> entry = (Map.Entry)var4.next();
                if (isFirst) {
                    isFirst = false;
                } else {
                    sb.append("&");
                }

                key = (String)entry.getKey();
                value = (String)entry.getValue();
                if (StringUtil.notBlank(value)) {
                    try {
                        value = URLEncoder.encode(value, "utf-8");
                    } catch (UnsupportedEncodingException var9) {
                        throw new RuntimeException(var9);
                    }
                }
            }

            return sb.toString();
        } else {
            return url;
        }
    }
}
