package com.api.action;

import com.api.MEnumError;
import com.api.anno.ApiUnCheck;
import com.api.view.account.OauthMobileInfo;
import com.api.view.account.SmsCode;
import com.api.view.home.UserInfo;
import com.base.Const;
import com.base.api.ApiBaseController;
import com.base.api.ApiException;
import com.base.api.MobileInfo;
import com.base.api.annotation.ApiMethod;
import com.base.api.sms.SmsCache;
import com.base.api.sms.SmsError;
import com.base.dao.model.Ret;
import com.base.support.PropertySupport;
import com.base.util.DateUtil;
import com.base.util.IPUtil;
import com.base.util.StringUtil;
import com.item.dao.model.User;
import com.item.dao.model.UserExample;
import com.item.dao.model.UserOauth;
import com.item.dao.model.UserOauthExample;
import com.item.service.MobileVerifyService;
import com.item.service.SmsSendLogService;
import com.item.service.UserOauthService;
import com.item.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping(value = "/api/account", produces = { "application/json;charset=UTF-8" })
@Api(tags = "账号登陆/注册")
public class ApiLoginController extends ApiBaseController {

	@Autowired
	private UserService userService;
	@Autowired
	private MobileVerifyService verifyService;
	@Autowired
	private SmsSendLogService smsSendLogService;
	@Autowired
	private UserOauthService oauthService;

	private enum MobileMsgEnum {
		REGIST(1), FORGET(2), LOGIN(3);
		private int code;

		MobileMsgEnum(int code) {
			this.code = code;
		}

		public int getCode() {
			return this.code;
		}
		
		public String getPhone(String phone){
			return phone+Const.SEP+this.name();
		}
	}

	/**
	 * 获取验证码
	 * 
	 * @param phone
	 *            手机号
	 * @param type
	 *            1:注册,绑定手机 2:忘记密码
	 */
	@ApiOperation(value = "获取验证码", notes = "不需要登录<br/>测试阶段直接取返回的code,不发送短信")
	@RequestMapping(value = "/getMobileMsg", method = RequestMethod.POST)
	@ApiMethod(isAuth = true)
	public SmsCode getMobileMsg(
			@ApiParam(value = "手机号", required = true) String phone,
			@ApiParam(value = "1:注册 2:忘记密码 3:验证码登陆,绑定手机", required = true) Integer type,
			@ApiParam(value = "设备唯一识别码", required = false) String deviceid,
			@ApiParam(hidden = true) HttpServletRequest request)
			throws Exception {

		if (StringUtils.isBlank(phone)) {
			throw new ApiException("phone");
		}
		if (type == null) {
			throw new ApiException("type");
		}
		// 查询
		UserExample example = new UserExample();
		example.createCriteria().andAccountEqualTo(phone);

		String key;
		
		if (type == MobileMsgEnum.REGIST.getCode()) {// 注册
			int count = userService.countByExample(example);
			if (count > 0) {
				throw new ApiException(MEnumError.PHONE_EXISTS_ERROR);
			}
			key = MobileMsgEnum.REGIST.getPhone(phone);
		} else if (type == MobileMsgEnum.FORGET.getCode()) {
			int count = userService.countByExample(example);
			if (count == 0) {
				throw new ApiException(MEnumError.PHONE_NOTEXISTS_ERROR);
			}
			key = MobileMsgEnum.FORGET.getPhone(phone);
		}else if (type == MobileMsgEnum.LOGIN.getCode()) {
			key = MobileMsgEnum.LOGIN.getPhone(phone);
		}else {
			throw new ApiException(-1,"参数错误");
		}

		// 发送短信
		String mobileCode = StringUtil.getRandomNum(6);
		String content = new String("您的验证码是：" + mobileCode + "。请不要把验证码泄露给其他人。请于5分钟内完成验证！");

		String result = smsSendLogService.sendIdentifySms(phone, deviceid,
				IPUtil.getClientMacAddress(request), content,type);
		if ("2".equals(result)) {
			// 发送成功
			SmsCache.putCache(key,mobileCode);
//			CacheSupport.put("mobileCodeCache", key, mobileCode);
		} else {
			// 发送失败,可以细化错误原因
			String error = SmsError.getSmsError(result);
			if (StringUtils.isNotBlank(error)) {
				logger.debug(error);
				throw new ApiException(MEnumError.SMS_ERROR.getErrorCode(),
						error);
			}
			throw new ApiException(MEnumError.SMS_ERROR);
		}

		SmsCode ret = new SmsCode();
		if (!"true".equals(PropertySupport.getProperty("sms.enable"))){
			ret.setCode(mobileCode + "");
		}
		return ret;
	}

	/**
	 * 验证验证码
	 * 
	 * @param phone
	 *            手机号
	 * @param code
	 *            验证码
	 */
	@ApiOperation(value = "验证验证码", notes = "不需要登录")
	@RequestMapping(value = "/checkCode", method = RequestMethod.POST)
	@ApiMethod
	public Ret checkCode(
			@ApiParam(value = "手机号", required = true) String phone,
			@ApiParam(value = "验证码", required = true) String code)
			throws Exception {
		if (StringUtils.isBlank(phone)) {
			throw new ApiException("phone");
		}
		if (StringUtils.isBlank(code)) {
			throw new ApiException("code");
		}
		// 验证码验证
		String value = SmsCache.getCache(MobileMsgEnum.REGIST.getPhone(phone));
//		String value = CacheSupport.get("mobileCodeCache", MobileMsgEnum.REGIST.getPhone(phone), String.class);
		
		if (!code.equals(value)) {
			throw new ApiException(MEnumError.MOBILE_CODE_ERROR);
		}
		Ret ret = new Ret();
		ret.setCode(0);

		return ret;
	}

	/**
	 * 验证昵称
	 * 
	 * @param nickName
	 *            昵称
	 */
	@ApiOperation(value = "验证昵称", notes = "不需要登录")
	@RequestMapping(value = "/checkNick", method = RequestMethod.POST)
	@ApiMethod
	public Ret checkNick(
			@ApiParam(value = "昵称", required = true) String nickName)
			throws Exception {
		if (StringUtils.isBlank(nickName)) {
			throw new ApiException("nickName");
		}
		UserExample example = new UserExample();
		example.createCriteria().andNickNameEqualTo(nickName);
		int i = userService.countByExample(example);
		if (i > 0) {
			throw new ApiException(MEnumError.NICK_EXISTS_ERROR);
		}
		Ret ret = new Ret();
		ret.setCode(0);

		return ret;
	}

	/**
	 * 注册
	 * 
	 * @param phone
	 *            手机号
	 * @param code
	 *            验证码
	 * @param password
	 *            密码(需要加密)
	 * @param deviceType
	 *            //设备类型 1:android 2:ios
	 * @param cid
	 *            //推送cid
	 */
	@ApiOperation(value = "注册", notes = "不需要登录")
	@RequestMapping(value = "/regist", method = RequestMethod.POST)
	@ApiMethod
	public MobileInfo regist(
			@ApiParam(value = "手机号", required = true) String phone,
			@ApiParam(value = "验证码", required = true) String code,
			@ApiParam(value = "密码(需要MD5加密)", required = true) String password,
//			@ApiParam(value = "昵称", required = true) String nickName,
			@ApiParam(value = "设备类型 0:网页1:android 2:ios", required = true) Integer deviceType,
			@ApiParam(value = "设备唯一识别码", required = false) String deviceid,
			@ApiParam(value = "推送cid", required = false) String cid)
			throws Exception {

		// 参数校验
		if (StringUtils.isBlank(phone)) {
			throw new ApiException("phone");
		}
		if (StringUtils.isBlank(code)) {
			throw new ApiException("code");
		}
		if (StringUtils.isBlank(password)) {
			throw new ApiException("password");
		}
//		if (StringUtils.isBlank(nickName)) {
//			throw new ApiException("nickName");
//		}

		if (deviceType == null) {
			throw new ApiException("deviceType");
		}

		if (deviceType != 0){
			if (StringUtils.isBlank(deviceid)) {
				throw new ApiException("deviceid");
			}
		}

		// 验证码验证
//		String value = CacheSupport.get("mobileCodeCache", MobileMsgEnum.REGIST.getPhone(phone), String.class);
		String value = SmsCache.getCache(MobileMsgEnum.REGIST.getPhone(phone));
		if (!code.equals(value)) {
			throw new ApiException(MEnumError.MOBILE_CODE_ERROR);
		} else {
			SmsCache.delCache(MobileMsgEnum.REGIST.getPhone(phone));
//			CacheSupport.remove("mobileCodeCache", phone);
		}

		UserExample example = new UserExample();
		example.createCriteria().andAccountEqualTo(phone);
		int i = userService.countByExample(example);
		if (i > 0) {
			throw new ApiException(MEnumError.PHONE_EXISTS_ERROR);
		}

//		example.clear();
//		example.createCriteria().andNickNameEqualTo(nickName);
//		i = userService.countByExample(example);
//		if (i > 0) {
//			throw new ApiException(MEnumError.NICK_EXISTS_ERROR);
//		}

		User record = regist(phone, password, deviceType, deviceid, cid,
				null);

		//新用户优惠券

		MobileInfo mobileInfo = new MobileInfo();
		mobileInfo.setUserid(record.getId());
		mobileInfo.setDeviceid(deviceid);
		String verify = verifyService.updateMobileVerify(mobileInfo,
				deviceType, cid);
		mobileInfo.setDeviceType(deviceType);
		mobileInfo.setToken(verify);
		mobileInfo.setIsComplete(record.getIsComplete());
		return mobileInfo;
	}

	private User regist(String phone, String password, Integer deviceType,
			String deviceid, String cid, String nickName) {
		// 添加数据
		Date date = new Date();
		User record = new User();
		record.setAccount(phone);
		record.setCreateTime(date);
		record.setState(1);
		record.setPassword(password);
//		record.setNickName("匿名用户");
		record.setCoin(0);
		record.setIsNear(0);
		record.setSex(0);
		record.setIsComplete(0);
		record.setLoginTime(date);
		record.setNickName(nickName);
		record.setPhysicalStrength(0);
		record.setIsBindWeixin(0);
		record.setIsBindWeibo(0);
		record.setIsBindQq(0);
		record.setIsComplete(0);
		record.setCredit(0);
		// 保存
		int cnt = userService.insertSelective(record);
		if (cnt == 0) {
			throw new ApiException(MEnumError.CREATE_ACCOUNT_ERROR);
		}

		//注册
//		userCouponService.regist(record.getId());

		return record;
	}

	/**
	 * @api 密码登录
	 * @param phone
	 *            手机号
	 * @param password
	 *            密码(需要加密)
	 * @param deviceType
	 *            设备类型 1:android 2:ios
	 * @param cid
	 *            //推送cid
	 */
	@ApiOperation(value = "密码登录")
	@RequestMapping(value = "/loginByPwd", method = RequestMethod.POST)
	@ApiMethod
	public MobileInfo loginByPwd(
			@ApiParam(value = "手机号", required = true) String phone,
			@ApiParam(value = "密码(需要MD5加密)", required = true) String password,
			@ApiParam(value = "设备类型 0:网页1:android 2:ios", required = true) Integer deviceType,
			@ApiParam(value = "设备唯一识别码(app必填)", required = false) String deviceid,
			@ApiParam(value = "推送cid", required = false) String cid)
			throws Exception {
		// 参数校验
		if (StringUtils.isBlank(phone)) {
			throw new ApiException("phone");
		}
		if (StringUtils.isBlank(password)) {
			throw new ApiException("password");
		}
		if (deviceType == null) {
			throw new ApiException("deviceType");
		}
		if(deviceType!=0){
			if (StringUtils.isBlank(deviceid)) {
				throw new ApiException("deviceid");
			}
		}

		// 校验登录
		UserExample example = new UserExample();
		example.createCriteria().andAccountEqualTo(phone);
		List<User> list = userService.selectByExample(example);
		if (list.size() == 0) {
			throw new ApiException(MEnumError.LOGIN_FAILURE_ERROR);
		}
		User user = list.get(0);
		if (!password.equals(user.getPassword())) {
			throw new ApiException(MEnumError.LOGIN_FAILURE_ERROR);
		}
		// 禁用
		if (user.getState() == 0) {
			throw new ApiException(MEnumError.ACCOUNT_STOP_ERROR);
		}
		MobileInfo mobileInfo = new MobileInfo();
		mobileInfo.setUserid(user.getId());
		mobileInfo.setDeviceid(deviceid);
		mobileInfo.setDeviceType(deviceType);
		String verify = verifyService.updateMobileVerify(mobileInfo,
				deviceType, cid);
		mobileInfo.setToken(verify);
		mobileInfo.setIsComplete(user.getIsComplete());
		return mobileInfo;
	}

	/**
	 * @api 短信登录
	 * @param phone
	 *            手机号
	 * @param code
	 *            验证码
	 * @param deviceType
	 *            设备类型 1:android 2:ios
	 * @param cid
	 *            //推送cid
	 */
	@ApiOperation(value = "短信登录")
	@RequestMapping(value = "/loginByCode", method = RequestMethod.POST)
	@ApiMethod
	public MobileInfo loginByCode(
			@ApiParam(value = "手机号", required = true) String phone,
			@ApiParam(value = "验证码", required = true) String code,
			@ApiParam(value = "设备类型 0:h5 1:android 2:ios", required = true) Integer deviceType,
			@ApiParam(value = "设备唯一识别码", required = false) String deviceid,
			@ApiParam(value = "推送cid", required = false) String cid)
			throws Exception {
		// 参数校验
		if (StringUtils.isBlank(phone)) {
			throw new ApiException("phone");
		}
		if (StringUtils.isBlank(code)) {
			throw new ApiException("code");
		}
		if (deviceType == null) {
			throw new ApiException("deviceType");
		}
		if (deviceType != 0){
			if (StringUtils.isBlank(deviceid)) {
				throw new ApiException("deviceid");
			}
		}

		// 验证码验证
//		String value = CacheSupport.get("mobileCodeCache", MobileMsgEnum.LOGIN.getPhone(phone), String.class);
		String value = SmsCache.getCache(MobileMsgEnum.LOGIN.getPhone(phone));
		if (!code.equals(value)) {
			throw new ApiException(MEnumError.MOBILE_CODE_ERROR);
		} else {
			SmsCache.delCache(MobileMsgEnum.LOGIN.getPhone(phone));
//			CacheSupport.remove("mobileCodeCache", phone);
		}

		// 校验登录
		UserExample example = new UserExample();
		example.createCriteria().andAccountEqualTo(phone);
		List<User> list = userService.selectByExample(example);
		User user;
		if (list.size() == 0) {
			user = regist(phone, null, deviceType, deviceid, cid,
					StringUtil.formatPhone(phone));
		} else {
			user = list.get(0);
		}

		// 禁用
		if (user.getState() == 0) {
			throw new ApiException(MEnumError.ACCOUNT_STOP_ERROR);
		}

		MobileInfo mobileInfo = new MobileInfo();
		mobileInfo.setUserid(user.getId());
		mobileInfo.setDeviceid(deviceid);
		mobileInfo.setDeviceType(deviceType);
		String verify = verifyService.updateMobileVerify(mobileInfo,
				deviceType, cid);
		mobileInfo.setToken(verify);
		mobileInfo.setIsComplete(user.getIsComplete());
		return mobileInfo;
	}

	/**
	 * 忘记密码
	 * 
	 * @param phone
	 *            手机号
	 * @param code
	 *            验证码
	 * @param password
	 *            密码(需要加密)
	 * @param deviceType
	 *            //设备类型 1:android 2:ios
	 * @param cid
	 *            //推送cid
	 */
	@ApiOperation(value = "忘记密码")
	@RequestMapping(value = "/forgetPwd", method = RequestMethod.POST)
	@ApiMethod
	public MobileInfo forgetPwd(
			@ApiParam(value = "手机号", required = true) String phone,
			@ApiParam(value = "验证码", required = true) String code,
			@ApiParam(value = "密码(需要加密)", required = true) String password,
			@ApiParam(value = "设备类型 0:网页 1:android 2:ios", required = true) Integer deviceType,
			@ApiParam(value = "设备唯一识别码", required = false) String deviceid,
			@ApiParam(value = "推送cid", required = false) String cid)
			throws Exception {
		// 参数校验
		if (StringUtils.isBlank(phone)) {
			throw new ApiException("phone");
		}
		if (StringUtils.isBlank(code)) {
			throw new ApiException("code");
		}
		if (deviceType == null) {
			throw new ApiException("deviceType");
		}

		if (deviceType != 0){
			if (StringUtils.isBlank(deviceid)) {
				throw new ApiException("deviceid");
			}
		}
		// 验证码验证
//		String value = CacheSupport.get("mobileCodeCache", MobileMsgEnum.FORGET.getPhone(phone), String.class);
		String value = SmsCache.getCache(MobileMsgEnum.FORGET.getPhone(phone));
		if (!code.equals(value)) {
			throw new ApiException(MEnumError.MOBILE_CODE_ERROR);
		} else {
			SmsCache.delCache(MobileMsgEnum.FORGET.getPhone(phone));
//			CacheSupport.remove("mobileCodeCache", phone);
		}

		// 修改数据
		UserExample uexample = new UserExample();
		uexample.createCriteria().andAccountEqualTo(phone);
		List<User> list = userService.selectByExample(uexample);

		if (list.size() == 0) {
			throw new ApiException(MEnumError.PHONE_NOTEXISTS_ERROR);
		}

		User record = list.get(0);
		record.setPassword(password);
		int cnt = userService.updateByPrimaryKeySelective(record);
		if (cnt == 0) {
			throw new ApiException(MEnumError.UPDATE_ACCOUNT_ERROR);
		}
		

		MobileInfo mobileInfo = new MobileInfo();
		mobileInfo.setUserid(record.getId());
		mobileInfo.setDeviceType(deviceType);
		mobileInfo.setDeviceid(deviceid);
		//删除其他登陆设备
		verifyService.deleteOther(mobileInfo);
		String verify = verifyService.updateMobileVerify(mobileInfo,
				deviceType, cid);
		mobileInfo.setToken(verify);
		mobileInfo.setIsComplete(record.getIsComplete());
		return mobileInfo;
	}

	@ApiOperation(value = "第三方登录")
	@RequestMapping(value = "/oauthLogin", method = RequestMethod.POST)
	@ApiMethod
	public OauthMobileInfo oauthLoginNew(
			@ApiParam(value = "1:qq 2:sina 3:wx", required = true) Integer type,
			@ApiParam(value = "openid", required = true) String openid,
			@ApiParam(value = "accessToken", required = true) String accessToken,
			@ApiParam(value = "设备类型 1:android 2:ios", required = true) Integer deviceType,
			@ApiParam(value = "设备唯一识别码", required = false) String deviceid,
			@ApiParam(value = "推送cid", required = false) String cid){
		// 参数校验
		if (type == null) {
			throw new ApiException("type");
		}
		if (StringUtils.isBlank(openid)) {
			throw new ApiException("openid");
		}
		if (deviceType == null) {
			throw new ApiException("deviceType");
		}
		if (StringUtils.isBlank(deviceid)) {
			throw new ApiException("deviceid");
		}
		UserOauthExample example = new UserOauthExample();
		example.createCriteria().andTypeEqualTo(type).andOpenIdEqualTo(openid);
		List<UserOauth> list = oauthService.selectByExample(example);
		OauthMobileInfo mobileInfo = new OauthMobileInfo();
		if (list.size() == 0){
			return mobileInfo;
		}
		UserOauth oauth = list.get(0);
		User user = userService.selectByPrimaryKey(oauth.getUserId());
		if (user == null) {
			oauthService.deleteByPrimaryKey(oauth.getId());
			return mobileInfo;
		} else if (user.getState() == 0) {
			throw new ApiException(MEnumError.ACCOUNT_STOP_ERROR);
		}
		mobileInfo.setSuccessLogin(1);
		mobileInfo.setUserid(user.getId());
		mobileInfo.setDeviceType(deviceType);
		mobileInfo.setDeviceid(deviceid);
		String verify = verifyService.updateMobileVerify(mobileInfo,
				deviceType, cid);
		mobileInfo.setToken(verify);
		mobileInfo.setIsComplete(user.getIsComplete());
		return mobileInfo;
	}

	@ApiOperation(value = "第三方登录绑定手机号")
	@RequestMapping(value = "/oauthLoginBind", method = RequestMethod.POST)
	@ApiMethod
	public MobileInfo oauthLoginBindPhone(
			@ApiParam(value = "手机号", required = true) String phone,
			@ApiParam(value = "验证码", required = true) String code,
			@ApiParam(value = "1:qq 2:sina 3:wx", required = true) Integer type,
			@ApiParam(value = "openid", required = true) String openid,
			@ApiParam(value = "accessToken", required = true) String accessToken,
			@ApiParam(value = "设备类型 1:android 2:ios", required = true) Integer deviceType,
			@ApiParam(value = "设备唯一识别码", required = false) String deviceid,
			@ApiParam(value = "推送cid", required = false) String cid){
		if (StringUtils.isBlank(phone)) {
			throw new ApiException("phone");
		}
		if (StringUtils.isBlank(code)) {
			throw new ApiException("code");
		}
		// 参数校验
		if (type == null) {
			throw new ApiException("type");
		}
		if (StringUtils.isBlank(openid)) {
			throw new ApiException("openid");
		}
		if (deviceType == null) {
			throw new ApiException("deviceType");
		}
		if (StringUtils.isBlank(deviceid)) {
			throw new ApiException("deviceid");
		}
		// 验证码验证
//		String value = CacheSupport.get("mobileCodeCache", MobileMsgEnum.LOGIN.getPhone(phone), String.class);
		String value = SmsCache.getCache(MobileMsgEnum.LOGIN.getPhone(phone));
		if (!code.equals(value)) {
			throw new ApiException(MEnumError.MOBILE_CODE_ERROR);
		} else {
			SmsCache.delCache(MobileMsgEnum.LOGIN.getPhone(phone));
//			CacheSupport.remove("mobileCodeCache", phone);
		}

		UserOauthExample example = new UserOauthExample();
		example.createCriteria().andTypeEqualTo(type).andOpenIdEqualTo(openid);
		List<UserOauth> list = oauthService.selectByExample(example);
		MobileInfo mobileInfo = new MobileInfo();
		if (list.size() > 0){
			throw new ApiException(-1,"账号已被绑定");
		}
		UserExample userExample = new UserExample();
		userExample.createCriteria().andAccountEqualTo(phone);
		List<User> users = userService.selectByExample(userExample);
		User user;
		if (users.size() == 0){
			user = regist(phone, null, deviceType, deviceid, cid, phone);
			// 第三方登陆获取头像昵称-第一次
//			if (StringUtils.isNotBlank(accessToken)) {
//				ThreadUtil.execute(new OauthLoginRunnable(user
//						.getId(), type, deviceType, openid, accessToken, userService));
//			}
		}else{
			user = users.get(0);
			if (user.getState() == 0) {
				throw new ApiException(MEnumError.ACCOUNT_STOP_ERROR);
			}
		}

		User bind = new User();
		bind.setId(user.getId());
		switch (type){
			case 1:
				if (user.getIsBindQq() == 1){
					throw new ApiException(-1,"已绑定QQ");
				}
				bind.setIsBindQq(1);
				break;
			case 2:
				if (user.getIsBindWeibo() == 1){
					throw new ApiException(-1,"已绑定weibo");
				}
				bind.setIsBindWeibo(1);
				break;
			case 3:
				if (user.getIsBindWeixin() == 1){
					throw new ApiException(-1,"已绑定微信");
				}
				bind.setIsBindWeixin(1);
				break;
			default:
				throw new ApiException(-1,"类型错误1:qq 2:sina 3:wx");
		}

		UserOauth oauth = new UserOauth();
		oauth.setAccessToken(accessToken);
		oauth.setOpenId(openid);
		oauth.setUserId(user.getId());
		oauth.setType(type);
		oauthService.insert(oauth);

		userService.updateByPrimaryKeySelective(bind);

		mobileInfo.setUserid(user.getId());
		mobileInfo.setDeviceType(deviceType);
		mobileInfo.setDeviceid(deviceid);
		String verify = verifyService.updateMobileVerify(mobileInfo,
				deviceType, cid);
		mobileInfo.setToken(verify);
		mobileInfo.setIsComplete(user.getIsComplete());
		return mobileInfo;
	}

	@ApiOperation(value = "第三方绑定",notes = "登陆")
	@RequestMapping(value = "/oauthBind", method = RequestMethod.POST)
	@ApiMethod(isLogin = true)
	public Ret oauthBind(MobileInfo mobileInfo,
									 @ApiParam(value = "1:qq 2:sina 3:wx", required = true) Integer type,
									 @ApiParam(value = "openid", required = true) String openid,
									 @ApiParam(value = "accessToken", required = true) String accessToken){
		// 参数校验
		if (type == null) {
			throw new ApiException("type");
		}
		if (StringUtils.isBlank(openid)) {
			throw new ApiException("openid");
		}
		UserOauthExample example = new UserOauthExample();
		example.createCriteria().andTypeEqualTo(type).andOpenIdEqualTo(openid);
		int cnt = oauthService.countByExample(example);
		if (cnt > 0){
			throw new ApiException(-1,"第三方账号已被绑定");
		}
		example.clear();
		example.createCriteria().andUserIdEqualTo(mobileInfo.getUserid()).andTypeEqualTo(type);
		cnt = oauthService.countByExample(example);
		if (cnt > 0){
			throw new ApiException(-1,"账号已绑定第三方账号");
		}
		User bind = new User();
		bind.setId(mobileInfo.getUserid());
		switch (type){
			case 1:
				bind.setIsBindQq(1);
				break;
			case 2:
				bind.setIsBindWeibo(1);
				break;
			case 3:
				bind.setIsBindWeixin(1);
				break;
			default:
				throw new ApiException(-1,"类型错误1:qq 2:sina 3:wx");
		}

		userService.updateByPrimaryKeySelective(bind);

		UserOauth oauth = new UserOauth();
		oauth.setAccessToken(accessToken);
		oauth.setOpenId(openid);
		oauth.setUserId(mobileInfo.getUserid());
		oauth.setType(type);
		oauthService.insert(oauth);

		return ok();
	}

	@ApiOperation(value = "第三方登录解绑")
	@RequestMapping(value = "/oauthLoginUnBind", method = RequestMethod.POST)
	@ApiMethod(isLogin = true)
	public Ret unBindOauth(MobileInfo mobileInfo,
						   @ApiParam(value = "1:qq 2:sina 3:wx", required = true) Integer type){
		if (type == null){
			throw new ApiException("type");
		}
		User user = userService.selectByPrimaryKey(mobileInfo.getUserid());
		User bind = new User();
		bind.setId(mobileInfo.getUserid());
		switch (type){
			case 1:
				if (user.getIsBindQq() == 0){
					throw new ApiException(-1,"未绑定QQ");
				}
				bind.setIsBindQq(0);
				break;
			case 2:
				if (user.getIsBindWeibo() == 0){
					throw new ApiException(-1,"未绑定weibo");
				}
				bind.setIsBindWeibo(0);
				break;
			case 3:
				if (user.getIsBindWeixin() == 0){
					throw new ApiException(-1,"未绑定微信");
				}
				bind.setIsBindWeixin(0);
				break;
			default:
				throw new ApiException(-1,"类型错误1:qq 2:sina 3:wx");
		}

		UserOauthExample example = new UserOauthExample();
		example.createCriteria().andUserIdEqualTo(mobileInfo.getUserid()).andTypeEqualTo(type);
		oauthService.deleteByExample(example);
		userService.updateByPrimaryKeySelective(bind);
		return ok();
	}

	/**
	 * 修改密码
	 * 
	 * @param oldPwd
	 *            旧密码(需要加密)
	 * @param newPwd
	 *            新密码(需要加密)
	 */

	/**
	 * 注销
	 */
	@ApiOperation(value = "注销*", notes = "")
	@RequestMapping(value = "/logout", method = RequestMethod.POST)
	@ApiMethod(isLogin = true)
	@ApiUnCheck
	public Ret logout(MobileInfo mobileInfo) throws Exception {
		verifyService.logout(mobileInfo);
		return new Ret();
	}

	/**
	 * @api 绑定手机
	 * @param phone
	 *            手机号
	 * @param password
	 *            密码
	 * @param code
	 *            短信验证码
	 */


	/**
	 * 修改个人资料
	 * */
	@ApiOperation(value = "完善个人资料*",notes="登陆")
	@RequestMapping(value="/editUser", method = RequestMethod.POST)
	@ApiMethod(isLogin = true)
	@ApiUnCheck
	public UserInfo editUser(MobileInfo mobileInfo, UserInfo userInfo){
		if (StringUtil.isBlank(userInfo.getNickName())){
			throw new ApiException("nickName");
		}
		if (StringUtil.isBlank(userInfo.getHeadImg())){
			throw new ApiException("headImg");
		}
		if (userInfo.getSex() == null){
			throw new ApiException("sex");
		}
		if (StringUtil.isBlank(userInfo.getBirthday())){
			throw new ApiException("birthday");
		}
//		if (StringUtil.isBlank(userInfo.getEmail())){
//			throw new ApiException("email");
//		}
		if (StringUtil.isBlank(userInfo.getCity())){
			throw new ApiException("city");
		}

		User user = userService.selectByPrimaryKey(mobileInfo.getUserid());
		if (user.getIsComplete() == 1){
			throw new ApiException(-1,"资料已完善");
		}

		User temp = new User();
		temp.setId(mobileInfo.getUserid());
		temp.setHeadImg(userInfo.getHeadImg());
		temp.setSex(userInfo.getSex());
		temp.setCity(userInfo.getCity());
		temp.setEmail(userInfo.getEmail());
		temp.setBirthday(DateUtil.strToDate(userInfo.getBirthday(),DateUtil.YYMMDD));
		temp.setInterest(userInfo.getInterest());
		temp.setIsComplete(1);
		userInfo.setId(mobileInfo.getUserid());

		if (StringUtil.isNotBlank(userInfo.getNickName())){
			// 校验登录
			UserExample example = new UserExample();
			example.createCriteria().andNickNameEqualTo(userInfo.getNickName()).andIdNotEqualTo(mobileInfo.getUserid());
			int i = userService.countByExample(example);
			if (i > 0) {
				throw new ApiException(MEnumError.NICK_EXISTS_ERROR);
			}
			temp.setNickName(userInfo.getNickName());
		}

		//更新
		userService.updateByPrimaryKeySelective(temp);


		temp = userService.selectByPrimaryKey(mobileInfo.getUserid());
		if (temp != null)
			userInfo.setAccount(temp.getAccount());
		return userInfo;
	}
}
