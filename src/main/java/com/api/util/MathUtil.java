package com.api.util;

import java.math.BigDecimal;
import java.util.Random;

import org.apache.commons.lang.StringUtils;

public class MathUtil {

	public static String getDistanceStr(Long temp) {
		if (temp == null){
			return "";
		}
		if (temp < 1000){
			return temp + "m";
		}else if (temp < 100000){
			return new BigDecimal((double) temp / 1000).setScale(2, 2)
					.toString() + "km";
		}else{
			return temp / 1000 + "km";
		}
	}

	public static Long getDistance(Double lat1, Double lng1, Double lat2,
			Double lng2) {
		return Math.round(6378.138 * 2 * Math.asin(Math.sqrt(Math.pow(
				Math.sin((lat1 * Math.PI / 180 - lat2 * Math.PI / 180) / 2), 2)
				+ Math.cos(lat1 * Math.PI / 180)
				* Math.cos(lat2 * Math.PI / 180)
				* Math.pow(Math.sin((lng1 * Math.PI / 180 - lng2 * Math.PI
						/ 180) / 2), 2))) * 1000);
	}

	public static Long getDistance(String lat0, String lng0, String lat3,
			String lng3) {
		try {
			Double lat1 = Double.valueOf(lat0);
			Double lat2 = Double.valueOf(lat3);
			Double lng1 = Double.valueOf(lng0);
			Double lng2 = Double.valueOf(lng3);
			return getDistance(lat1, lng1, lat2, lng2);
		} catch (Exception e) {
			return null;
		}

	}

	public static String getDiscount(BigDecimal a, BigDecimal b) {
		if (a.compareTo(b) > 0) {
			return getDiscountOld(b, a);
		} else {
			return getDiscountOld(a, b);
		}
	}

	private static String getDiscountOld(BigDecimal a, BigDecimal b) {
		if (a.doubleValue() == 0)
			return "免费";
		if (b.doubleValue() == 0)
			return "";
		if (a.compareTo(b) == 0)
			return "";
		String str = "";
		BigDecimal c = a.divide(b, 2, BigDecimal.ROUND_HALF_UP);
		int temp = c.multiply(new BigDecimal(10)).intValue();

		if (temp <= 0) {
			str = "0.1折";
		} else {
			str = temp + "折";
		}

		return str;
	}

	public static String getBigString(BigDecimal a) {
		return bigToString(a);
	}

	public static String bigToString(BigDecimal a) {
		if (a == null)
			return "";
		if (!a.toString().endsWith("0"))
			return a.toString();
		int i = a.scale();
		String str = "";
		int k = a.toString().indexOf(".");

		if (k == -1) {
			return a.toString();
		}

		if (k == a.toString().length() - 1)
			return a.toString().substring(0, k);

		if (a.toString().substring(k + 1).equals(getZero(i))) {
			return a.toString().substring(0, k);
		}

		for (int j = k + 2; j < a.toString().length(); j++) {
			if (a.toString().substring(j).equals(getZero(--i))) {
				str = a.toString().substring(0, j);
				break;
			}
		}

		return str;
	}

	public static BigDecimal substract(BigDecimal a, BigDecimal b) {
		if (a == null || b == null)
			return new BigDecimal(0);
		if (a.compareTo(b) >= 0)
			return sub(a, b);
		else
			return sub(b, a);
	}

	private static BigDecimal sub(BigDecimal a, BigDecimal b) {
		return a.subtract(b);
	}

	private static String getZero(int a) {
		if (a <= 0)
			return "0";
		StringBuffer buffer = new StringBuffer();
		for (int i = 0; i < a; i++) {
			buffer.append('0');
		}
		return buffer.toString();
	}

	public static Integer getCredits(BigDecimal a) {
		if (a == null)
			return null;
		BigDecimal b = a
				.divide(new BigDecimal(20), 0, BigDecimal.ROUND_CEILING);
		return b.intValue();
	}

	public static BigDecimal srtToBig(String str) throws Exception {
		try {
			BigDecimal big = new BigDecimal(str).setScale(2);
			return big;
		} catch (Exception e) {
			throw new Exception(str + "格式不正确");
		}
	}

	public static Integer strToInt(String str) throws Exception {
		try {
			Integer i = Integer.parseInt(str);
			return i;
		} catch (Exception e) {
			throw new Exception(str + "不是整数");
		}
	}

	public static String getCodeNumber() {
		return getFixLenthString(12);
	}

	public static String getFixLenthString(int strLength) {
		Random rm = new Random();
		double pross = (1 + rm.nextDouble()) * Math.pow(10, strLength);
		String fixLenthString = String.valueOf(pross);
		return fixLenthString.substring(2, strLength + 2);
	}

	public static boolean compare(BigDecimal a, BigDecimal b) {
		if (a == null || b == null)
			return false;
        return a.compareTo(b) > 0;
	}
	
	public static String str2f(String str){
		if (str == null) return null;
		String[] strs = str.split(",|，|\\s");
		for (int i = 0; i < strs.length; i++){
			strs[i] = "["+strs[i]+"]";
		}
		str = StringUtils.join(strs,",");
		return str;
	}
	
	public static String f2Str(String str){
		if (str == null) return null;
		str = str.replaceAll("\\[|\\]", "");
		return str;
	}
}
