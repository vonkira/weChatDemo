package com.api.view.account;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;

@ApiModel
public class MSimpleUserInfo {
    @ApiModelProperty(value="用户id")
    @ApiParam(hidden=true)
    private Integer userId;
    @ApiModelProperty(value="昵称")
    private String nickName;
    @ApiModelProperty(value="头像")
    private String headImg;
    @ApiModelProperty(value="性别0:女 1:男")
    @ApiParam(hidden=true)
    private Integer sex;

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getHeadImg() {
        return headImg;
    }

    public void setHeadImg(String headImg) {
        this.headImg = headImg;
    }

    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }
}
