package com.api.view.account;

import com.base.api.MobileInfo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel
public class OauthMobileInfo extends MobileInfo{

    @ApiModelProperty("1第三方登陆成功0未绑定手机号跳转第三方登录绑定手机号")
    private int successLogin;

    public int getSuccessLogin() {
        return successLogin;
    }

    public void setSuccessLogin(int successLogin) {
        this.successLogin = successLogin;
    }
}
