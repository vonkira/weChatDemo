package com.api.view.map;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel
public class MGoodsMaterial {
    @ApiModelProperty("id")
    private Integer id;
    @ApiModelProperty("碎片id")
    private Integer goodsId;
    @ApiModelProperty("需要数量")
    private Integer num;
    @ApiModelProperty("已有数量")
    private Integer userNum;
    @ApiModelProperty("碎片名称")
    private String goodsName;
    @ApiModelProperty("碎片图标")
    private String goodsImg;
    @ApiModelProperty("物品类别")
    private String cateCode;
    @ApiModelProperty("灰色图标")
    private String goodsImgGray;
    @ApiModelProperty("物品描述")
    private String goodsInfo;
    @ApiModelProperty("物品来源途径")
    private String goodsSource;

    public String getGoodsImg() {
        return goodsImg;
    }

    public void setGoodsImg(String goodsImg) {
        this.goodsImg = goodsImg;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public Integer getUserNum() {
        return userNum;
    }

    public void setUserNum(Integer userNum) {
        this.userNum = userNum;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public Integer getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Integer goodsId) {
        this.goodsId = goodsId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCateCode() {
        return cateCode;
    }

    public void setCateCode(String cateCode) {
        this.cateCode = cateCode;
    }

    public String getGoodsImgGray() {
        return goodsImgGray;
    }

    public void setGoodsImgGray(String goodsImgGray) {
        this.goodsImgGray = goodsImgGray;
    }

    public String getGoodsInfo() {
        return goodsInfo;
    }

    public void setGoodsInfo(String goodsInfo) {
        this.goodsInfo = goodsInfo;
    }

    public String getGoodsSource() {
        return goodsSource;
    }

    public void setGoodsSource(String goodsSource) {
        this.goodsSource = goodsSource;
    }
}
