package com.api.view.map;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel
public class MUserStrength {
    @ApiModelProperty("用户当前体力值")
    private int strength;
    @ApiModelProperty("用户最大体力值")
    private int maxStrength;
    @ApiModelProperty("剩余体力恢复秒")
    private int strengthSeconds;

    public int getStrength() {
        return strength;
    }

    public void setStrength(int strength) {
        this.strength = strength;
    }

    public int getMaxStrength() {
        return maxStrength;
    }

    public void setMaxStrength(int maxStrength) {
        this.maxStrength = maxStrength;
    }

    public int getStrengthSeconds() {
        return strengthSeconds;
    }

    public void setStrengthSeconds(int strengthSeconds) {
        this.strengthSeconds = strengthSeconds;
    }
}
