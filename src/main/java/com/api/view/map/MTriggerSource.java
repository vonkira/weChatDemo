package com.api.view.map;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel
public class MTriggerSource {
    @ApiModelProperty("1消耗体力0否")
    private Integer isStrength;
    @ApiModelProperty("1消耗金币0否")
    private Integer isCoin;
    @ApiModelProperty("1消耗物品0否")
    private Integer isGoods;
    @ApiModelProperty("消耗体力数量")
    private Integer strengthNum;
    @ApiModelProperty("消耗金币数量")
    private Integer coinNum;
    @ApiModelProperty("消耗物品数量")
    private Integer goodsNum;
    @ApiModelProperty("消耗物品名称")
    private String goodsName;

    public Integer getIsStrength() {
        return isStrength;
    }

    public void setIsStrength(Integer isStrength) {
        this.isStrength = isStrength;
    }

    public Integer getIsCoin() {
        return isCoin;
    }

    public void setIsCoin(Integer isCoin) {
        this.isCoin = isCoin;
    }

    public Integer getIsGoods() {
        return isGoods;
    }

    public void setIsGoods(Integer isGoods) {
        this.isGoods = isGoods;
    }

    public Integer getStrengthNum() {
        return strengthNum;
    }

    public void setStrengthNum(Integer strengthNum) {
        this.strengthNum = strengthNum;
    }

    public Integer getCoinNum() {
        return coinNum;
    }

    public void setCoinNum(Integer coinNum) {
        this.coinNum = coinNum;
    }

    public Integer getGoodsNum() {
        return goodsNum;
    }

    public void setGoodsNum(Integer goodsNum) {
        this.goodsNum = goodsNum;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }
}
