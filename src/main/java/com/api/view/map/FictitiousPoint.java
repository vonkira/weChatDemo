package com.api.view.map;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel
public class FictitiousPoint {
    @ApiModelProperty("点id")
    private String id;
    @ApiModelProperty("点lng")
    private String lng;
    @ApiModelProperty("点lat")
    private String lat;
    @ApiModelProperty("载体图片")
    private String container;
    @ApiModelProperty("活动id")
    private Integer activityId;
    @ApiModelProperty("载体图片类型")
    private String containerMime;
    @ApiModelProperty("载体打开动画")
    private String containerAnimation;
    @ApiModelProperty("载体大图")
    private String containerLarge;
    @ApiModelProperty("消耗资源")
    private MTriggerSource triggerSource;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getContainer() {
        return container;
    }

    public void setContainer(String container) {
        this.container = container;
    }

    public Integer getActivityId() {
        return activityId;
    }

    public void setActivityId(Integer activityId) {
        this.activityId = activityId;
    }

    public String getContainerAnimation() {
        return containerAnimation;
    }

    public void setContainerAnimation(String containerAnimation) {
        this.containerAnimation = containerAnimation;
    }

    public String getContainerMime() {
        return containerMime;
    }

    public void setContainerMime(String containerMime) {
        this.containerMime = containerMime;
    }

    public String getContainerLarge() {
        return containerLarge;
    }

    public void setContainerLarge(String containerLarge) {
        this.containerLarge = containerLarge;
    }

    public MTriggerSource getTriggerSource() {
        return triggerSource;
    }

    public void setTriggerSource(MTriggerSource triggerSource) {
        this.triggerSource = triggerSource;
    }
}
