package com.api.view.map;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.List;

@ApiModel
public class MGoodsGroup {
    @ApiModelProperty("id")
    private Integer id;
    @ApiModelProperty("物品名称")
    private String goodsName;
    @ApiModelProperty("物品图片")
    private String goodsImg;
    @ApiModelProperty("灰色图标")
    private String goodsImgGray;
    @ApiModelProperty("物品数量")
    private Integer goodsNum;
    @ApiModelProperty("物品描述")
    private String goodsInfo;
    @ApiModelProperty("物品来源途径")
    private String goodsSource;
    @ApiModelProperty("活动名称")
    private String activityName;
    @ApiModelProperty("活动id")
    private Integer activityId;
    @ApiModelProperty("活动开始时间")
    private String beginTime;
    @ApiModelProperty("活动结束时间")
    private String endTIme;
    @ApiModelProperty("用户已有数量")
    private Integer userNum;
    @ApiModelProperty("距离结束秒数")
    private Integer seconds;
    @ApiModelProperty("组合材料")
    private List<MGoodsMaterial> materials;
    @ApiModelProperty("能否兑换1能0否")
    private int canGroup;
    @ApiModelProperty("进度:1/2")
    private String progress;
    @ApiModelProperty("活动规则")
    private String info;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public String getGoodsImg() {
        return goodsImg;
    }

    public void setGoodsImg(String goodsImg) {
        this.goodsImg = goodsImg;
    }

    public String getActivityName() {
        return activityName;
    }

    public void setActivityName(String activityName) {
        this.activityName = activityName;
    }

    public Integer getActivityId() {
        return activityId;
    }

    public void setActivityId(Integer activityId) {
        this.activityId = activityId;
    }

    public String getBeginTime() {
        return beginTime;
    }

    public void setBeginTime(String beginTime) {
        this.beginTime = beginTime;
    }

    public String getEndTIme() {
        return endTIme;
    }

    public void setEndTIme(String endTIme) {
        this.endTIme = endTIme;
    }

    public Integer getUserNum() {
        return userNum;
    }

    public void setUserNum(Integer userNum) {
        this.userNum = userNum;
    }

    public Integer getSeconds() {
        return seconds;
    }

    public void setSeconds(Integer seconds) {
        this.seconds = seconds;
    }

    public List<MGoodsMaterial> getMaterials() {
        return materials;
    }

    public void setMaterials(List<MGoodsMaterial> materials) {
        this.materials = materials;
    }

    public int getCanGroup() {
        return canGroup;
    }

    public void setCanGroup(int canGroup) {
        this.canGroup = canGroup;
    }

    public String getProgress() {
        return progress;
    }

    public void setProgress(String progress) {
        this.progress = progress;
    }

    public Integer getGoodsNum() {
        return goodsNum;
    }

    public void setGoodsNum(Integer goodsNum) {
        this.goodsNum = goodsNum;
    }

    public String getGoodsInfo() {
        return goodsInfo;
    }

    public void setGoodsInfo(String goodsInfo) {
        this.goodsInfo = goodsInfo;
    }

    public String getGoodsSource() {
        return goodsSource;
    }

    public void setGoodsSource(String goodsSource) {
        this.goodsSource = goodsSource;
    }

    public String getGoodsImgGray() {
        return goodsImgGray;
    }

    public void setGoodsImgGray(String goodsImgGray) {
        this.goodsImgGray = goodsImgGray;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }
}
