package com.api.view.map;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.List;

@ApiModel
public class ApiActivityResult {
    @ApiModelProperty("0未获取>0获取物品数量")
    private int result;
    @ApiModelProperty("获取的物品")
    private List<ClickGoods> goods;

    public int getResult() {
        return result;
    }

    public void setResult(int result) {
        this.result = result;
    }

    public List<ClickGoods> getGoods() {
        return goods;
    }

    public void setGoods(List<ClickGoods> goods) {
        this.goods = goods;
    }
}
