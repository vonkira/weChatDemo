package com.api.view.map;

import com.api.view.store.MStore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.List;

@ApiModel
public class IndexMap {
    @ApiModelProperty("首页虚拟点")
    private List<FictitiousPoint> fictitiousPoints;

    @ApiModelProperty("商家")
    private List<MStore> stores;

    @ApiModelProperty("用户投放")
    private List<MUserPush> userPushes;

    @ApiModelProperty("全景活动:url跳转")
    private List<MVrAct> vrActs;

    public List<FictitiousPoint> getFictitiousPoints() {
        return fictitiousPoints;
    }

    public void setFictitiousPoints(List<FictitiousPoint> fictitiousPoints) {
        this.fictitiousPoints = fictitiousPoints;
    }

    public List<MStore> getStores() {
        return stores;
    }

    public void setStores(List<MStore> stores) {
        this.stores = stores;
    }

    public List<MUserPush> getUserPushes() {
        return userPushes;
    }

    public void setUserPushes(List<MUserPush> userPushes) {
        this.userPushes = userPushes;
    }

    public List<MVrAct> getVrActs() {
        return vrActs;
    }

    public void setVrActs(List<MVrAct> vrActs) {
        this.vrActs = vrActs;
    }
}
