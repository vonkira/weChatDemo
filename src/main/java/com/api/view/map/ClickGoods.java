package com.api.view.map;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel
public class ClickGoods {

    @ApiModelProperty("物品id")
    private Integer goodsId;
    @ApiModelProperty("物品名称")
    private String goodsName;
    @ApiModelProperty("物品图片")
    private String goodsImg;
    @ApiModelProperty("物品动画")
    private String goodsAnimation;
    @ApiModelProperty("物品数量")
    private Integer num;
    @ApiModelProperty("物品类型,jb等等")
    private String cateCode;
    @ApiModelProperty("描述")
    private String info;
    @ApiModelProperty("开始时间")
    private String begin;
    @ApiModelProperty("物品详情h5地址")
    private String detailUrl;
    @ApiModelProperty("商家id")
    private Integer storeId;
    @ApiModelProperty("现金红包金额")
    private String money;

    public String getBegin() {
        return begin;
    }

    public void setBegin(String begin) {
        this.begin = begin;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    @ApiModelProperty("结束时间")
    private String end;

    public Integer getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Integer goodsId) {
        this.goodsId = goodsId;
    }

    public String getGoodsImg() {
        return goodsImg;
    }

    public void setGoodsImg(String goodsImg) {
        this.goodsImg = goodsImg;
    }

    public String getGoodsAnimation() {
        return goodsAnimation;
    }

    public void setGoodsAnimation(String goodsAnimation) {
        this.goodsAnimation = goodsAnimation;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public String getCateCode() {
        return cateCode;
    }

    public void setCateCode(String cateCode) {
        this.cateCode = cateCode;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getDetailUrl() {
        return detailUrl;
    }

    public void setDetailUrl(String detailUrl) {
        this.detailUrl = detailUrl;
    }

    public Integer getStoreId() {
        return storeId;
    }

    public void setStoreId(Integer storeId) {
        this.storeId = storeId;
    }

    public String getMoney() {
        return money;
    }

    public void setMoney(String money) {
        this.money = money;
    }
}
