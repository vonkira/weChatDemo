package com.api.view.map;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel
public class MTimeGoodsState {
    @ApiModelProperty("是否开启定时宝箱功能")
    private int is_open;
    @ApiModelProperty("下次领取距离现在的秒数")
    private int next_get_seconds;

    public int getIs_open() {
        return is_open;
    }

    public void setIs_open(int is_open) {
        this.is_open = is_open;
    }

    public int getNext_get_seconds() {
        return next_get_seconds;
    }

    public void setNext_get_seconds(int next_get_seconds) {
        this.next_get_seconds = next_get_seconds;
    }

    public int getCan_get() {
        return can_get;
    }

    public void setCan_get(int can_get) {
        this.can_get = can_get;
    }

    @ApiModelProperty("能否领取 0 不能 1能")
    private int can_get;

}
