package com.api.view.pet;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel
public class MGetPetInfo extends MPetInfo{
    @ApiModelProperty("1有宠物0没有")
    private int pet;

    public int getPet() {
        return pet;
    }

    public void setPet(int pet) {
        this.pet = pet;
    }
}
