package com.api.view.pet;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel
public class MPetInfo {
    @ApiModelProperty
    private Integer id;
    @ApiModelProperty("宠物名字")
    private String petName;
    @ApiModelProperty("宠物小图")
    private String petImg;
    @ApiModelProperty("宠物放大")
    private String petLargeImg;

    public String getPetName() {
        return petName;
    }

    public void setPetName(String petName) {
        this.petName = petName;
    }

    public String getPetImg() {
        return petImg;
    }

    public void setPetImg(String petImg) {
        this.petImg = petImg;
    }

    public String getPetLargeImg() {
        return petLargeImg;
    }

    public void setPetLargeImg(String petLargeImg) {
        this.petLargeImg = petLargeImg;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
