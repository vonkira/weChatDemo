package com.api.view.pet;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel
public class MGetPet extends MPetInfo{
    @ApiModelProperty("1获取成功0获取失败")
    private int pet;
    @ApiModelProperty("获取宠物动画")
    private String animation;

    public int getPet() {
        return pet;
    }

    public void setPet(int pet) {
        this.pet = pet;
    }

    public String getAnimation() {
        return animation;
    }

    public void setAnimation(String animation) {
        this.animation = animation;
    }
}
