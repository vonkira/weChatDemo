package com.api.view.config;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.List;


@ApiModel
public class MIndexConfig {

    @ApiModelProperty("用户可见宝箱范围,默认5000米")
    private String userDistance;
    @ApiModelProperty("用户可拾取宝箱范围")
    private String userGetDistance;
    @ApiModelProperty("用户可见商家范围")
    private String storeDistance;
    @ApiModelProperty("用户可见投放范围")
    private String userPushDistance;
    @ApiModelProperty("宝箱是否激活1激活")
    private Integer bxIsOpen;
    @ApiModelProperty("首页活动公告是否激活1激活")
    private Integer actIsOpen;
    @ApiModelProperty("首页活动公告地址")
    private String actUrl;
    @ApiModelProperty("首页活动公告图片")
    private String actImg;
    @ApiModelProperty("首页活动公告图片类型")
    private String actImgMime;
    @ApiModelProperty("用户投放显示的图标")
    private String userPushImg;
    @ApiModelProperty("用户投放显示的图标MIME类型")
    private String userPushImgMime;
    @ApiModelProperty("用户最大体力值")
    private String maxStrength;
    @ApiModelProperty("用户体力值回复时间")
    private String strengthSeconds;
    @ApiModelProperty("用户注册协议")
    private String userRegistProtocol;
    @ApiModelProperty("金币音效")
    private String coinAudio;
    @ApiModelProperty("其他音效")
    private String otherAudio;
    @ApiModelProperty("客服电话")
    private String serviceTel;

    @ApiModelProperty("系统ar")
    private List<MAr> ars;

    public List<MAr> getArs() {
        return ars;
    }

    public void setArs(List<MAr> ars) {
        this.ars = ars;
    }

    public String getUserDistance() {
        return userDistance;
    }

    public void setUserDistance(String userDistance) {
        this.userDistance = userDistance;
    }

    public String getStoreDistance() {
        return storeDistance;
    }

    public void setStoreDistance(String storeDistance) {
        this.storeDistance = storeDistance;
    }

    public Integer getBxIsOpen() {
        return bxIsOpen;
    }

    public void setBxIsOpen(Integer bxIsOpen) {
        this.bxIsOpen = bxIsOpen;
    }

    public Integer getActIsOpen() {
        return actIsOpen;
    }

    public void setActIsOpen(Integer actIsOpen) {
        this.actIsOpen = actIsOpen;
    }

    public String getActUrl() {
        return actUrl;
    }

    public void setActUrl(String actUrl) {
        this.actUrl = actUrl;
    }

    public String getActImg() {
        return actImg;
    }

    public void setActImg(String actImg) {
        this.actImg = actImg;
    }

    public String getUserPushDistance() {
        return userPushDistance;
    }

    public void setUserPushDistance(String userPushDistance) {
        this.userPushDistance = userPushDistance;
    }

    public String getUserPushImg() {
        return userPushImg;
    }

    public void setUserPushImg(String userPushImg) {
        this.userPushImg = userPushImg;
    }

    public String getUserPushImgMime() {
        return userPushImgMime;
    }

    public void setUserPushImgMime(String userPushImgMime) {
        this.userPushImgMime = userPushImgMime;
    }

    public String getActImgMime() {
        return actImgMime;
    }

    public void setActImgMime(String actImgMime) {
        this.actImgMime = actImgMime;
    }

    public String getMaxStrength() {
        return maxStrength;
    }

    public void setMaxStrength(String maxStrength) {
        this.maxStrength = maxStrength;
    }

    public String getStrengthSeconds() {
        return strengthSeconds;
    }

    public void setStrengthSeconds(String strengthSeconds) {
        this.strengthSeconds = strengthSeconds;
    }

    public String getUserRegistProtocol() {
        return userRegistProtocol;
    }

    public void setUserRegistProtocol(String userRegistProtocol) {
        this.userRegistProtocol = userRegistProtocol;
    }

    public String getUserGetDistance() {
        return userGetDistance;
    }

    public void setUserGetDistance(String userGetDistance) {
        this.userGetDistance = userGetDistance;
    }

    public String getCoinAudio() {
        return coinAudio;
    }

    public void setCoinAudio(String coinAudio) {
        this.coinAudio = coinAudio;
    }

    public String getOtherAudio() {
        return otherAudio;
    }

    public void setOtherAudio(String otherAudio) {
        this.otherAudio = otherAudio;
    }

    public String getServiceTel() {
        return serviceTel;
    }

    public void setServiceTel(String serviceTel) {
        this.serviceTel = serviceTel;
    }
}
