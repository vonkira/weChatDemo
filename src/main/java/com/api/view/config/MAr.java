package com.api.view.config;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel
public class MAr {
    @ApiModelProperty("ar的id")
    private Integer id;
    @ApiModelProperty("ar名称")
    private String name;
    @ApiModelProperty("目标图的url")
    private String picUrl;
    @ApiModelProperty("AR内容类型 -1:未设置 0:图片 1:视频 2:语音 3:3D模型")
    private Integer conType;
    @ApiModelProperty("AR链接文件后缀 只有3D模型才有这个字段")
    private String fileSuffixes;
    @ApiModelProperty("AR内容的url")
    private String arUrl;
    @ApiModelProperty("点击AR后的跳转链接")
    private String arJumpUrl;
    @ApiModelProperty("1商家2用户")
    private Integer source;
    @ApiModelProperty("载体小图")
    private String containerImg;
    @ApiModelProperty("载体大图")
    private String containerLargeImg;
    @ApiModelProperty("载体动画")
    private String containerAnimation;
    @ApiModelProperty("载体图片类型")
    private String containerImgMime;
    @ApiModelProperty("1有范围0无范围")
    private Integer isScope;
    @ApiModelProperty("lat")
    private String lat;
    @ApiModelProperty("lng")
    private String lng;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPicUrl() {
        return picUrl;
    }

    public void setPicUrl(String picUrl) {
        this.picUrl = picUrl;
    }

    public Integer getConType() {
        return conType;
    }

    public void setConType(Integer conType) {
        this.conType = conType;
    }

    public String getFileSuffixes() {
        return fileSuffixes;
    }

    public void setFileSuffixes(String fileSuffixes) {
        this.fileSuffixes = fileSuffixes;
    }

    public String getArUrl() {
        return arUrl;
    }

    public void setArUrl(String arUrl) {
        this.arUrl = arUrl;
    }

    public String getArJumpUrl() {
        return arJumpUrl;
    }

    public void setArJumpUrl(String arJumpUrl) {
        this.arJumpUrl = arJumpUrl;
    }

    public Integer getSource() {
        return source;
    }

    public void setSource(Integer source) {
        this.source = source;
    }

    public String getContainerImg() {
        return containerImg;
    }

    public void setContainerImg(String containerImg) {
        this.containerImg = containerImg;
    }

    public String getContainerLargeImg() {
        return containerLargeImg;
    }

    public void setContainerLargeImg(String containerLargeImg) {
        this.containerLargeImg = containerLargeImg;
    }

    public String getContainerAnimation() {
        return containerAnimation;
    }

    public void setContainerAnimation(String containerAnimation) {
        this.containerAnimation = containerAnimation;
    }

    public String getContainerImgMime() {
        return containerImgMime;
    }

    public void setContainerImgMime(String containerImgMime) {
        this.containerImgMime = containerImgMime;
    }

    public Integer getIsScope() {
        return isScope;
    }

    public void setIsScope(Integer isScope) {
        this.isScope = isScope;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }
}
