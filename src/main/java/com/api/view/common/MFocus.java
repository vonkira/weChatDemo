package com.api.view.common;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel
public class MFocus {
    @ApiModelProperty("id")
    private int id;
    @ApiModelProperty("图片")
    private String img;
    @ApiModelProperty("跳转类型0:不跳转 1:url 2:富文本(url)")
    private int redirectType;
    @ApiModelProperty("跳转内容")
    private String redirectContent;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public int getRedirectType() {
        return redirectType;
    }

    public void setRedirectType(int redirectType) {
        this.redirectType = redirectType;
    }

    public String getRedirectContent() {
        return redirectContent;
    }

    public void setRedirectContent(String redirectContent) {
        this.redirectContent = redirectContent;
    }
}
