package com.api.view.store;

import com.api.view.map.MTriggerSource;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel
public class MStore {
    @ApiModelProperty(value = "id")
    private int id;
    @ApiModelProperty(value = "名称")
    private String name;
    @ApiModelProperty(value = "编号")
    private String code;
    @ApiModelProperty("商家logo(真实logo)")
    private String logo;
    @ApiModelProperty("商家logo(地图显示)")
    private String img;
    @ApiModelProperty(value = "1商家2代理商3综合体")
    private int type;
    @ApiModelProperty("地址")
    private String address;
    @ApiModelProperty("lat")
    private String lat;
    @ApiModelProperty("lng")
    private String lng;
    @ApiModelProperty("距离")
    private String distance;
    @ApiModelProperty("电话")
    private String phone;
    @ApiModelProperty("商家主页")
    private String homeUrl;
    @ApiModelProperty("商家全景URL")
    private String vrUrl;
    @ApiModelProperty("商家地图图片类型(地图用)")
    private String mapImgMime;
    @ApiModelProperty("商家地图标志")
    private String mapImg;
    @ApiModelProperty("商家直播URL")
    private String liveUrl;
    @ApiModelProperty("商家活动URL")
    private String actUrl;
    @ApiModelProperty("1有活动0无")
    private Integer isActivity;
    @ApiModelProperty("1有范围0无")
    private Integer isScope;
    @ApiModelProperty("活动范围")
    private Integer scope;
    @ApiModelProperty("有活动时消耗资源")
    private MTriggerSource triggerSource;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getVrUrl() {
        return vrUrl;
    }

    public void setVrUrl(String vrUrl) {
        this.vrUrl = vrUrl;
    }

    public String getHomeUrl() {
        return homeUrl;
    }

    public void setHomeUrl(String homeUrl) {
        this.homeUrl = homeUrl;
    }

    public String getMapImg() {
        return mapImg;
    }

    public void setMapImg(String mapImg) {
        this.mapImg = mapImg;
    }

    public String getMapImgMime() {
        return mapImgMime;
    }

    public void setMapImgMime(String mapImgMime) {
        this.mapImgMime = mapImgMime;
    }

    public String getActUrl() {
        return actUrl;
    }

    public void setActUrl(String actUrl) {
        this.actUrl = actUrl;
    }

    public String getLiveUrl() {
        return liveUrl;
    }

    public void setLiveUrl(String liveUrl) {
        this.liveUrl = liveUrl;
    }

    public Integer getIsActivity() {
        return isActivity;
    }

    public void setIsActivity(Integer isActivity) {
        this.isActivity = isActivity;
    }

    public MTriggerSource getTriggerSource() {
        return triggerSource;
    }

    public void setTriggerSource(MTriggerSource triggerSource) {
        this.triggerSource = triggerSource;
    }

    public Integer getScope() {
        return scope;
    }

    public void setScope(Integer scope) {
        this.scope = scope;
    }

    public Integer getIsScope() {
        return isScope;
    }

    public void setIsScope(Integer isScope) {
        this.isScope = isScope;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }
}
