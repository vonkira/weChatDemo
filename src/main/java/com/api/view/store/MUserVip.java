package com.api.view.store;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel
public class MUserVip {
    @ApiModelProperty("用户卡Id")
    private Integer id;
    @ApiModelProperty("border_file")
    private String borderFile;
    @ApiModelProperty("number")
    private String number;
    @ApiModelProperty("store_type_name")
    private String storeTypeName;
    @ApiModelProperty("address")
    private String address;
    @ApiModelProperty("logo")
    private String logo;
    @ApiModelProperty("type")
    private String type;
    @ApiModelProperty("name")
    private String name;
    @ApiModelProperty("商家id")
    private Integer storeId;
    private String lat;
    private String lng;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBorderFile() {
        return borderFile;
    }

    public void setBorderFile(String borderFile) {
        this.borderFile = borderFile;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getStoreTypeName() {
        return storeTypeName;
    }

    public void setStoreTypeName(String storeTypeName) {
        this.storeTypeName = storeTypeName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getStoreId() {
        return storeId;
    }

    public void setStoreId(Integer storeId) {
        this.storeId = storeId;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }
}
