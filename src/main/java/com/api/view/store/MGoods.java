package com.api.view.store;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel
public class MGoods {
    @ApiModelProperty("id")
    private int id;
    @ApiModelProperty("名称")
    private String name;
    @ApiModelProperty("图片")
    private String img;
    @ApiModelProperty("组图")
    private String imgs;
    @ApiModelProperty("详情url")
    private String url;
    @ApiModelProperty("价格")
    private String price;
    @ApiModelProperty("1小保养2大保养")
    private Integer type;
    @ApiModelProperty("服务费")
    private String servicePrice;
    @ApiModelProperty("分类")
    private Integer cateId;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getImgs() {
        return imgs;
    }

    public void setImgs(String imgs) {
        this.imgs = imgs;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getServicePrice() {
        return servicePrice;
    }

    public void setServicePrice(String servicePrice) {
        this.servicePrice = servicePrice;
    }

    public Integer getCateId() {
        return cateId;
    }

    public void setCateId(Integer cateId) {
        this.cateId = cateId;
    }
}
