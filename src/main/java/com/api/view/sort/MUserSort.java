package com.api.view.sort;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel
public class MUserSort {
    @ApiModelProperty("排名")
    private int sort;
    @ApiModelProperty("头像")
    private String headImg;
    @ApiModelProperty("昵称")
    private String nickName;
    @ApiModelProperty("金币或物品数量")
    private int num;
    @ApiModelProperty("用户id")
    private Integer userId;
    @ApiModelProperty("1:好友")
    private Integer isFriend;
    @ApiModelProperty("1:自己")
    private Integer isSelf;

    public int getSort() {
        return sort;
    }

    public void setSort(int sort) {
        this.sort = sort;
    }

    public String getHeadImg() {
        return headImg;
    }

    public void setHeadImg(String headImg) {
        this.headImg = headImg;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getIsFriend() {
        return isFriend;
    }

    public void setIsFriend(Integer isFriend) {
        this.isFriend = isFriend;
    }

    public Integer getIsSelf() {
        return isSelf;
    }

    public void setIsSelf(Integer isSelf) {
        this.isSelf = isSelf;
    }
}
