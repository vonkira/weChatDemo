package com.api.view.sort;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel
public class MMySort {
    @ApiModelProperty("头像")
    private String headImg;
    @ApiModelProperty("金币或物品数量")
    private int num;
    @ApiModelProperty("好友排名")
    private int friendSort;
    @ApiModelProperty("世界排名")
    private int worldSort;

    public String getHeadImg() {
        return headImg;
    }

    public void setHeadImg(String headImg) {
        this.headImg = headImg;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public int getFriendSort() {
        return friendSort;
    }

    public void setFriendSort(int friendSort) {
        this.friendSort = friendSort;
    }

    public int getWorldSort() {
        return worldSort;
    }

    public void setWorldSort(int worldSort) {
        this.worldSort = worldSort;
    }
}
