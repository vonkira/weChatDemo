package com.api.view.h5;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel
public class H5ActivityTrigger {
    @ApiModelProperty("1体力；2金币；3非金币物品")
    private Integer expend_type;
    @ApiModelProperty("消耗数量")
    private Integer expend_num;
    @ApiModelProperty("消耗物品名称")
    private String expend_goods_name;

    public Integer getExpend_type() {
        return expend_type;
    }

    public void setExpend_type(Integer expend_type) {
        this.expend_type = expend_type;
    }

    public Integer getExpend_num() {
        return expend_num;
    }

    public void setExpend_num(Integer expend_num) {
        this.expend_num = expend_num;
    }

    public String getExpend_goods_name() {
        return expend_goods_name;
    }

    public void setExpend_goods_name(String expend_goods_name) {
        this.expend_goods_name = expend_goods_name;
    }
}
