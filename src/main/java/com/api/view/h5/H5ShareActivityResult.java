package com.api.view.h5;

import com.api.view.home.ApiUserGoods;
import com.api.view.map.ApiActivityResult;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.List;

@ApiModel
public class H5ShareActivityResult {
    @ApiModelProperty("好友")
    private ApiActivityResult target;
    @ApiModelProperty("自己")
    private ApiActivityResult me;
    @ApiModelProperty("协助可以获得的物品")
    private List<ApiUserGoods> goods;
    @ApiModelProperty("1可以登录领取物品")
    private Integer loginAct;

    public ApiActivityResult getTarget() {
        return target;
    }

    public void setTarget(ApiActivityResult target) {
        this.target = target;
    }

    public ApiActivityResult getMe() {
        return me;
    }

    public void setMe(ApiActivityResult me) {
        this.me = me;
    }

    public List<ApiUserGoods> getGoods() {
        return goods;
    }

    public void setGoods(List<ApiUserGoods> goods) {
        this.goods = goods;
    }

    public Integer getLoginAct() {
        return loginAct;
    }

    public void setLoginAct(Integer loginAct) {
        this.loginAct = loginAct;
    }
}
