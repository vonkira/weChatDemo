package com.api.view.h5;

import com.api.view.home.ApiUserGoods;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.List;

@ApiModel
public class H5ActivityInfo {
    @ApiModelProperty("活动编号")
    private String number;
    @ApiModelProperty("活动类型：1抽奖；2游戏得分兑换；")
    private Integer type;
    @ApiModelProperty("活动名称")
    private String name;
    @ApiModelProperty("说明")
    private String introduce;
    @ApiModelProperty("活动状态：1未开始；2已结束；3进行中；")
    private Integer state;
    @ApiModelProperty("活动开始时间")
    private String start_time;
    @ApiModelProperty("活动结束时间")
    private String end_time;
    @ApiModelProperty("抽奖活动:-1:无次数限制，否则次数限制")
    private Integer actNum;
    @ApiModelProperty("actNum > -1,抽奖间隔时间")
    private Integer actNumTime;
    @ApiModelProperty("活动消耗信息")
    private List<H5ActivityTrigger> expend_info;
    @ApiModelProperty("奖品信息")
    private List<ApiUserGoods> reward_info;
    @ApiModelProperty("获奖记录")
    private List<H5ActivityRecord> records;

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIntroduce() {
        return introduce;
    }

    public void setIntroduce(String introduce) {
        this.introduce = introduce;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public String getEnd_time() {
        return end_time;
    }

    public void setEnd_time(String end_time) {
        this.end_time = end_time;
    }

    public List<H5ActivityTrigger> getExpend_info() {
        return expend_info;
    }

    public void setExpend_info(List<H5ActivityTrigger> expend_info) {
        this.expend_info = expend_info;
    }

    public List<ApiUserGoods> getReward_info() {
        return reward_info;
    }

    public void setReward_info(List<ApiUserGoods> reward_info) {
        this.reward_info = reward_info;
    }

    public List<H5ActivityRecord> getRecords() {
        return records;
    }

    public void setRecords(List<H5ActivityRecord> records) {
        this.records = records;
    }

    public Integer getActNum() {
        return actNum;
    }

    public void setActNum(Integer actNum) {
        this.actNum = actNum;
    }

    public Integer getActNumTime() {
        return actNumTime;
    }

    public void setActNumTime(Integer actNumTime) {
        this.actNumTime = actNumTime;
    }
}
