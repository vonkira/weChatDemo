package com.api.view.h5;

import com.api.view.home.ApiUserGoods;
import io.swagger.annotations.ApiModelProperty;

import java.util.List;

public class H5ActivityTriggerGoods {
    @ApiModelProperty("奖品集合名称")
    private String name;
    @ApiModelProperty("2017-10-20新增字段，是否可用，1是0否，值为0的情况一般是该集合到达了获取次数限制")
    private Integer is_available;
    @ApiModelProperty("集合内的物品信息")
    private List<ApiUserGoods> detail;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getIs_available() {
        return is_available;
    }

    public void setIs_available(Integer is_available) {
        this.is_available = is_available;
    }

    public List<ApiUserGoods> getDetail() {
        return detail;
    }

    public void setDetail(List<ApiUserGoods> detail) {
        this.detail = detail;
    }
}
