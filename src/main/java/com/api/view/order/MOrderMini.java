package com.api.view.order;

import com.api.view.store.MStore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel
public class MOrderMini {
    private int id;
    @ApiModelProperty("订单编号")
    private String code;
    @ApiModelProperty("商家信息")
    private MStore store;
    @ApiModelProperty("下单时间")
    private String createTime;
    @ApiModelProperty("价格")
    private String price;
    @ApiModelProperty("支付方式:0金币1支付宝2微信")
    private Integer payType;
    @ApiModelProperty("-1已取消1待付款2待发货3待确认4已完成")
    private int state;
    @ApiModelProperty("商品名称")
    private String goodsName;
    @ApiModelProperty("商品数量")
    private Integer num;
    @ApiModelProperty("商品图片")
    private String goodsImg;
    @ApiModelProperty("商品支持的支付方式1金币2现金3金币或现金")
    private Integer goodsPayType;
    @ApiModelProperty("金币")
    private Integer coin;
    @ApiModelProperty("现金")
    private String cash;
    @ApiModelProperty("什么电话?")
    private String tel;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public MStore getStore() {
        return store;
    }

    public void setStore(MStore store) {
        this.store = store;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public Integer getPayType() {
        return payType;
    }

    public void setPayType(Integer payType) {
        this.payType = payType;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public String getGoodsImg() {
        return goodsImg;
    }

    public void setGoodsImg(String goodsImg) {
        this.goodsImg = goodsImg;
    }

    public Integer getGoodsPayType() {
        return goodsPayType;
    }

    public void setGoodsPayType(Integer goodsPayType) {
        this.goodsPayType = goodsPayType;
    }

    public String getCash() {
        return cash;
    }

    public void setCash(String cash) {
        this.cash = cash;
    }

    public Integer getCoin() {
        return coin;
    }

    public void setCoin(Integer coin) {
        this.coin = coin;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }
}
