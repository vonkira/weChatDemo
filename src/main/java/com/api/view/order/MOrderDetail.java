package com.api.view.order;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.List;

@ApiModel
public class MOrderDetail extends MOrderMini{

    @ApiModelProperty("商品id")
    private Integer goodsId;
    @ApiModelProperty("支付宝或微信流水号")
    private String tradeNo;
    @ApiModelProperty("支付时间")
    private String payTime;

    @ApiModelProperty("收件人")
    private String shipUser;
    @ApiModelProperty("联系方式")
    private String shipPhone;
    @ApiModelProperty("收货地址")
    private String shipAddress;

    public Integer getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Integer goodsId) {
        this.goodsId = goodsId;
    }

    public String getPayTime() {
        return payTime;
    }

    public void setPayTime(String payTime) {
        this.payTime = payTime;
    }

    public String getTradeNo() {
        return tradeNo;
    }

    public void setTradeNo(String tradeNo) {
        this.tradeNo = tradeNo;
    }

    public String getShipUser() {
        return shipUser;
    }

    public void setShipUser(String shipUser) {
        this.shipUser = shipUser;
    }

    public String getShipPhone() {
        return shipPhone;
    }

    public void setShipPhone(String shipPhone) {
        this.shipPhone = shipPhone;
    }

    public String getShipAddress() {
        return shipAddress;
    }

    public void setShipAddress(String shipAddress) {
        this.shipAddress = shipAddress;
    }
}
