package com.api.view.order;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.List;

@ApiModel
public class MOrderCart {

    @ApiModelProperty("门店id")
    private Integer storeId;
    @ApiModelProperty("预约时间yyyy-MM-dd HH:mm")
    private String orderTime;
    @ApiModelProperty("用户车辆id")
    private Integer userCar;
    @ApiModelProperty("优惠券id")
    private Integer couponId;
    @ApiModelProperty("商品列表")
    private List<MCart> carts;

    public Integer getStoreId() {
        return storeId;
    }

    public void setStoreId(Integer storeId) {
        this.storeId = storeId;
    }

    public String getOrderTime() {
        return orderTime;
    }

    public void setOrderTime(String orderTime) {
        this.orderTime = orderTime;
    }

    public List<MCart> getCarts() {
        return carts;
    }

    public void setCarts(List<MCart> carts) {
        this.carts = carts;
    }

    public Integer getUserCar() {
        return userCar;
    }

    public void setUserCar(Integer userCar) {
        this.userCar = userCar;
    }

    public Integer getCouponId() {
        return couponId;
    }

    public void setCouponId(Integer couponId) {
        this.couponId = couponId;
    }
}
