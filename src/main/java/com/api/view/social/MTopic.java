package com.api.view.social;

import com.api.view.account.MSimpleUserInfo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel
public class MTopic extends MSimpleUserInfo{
    @ApiModelProperty("发布id")
    private Integer id;
    @ApiModelProperty("1文字2图片3视频")
    private Integer type;
    @ApiModelProperty("内容")
    private String content;
    @ApiModelProperty("多媒体资源")
    private String source;
    @ApiModelProperty("发布时间")
    private String time;
    @ApiModelProperty("距离")
    private String distance;
    @ApiModelProperty("1已赞0未赞")
    private Integer isPraise;

    public Integer getId() {
        return id;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIsPraise() {
        return isPraise;
    }

    public void setIsPraise(Integer isPraise) {
        this.isPraise = isPraise;
    }
}
