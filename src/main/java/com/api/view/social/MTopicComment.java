package com.api.view.social;

import com.api.view.account.MSimpleUserInfo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel
public class MTopicComment extends MSimpleUserInfo{
    @ApiModelProperty("内容")
    private String content;
    @ApiModelProperty("发布时间")
    private String time;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
