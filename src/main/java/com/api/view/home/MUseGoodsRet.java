package com.api.view.home;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel
public class MUseGoodsRet {
    @ApiModelProperty("1使用成功")
    private int result;
    @ApiModelProperty("券或红包生成二维码")
    private String code;
    @ApiModelProperty("物品使用地址(奖品码)")
    private String useUrl;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }


    public int getResult() {
        return result;
    }

    public void setResult(int result) {
        this.result = result;
    }

    public String getUseUrl() {
        return useUrl;
    }

    public void setUseUrl(String useUrl) {
        this.useUrl = useUrl;
    }
}
