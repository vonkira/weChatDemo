package com.api.view.home;

public class MPutGoodsJson {
    private Integer userGoodsId;
    private Integer putCount;

    public Integer getUserGoodsId() {
        return userGoodsId;
    }

    public void setUserGoodsId(Integer userGoodsId) {
        this.userGoodsId = userGoodsId;
    }

    public Integer getPutCount() {
        return putCount;
    }

    public void setPutCount(Integer putCount) {
        this.putCount = putCount;
    }
}
