package com.api.view.home;

import io.swagger.annotations.ApiModel;

import java.util.List;

@ApiModel
public class ApiUserGoodsResult {
    private List<ApiUserGoods> goods;

    public List<ApiUserGoods> getGoods() {
        return goods;
    }

    public void setGoods(List<ApiUserGoods> goods) {
        this.goods = goods;
    }
}
