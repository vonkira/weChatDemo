package com.api.view.home;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel
public class ApiUserGoodsDiscount extends ApiUserGoods{
    @ApiModelProperty("抵扣金币")
    private Integer discountCoin;
    @ApiModelProperty("抵扣人民币")
    private String discountPrice;
    @ApiModelProperty("抵扣id")
    private Integer dicountId;

    public String getDiscountPrice() {
        return discountPrice;
    }

    public void setDiscountPrice(String discountPrice) {
        this.discountPrice = discountPrice;
    }

    public Integer getDiscountCoin() {
        return discountCoin;
    }

    public void setDiscountCoin(Integer discountCoin) {
        this.discountCoin = discountCoin;
    }

    public Integer getDicountId() {
        return dicountId;
    }

    public void setDicountId(Integer dicountId) {
        this.dicountId = dicountId;
    }
}
