package com.api.view.home;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel
public class MContactUser extends UserInfo{

    @ApiModelProperty("是否注册 0 为注册 1 注册")
    private Integer isRegister;
    @ApiModelProperty("手机号")
    private String phone;
    @ApiModelProperty("通讯录姓名")
    private String name;

    public Integer getIsRegister() {
        return isRegister;
    }

    public void setIsRegister(Integer isRegister) {
        this.isRegister = isRegister;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
