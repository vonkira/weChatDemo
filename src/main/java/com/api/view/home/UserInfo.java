package com.api.view.home;

import com.api.view.map.Latitude;
import com.base.util.DFA;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import net.sf.ehcache.search.aggregator.Min;

import java.util.List;

@ApiModel(value="userInfo")
public class UserInfo extends Latitude{

	@ApiModelProperty(value="用户id")
	@ApiParam(hidden=true)
	private Integer id;
	@ApiModelProperty(value="账号")
	@ApiParam(hidden=true)
	private String account;
	@ApiModelProperty(value="昵称")
	private String nickName;
	@ApiModelProperty(value="头像")
	private String headImg;
	@ApiModelProperty(value="性别0:女 1:男")
	private Integer sex;
	@ApiModelProperty(value="用户体力")
	@ApiParam(hidden=true)
	private Integer strength;
	@ApiModelProperty(value="用户金币")
	@ApiParam(hidden=true)
	private Integer coin;
	@ApiModelProperty(value="用户物品数量")
	@ApiParam(hidden=true)
	private Integer goodsNum;
	@ApiModelProperty(value="1已绑定qq0未绑定")
	@ApiParam(hidden=true)
	private Integer bindQQ;
	@ApiModelProperty(value="1已绑定weixin0未绑定")
	@ApiParam(hidden=true)
	private Integer bindWeixin;
	@ApiModelProperty(value="1已绑定weibo0未绑定")
	@ApiParam(hidden=true)
	private Integer bindWeibo;
	@ApiModelProperty(value="城市")
	private String city;
	@ApiModelProperty(value="生日2017-01-01")
	private String birthday;
	@ApiModelProperty(value="邮箱")
	private String email;
	@ApiModelProperty(hidden = true)
	@ApiParam("个人标签逗号隔开")
	private String interest;
	@ApiModelProperty(value="1好友0不是")
	@ApiParam(hidden=true)
	private int isFriend;
	@ApiModelProperty(value="会员卡数量")
	@ApiParam(hidden=true)
	private int vipNum;
	@ApiModelProperty(value="个人标签")
	@ApiParam(hidden=true)
	private List<MInterest> interests;

	public UserInfo(){};
	
	public UserInfo(Integer id,String nickName){
		this.id = id;
		this.nickName = nickName;
		this.sex = 0;
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getAccount() {
		return account;
	}
	public void setAccount(String account) {
		this.account = account;
	}
	public String getNickName() {
		return DFA.replaceKeys(nickName);
	}
	public void setNickName(String nickName) {
		this.nickName = nickName;
	}
	public String getHeadImg() {
		return headImg;
	}
	public void setHeadImg(String headImg) {
		this.headImg = headImg;
	}
	public Integer getSex() {
		return sex;
	}
	public void setSex(Integer sex) {
		this.sex = sex;
	}

	public Integer getStrength() {
		return strength;
	}

	public void setStrength(Integer strength) {
		this.strength = strength;
	}

	public Integer getCoin() {
		return coin;
	}

	public void setCoin(Integer coin) {
		this.coin = coin;
	}

	public Integer getBindQQ() {
		return bindQQ;
	}

	public void setBindQQ(Integer bindQQ) {
		this.bindQQ = bindQQ;
	}

	public Integer getBindWeixin() {
		return bindWeixin;
	}

	public void setBindWeixin(Integer bindWeixin) {
		this.bindWeixin = bindWeixin;
	}

	public Integer getBindWeibo() {
		return bindWeibo;
	}

	public void setBindWeibo(Integer bindWeibo) {
		this.bindWeibo = bindWeibo;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getBirthday() {
		return birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getInterest() {
		return interest;
	}

	public void setInterest(String interest) {
		this.interest = interest;
	}

	public int getIsFriend() {
		return isFriend;
	}

	public void setIsFriend(int isFriend) {
		this.isFriend = isFriend;
	}

	public Integer getGoodsNum() {
		return goodsNum;
	}

	public void setGoodsNum(Integer goodsNum) {
		this.goodsNum = goodsNum;
	}

	public int getVipNum() {
		return vipNum;
	}

	public void setVipNum(int vipNum) {
		this.vipNum = vipNum;
	}

	public List<MInterest> getInterests() {
		return interests;
	}

	public void setInterests(List<MInterest> interests) {
		this.interests = interests;
	}
}
