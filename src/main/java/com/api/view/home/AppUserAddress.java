package com.api.view.home;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;

/**
 * 用户地址管理
 * @author vonkira
 *
 */
@ApiModel
public class AppUserAddress {
    @ApiModelProperty(value="id")
    @ApiParam("添加不传,修改必传")
    private Integer id;
    @ApiModelProperty(value="地区")
    @ApiParam(value = "地区",required = true)
    private String area;
    @ApiModelProperty(value="详细地址")
    @ApiParam(value = "详细地址",required = true)
    private String address;
    @ApiModelProperty(value="是否默认1默认0否")
    @ApiParam(value = "是否默认1默认0否",required = true)
    private Integer isDefault;
    @ApiModelProperty(value="姓名")
    @ApiParam(value = "姓名",required = true)
    private String name;
    @ApiModelProperty(value="联系电话")
    @ApiParam(value = "联系电话",required = true)
    private String phone;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Integer getIsDefault() {
        return isDefault;
    }

    public void setIsDefault(Integer isDefault) {
        this.isDefault = isDefault;
    }
}
