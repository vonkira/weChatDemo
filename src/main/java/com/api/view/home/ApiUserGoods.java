package com.api.view.home;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel
public class ApiUserGoods {

    private Integer user_goods_id;
    private Integer goods_id;
    private Integer nums;
    private Integer is_lock;
    private String mod_file;
    private Integer store_id;
    private String intro;
    private String name;
    private String address;
    private String tel;
    private String code;
    private Integer is_has_expiry;
    private Integer award_gold_nums;
    private String goods_name;
    private String date_begin;
    private String date_end;
    private String number;
    private String goods_use_url;
    private String source;
    private String level;
    private String weburl;
    private Integer is_combine;
    private Integer is_valid;
    @ApiModelProperty("1允许用户投放")
    private Integer isUserPush;

    public Integer getUser_goods_id() {
        return user_goods_id;
    }

    public void setUser_goods_id(Integer user_goods_id) {
        this.user_goods_id = user_goods_id;
    }

    public Integer getGoods_id() {
        return goods_id;
    }

    public void setGoods_id(Integer goods_id) {
        this.goods_id = goods_id;
    }

    public Integer getNums() {
        return nums;
    }

    public void setNums(Integer nums) {
        this.nums = nums;
    }

    public Integer getIs_lock() {
        return is_lock;
    }

    public void setIs_lock(Integer is_lock) {
        this.is_lock = is_lock;
    }

    public String getMod_file() {
        return mod_file;
    }

    public void setMod_file(String mod_file) {
        this.mod_file = mod_file;
    }

    public Integer getStore_id() {
        return store_id;
    }

    public void setStore_id(Integer store_id) {
        this.store_id = store_id;
    }

    public String getIntro() {
        return intro;
    }

    public void setIntro(String intro) {
        this.intro = intro;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Integer getIs_has_expiry() {
        return is_has_expiry;
    }

    public void setIs_has_expiry(Integer is_has_expiry) {
        this.is_has_expiry = is_has_expiry;
    }

    public Integer getAward_gold_nums() {
        return award_gold_nums;
    }

    public void setAward_gold_nums(Integer award_gold_nums) {
        this.award_gold_nums = award_gold_nums;
    }

    public String getGoods_name() {
        return goods_name;
    }

    public void setGoods_name(String goods_name) {
        this.goods_name = goods_name;
    }

    public String getDate_begin() {
        return date_begin;
    }

    public void setDate_begin(String date_begin) {
        this.date_begin = date_begin;
    }

    public String getDate_end() {
        return date_end;
    }

    public void setDate_end(String date_end) {
        this.date_end = date_end;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getGoods_use_url() {
        return goods_use_url;
    }

    public void setGoods_use_url(String goods_use_url) {
        this.goods_use_url = goods_use_url;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getWeburl() {
        return weburl;
    }

    public void setWeburl(String weburl) {
        this.weburl = weburl;
    }

    public Integer getIs_combine() {
        return is_combine;
    }

    public void setIs_combine(Integer is_combine) {
        this.is_combine = is_combine;
    }

    public Integer getIs_valid() {
        return is_valid;
    }

    public void setIs_valid(Integer is_valid) {
        this.is_valid = is_valid;
    }

    public Integer getIsUserPush() {
        return isUserPush;
    }

    public void setIsUserPush(Integer isUserPush) {
        this.isUserPush = isUserPush;
    }
}
