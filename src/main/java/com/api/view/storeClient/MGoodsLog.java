package com.api.view.storeClient;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel
public class MGoodsLog {
    @ApiModelProperty("用户账号")
    private String account;
    @ApiModelProperty("用户昵称")
    private String nickName;
    @ApiModelProperty("物品名称")
    private String goodsName;
    @ApiModelProperty("获取来源或使用说明")
    private String item;
    @ApiModelProperty("详细描述")
    private String info;
    @ApiModelProperty("1用户获取2用户扣除")
    private Integer type;
    @ApiModelProperty("时间")
    private String time;
    @ApiModelProperty("id")
    private Integer id;
    @ApiModelProperty("核销")
    private String checkTime;

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getCheckTime() {
        return checkTime;
    }

    public void setCheckTime(String checkTime) {
        this.checkTime = checkTime;
    }
}
