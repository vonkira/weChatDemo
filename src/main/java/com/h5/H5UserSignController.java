package com.h5;

import cn.afterturn.easypoi.entity.ImageEntity;
import cn.afterturn.easypoi.word.WordExportUtil;
import com.base.dao.model.BFile;
import com.base.dao.model.Ret;
import com.base.date.DateUtil;
import com.base.io.FileUtil;
import com.base.service.BFileService;
import com.hr.HrConstants;
import com.hr.action.ContractController;
import com.hr.dao.model.Contract;
import com.hr.dao.model.ContractPath;
import com.hr.daoEx.model.ContractEx;
import com.hr.service.ContractPathService;
import com.hr.service.ContractService;
import com.item.ConstantsCode;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import java.io.FileOutputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@RequestMapping("/m/sign")
@Controller
public class H5UserSignController extends H5BaseController{

    @Autowired
    private ContractService contractService;
    @Autowired
    private ContractPathService contractPathService;
    @Autowired
    private BFileService bFileService;

    @RequestMapping("userSign")
    public String UserSign(ModelMap map,Integer id){
        Contract contract = contractService.selectByPrimaryKey(id);
        BFile bFile = bFileService.selectByPrimaryKey(contract.getReadPath());
        //contract.setReadPath(ConstantsCode.FILE_PATH + bFile.getFilePath());
        contract.setReadPath(bFile.getFileId());
        map.put("contract",contract);


        return BASE_PATH+"/signature/signature";
    }


}
