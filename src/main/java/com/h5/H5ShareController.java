package com.h5;

import com.base.CoreConstants;
import com.base.util.StringUtil;
import com.item.dao.model.MobileVerify;
import com.item.service.MobileVerifyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/m/share")
@Controller
public class H5ShareController extends H5BaseController{
    @Autowired
    private MobileVerifyService verifyService;

    @RequestMapping
    public String defaults(Integer type,String id,String num,String img, Integer uid,ModelMap map){
       if (type == 2){
           if (id.indexOf(CoreConstants.VIDEO_NORMAL) == -1){
               id += CoreConstants.VIDEO_NORMAL;
           }
       }
        map.put("type",type);
        map.put("id",id);
        map.put("num",num);
        map.put("img",img);
        map.put("uid",uid);
        return BASE_PATH+"/share/video/share";
    }

    @RequestMapping("video/{id}")
    public String share(@PathVariable String id, ModelMap map){
        if (id.indexOf(CoreConstants.VIDEO_NORMAL) == -1){
            id += CoreConstants.VIDEO_NORMAL;
        }
        map.put("type",2);
        map.put("id",id);
        return BASE_PATH+"/share/video/share";
    }

    @RequestMapping("invite/{id}")
    public String invite(@PathVariable String id, ModelMap map){
        map.put("type",3);
        map.put("id",id);
        return BASE_PATH+"/share/video/share";
    }

    @RequestMapping("goods/{id}")
    public String goods(@PathVariable String id, Integer num,String img, ModelMap map){
        map.put("type",1);
//        String[] ids = id.split(",");
//        String[] goodsId = new String[ids.length];
//        Integer[] goodsNum = new Integer[ids.length];
//        for (int i = 0;i < ids.length; i++){
//            goodsId[i] = ids[i].substring(0,ids[i].indexOf("_"));
//            goodsNum[i] = Integer.valueOf(ids[i].substring(ids[i].indexOf("_"),ids[i].length()-1));
//        }
        map.put("id",id);
        map.put("num",num);
        map.put("img",img);
        return BASE_PATH+"/share/video/share";
    }

    @RequestMapping("/regist")
    public String shareRegist(String token, ModelMap map){
        if (StringUtil.isNotBlank(token)){
            MobileVerify verify = verifyService.queryByToken(token);
            if (verify != null){
                map.put("inviteId",verify.getUserId());
            }
        }
        return BASE_PATH+"/share/regist/share_sign";
    }
}
