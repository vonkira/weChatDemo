package com.hr.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.hr.dao.model.ContractPath;
import com.hr.dao.model.ContractPathExample;

public interface ContractPathMapper {
	int countByExample(ContractPathExample example);

	int deleteByExample(ContractPathExample example);

	int deleteByPrimaryKey(Integer id);

	int insert(ContractPath record);

	int insertSelective(ContractPath record);

	List<ContractPath> selectByExample(ContractPathExample example);

	ContractPath selectByPrimaryKey(Integer id);

	int updateByExampleSelective(@Param("record") ContractPath record,@Param("example") ContractPathExample example);

	int updateByExample(@Param("record") ContractPath record,@Param("example") ContractPathExample example);

	int updateByPrimaryKeySelective(ContractPath record);

	int updateByPrimaryKey(ContractPath record);

}
