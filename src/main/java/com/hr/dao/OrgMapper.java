package com.hr.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.hr.dao.model.Org;
import com.hr.dao.model.OrgExample;

public interface OrgMapper {
	int countByExample(OrgExample example);

	int deleteByExample(OrgExample example);

	int deleteByPrimaryKey(Integer id);

	int insert(Org record);

	int insertSelective(Org record);

	List<Org> selectByExample(OrgExample example);

	Org selectByPrimaryKey(Integer id);

	int updateByExampleSelective(@Param("record") Org record,@Param("example") OrgExample example);

	int updateByExample(@Param("record") Org record,@Param("example") OrgExample example);

	int updateByPrimaryKeySelective(Org record);

	int updateByPrimaryKey(Org record);

}
