package com.hr.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.hr.dao.model.Point;
import com.hr.dao.model.PointExample;

public interface PointMapper {
	int countByExample(PointExample example);

	int deleteByExample(PointExample example);

	int deleteByPrimaryKey(Integer id);

	int insert(Point record);

	int insertSelective(Point record);

	List<Point> selectByExample(PointExample example);

	Point selectByPrimaryKey(Integer id);

	int updateByExampleSelective(@Param("record") Point record,@Param("example") PointExample example);

	int updateByExample(@Param("record") Point record,@Param("example") PointExample example);

	int updateByPrimaryKeySelective(Point record);

	int updateByPrimaryKey(Point record);

}
