package com.hr.dao.model;



/**
 *
 */
public class Contract {

	/**
	 *
	 */
	private Integer id;

	/**
	 *合同模板id
	 */
	private String conId;

	/**
	 *所属组织id
	 */
	private String orgCode;

	/**
	 *城市
	 */
	private Integer cityId;

	/**
	 *站点
	 */
	private Integer pointId;

	/**
	 *全职兼职 0全职 1兼职
	 */
	private String state;

	/**
	 *用户名称
	 */
	private String name;

	/**
	 *性别 1男 2女
	 */
	private Integer sex;

	/**
	 *手机号码
	 */
	private String phone;

	/**
	 *身份证号码
	 */
	private String idCard;

	/**
	 *住址
	 */
	private String address;

	/**
	 *户籍地址
	 */
	private String idAddress;

	/**
	 *紧急联系人姓名
	 */
	private String cirName;

	/**
	 *紧急人联系电话
	 */
	private String cirPhone;

	/**
	 *雇佣人工作
	 */
	private String job;

	/**
	 *学历
	 */
	private String eduction;

	/**
	 *每月的价格
	 */
	private String money;

	/**
	 *入职时间
	 */
	private java.util.Date joinTime;

	/**
	 *合同到期时间
	 */
	private java.util.Date endTime;

	/**
	 *合同类型0未签署 1已签 2合同到期
	 */
	private Integer type;

	/**
	 *合同上传路径
	 */
	private String readPath;

	/**
	 *印章路径
	 */
	private String yzPath;

	/**
	 *用户签名图片路径
	 */
	private String signPath;

	/**
	 *是否删除 0不删除 1删除
	 */
	private Integer isDel;

	/**
	 *创建时间
	 */
	private java.util.Date createTime;

	public void setId(Integer id) {
		this.id=id;
	}

	public Integer getId() {
		return id;
	}

	public void setConId(String conId) {
		this.conId=conId == null ? conId : conId.trim();
	}

	public String getConId() {
		return conId;
	}

	public void setOrgCode(String orgCode) {
		this.orgCode=orgCode == null ? orgCode : orgCode.trim();
	}

	public String getOrgCode() {
		return orgCode;
	}

	public void setCityId(Integer cityId) {
		this.cityId=cityId;
	}

	public Integer getCityId() {
		return cityId;
	}

	public void setPointId(Integer pointId) {
		this.pointId=pointId;
	}

	public Integer getPointId() {
		return pointId;
	}

	public void setState(String state) {
		this.state=state == null ? state : state.trim();
	}

	public String getState() {
		return state;
	}

	public void setName(String name) {
		this.name=name == null ? name : name.trim();
	}

	public String getName() {
		return name;
	}

	public void setSex(Integer sex) {
		this.sex=sex;
	}

	public Integer getSex() {
		return sex;
	}

	public void setPhone(String phone) {
		this.phone=phone == null ? phone : phone.trim();
	}

	public String getPhone() {
		return phone;
	}

	public void setIdCard(String idCard) {
		this.idCard=idCard == null ? idCard : idCard.trim();
	}

	public String getIdCard() {
		return idCard;
	}

	public void setAddress(String address) {
		this.address=address == null ? address : address.trim();
	}

	public String getAddress() {
		return address;
	}

	public void setIdAddress(String idAddress) {
		this.idAddress=idAddress == null ? idAddress : idAddress.trim();
	}

	public String getIdAddress() {
		return idAddress;
	}

	public void setCirName(String cirName) {
		this.cirName=cirName == null ? cirName : cirName.trim();
	}

	public String getCirName() {
		return cirName;
	}

	public void setCirPhone(String cirPhone) {
		this.cirPhone=cirPhone == null ? cirPhone : cirPhone.trim();
	}

	public String getCirPhone() {
		return cirPhone;
	}

	public void setJob(String job) {
		this.job=job == null ? job : job.trim();
	}

	public String getJob() {
		return job;
	}

	public void setEduction(String eduction) {
		this.eduction=eduction == null ? eduction : eduction.trim();
	}

	public String getEduction() {
		return eduction;
	}

	public void setMoney(String money) {
		this.money=money == null ? money : money.trim();
	}

	public String getMoney() {
		return money;
	}

	public void setJoinTime(java.util.Date joinTime) {
		this.joinTime=joinTime;
	}

	public java.util.Date getJoinTime() {
		return joinTime;
	}

	public void setEndTime(java.util.Date endTime) {
		this.endTime=endTime;
	}

	public java.util.Date getEndTime() {
		return endTime;
	}

	public void setType(Integer type) {
		this.type=type;
	}

	public Integer getType() {
		return type;
	}

	public void setReadPath(String readPath) {
		this.readPath=readPath == null ? readPath : readPath.trim();
	}

	public String getReadPath() {
		return readPath;
	}

	public void setYzPath(String yzPath) {
		this.yzPath=yzPath == null ? yzPath : yzPath.trim();
	}

	public String getYzPath() {
		return yzPath;
	}

	public void setSignPath(String signPath) {
		this.signPath=signPath == null ? signPath : signPath.trim();
	}

	public String getSignPath() {
		return signPath;
	}

	public void setIsDel(Integer isDel) {
		this.isDel=isDel;
	}

	public Integer getIsDel() {
		return isDel;
	}

	public void setCreateTime(java.util.Date createTime) {
		this.createTime=createTime;
	}

	public java.util.Date getCreateTime() {
		return createTime;
	}

}
