package com.hr.dao.model;



/**
 *
 */
public class City {

	/**
	 *
	 */
	private Integer id;

	/**
	 *所在城市名称
	 */
	private String cityName;

	/**
	 *创建时间
	 */
	private java.util.Date createTime;

	public void setId(Integer id) {
		this.id=id;
	}

	public Integer getId() {
		return id;
	}

	public void setCityName(String cityName) {
		this.cityName=cityName == null ? cityName : cityName.trim();
	}

	public String getCityName() {
		return cityName;
	}

	public void setCreateTime(java.util.Date createTime) {
		this.createTime=createTime;
	}

	public java.util.Date getCreateTime() {
		return createTime;
	}

}
