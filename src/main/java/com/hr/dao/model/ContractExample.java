package com.hr.dao.model;

import java.util.List;
import java.util.ArrayList;

public class ContractExample {
	protected String orderByClause;

	protected boolean distinct;

	protected List<Criteria> oredCriteria;

	public ContractExample(){
		oredCriteria = new ArrayList<Criteria>();
	}

	public void setOrderByClause(String orderByClause) {
		this.orderByClause = orderByClause;
	}

	public String getOrderByClause() {
		return orderByClause;
	}

	public void setDistinct(boolean distinct) {
		this.distinct = distinct;
	}

	public boolean isDistinct() {
		return distinct;
	}

	public List<Criteria> getOredCriteria() {
		return oredCriteria;
	}

	public void or(Criteria criteria) {
		oredCriteria.add(criteria);
	}

	public Criteria or() {
		Criteria criteria = createCriteriaInternal();
		oredCriteria.add(criteria);
		return criteria;
	}

	public Criteria createCriteria() {
		Criteria criteria = createCriteriaInternal();
		if (oredCriteria.size() == 0) {
			oredCriteria.add(criteria);
		}
		return criteria;
	}

	protected Criteria createCriteriaInternal() {
		Criteria criteria = new Criteria();
		return criteria;
	}

	public void clear() {
		oredCriteria.clear();
		orderByClause = null;
		distinct = false;
	}

	protected abstract static class GeneratedCriteria {
		protected List<Criterion> criteria;

		protected GeneratedCriteria() {
			super();
			criteria = new ArrayList<Criterion>();
		}

		public boolean isValid() {
			return criteria.size() > 0;
		}

		public List<Criterion> getAllCriteria() {
			return criteria;
		}

		public List<Criterion> getCriteria() {
			return criteria;
		}

		protected void addCriterion(String condition) {
			if (condition == null) {
				throw new RuntimeException("Value for condition cannot be null");
			}
			criteria.add(new Criterion(condition));
		}

		protected void addCriterion(String condition, Object value, String property) {
			if (value == null) {
				throw new RuntimeException("Value for " + property + " cannot be null");
			}
			criteria.add(new Criterion(condition, value));
		}

		protected void addCriterion(String condition, Object value1, Object value2, String property) {
			if (value1 == null || value2 == null) {
				throw new RuntimeException("Between values for " + property + " cannot be null");
			}
			criteria.add(new Criterion(condition, value1, value2));
		}

		public Criteria andIdIsNull() {
			addCriterion("id is null");
			return (Criteria) this;
		}

		public Criteria andIdIsNotNull() {
			addCriterion("id is not null");
			return (Criteria) this;
		}

		public Criteria andIdEqualTo(Integer value) {
			addCriterion("id =", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotEqualTo(Integer value) {
			addCriterion("id <>", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdGreaterThan(Integer value) {
			addCriterion("id >", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdGreaterThanOrEqualTo(Integer value) {
			addCriterion("id >=", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdLessThan(Integer value) {
			addCriterion("id <", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdLessThanOrEqualTo(Integer value) {
			addCriterion("id <=", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdIn(List<Integer> values) {
			addCriterion("id in", values, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotIn(List<Integer> values) {
			addCriterion("id not in", values, "id");
			return (Criteria) this;
		}

		public Criteria andIdBetween(Integer value1, Integer value2) {
			addCriterion("id between", value1, value2, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotBetween(Integer value1, Integer value2) {
			addCriterion("id not between", value1, value2, "id");
			return (Criteria) this;
		}

		public Criteria andConIdIsNull() {
			addCriterion("con_id is null");
			return (Criteria) this;
		}

		public Criteria andConIdIsNotNull() {
			addCriterion("con_id is not null");
			return (Criteria) this;
		}

		public Criteria andConIdEqualTo(String value) {
			addCriterion("con_id =", value, "conId");
			return (Criteria) this;
		}

		public Criteria andConIdNotEqualTo(String value) {
			addCriterion("con_id <>", value, "conId");
			return (Criteria) this;
		}

		public Criteria andConIdGreaterThan(String value) {
			addCriterion("con_id >", value, "conId");
			return (Criteria) this;
		}

		public Criteria andConIdGreaterThanOrEqualTo(String value) {
			addCriterion("con_id >=", value, "conId");
			return (Criteria) this;
		}

		public Criteria andConIdLessThan(String value) {
			addCriterion("con_id <", value, "conId");
			return (Criteria) this;
		}

		public Criteria andConIdLessThanOrEqualTo(String value) {
			addCriterion("con_id <=", value, "conId");
			return (Criteria) this;
		}

		public Criteria andConIdLike(String value) {
			addCriterion("con_id like", value, "conId");
			return (Criteria) this;
		}

		public Criteria andConIdNotLike(String value) {
			addCriterion("con_id not like", value, "conId");
			return (Criteria) this;
		}

		public Criteria andConIdIn(List<String> values) {
			addCriterion("con_id in", values, "conId");
			return (Criteria) this;
		}

		public Criteria andConIdNotIn(List<String> values) {
			addCriterion("con_id not in", values, "conId");
			return (Criteria) this;
		}

		public Criteria andConIdBetween(String value1, String value2) {
			addCriterion("con_id between", value1, value2, "conId");
			return (Criteria) this;
		}

		public Criteria andConIdNotBetween(String value1, String value2) {
			addCriterion("con_id not between", value1, value2, "conId");
			return (Criteria) this;
		}

		public Criteria andOrgCodeIsNull() {
			addCriterion("org_code is null");
			return (Criteria) this;
		}

		public Criteria andOrgCodeIsNotNull() {
			addCriterion("org_code is not null");
			return (Criteria) this;
		}

		public Criteria andOrgCodeEqualTo(String value) {
			addCriterion("org_code =", value, "orgCode");
			return (Criteria) this;
		}

		public Criteria andOrgCodeNotEqualTo(String value) {
			addCriterion("org_code <>", value, "orgCode");
			return (Criteria) this;
		}

		public Criteria andOrgCodeGreaterThan(String value) {
			addCriterion("org_code >", value, "orgCode");
			return (Criteria) this;
		}

		public Criteria andOrgCodeGreaterThanOrEqualTo(String value) {
			addCriterion("org_code >=", value, "orgCode");
			return (Criteria) this;
		}

		public Criteria andOrgCodeLessThan(String value) {
			addCriterion("org_code <", value, "orgCode");
			return (Criteria) this;
		}

		public Criteria andOrgCodeLessThanOrEqualTo(String value) {
			addCriterion("org_code <=", value, "orgCode");
			return (Criteria) this;
		}

		public Criteria andOrgCodeLike(String value) {
			addCriterion("org_code like", value, "orgCode");
			return (Criteria) this;
		}

		public Criteria andOrgCodeNotLike(String value) {
			addCriterion("org_code not like", value, "orgCode");
			return (Criteria) this;
		}

		public Criteria andOrgCodeIn(List<String> values) {
			addCriterion("org_code in", values, "orgCode");
			return (Criteria) this;
		}

		public Criteria andOrgCodeNotIn(List<String> values) {
			addCriterion("org_code not in", values, "orgCode");
			return (Criteria) this;
		}

		public Criteria andOrgCodeBetween(String value1, String value2) {
			addCriterion("org_code between", value1, value2, "orgCode");
			return (Criteria) this;
		}

		public Criteria andOrgCodeNotBetween(String value1, String value2) {
			addCriterion("org_code not between", value1, value2, "orgCode");
			return (Criteria) this;
		}

		public Criteria andCityIdIsNull() {
			addCriterion("city_id is null");
			return (Criteria) this;
		}

		public Criteria andCityIdIsNotNull() {
			addCriterion("city_id is not null");
			return (Criteria) this;
		}

		public Criteria andCityIdEqualTo(Integer value) {
			addCriterion("city_id =", value, "cityId");
			return (Criteria) this;
		}

		public Criteria andCityIdNotEqualTo(Integer value) {
			addCriterion("city_id <>", value, "cityId");
			return (Criteria) this;
		}

		public Criteria andCityIdGreaterThan(Integer value) {
			addCriterion("city_id >", value, "cityId");
			return (Criteria) this;
		}

		public Criteria andCityIdGreaterThanOrEqualTo(Integer value) {
			addCriterion("city_id >=", value, "cityId");
			return (Criteria) this;
		}

		public Criteria andCityIdLessThan(Integer value) {
			addCriterion("city_id <", value, "cityId");
			return (Criteria) this;
		}

		public Criteria andCityIdLessThanOrEqualTo(Integer value) {
			addCriterion("city_id <=", value, "cityId");
			return (Criteria) this;
		}

		public Criteria andCityIdIn(List<Integer> values) {
			addCriterion("city_id in", values, "cityId");
			return (Criteria) this;
		}

		public Criteria andCityIdNotIn(List<Integer> values) {
			addCriterion("city_id not in", values, "cityId");
			return (Criteria) this;
		}

		public Criteria andCityIdBetween(Integer value1, Integer value2) {
			addCriterion("city_id between", value1, value2, "cityId");
			return (Criteria) this;
		}

		public Criteria andCityIdNotBetween(Integer value1, Integer value2) {
			addCriterion("city_id not between", value1, value2, "cityId");
			return (Criteria) this;
		}

		public Criteria andPointIdIsNull() {
			addCriterion("point_id is null");
			return (Criteria) this;
		}

		public Criteria andPointIdIsNotNull() {
			addCriterion("point_id is not null");
			return (Criteria) this;
		}

		public Criteria andPointIdEqualTo(Integer value) {
			addCriterion("point_id =", value, "pointId");
			return (Criteria) this;
		}

		public Criteria andPointIdNotEqualTo(Integer value) {
			addCriterion("point_id <>", value, "pointId");
			return (Criteria) this;
		}

		public Criteria andPointIdGreaterThan(Integer value) {
			addCriterion("point_id >", value, "pointId");
			return (Criteria) this;
		}

		public Criteria andPointIdGreaterThanOrEqualTo(Integer value) {
			addCriterion("point_id >=", value, "pointId");
			return (Criteria) this;
		}

		public Criteria andPointIdLessThan(Integer value) {
			addCriterion("point_id <", value, "pointId");
			return (Criteria) this;
		}

		public Criteria andPointIdLessThanOrEqualTo(Integer value) {
			addCriterion("point_id <=", value, "pointId");
			return (Criteria) this;
		}

		public Criteria andPointIdIn(List<Integer> values) {
			addCriterion("point_id in", values, "pointId");
			return (Criteria) this;
		}

		public Criteria andPointIdNotIn(List<Integer> values) {
			addCriterion("point_id not in", values, "pointId");
			return (Criteria) this;
		}

		public Criteria andPointIdBetween(Integer value1, Integer value2) {
			addCriterion("point_id between", value1, value2, "pointId");
			return (Criteria) this;
		}

		public Criteria andPointIdNotBetween(Integer value1, Integer value2) {
			addCriterion("point_id not between", value1, value2, "pointId");
			return (Criteria) this;
		}

		public Criteria andStateIsNull() {
			addCriterion("state is null");
			return (Criteria) this;
		}

		public Criteria andStateIsNotNull() {
			addCriterion("state is not null");
			return (Criteria) this;
		}

		public Criteria andStateEqualTo(String value) {
			addCriterion("state =", value, "state");
			return (Criteria) this;
		}

		public Criteria andStateNotEqualTo(String value) {
			addCriterion("state <>", value, "state");
			return (Criteria) this;
		}

		public Criteria andStateGreaterThan(String value) {
			addCriterion("state >", value, "state");
			return (Criteria) this;
		}

		public Criteria andStateGreaterThanOrEqualTo(String value) {
			addCriterion("state >=", value, "state");
			return (Criteria) this;
		}

		public Criteria andStateLessThan(String value) {
			addCriterion("state <", value, "state");
			return (Criteria) this;
		}

		public Criteria andStateLessThanOrEqualTo(String value) {
			addCriterion("state <=", value, "state");
			return (Criteria) this;
		}

		public Criteria andStateLike(String value) {
			addCriterion("state like", value, "state");
			return (Criteria) this;
		}

		public Criteria andStateNotLike(String value) {
			addCriterion("state not like", value, "state");
			return (Criteria) this;
		}

		public Criteria andStateIn(List<String> values) {
			addCriterion("state in", values, "state");
			return (Criteria) this;
		}

		public Criteria andStateNotIn(List<String> values) {
			addCriterion("state not in", values, "state");
			return (Criteria) this;
		}

		public Criteria andStateBetween(String value1, String value2) {
			addCriterion("state between", value1, value2, "state");
			return (Criteria) this;
		}

		public Criteria andStateNotBetween(String value1, String value2) {
			addCriterion("state not between", value1, value2, "state");
			return (Criteria) this;
		}

		public Criteria andNameIsNull() {
			addCriterion("name is null");
			return (Criteria) this;
		}

		public Criteria andNameIsNotNull() {
			addCriterion("name is not null");
			return (Criteria) this;
		}

		public Criteria andNameEqualTo(String value) {
			addCriterion("name =", value, "name");
			return (Criteria) this;
		}

		public Criteria andNameNotEqualTo(String value) {
			addCriterion("name <>", value, "name");
			return (Criteria) this;
		}

		public Criteria andNameGreaterThan(String value) {
			addCriterion("name >", value, "name");
			return (Criteria) this;
		}

		public Criteria andNameGreaterThanOrEqualTo(String value) {
			addCriterion("name >=", value, "name");
			return (Criteria) this;
		}

		public Criteria andNameLessThan(String value) {
			addCriterion("name <", value, "name");
			return (Criteria) this;
		}

		public Criteria andNameLessThanOrEqualTo(String value) {
			addCriterion("name <=", value, "name");
			return (Criteria) this;
		}

		public Criteria andNameLike(String value) {
			addCriterion("name like", value, "name");
			return (Criteria) this;
		}

		public Criteria andNameNotLike(String value) {
			addCriterion("name not like", value, "name");
			return (Criteria) this;
		}

		public Criteria andNameIn(List<String> values) {
			addCriterion("name in", values, "name");
			return (Criteria) this;
		}

		public Criteria andNameNotIn(List<String> values) {
			addCriterion("name not in", values, "name");
			return (Criteria) this;
		}

		public Criteria andNameBetween(String value1, String value2) {
			addCriterion("name between", value1, value2, "name");
			return (Criteria) this;
		}

		public Criteria andNameNotBetween(String value1, String value2) {
			addCriterion("name not between", value1, value2, "name");
			return (Criteria) this;
		}

		public Criteria andSexIsNull() {
			addCriterion("sex is null");
			return (Criteria) this;
		}

		public Criteria andSexIsNotNull() {
			addCriterion("sex is not null");
			return (Criteria) this;
		}

		public Criteria andSexEqualTo(Integer value) {
			addCriterion("sex =", value, "sex");
			return (Criteria) this;
		}

		public Criteria andSexNotEqualTo(Integer value) {
			addCriterion("sex <>", value, "sex");
			return (Criteria) this;
		}

		public Criteria andSexGreaterThan(Integer value) {
			addCriterion("sex >", value, "sex");
			return (Criteria) this;
		}

		public Criteria andSexGreaterThanOrEqualTo(Integer value) {
			addCriterion("sex >=", value, "sex");
			return (Criteria) this;
		}

		public Criteria andSexLessThan(Integer value) {
			addCriterion("sex <", value, "sex");
			return (Criteria) this;
		}

		public Criteria andSexLessThanOrEqualTo(Integer value) {
			addCriterion("sex <=", value, "sex");
			return (Criteria) this;
		}

		public Criteria andSexIn(List<Integer> values) {
			addCriterion("sex in", values, "sex");
			return (Criteria) this;
		}

		public Criteria andSexNotIn(List<Integer> values) {
			addCriterion("sex not in", values, "sex");
			return (Criteria) this;
		}

		public Criteria andSexBetween(Integer value1, Integer value2) {
			addCriterion("sex between", value1, value2, "sex");
			return (Criteria) this;
		}

		public Criteria andSexNotBetween(Integer value1, Integer value2) {
			addCriterion("sex not between", value1, value2, "sex");
			return (Criteria) this;
		}

		public Criteria andPhoneIsNull() {
			addCriterion("phone is null");
			return (Criteria) this;
		}

		public Criteria andPhoneIsNotNull() {
			addCriterion("phone is not null");
			return (Criteria) this;
		}

		public Criteria andPhoneEqualTo(String value) {
			addCriterion("phone =", value, "phone");
			return (Criteria) this;
		}

		public Criteria andPhoneNotEqualTo(String value) {
			addCriterion("phone <>", value, "phone");
			return (Criteria) this;
		}

		public Criteria andPhoneGreaterThan(String value) {
			addCriterion("phone >", value, "phone");
			return (Criteria) this;
		}

		public Criteria andPhoneGreaterThanOrEqualTo(String value) {
			addCriterion("phone >=", value, "phone");
			return (Criteria) this;
		}

		public Criteria andPhoneLessThan(String value) {
			addCriterion("phone <", value, "phone");
			return (Criteria) this;
		}

		public Criteria andPhoneLessThanOrEqualTo(String value) {
			addCriterion("phone <=", value, "phone");
			return (Criteria) this;
		}

		public Criteria andPhoneLike(String value) {
			addCriterion("phone like", value, "phone");
			return (Criteria) this;
		}

		public Criteria andPhoneNotLike(String value) {
			addCriterion("phone not like", value, "phone");
			return (Criteria) this;
		}

		public Criteria andPhoneIn(List<String> values) {
			addCriterion("phone in", values, "phone");
			return (Criteria) this;
		}

		public Criteria andPhoneNotIn(List<String> values) {
			addCriterion("phone not in", values, "phone");
			return (Criteria) this;
		}

		public Criteria andPhoneBetween(String value1, String value2) {
			addCriterion("phone between", value1, value2, "phone");
			return (Criteria) this;
		}

		public Criteria andPhoneNotBetween(String value1, String value2) {
			addCriterion("phone not between", value1, value2, "phone");
			return (Criteria) this;
		}

		public Criteria andIdCardIsNull() {
			addCriterion("id_card is null");
			return (Criteria) this;
		}

		public Criteria andIdCardIsNotNull() {
			addCriterion("id_card is not null");
			return (Criteria) this;
		}

		public Criteria andIdCardEqualTo(String value) {
			addCriterion("id_card =", value, "idCard");
			return (Criteria) this;
		}

		public Criteria andIdCardNotEqualTo(String value) {
			addCriterion("id_card <>", value, "idCard");
			return (Criteria) this;
		}

		public Criteria andIdCardGreaterThan(String value) {
			addCriterion("id_card >", value, "idCard");
			return (Criteria) this;
		}

		public Criteria andIdCardGreaterThanOrEqualTo(String value) {
			addCriterion("id_card >=", value, "idCard");
			return (Criteria) this;
		}

		public Criteria andIdCardLessThan(String value) {
			addCriterion("id_card <", value, "idCard");
			return (Criteria) this;
		}

		public Criteria andIdCardLessThanOrEqualTo(String value) {
			addCriterion("id_card <=", value, "idCard");
			return (Criteria) this;
		}

		public Criteria andIdCardLike(String value) {
			addCriterion("id_card like", value, "idCard");
			return (Criteria) this;
		}

		public Criteria andIdCardNotLike(String value) {
			addCriterion("id_card not like", value, "idCard");
			return (Criteria) this;
		}

		public Criteria andIdCardIn(List<String> values) {
			addCriterion("id_card in", values, "idCard");
			return (Criteria) this;
		}

		public Criteria andIdCardNotIn(List<String> values) {
			addCriterion("id_card not in", values, "idCard");
			return (Criteria) this;
		}

		public Criteria andIdCardBetween(String value1, String value2) {
			addCriterion("id_card between", value1, value2, "idCard");
			return (Criteria) this;
		}

		public Criteria andIdCardNotBetween(String value1, String value2) {
			addCriterion("id_card not between", value1, value2, "idCard");
			return (Criteria) this;
		}

		public Criteria andAddressIsNull() {
			addCriterion("address is null");
			return (Criteria) this;
		}

		public Criteria andAddressIsNotNull() {
			addCriterion("address is not null");
			return (Criteria) this;
		}

		public Criteria andAddressEqualTo(String value) {
			addCriterion("address =", value, "address");
			return (Criteria) this;
		}

		public Criteria andAddressNotEqualTo(String value) {
			addCriterion("address <>", value, "address");
			return (Criteria) this;
		}

		public Criteria andAddressGreaterThan(String value) {
			addCriterion("address >", value, "address");
			return (Criteria) this;
		}

		public Criteria andAddressGreaterThanOrEqualTo(String value) {
			addCriterion("address >=", value, "address");
			return (Criteria) this;
		}

		public Criteria andAddressLessThan(String value) {
			addCriterion("address <", value, "address");
			return (Criteria) this;
		}

		public Criteria andAddressLessThanOrEqualTo(String value) {
			addCriterion("address <=", value, "address");
			return (Criteria) this;
		}

		public Criteria andAddressLike(String value) {
			addCriterion("address like", value, "address");
			return (Criteria) this;
		}

		public Criteria andAddressNotLike(String value) {
			addCriterion("address not like", value, "address");
			return (Criteria) this;
		}

		public Criteria andAddressIn(List<String> values) {
			addCriterion("address in", values, "address");
			return (Criteria) this;
		}

		public Criteria andAddressNotIn(List<String> values) {
			addCriterion("address not in", values, "address");
			return (Criteria) this;
		}

		public Criteria andAddressBetween(String value1, String value2) {
			addCriterion("address between", value1, value2, "address");
			return (Criteria) this;
		}

		public Criteria andAddressNotBetween(String value1, String value2) {
			addCriterion("address not between", value1, value2, "address");
			return (Criteria) this;
		}

		public Criteria andIdAddressIsNull() {
			addCriterion("id_address is null");
			return (Criteria) this;
		}

		public Criteria andIdAddressIsNotNull() {
			addCriterion("id_address is not null");
			return (Criteria) this;
		}

		public Criteria andIdAddressEqualTo(String value) {
			addCriterion("id_address =", value, "idAddress");
			return (Criteria) this;
		}

		public Criteria andIdAddressNotEqualTo(String value) {
			addCriterion("id_address <>", value, "idAddress");
			return (Criteria) this;
		}

		public Criteria andIdAddressGreaterThan(String value) {
			addCriterion("id_address >", value, "idAddress");
			return (Criteria) this;
		}

		public Criteria andIdAddressGreaterThanOrEqualTo(String value) {
			addCriterion("id_address >=", value, "idAddress");
			return (Criteria) this;
		}

		public Criteria andIdAddressLessThan(String value) {
			addCriterion("id_address <", value, "idAddress");
			return (Criteria) this;
		}

		public Criteria andIdAddressLessThanOrEqualTo(String value) {
			addCriterion("id_address <=", value, "idAddress");
			return (Criteria) this;
		}

		public Criteria andIdAddressLike(String value) {
			addCriterion("id_address like", value, "idAddress");
			return (Criteria) this;
		}

		public Criteria andIdAddressNotLike(String value) {
			addCriterion("id_address not like", value, "idAddress");
			return (Criteria) this;
		}

		public Criteria andIdAddressIn(List<String> values) {
			addCriterion("id_address in", values, "idAddress");
			return (Criteria) this;
		}

		public Criteria andIdAddressNotIn(List<String> values) {
			addCriterion("id_address not in", values, "idAddress");
			return (Criteria) this;
		}

		public Criteria andIdAddressBetween(String value1, String value2) {
			addCriterion("id_address between", value1, value2, "idAddress");
			return (Criteria) this;
		}

		public Criteria andIdAddressNotBetween(String value1, String value2) {
			addCriterion("id_address not between", value1, value2, "idAddress");
			return (Criteria) this;
		}

		public Criteria andCirNameIsNull() {
			addCriterion("cir_name is null");
			return (Criteria) this;
		}

		public Criteria andCirNameIsNotNull() {
			addCriterion("cir_name is not null");
			return (Criteria) this;
		}

		public Criteria andCirNameEqualTo(String value) {
			addCriterion("cir_name =", value, "cirName");
			return (Criteria) this;
		}

		public Criteria andCirNameNotEqualTo(String value) {
			addCriterion("cir_name <>", value, "cirName");
			return (Criteria) this;
		}

		public Criteria andCirNameGreaterThan(String value) {
			addCriterion("cir_name >", value, "cirName");
			return (Criteria) this;
		}

		public Criteria andCirNameGreaterThanOrEqualTo(String value) {
			addCriterion("cir_name >=", value, "cirName");
			return (Criteria) this;
		}

		public Criteria andCirNameLessThan(String value) {
			addCriterion("cir_name <", value, "cirName");
			return (Criteria) this;
		}

		public Criteria andCirNameLessThanOrEqualTo(String value) {
			addCriterion("cir_name <=", value, "cirName");
			return (Criteria) this;
		}

		public Criteria andCirNameLike(String value) {
			addCriterion("cir_name like", value, "cirName");
			return (Criteria) this;
		}

		public Criteria andCirNameNotLike(String value) {
			addCriterion("cir_name not like", value, "cirName");
			return (Criteria) this;
		}

		public Criteria andCirNameIn(List<String> values) {
			addCriterion("cir_name in", values, "cirName");
			return (Criteria) this;
		}

		public Criteria andCirNameNotIn(List<String> values) {
			addCriterion("cir_name not in", values, "cirName");
			return (Criteria) this;
		}

		public Criteria andCirNameBetween(String value1, String value2) {
			addCriterion("cir_name between", value1, value2, "cirName");
			return (Criteria) this;
		}

		public Criteria andCirNameNotBetween(String value1, String value2) {
			addCriterion("cir_name not between", value1, value2, "cirName");
			return (Criteria) this;
		}

		public Criteria andCirPhoneIsNull() {
			addCriterion("cir_phone is null");
			return (Criteria) this;
		}

		public Criteria andCirPhoneIsNotNull() {
			addCriterion("cir_phone is not null");
			return (Criteria) this;
		}

		public Criteria andCirPhoneEqualTo(String value) {
			addCriterion("cir_phone =", value, "cirPhone");
			return (Criteria) this;
		}

		public Criteria andCirPhoneNotEqualTo(String value) {
			addCriterion("cir_phone <>", value, "cirPhone");
			return (Criteria) this;
		}

		public Criteria andCirPhoneGreaterThan(String value) {
			addCriterion("cir_phone >", value, "cirPhone");
			return (Criteria) this;
		}

		public Criteria andCirPhoneGreaterThanOrEqualTo(String value) {
			addCriterion("cir_phone >=", value, "cirPhone");
			return (Criteria) this;
		}

		public Criteria andCirPhoneLessThan(String value) {
			addCriterion("cir_phone <", value, "cirPhone");
			return (Criteria) this;
		}

		public Criteria andCirPhoneLessThanOrEqualTo(String value) {
			addCriterion("cir_phone <=", value, "cirPhone");
			return (Criteria) this;
		}

		public Criteria andCirPhoneLike(String value) {
			addCriterion("cir_phone like", value, "cirPhone");
			return (Criteria) this;
		}

		public Criteria andCirPhoneNotLike(String value) {
			addCriterion("cir_phone not like", value, "cirPhone");
			return (Criteria) this;
		}

		public Criteria andCirPhoneIn(List<String> values) {
			addCriterion("cir_phone in", values, "cirPhone");
			return (Criteria) this;
		}

		public Criteria andCirPhoneNotIn(List<String> values) {
			addCriterion("cir_phone not in", values, "cirPhone");
			return (Criteria) this;
		}

		public Criteria andCirPhoneBetween(String value1, String value2) {
			addCriterion("cir_phone between", value1, value2, "cirPhone");
			return (Criteria) this;
		}

		public Criteria andCirPhoneNotBetween(String value1, String value2) {
			addCriterion("cir_phone not between", value1, value2, "cirPhone");
			return (Criteria) this;
		}

		public Criteria andJobIsNull() {
			addCriterion("job is null");
			return (Criteria) this;
		}

		public Criteria andJobIsNotNull() {
			addCriterion("job is not null");
			return (Criteria) this;
		}

		public Criteria andJobEqualTo(String value) {
			addCriterion("job =", value, "job");
			return (Criteria) this;
		}

		public Criteria andJobNotEqualTo(String value) {
			addCriterion("job <>", value, "job");
			return (Criteria) this;
		}

		public Criteria andJobGreaterThan(String value) {
			addCriterion("job >", value, "job");
			return (Criteria) this;
		}

		public Criteria andJobGreaterThanOrEqualTo(String value) {
			addCriterion("job >=", value, "job");
			return (Criteria) this;
		}

		public Criteria andJobLessThan(String value) {
			addCriterion("job <", value, "job");
			return (Criteria) this;
		}

		public Criteria andJobLessThanOrEqualTo(String value) {
			addCriterion("job <=", value, "job");
			return (Criteria) this;
		}

		public Criteria andJobLike(String value) {
			addCriterion("job like", value, "job");
			return (Criteria) this;
		}

		public Criteria andJobNotLike(String value) {
			addCriterion("job not like", value, "job");
			return (Criteria) this;
		}

		public Criteria andJobIn(List<String> values) {
			addCriterion("job in", values, "job");
			return (Criteria) this;
		}

		public Criteria andJobNotIn(List<String> values) {
			addCriterion("job not in", values, "job");
			return (Criteria) this;
		}

		public Criteria andJobBetween(String value1, String value2) {
			addCriterion("job between", value1, value2, "job");
			return (Criteria) this;
		}

		public Criteria andJobNotBetween(String value1, String value2) {
			addCriterion("job not between", value1, value2, "job");
			return (Criteria) this;
		}

		public Criteria andEductionIsNull() {
			addCriterion("eduction is null");
			return (Criteria) this;
		}

		public Criteria andEductionIsNotNull() {
			addCriterion("eduction is not null");
			return (Criteria) this;
		}

		public Criteria andEductionEqualTo(String value) {
			addCriterion("eduction =", value, "eduction");
			return (Criteria) this;
		}

		public Criteria andEductionNotEqualTo(String value) {
			addCriterion("eduction <>", value, "eduction");
			return (Criteria) this;
		}

		public Criteria andEductionGreaterThan(String value) {
			addCriterion("eduction >", value, "eduction");
			return (Criteria) this;
		}

		public Criteria andEductionGreaterThanOrEqualTo(String value) {
			addCriterion("eduction >=", value, "eduction");
			return (Criteria) this;
		}

		public Criteria andEductionLessThan(String value) {
			addCriterion("eduction <", value, "eduction");
			return (Criteria) this;
		}

		public Criteria andEductionLessThanOrEqualTo(String value) {
			addCriterion("eduction <=", value, "eduction");
			return (Criteria) this;
		}

		public Criteria andEductionLike(String value) {
			addCriterion("eduction like", value, "eduction");
			return (Criteria) this;
		}

		public Criteria andEductionNotLike(String value) {
			addCriterion("eduction not like", value, "eduction");
			return (Criteria) this;
		}

		public Criteria andEductionIn(List<String> values) {
			addCriterion("eduction in", values, "eduction");
			return (Criteria) this;
		}

		public Criteria andEductionNotIn(List<String> values) {
			addCriterion("eduction not in", values, "eduction");
			return (Criteria) this;
		}

		public Criteria andEductionBetween(String value1, String value2) {
			addCriterion("eduction between", value1, value2, "eduction");
			return (Criteria) this;
		}

		public Criteria andEductionNotBetween(String value1, String value2) {
			addCriterion("eduction not between", value1, value2, "eduction");
			return (Criteria) this;
		}

		public Criteria andMoneyIsNull() {
			addCriterion("money is null");
			return (Criteria) this;
		}

		public Criteria andMoneyIsNotNull() {
			addCriterion("money is not null");
			return (Criteria) this;
		}

		public Criteria andMoneyEqualTo(String value) {
			addCriterion("money =", value, "money");
			return (Criteria) this;
		}

		public Criteria andMoneyNotEqualTo(String value) {
			addCriterion("money <>", value, "money");
			return (Criteria) this;
		}

		public Criteria andMoneyGreaterThan(String value) {
			addCriterion("money >", value, "money");
			return (Criteria) this;
		}

		public Criteria andMoneyGreaterThanOrEqualTo(String value) {
			addCriterion("money >=", value, "money");
			return (Criteria) this;
		}

		public Criteria andMoneyLessThan(String value) {
			addCriterion("money <", value, "money");
			return (Criteria) this;
		}

		public Criteria andMoneyLessThanOrEqualTo(String value) {
			addCriterion("money <=", value, "money");
			return (Criteria) this;
		}

		public Criteria andMoneyLike(String value) {
			addCriterion("money like", value, "money");
			return (Criteria) this;
		}

		public Criteria andMoneyNotLike(String value) {
			addCriterion("money not like", value, "money");
			return (Criteria) this;
		}

		public Criteria andMoneyIn(List<String> values) {
			addCriterion("money in", values, "money");
			return (Criteria) this;
		}

		public Criteria andMoneyNotIn(List<String> values) {
			addCriterion("money not in", values, "money");
			return (Criteria) this;
		}

		public Criteria andMoneyBetween(String value1, String value2) {
			addCriterion("money between", value1, value2, "money");
			return (Criteria) this;
		}

		public Criteria andMoneyNotBetween(String value1, String value2) {
			addCriterion("money not between", value1, value2, "money");
			return (Criteria) this;
		}

		public Criteria andJoinTimeIsNull() {
			addCriterion("join_time is null");
			return (Criteria) this;
		}

		public Criteria andJoinTimeIsNotNull() {
			addCriterion("join_time is not null");
			return (Criteria) this;
		}

		public Criteria andJoinTimeEqualTo(java.util.Date value) {
			addCriterion("join_time =", value, "joinTime");
			return (Criteria) this;
		}

		public Criteria andJoinTimeNotEqualTo(java.util.Date value) {
			addCriterion("join_time <>", value, "joinTime");
			return (Criteria) this;
		}

		public Criteria andJoinTimeGreaterThan(java.util.Date value) {
			addCriterion("join_time >", value, "joinTime");
			return (Criteria) this;
		}

		public Criteria andJoinTimeGreaterThanOrEqualTo(java.util.Date value) {
			addCriterion("join_time >=", value, "joinTime");
			return (Criteria) this;
		}

		public Criteria andJoinTimeLessThan(java.util.Date value) {
			addCriterion("join_time <", value, "joinTime");
			return (Criteria) this;
		}

		public Criteria andJoinTimeLessThanOrEqualTo(java.util.Date value) {
			addCriterion("join_time <=", value, "joinTime");
			return (Criteria) this;
		}

		public Criteria andJoinTimeIn(List<java.util.Date> values) {
			addCriterion("join_time in", values, "joinTime");
			return (Criteria) this;
		}

		public Criteria andJoinTimeNotIn(List<java.util.Date> values) {
			addCriterion("join_time not in", values, "joinTime");
			return (Criteria) this;
		}

		public Criteria andJoinTimeBetween(java.util.Date value1, java.util.Date value2) {
			addCriterion("join_time between", value1, value2, "joinTime");
			return (Criteria) this;
		}

		public Criteria andJoinTimeNotBetween(java.util.Date value1, java.util.Date value2) {
			addCriterion("join_time not between", value1, value2, "joinTime");
			return (Criteria) this;
		}

		public Criteria andEndTimeIsNull() {
			addCriterion("end_time is null");
			return (Criteria) this;
		}

		public Criteria andEndTimeIsNotNull() {
			addCriterion("end_time is not null");
			return (Criteria) this;
		}

		public Criteria andEndTimeEqualTo(java.util.Date value) {
			addCriterion("end_time =", value, "endTime");
			return (Criteria) this;
		}

		public Criteria andEndTimeNotEqualTo(java.util.Date value) {
			addCriterion("end_time <>", value, "endTime");
			return (Criteria) this;
		}

		public Criteria andEndTimeGreaterThan(java.util.Date value) {
			addCriterion("end_time >", value, "endTime");
			return (Criteria) this;
		}

		public Criteria andEndTimeGreaterThanOrEqualTo(java.util.Date value) {
			addCriterion("end_time >=", value, "endTime");
			return (Criteria) this;
		}

		public Criteria andEndTimeLessThan(java.util.Date value) {
			addCriterion("end_time <", value, "endTime");
			return (Criteria) this;
		}

		public Criteria andEndTimeLessThanOrEqualTo(java.util.Date value) {
			addCriterion("end_time <=", value, "endTime");
			return (Criteria) this;
		}

		public Criteria andEndTimeIn(List<java.util.Date> values) {
			addCriterion("end_time in", values, "endTime");
			return (Criteria) this;
		}

		public Criteria andEndTimeNotIn(List<java.util.Date> values) {
			addCriterion("end_time not in", values, "endTime");
			return (Criteria) this;
		}

		public Criteria andEndTimeBetween(java.util.Date value1, java.util.Date value2) {
			addCriterion("end_time between", value1, value2, "endTime");
			return (Criteria) this;
		}

		public Criteria andEndTimeNotBetween(java.util.Date value1, java.util.Date value2) {
			addCriterion("end_time not between", value1, value2, "endTime");
			return (Criteria) this;
		}

		public Criteria andTypeIsNull() {
			addCriterion("type is null");
			return (Criteria) this;
		}

		public Criteria andTypeIsNotNull() {
			addCriterion("type is not null");
			return (Criteria) this;
		}

		public Criteria andTypeEqualTo(Integer value) {
			addCriterion("type =", value, "type");
			return (Criteria) this;
		}

		public Criteria andTypeNotEqualTo(Integer value) {
			addCriterion("type <>", value, "type");
			return (Criteria) this;
		}

		public Criteria andTypeGreaterThan(Integer value) {
			addCriterion("type >", value, "type");
			return (Criteria) this;
		}

		public Criteria andTypeGreaterThanOrEqualTo(Integer value) {
			addCriterion("type >=", value, "type");
			return (Criteria) this;
		}

		public Criteria andTypeLessThan(Integer value) {
			addCriterion("type <", value, "type");
			return (Criteria) this;
		}

		public Criteria andTypeLessThanOrEqualTo(Integer value) {
			addCriterion("type <=", value, "type");
			return (Criteria) this;
		}

		public Criteria andTypeIn(List<Integer> values) {
			addCriterion("type in", values, "type");
			return (Criteria) this;
		}

		public Criteria andTypeNotIn(List<Integer> values) {
			addCriterion("type not in", values, "type");
			return (Criteria) this;
		}

		public Criteria andTypeBetween(Integer value1, Integer value2) {
			addCriterion("type between", value1, value2, "type");
			return (Criteria) this;
		}

		public Criteria andTypeNotBetween(Integer value1, Integer value2) {
			addCriterion("type not between", value1, value2, "type");
			return (Criteria) this;
		}

		public Criteria andReadPathIsNull() {
			addCriterion("read_path is null");
			return (Criteria) this;
		}

		public Criteria andReadPathIsNotNull() {
			addCriterion("read_path is not null");
			return (Criteria) this;
		}

		public Criteria andReadPathEqualTo(String value) {
			addCriterion("read_path =", value, "readPath");
			return (Criteria) this;
		}

		public Criteria andReadPathNotEqualTo(String value) {
			addCriterion("read_path <>", value, "readPath");
			return (Criteria) this;
		}

		public Criteria andReadPathGreaterThan(String value) {
			addCriterion("read_path >", value, "readPath");
			return (Criteria) this;
		}

		public Criteria andReadPathGreaterThanOrEqualTo(String value) {
			addCriterion("read_path >=", value, "readPath");
			return (Criteria) this;
		}

		public Criteria andReadPathLessThan(String value) {
			addCriterion("read_path <", value, "readPath");
			return (Criteria) this;
		}

		public Criteria andReadPathLessThanOrEqualTo(String value) {
			addCriterion("read_path <=", value, "readPath");
			return (Criteria) this;
		}

		public Criteria andReadPathLike(String value) {
			addCriterion("read_path like", value, "readPath");
			return (Criteria) this;
		}

		public Criteria andReadPathNotLike(String value) {
			addCriterion("read_path not like", value, "readPath");
			return (Criteria) this;
		}

		public Criteria andReadPathIn(List<String> values) {
			addCriterion("read_path in", values, "readPath");
			return (Criteria) this;
		}

		public Criteria andReadPathNotIn(List<String> values) {
			addCriterion("read_path not in", values, "readPath");
			return (Criteria) this;
		}

		public Criteria andReadPathBetween(String value1, String value2) {
			addCriterion("read_path between", value1, value2, "readPath");
			return (Criteria) this;
		}

		public Criteria andReadPathNotBetween(String value1, String value2) {
			addCriterion("read_path not between", value1, value2, "readPath");
			return (Criteria) this;
		}

		public Criteria andYzPathIsNull() {
			addCriterion("yz_path is null");
			return (Criteria) this;
		}

		public Criteria andYzPathIsNotNull() {
			addCriterion("yz_path is not null");
			return (Criteria) this;
		}

		public Criteria andYzPathEqualTo(String value) {
			addCriterion("yz_path =", value, "yzPath");
			return (Criteria) this;
		}

		public Criteria andYzPathNotEqualTo(String value) {
			addCriterion("yz_path <>", value, "yzPath");
			return (Criteria) this;
		}

		public Criteria andYzPathGreaterThan(String value) {
			addCriterion("yz_path >", value, "yzPath");
			return (Criteria) this;
		}

		public Criteria andYzPathGreaterThanOrEqualTo(String value) {
			addCriterion("yz_path >=", value, "yzPath");
			return (Criteria) this;
		}

		public Criteria andYzPathLessThan(String value) {
			addCriterion("yz_path <", value, "yzPath");
			return (Criteria) this;
		}

		public Criteria andYzPathLessThanOrEqualTo(String value) {
			addCriterion("yz_path <=", value, "yzPath");
			return (Criteria) this;
		}

		public Criteria andYzPathLike(String value) {
			addCriterion("yz_path like", value, "yzPath");
			return (Criteria) this;
		}

		public Criteria andYzPathNotLike(String value) {
			addCriterion("yz_path not like", value, "yzPath");
			return (Criteria) this;
		}

		public Criteria andYzPathIn(List<String> values) {
			addCriterion("yz_path in", values, "yzPath");
			return (Criteria) this;
		}

		public Criteria andYzPathNotIn(List<String> values) {
			addCriterion("yz_path not in", values, "yzPath");
			return (Criteria) this;
		}

		public Criteria andYzPathBetween(String value1, String value2) {
			addCriterion("yz_path between", value1, value2, "yzPath");
			return (Criteria) this;
		}

		public Criteria andYzPathNotBetween(String value1, String value2) {
			addCriterion("yz_path not between", value1, value2, "yzPath");
			return (Criteria) this;
		}

		public Criteria andSignPathIsNull() {
			addCriterion("sign_path is null");
			return (Criteria) this;
		}

		public Criteria andSignPathIsNotNull() {
			addCriterion("sign_path is not null");
			return (Criteria) this;
		}

		public Criteria andSignPathEqualTo(String value) {
			addCriterion("sign_path =", value, "signPath");
			return (Criteria) this;
		}

		public Criteria andSignPathNotEqualTo(String value) {
			addCriterion("sign_path <>", value, "signPath");
			return (Criteria) this;
		}

		public Criteria andSignPathGreaterThan(String value) {
			addCriterion("sign_path >", value, "signPath");
			return (Criteria) this;
		}

		public Criteria andSignPathGreaterThanOrEqualTo(String value) {
			addCriterion("sign_path >=", value, "signPath");
			return (Criteria) this;
		}

		public Criteria andSignPathLessThan(String value) {
			addCriterion("sign_path <", value, "signPath");
			return (Criteria) this;
		}

		public Criteria andSignPathLessThanOrEqualTo(String value) {
			addCriterion("sign_path <=", value, "signPath");
			return (Criteria) this;
		}

		public Criteria andSignPathLike(String value) {
			addCriterion("sign_path like", value, "signPath");
			return (Criteria) this;
		}

		public Criteria andSignPathNotLike(String value) {
			addCriterion("sign_path not like", value, "signPath");
			return (Criteria) this;
		}

		public Criteria andSignPathIn(List<String> values) {
			addCriterion("sign_path in", values, "signPath");
			return (Criteria) this;
		}

		public Criteria andSignPathNotIn(List<String> values) {
			addCriterion("sign_path not in", values, "signPath");
			return (Criteria) this;
		}

		public Criteria andSignPathBetween(String value1, String value2) {
			addCriterion("sign_path between", value1, value2, "signPath");
			return (Criteria) this;
		}

		public Criteria andSignPathNotBetween(String value1, String value2) {
			addCriterion("sign_path not between", value1, value2, "signPath");
			return (Criteria) this;
		}

		public Criteria andIsDelIsNull() {
			addCriterion("is_del is null");
			return (Criteria) this;
		}

		public Criteria andIsDelIsNotNull() {
			addCriterion("is_del is not null");
			return (Criteria) this;
		}

		public Criteria andIsDelEqualTo(Integer value) {
			addCriterion("is_del =", value, "isDel");
			return (Criteria) this;
		}

		public Criteria andIsDelNotEqualTo(Integer value) {
			addCriterion("is_del <>", value, "isDel");
			return (Criteria) this;
		}

		public Criteria andIsDelGreaterThan(Integer value) {
			addCriterion("is_del >", value, "isDel");
			return (Criteria) this;
		}

		public Criteria andIsDelGreaterThanOrEqualTo(Integer value) {
			addCriterion("is_del >=", value, "isDel");
			return (Criteria) this;
		}

		public Criteria andIsDelLessThan(Integer value) {
			addCriterion("is_del <", value, "isDel");
			return (Criteria) this;
		}

		public Criteria andIsDelLessThanOrEqualTo(Integer value) {
			addCriterion("is_del <=", value, "isDel");
			return (Criteria) this;
		}

		public Criteria andIsDelIn(List<Integer> values) {
			addCriterion("is_del in", values, "isDel");
			return (Criteria) this;
		}

		public Criteria andIsDelNotIn(List<Integer> values) {
			addCriterion("is_del not in", values, "isDel");
			return (Criteria) this;
		}

		public Criteria andIsDelBetween(Integer value1, Integer value2) {
			addCriterion("is_del between", value1, value2, "isDel");
			return (Criteria) this;
		}

		public Criteria andIsDelNotBetween(Integer value1, Integer value2) {
			addCriterion("is_del not between", value1, value2, "isDel");
			return (Criteria) this;
		}

		public Criteria andCreateTimeIsNull() {
			addCriterion("create_time is null");
			return (Criteria) this;
		}

		public Criteria andCreateTimeIsNotNull() {
			addCriterion("create_time is not null");
			return (Criteria) this;
		}

		public Criteria andCreateTimeEqualTo(java.util.Date value) {
			addCriterion("create_time =", value, "createTime");
			return (Criteria) this;
		}

		public Criteria andCreateTimeNotEqualTo(java.util.Date value) {
			addCriterion("create_time <>", value, "createTime");
			return (Criteria) this;
		}

		public Criteria andCreateTimeGreaterThan(java.util.Date value) {
			addCriterion("create_time >", value, "createTime");
			return (Criteria) this;
		}

		public Criteria andCreateTimeGreaterThanOrEqualTo(java.util.Date value) {
			addCriterion("create_time >=", value, "createTime");
			return (Criteria) this;
		}

		public Criteria andCreateTimeLessThan(java.util.Date value) {
			addCriterion("create_time <", value, "createTime");
			return (Criteria) this;
		}

		public Criteria andCreateTimeLessThanOrEqualTo(java.util.Date value) {
			addCriterion("create_time <=", value, "createTime");
			return (Criteria) this;
		}

		public Criteria andCreateTimeIn(List<java.util.Date> values) {
			addCriterion("create_time in", values, "createTime");
			return (Criteria) this;
		}

		public Criteria andCreateTimeNotIn(List<java.util.Date> values) {
			addCriterion("create_time not in", values, "createTime");
			return (Criteria) this;
		}

		public Criteria andCreateTimeBetween(java.util.Date value1, java.util.Date value2) {
			addCriterion("create_time between", value1, value2, "createTime");
			return (Criteria) this;
		}

		public Criteria andCreateTimeNotBetween(java.util.Date value1, java.util.Date value2) {
			addCriterion("create_time not between", value1, value2, "createTime");
			return (Criteria) this;
		}

}

	public static class Criteria extends GeneratedCriteria {

		protected Criteria() {
			super();
		}
	}

	public static class Criterion {
		private String condition;

		private Object value;

		private Object secondValue;

		private boolean noValue;

		private boolean singleValue;

		private boolean betweenValue;

		private boolean listValue;

		private String typeHandler;

		public String getCondition() {
			return condition;
		}

		public Object getValue() {
			return value;
		}

		public Object getSecondValue() {
			return secondValue;
		}

		public boolean isNoValue() {
		return noValue;
		}

		public boolean isSingleValue() {
			return singleValue;
		}

		public boolean isBetweenValue() {
			return betweenValue;
		}

		public boolean isListValue() {
			return listValue;
		}

		public String getTypeHandler() {
			return typeHandler;
		}

		protected Criterion(String condition) {
			super();
			this.condition = condition;
			this.typeHandler = null;
			this.noValue = true;
		}

		protected Criterion(String condition, Object value, String typeHandler) {
			super();
			this.condition = condition;
			this.value = value;
			this.typeHandler = typeHandler;
			if (value instanceof List<?>) {
				this.listValue = true;
			} else {
				this.singleValue = true;
			}
		}

		protected Criterion(String condition, Object value) {
			this(condition, value, null);
		}

		protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
			super();
			this.condition = condition;
			this.value = value;
			this.secondValue = secondValue;
			this.typeHandler = typeHandler;
			this.betweenValue = true;
		}

		protected Criterion(String condition, Object value, Object secondValue) {
			this(condition, value, secondValue, null);
		}
	}
}