package com.hr.dao.model;

import java.util.List;
import java.util.ArrayList;

public class ContractPathExample {
	protected String orderByClause;

	protected boolean distinct;

	protected List<Criteria> oredCriteria;

	public ContractPathExample(){
		oredCriteria = new ArrayList<Criteria>();
	}

	public void setOrderByClause(String orderByClause) {
		this.orderByClause = orderByClause;
	}

	public String getOrderByClause() {
		return orderByClause;
	}

	public void setDistinct(boolean distinct) {
		this.distinct = distinct;
	}

	public boolean isDistinct() {
		return distinct;
	}

	public List<Criteria> getOredCriteria() {
		return oredCriteria;
	}

	public void or(Criteria criteria) {
		oredCriteria.add(criteria);
	}

	public Criteria or() {
		Criteria criteria = createCriteriaInternal();
		oredCriteria.add(criteria);
		return criteria;
	}

	public Criteria createCriteria() {
		Criteria criteria = createCriteriaInternal();
		if (oredCriteria.size() == 0) {
			oredCriteria.add(criteria);
		}
		return criteria;
	}

	protected Criteria createCriteriaInternal() {
		Criteria criteria = new Criteria();
		return criteria;
	}

	public void clear() {
		oredCriteria.clear();
		orderByClause = null;
		distinct = false;
	}

	protected abstract static class GeneratedCriteria {
		protected List<Criterion> criteria;

		protected GeneratedCriteria() {
			super();
			criteria = new ArrayList<Criterion>();
		}

		public boolean isValid() {
			return criteria.size() > 0;
		}

		public List<Criterion> getAllCriteria() {
			return criteria;
		}

		public List<Criterion> getCriteria() {
			return criteria;
		}

		protected void addCriterion(String condition) {
			if (condition == null) {
				throw new RuntimeException("Value for condition cannot be null");
			}
			criteria.add(new Criterion(condition));
		}

		protected void addCriterion(String condition, Object value, String property) {
			if (value == null) {
				throw new RuntimeException("Value for " + property + " cannot be null");
			}
			criteria.add(new Criterion(condition, value));
		}

		protected void addCriterion(String condition, Object value1, Object value2, String property) {
			if (value1 == null || value2 == null) {
				throw new RuntimeException("Between values for " + property + " cannot be null");
			}
			criteria.add(new Criterion(condition, value1, value2));
		}

		public Criteria andIdIsNull() {
			addCriterion("id is null");
			return (Criteria) this;
		}

		public Criteria andIdIsNotNull() {
			addCriterion("id is not null");
			return (Criteria) this;
		}

		public Criteria andIdEqualTo(Integer value) {
			addCriterion("id =", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotEqualTo(Integer value) {
			addCriterion("id <>", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdGreaterThan(Integer value) {
			addCriterion("id >", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdGreaterThanOrEqualTo(Integer value) {
			addCriterion("id >=", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdLessThan(Integer value) {
			addCriterion("id <", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdLessThanOrEqualTo(Integer value) {
			addCriterion("id <=", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdIn(List<Integer> values) {
			addCriterion("id in", values, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotIn(List<Integer> values) {
			addCriterion("id not in", values, "id");
			return (Criteria) this;
		}

		public Criteria andIdBetween(Integer value1, Integer value2) {
			addCriterion("id between", value1, value2, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotBetween(Integer value1, Integer value2) {
			addCriterion("id not between", value1, value2, "id");
			return (Criteria) this;
		}

		public Criteria andTitleIsNull() {
			addCriterion("title is null");
			return (Criteria) this;
		}

		public Criteria andTitleIsNotNull() {
			addCriterion("title is not null");
			return (Criteria) this;
		}

		public Criteria andTitleEqualTo(String value) {
			addCriterion("title =", value, "title");
			return (Criteria) this;
		}

		public Criteria andTitleNotEqualTo(String value) {
			addCriterion("title <>", value, "title");
			return (Criteria) this;
		}

		public Criteria andTitleGreaterThan(String value) {
			addCriterion("title >", value, "title");
			return (Criteria) this;
		}

		public Criteria andTitleGreaterThanOrEqualTo(String value) {
			addCriterion("title >=", value, "title");
			return (Criteria) this;
		}

		public Criteria andTitleLessThan(String value) {
			addCriterion("title <", value, "title");
			return (Criteria) this;
		}

		public Criteria andTitleLessThanOrEqualTo(String value) {
			addCriterion("title <=", value, "title");
			return (Criteria) this;
		}

		public Criteria andTitleLike(String value) {
			addCriterion("title like", value, "title");
			return (Criteria) this;
		}

		public Criteria andTitleNotLike(String value) {
			addCriterion("title not like", value, "title");
			return (Criteria) this;
		}

		public Criteria andTitleIn(List<String> values) {
			addCriterion("title in", values, "title");
			return (Criteria) this;
		}

		public Criteria andTitleNotIn(List<String> values) {
			addCriterion("title not in", values, "title");
			return (Criteria) this;
		}

		public Criteria andTitleBetween(String value1, String value2) {
			addCriterion("title between", value1, value2, "title");
			return (Criteria) this;
		}

		public Criteria andTitleNotBetween(String value1, String value2) {
			addCriterion("title not between", value1, value2, "title");
			return (Criteria) this;
		}

		public Criteria andStateIsNull() {
			addCriterion("state is null");
			return (Criteria) this;
		}

		public Criteria andStateIsNotNull() {
			addCriterion("state is not null");
			return (Criteria) this;
		}

		public Criteria andStateEqualTo(Integer value) {
			addCriterion("state =", value, "state");
			return (Criteria) this;
		}

		public Criteria andStateNotEqualTo(Integer value) {
			addCriterion("state <>", value, "state");
			return (Criteria) this;
		}

		public Criteria andStateGreaterThan(Integer value) {
			addCriterion("state >", value, "state");
			return (Criteria) this;
		}

		public Criteria andStateGreaterThanOrEqualTo(Integer value) {
			addCriterion("state >=", value, "state");
			return (Criteria) this;
		}

		public Criteria andStateLessThan(Integer value) {
			addCriterion("state <", value, "state");
			return (Criteria) this;
		}

		public Criteria andStateLessThanOrEqualTo(Integer value) {
			addCriterion("state <=", value, "state");
			return (Criteria) this;
		}

		public Criteria andStateIn(List<Integer> values) {
			addCriterion("state in", values, "state");
			return (Criteria) this;
		}

		public Criteria andStateNotIn(List<Integer> values) {
			addCriterion("state not in", values, "state");
			return (Criteria) this;
		}

		public Criteria andStateBetween(Integer value1, Integer value2) {
			addCriterion("state between", value1, value2, "state");
			return (Criteria) this;
		}

		public Criteria andStateNotBetween(Integer value1, Integer value2) {
			addCriterion("state not between", value1, value2, "state");
			return (Criteria) this;
		}

		public Criteria andContractIdIsNull() {
			addCriterion("contract_id is null");
			return (Criteria) this;
		}

		public Criteria andContractIdIsNotNull() {
			addCriterion("contract_id is not null");
			return (Criteria) this;
		}

		public Criteria andContractIdEqualTo(Integer value) {
			addCriterion("contract_id =", value, "contractId");
			return (Criteria) this;
		}

		public Criteria andContractIdNotEqualTo(Integer value) {
			addCriterion("contract_id <>", value, "contractId");
			return (Criteria) this;
		}

		public Criteria andContractIdGreaterThan(Integer value) {
			addCriterion("contract_id >", value, "contractId");
			return (Criteria) this;
		}

		public Criteria andContractIdGreaterThanOrEqualTo(Integer value) {
			addCriterion("contract_id >=", value, "contractId");
			return (Criteria) this;
		}

		public Criteria andContractIdLessThan(Integer value) {
			addCriterion("contract_id <", value, "contractId");
			return (Criteria) this;
		}

		public Criteria andContractIdLessThanOrEqualTo(Integer value) {
			addCriterion("contract_id <=", value, "contractId");
			return (Criteria) this;
		}

		public Criteria andContractIdIn(List<Integer> values) {
			addCriterion("contract_id in", values, "contractId");
			return (Criteria) this;
		}

		public Criteria andContractIdNotIn(List<Integer> values) {
			addCriterion("contract_id not in", values, "contractId");
			return (Criteria) this;
		}

		public Criteria andContractIdBetween(Integer value1, Integer value2) {
			addCriterion("contract_id between", value1, value2, "contractId");
			return (Criteria) this;
		}

		public Criteria andContractIdNotBetween(Integer value1, Integer value2) {
			addCriterion("contract_id not between", value1, value2, "contractId");
			return (Criteria) this;
		}

		public Criteria andFilePathIsNull() {
			addCriterion("file_path is null");
			return (Criteria) this;
		}

		public Criteria andFilePathIsNotNull() {
			addCriterion("file_path is not null");
			return (Criteria) this;
		}

		public Criteria andFilePathEqualTo(String value) {
			addCriterion("file_path =", value, "filePath");
			return (Criteria) this;
		}

		public Criteria andFilePathNotEqualTo(String value) {
			addCriterion("file_path <>", value, "filePath");
			return (Criteria) this;
		}

		public Criteria andFilePathGreaterThan(String value) {
			addCriterion("file_path >", value, "filePath");
			return (Criteria) this;
		}

		public Criteria andFilePathGreaterThanOrEqualTo(String value) {
			addCriterion("file_path >=", value, "filePath");
			return (Criteria) this;
		}

		public Criteria andFilePathLessThan(String value) {
			addCriterion("file_path <", value, "filePath");
			return (Criteria) this;
		}

		public Criteria andFilePathLessThanOrEqualTo(String value) {
			addCriterion("file_path <=", value, "filePath");
			return (Criteria) this;
		}

		public Criteria andFilePathLike(String value) {
			addCriterion("file_path like", value, "filePath");
			return (Criteria) this;
		}

		public Criteria andFilePathNotLike(String value) {
			addCriterion("file_path not like", value, "filePath");
			return (Criteria) this;
		}

		public Criteria andFilePathIn(List<String> values) {
			addCriterion("file_path in", values, "filePath");
			return (Criteria) this;
		}

		public Criteria andFilePathNotIn(List<String> values) {
			addCriterion("file_path not in", values, "filePath");
			return (Criteria) this;
		}

		public Criteria andFilePathBetween(String value1, String value2) {
			addCriterion("file_path between", value1, value2, "filePath");
			return (Criteria) this;
		}

		public Criteria andFilePathNotBetween(String value1, String value2) {
			addCriterion("file_path not between", value1, value2, "filePath");
			return (Criteria) this;
		}

		public Criteria andUserRoleIsNull() {
			addCriterion("user_role is null");
			return (Criteria) this;
		}

		public Criteria andUserRoleIsNotNull() {
			addCriterion("user_role is not null");
			return (Criteria) this;
		}

		public Criteria andUserRoleEqualTo(String value) {
			addCriterion("user_role =", value, "userRole");
			return (Criteria) this;
		}

		public Criteria andUserRoleNotEqualTo(String value) {
			addCriterion("user_role <>", value, "userRole");
			return (Criteria) this;
		}

		public Criteria andUserRoleGreaterThan(String value) {
			addCriterion("user_role >", value, "userRole");
			return (Criteria) this;
		}

		public Criteria andUserRoleGreaterThanOrEqualTo(String value) {
			addCriterion("user_role >=", value, "userRole");
			return (Criteria) this;
		}

		public Criteria andUserRoleLessThan(String value) {
			addCriterion("user_role <", value, "userRole");
			return (Criteria) this;
		}

		public Criteria andUserRoleLessThanOrEqualTo(String value) {
			addCriterion("user_role <=", value, "userRole");
			return (Criteria) this;
		}

		public Criteria andUserRoleLike(String value) {
			addCriterion("user_role like", value, "userRole");
			return (Criteria) this;
		}

		public Criteria andUserRoleNotLike(String value) {
			addCriterion("user_role not like", value, "userRole");
			return (Criteria) this;
		}

		public Criteria andUserRoleIn(List<String> values) {
			addCriterion("user_role in", values, "userRole");
			return (Criteria) this;
		}

		public Criteria andUserRoleNotIn(List<String> values) {
			addCriterion("user_role not in", values, "userRole");
			return (Criteria) this;
		}

		public Criteria andUserRoleBetween(String value1, String value2) {
			addCriterion("user_role between", value1, value2, "userRole");
			return (Criteria) this;
		}

		public Criteria andUserRoleNotBetween(String value1, String value2) {
			addCriterion("user_role not between", value1, value2, "userRole");
			return (Criteria) this;
		}

		public Criteria andCreateTimeIsNull() {
			addCriterion("create_time is null");
			return (Criteria) this;
		}

		public Criteria andCreateTimeIsNotNull() {
			addCriterion("create_time is not null");
			return (Criteria) this;
		}

		public Criteria andCreateTimeEqualTo(java.util.Date value) {
			addCriterion("create_time =", value, "createTime");
			return (Criteria) this;
		}

		public Criteria andCreateTimeNotEqualTo(java.util.Date value) {
			addCriterion("create_time <>", value, "createTime");
			return (Criteria) this;
		}

		public Criteria andCreateTimeGreaterThan(java.util.Date value) {
			addCriterion("create_time >", value, "createTime");
			return (Criteria) this;
		}

		public Criteria andCreateTimeGreaterThanOrEqualTo(java.util.Date value) {
			addCriterion("create_time >=", value, "createTime");
			return (Criteria) this;
		}

		public Criteria andCreateTimeLessThan(java.util.Date value) {
			addCriterion("create_time <", value, "createTime");
			return (Criteria) this;
		}

		public Criteria andCreateTimeLessThanOrEqualTo(java.util.Date value) {
			addCriterion("create_time <=", value, "createTime");
			return (Criteria) this;
		}

		public Criteria andCreateTimeIn(List<java.util.Date> values) {
			addCriterion("create_time in", values, "createTime");
			return (Criteria) this;
		}

		public Criteria andCreateTimeNotIn(List<java.util.Date> values) {
			addCriterion("create_time not in", values, "createTime");
			return (Criteria) this;
		}

		public Criteria andCreateTimeBetween(java.util.Date value1, java.util.Date value2) {
			addCriterion("create_time between", value1, value2, "createTime");
			return (Criteria) this;
		}

		public Criteria andCreateTimeNotBetween(java.util.Date value1, java.util.Date value2) {
			addCriterion("create_time not between", value1, value2, "createTime");
			return (Criteria) this;
		}

}

	public static class Criteria extends GeneratedCriteria {

		protected Criteria() {
			super();
		}
	}

	public static class Criterion {
		private String condition;

		private Object value;

		private Object secondValue;

		private boolean noValue;

		private boolean singleValue;

		private boolean betweenValue;

		private boolean listValue;

		private String typeHandler;

		public String getCondition() {
			return condition;
		}

		public Object getValue() {
			return value;
		}

		public Object getSecondValue() {
			return secondValue;
		}

		public boolean isNoValue() {
		return noValue;
		}

		public boolean isSingleValue() {
			return singleValue;
		}

		public boolean isBetweenValue() {
			return betweenValue;
		}

		public boolean isListValue() {
			return listValue;
		}

		public String getTypeHandler() {
			return typeHandler;
		}

		protected Criterion(String condition) {
			super();
			this.condition = condition;
			this.typeHandler = null;
			this.noValue = true;
		}

		protected Criterion(String condition, Object value, String typeHandler) {
			super();
			this.condition = condition;
			this.value = value;
			this.typeHandler = typeHandler;
			if (value instanceof List<?>) {
				this.listValue = true;
			} else {
				this.singleValue = true;
			}
		}

		protected Criterion(String condition, Object value) {
			this(condition, value, null);
		}

		protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
			super();
			this.condition = condition;
			this.value = value;
			this.secondValue = secondValue;
			this.typeHandler = typeHandler;
			this.betweenValue = true;
		}

		protected Criterion(String condition, Object value, Object secondValue) {
			this(condition, value, secondValue, null);
		}
	}
}