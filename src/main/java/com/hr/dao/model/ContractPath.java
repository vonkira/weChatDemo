package com.hr.dao.model;



/**
 *
 */
public class ContractPath {

	/**
	 *
	 */
	private Integer id;

	/**
	 *合同标题
	 */
	private String title;

	/**
	 *文件类型 0合同 1印章
	 */
	private Integer state;

	/**
	 *合同id
	 */
	private Integer contractId;

	/**
	 *文件存放路径
	 */
	private String filePath;

	/**
	 *用户权限
	 */
	private String userRole;

	/**
	 *
	 */
	private java.util.Date createTime;

	public void setId(Integer id) {
		this.id=id;
	}

	public Integer getId() {
		return id;
	}

	public void setTitle(String title) {
		this.title=title == null ? title : title.trim();
	}

	public String getTitle() {
		return title;
	}

	public void setState(Integer state) {
		this.state=state;
	}

	public Integer getState() {
		return state;
	}

	public void setContractId(Integer contractId) {
		this.contractId=contractId;
	}

	public Integer getContractId() {
		return contractId;
	}

	public void setFilePath(String filePath) {
		this.filePath=filePath == null ? filePath : filePath.trim();
	}

	public String getFilePath() {
		return filePath;
	}

	public void setUserRole(String userRole) {
		this.userRole=userRole == null ? userRole : userRole.trim();
	}

	public String getUserRole() {
		return userRole;
	}

	public void setCreateTime(java.util.Date createTime) {
		this.createTime=createTime;
	}

	public java.util.Date getCreateTime() {
		return createTime;
	}

}
