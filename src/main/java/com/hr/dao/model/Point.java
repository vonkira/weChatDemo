package com.hr.dao.model;



/**
 *
 */
public class Point {

	/**
	 *
	 */
	private Integer id;

	/**
	 *城市id
	 */
	private Integer cityId;

	/**
	 *站点名称
	 */
	private String pointName;

	/**
	 *创建时间
	 */
	private java.util.Date createTime;

	public void setId(Integer id) {
		this.id=id;
	}

	public Integer getId() {
		return id;
	}

	public void setCityId(Integer cityId) {
		this.cityId=cityId;
	}

	public Integer getCityId() {
		return cityId;
	}

	public void setPointName(String pointName) {
		this.pointName=pointName == null ? pointName : pointName.trim();
	}

	public String getPointName() {
		return pointName;
	}

	public void setCreateTime(java.util.Date createTime) {
		this.createTime=createTime;
	}

	public java.util.Date getCreateTime() {
		return createTime;
	}

}
