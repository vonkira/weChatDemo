package com.hr.daoEx;

import com.hr.daoEx.model.ContractEx;

import java.util.List;
import java.util.Map;

/**
@author sun
*/
public interface ContractMapperEx {

    //查询员工合同详情
    ContractEx selectDetail(Integer id);

    List<ContractEx> selectList(Map<String, Object> map);

    ContractEx checkDetail(Integer id);
}
