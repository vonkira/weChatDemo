package com.hr.daoEx.model;

import com.hr.dao.model.Point;

/**
@author sun
*/
public class PointEx extends Point {

    private String cityName;

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }
}