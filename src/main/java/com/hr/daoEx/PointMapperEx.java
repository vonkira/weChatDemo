package com.hr.daoEx;

import com.hr.daoEx.model.PointEx;

import java.util.List;
import java.util.Map; /**
@author sun
*/
public interface PointMapperEx {

    //后台查找
    List<PointEx> selectList(Map<String, Object> map);
}
