package com.hr.action;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.base.dialect.PaginationSupport;
import com.base.dao.model.Ret;
import com.base.dao.model.Grid;
import com.hr.daoEx.model.PointEx;
import net.sf.ehcache.transaction.xa.EhcacheXAException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.apache.commons.lang.StringUtils;
import com.base.util.JSONUtils;
import com.base.web.annotation.LoginFilter;
import org.springframework.beans.factory.annotation.Autowired;
import com.base.action.CoreController;
import com.hr.dao.model.Point;
import com.hr.dao.model.PointExample;
import com.hr.service.PointService;

/**
@author sun
*/
@RequestMapping("point")
@Controller
@LoginFilter
public class PointController extends CoreController{

    @Autowired
    private PointService pointService;

    @RequestMapping("/list")
	@ResponseBody
    public String list(Integer page, Integer rows,Point point){
    	PaginationSupport.byPage(page, rows);
    	//PointExample example = new PointExample();
		Map<String, Object> map = new HashMap<>();
		map.put("city_id",point.getCityId());
		map.put("point_name",point.getPointName());
		List<PointEx> list = pointService.selectList(map);
    	//List<Point> list = pointService.selectByExample(example);
      	return page(list);
    }

    @RequestMapping("/save")
	@ResponseBody
    public String save(Point point){
    	if (point.getId() == null){
    		pointService.insert(point);
    	}else{
    		pointService.updateByPrimaryKeySelective(point);
    	}
       	return ok();
    }

    @RequestMapping("/findById")
	@ResponseBody
    public String find(Integer id){
    	Point point = pointService.selectByPrimaryKey(id);
       	return ok(point);
    }

    @RequestMapping("/del")
	@ResponseBody
    public String del(String id){
    	String[] ids = id.split(",");
    	for (String str : ids){
    		pointService.deleteByPrimaryKey(Integer.parseInt(str));
    	}
       	return ok();
    }

	@RequestMapping("/pointList")
	@ResponseBody
	public List<Point> pointList(Integer cityId){
		PointExample example = new PointExample();
		example.createCriteria().andCityIdEqualTo(cityId);
		List<Point> list = pointService.selectByExample(example);
		return list;
	}

}