package com.hr.action;

import java.util.List;
import com.base.dialect.PaginationSupport;
import com.base.dao.model.Ret;
import com.base.dao.model.Grid;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.apache.commons.lang.StringUtils;
import com.base.util.JSONUtils;
import com.base.web.annotation.LoginFilter;
import org.springframework.beans.factory.annotation.Autowired;
import com.base.action.CoreController;
import com.hr.dao.model.City;
import com.hr.dao.model.CityExample;
import com.hr.service.CityService;

/**
@author sun
*/
@RequestMapping("city")
@Controller
@LoginFilter
public class CityController extends CoreController{

    @Autowired
    private CityService cityService;

    @RequestMapping("/list")
	@ResponseBody
    public String list(Integer page, Integer rows, String cityName){
    	PaginationSupport.byPage(page, rows);
    	CityExample example = new CityExample();
    	if(null != cityName){
    		example.createCriteria().andCityNameLike("%"+cityName+"%");
		}
		example.setOrderByClause("create_time desc");

    	List<City> list = cityService.selectByExample(example);
      	return page(list);
    }

    @RequestMapping("/save")
	@ResponseBody
    public String save(City city){
    	if (city.getId() == null){
    		cityService.insert(city);
    	}else{
    		cityService.updateByPrimaryKeySelective(city);
    	}
       	return ok();
    }

    @RequestMapping("/findById")
	@ResponseBody
    public String find(Integer id){
    	City city = cityService.selectByPrimaryKey(id);
       	return ok(city);
    }

    @RequestMapping("/del")
	@ResponseBody
    public String del(String id){
    	String[] ids = id.split(",");
    	for (String str : ids){
    		cityService.deleteByPrimaryKey(Integer.parseInt(str));
    	}
       	return ok();
    }


	@RequestMapping("/cityList")
	@ResponseBody
	public List<City> cityList(){
    	CityExample example = new CityExample();
		example.setOrderByClause("create_time desc");
		List<City> list = cityService.selectByExample(example);
		return list;
	}


}