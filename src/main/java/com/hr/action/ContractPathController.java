package com.hr.action;

import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.afterturn.easypoi.word.WordExportUtil;
import com.base.dao.model.BFile;
import com.base.dialect.PaginationSupport;
import com.base.dao.model.Ret;
import com.base.dao.model.Grid;
import com.base.security.util.UserUtils;
import com.base.service.BFileService;
import com.hr.dao.model.Contract;
import com.hr.dao.model.ContractExample;
import com.hr.daoEx.model.ContractEx;
import com.hr.service.ContractService;
import com.item.ConstantsCode;
import com.item.dao.model.Admin;
import com.item.service.AdminService;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.apache.commons.lang.StringUtils;
import com.base.util.JSONUtils;
import com.base.web.annotation.LoginFilter;
import org.springframework.beans.factory.annotation.Autowired;
import com.base.action.CoreController;
import com.hr.dao.model.ContractPath;
import com.hr.dao.model.ContractPathExample;
import com.hr.service.ContractPathService;

/**
@author sun
*/
@RequestMapping("contractPath")
@Controller
@LoginFilter
public class ContractPathController extends CoreController{

    @Autowired
    private ContractPathService contractPathService;
	@Autowired
	private ContractService contractService;
	@Autowired
	private AdminService adminService;
	@Autowired
	private BFileService bFileService;

    @RequestMapping("/list")
	@ResponseBody
    public String list(Integer page, Integer rows,Integer state){
    	PaginationSupport.byPage(page, rows);
    	ContractPathExample example = new ContractPathExample();
		Integer id = UserUtils.getPrincipal().getId();
		//用户权限--查出客户权限
		Admin admin = adminService.selectByPrimaryKey(id);
//		if("kehu".equals(admin.getRoleCode())){
//			example.createCriteria().andUserRoleEqualTo(admin.getId().toString());
//		}
		if("kehu".equals(admin.getRoleCode())){
			example.createCriteria().andUserRoleEqualTo(admin.getId().toString()).andStateEqualTo(state);
		}else{
			example.createCriteria().andStateEqualTo(state);
		}



    	List<ContractPath> list = contractPathService.selectByExample(example);
      	return page(list);
    }

	@RequestMapping("/roleList")
	@ResponseBody
	public Ret list(Integer state){
		ContractPathExample example = new ContractPathExample();
		Integer id = UserUtils.getPrincipal().getId();
		Admin admin = adminService.selectByPrimaryKey(id);

		if(!"kehu".equals(admin.getRoleCode())){
			//不是机构组织登录
			return new Ret(1);
		}
		return new Ret(0);
	}

    @RequestMapping("/save")
	@ResponseBody
    public String save(ContractPath contractPath){

    	//找出当前登陆者权限
		Integer id = UserUtils.getPrincipal().getId();
		Admin admin = adminService.selectByPrimaryKey(id);

		contractPath.setUserRole(admin.getId().toString());

    	if (contractPath.getId() == null){
    		contractPathService.insert(contractPath);
    	}else{
    		contractPathService.updateByPrimaryKeySelective(contractPath);
    	}
       	return ok();
    }

    @RequestMapping("/findById")
	@ResponseBody
    public String find(Integer id){
    	ContractPath contractPath = contractPathService.selectByPrimaryKey(id);
       	return ok(contractPath);
    }

    @RequestMapping("/del")
	@ResponseBody
    public String del(String id){
    	String[] ids = id.split(",");
    	for (String str : ids){
    		contractPathService.deleteByPrimaryKey(Integer.parseInt(str));
    	}
       	return ok();
    }

    @RequestMapping("/contractPathList")
	@ResponseBody
	public List<ContractPath> contractPathList(){
//		Integer id = UserUtils.getPrincipal().getId();
//		Admin admin = adminService.selectByPrimaryKey(id);
		ContractPathExample example = new ContractPathExample();
		example.createCriteria().andStateEqualTo(0);
    	List<ContractPath> list = contractPathService.selectByExample(example);
    	return list;
	}

	@RequestMapping("/changeModel")
	@ResponseBody
	public Ret changeModel(Integer id){


		//找出合同
		ContractEx contract = contractService.selectDetail(id);

		Map<String, Object> params = new HashMap<>();
		params.put("org",contract.getOrgName());
		params.put("city",contract.getCityId());
		params.put("point",contract.getPointId());
		try {
			XWPFDocument out = WordExportUtil.exportWord07("C:\\Project\\知行智配\\test.docx",params);
			FileOutputStream outputStream = new FileOutputStream("C:/Project/知行智配/testNew.docx");
			out.write(outputStream);
			outputStream.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return new Ret(1);
	}
}