package com.hr.action;

import com.base.action.CoreController;
import com.base.dao.model.Ret;
import com.base.des.Md5;
import com.base.dialect.PaginationSupport;
import com.base.web.annotation.LoginFilter;
import com.item.dao.model.Admin;
import com.item.dao.model.AdminExample;
import com.item.daoEx.model.AdminEx;
import com.item.service.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


@RequestMapping("kehu")
@Controller
@LoginFilter
public class KehuController extends CoreController {

    /**
     * 客户role_id为kehu
     */

    @Autowired
    private AdminService adminService;

    @RequestMapping("/list")
    @ResponseBody
    public String userList(Integer page, Integer rows, String account){
        PaginationSupport.byPage(page, rows);
        AdminExample example = new AdminExample();
        //example.createCriteria().andRoleCodeEqualTo("kehu");
        //example.setOrderByClause("create_time desc");
        Map<String, Object> map = new HashMap<>();
        map.put("account",account);
        map.put("role_code","kehu");
        List<AdminEx> list = adminService.selectList(map);
        return page(list);
    }

    @RequestMapping("/save")
    @ResponseBody
    public String save(Admin admin){
        if (admin.getId() == null){
            try {
                admin.setPassword(Md5.md5("123456"));
                admin.setRoleCode("kehu");
                admin.setType(2);
                admin.setState(1);
            } catch (Exception e) {
                e.printStackTrace();
            }
            adminService.insert(admin);
        }else{
            adminService.updateByPrimaryKeySelective(admin);
        }
        return ok();
    }

    @RequestMapping("/initPassword")
    @ResponseBody
    public Ret initPassword(Admin admin){
        Admin c = adminService.selectByPrimaryKey(admin.getId());
        c.setPassword(Md5.mD5("123456"));
        adminService.updateByPrimaryKeySelective(c);
        return new Ret(1);
    }
}
