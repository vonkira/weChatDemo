package com.hr.service;

import java.util.List;
import java.util.Map;

import com.hr.daoEx.PointMapperEx;
import com.hr.daoEx.model.PointEx;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hr.dao.PointMapper;
import com.hr.dao.model.Point;
import com.hr.dao.model.PointExample;

@Service
public class PointService {
	@Autowired
	private PointMapper pointMapper;
	@Autowired
	private PointMapperEx pointMapperEx;

	public int countByExample(PointExample example) {
		return this.pointMapper.countByExample(example);
	}

	public Point selectByPrimaryKey(Integer id) {
		return this.pointMapper.selectByPrimaryKey(id);
	}

	public List<Point> selectByExample(PointExample example) {
		return this.pointMapper.selectByExample(example);
	}

	public int deleteByPrimaryKey(Integer id) {
		return this.pointMapper.deleteByPrimaryKey(id);
	}

	public int updateByPrimaryKeySelective(Point record) {
		return this.pointMapper.updateByPrimaryKeySelective(record);
	}

	public int updateByPrimaryKey(Point record) {
		return this.pointMapper.updateByPrimaryKey(record);
	}

	public int deleteByExample(PointExample example) {
		return this.pointMapper.deleteByExample(example);
	}

	public int updateByExampleSelective(Point record, PointExample example) {
		return this.pointMapper.updateByExampleSelective(record, example);
	}

	public int updateByExample(Point record, PointExample example) {
		return this.pointMapper.updateByExample(record, example);
	}

	public int insert(Point record) {
		return this.pointMapper.insert(record);
	}

	public int insertSelective(Point record) {
		return this.pointMapper.insertSelective(record);
	}

    public List<PointEx> selectList(Map<String, Object> map) {
		return this.pointMapperEx.selectList(map);
    }
}
