package com.hr.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hr.dao.CityMapper;
import com.hr.dao.model.City;
import com.hr.dao.model.CityExample;

@Service
public class CityService {
	@Autowired
	private CityMapper cityMapper;

	public int countByExample(CityExample example) {
		return this.cityMapper.countByExample(example);
	}

	public City selectByPrimaryKey(Integer id) {
		return this.cityMapper.selectByPrimaryKey(id);
	}

	public List<City> selectByExample(CityExample example) {
		return this.cityMapper.selectByExample(example);
	}

	public int deleteByPrimaryKey(Integer id) {
		return this.cityMapper.deleteByPrimaryKey(id);
	}

	public int updateByPrimaryKeySelective(City record) {
		return this.cityMapper.updateByPrimaryKeySelective(record);
	}

	public int updateByPrimaryKey(City record) {
		return this.cityMapper.updateByPrimaryKey(record);
	}

	public int deleteByExample(CityExample example) {
		return this.cityMapper.deleteByExample(example);
	}

	public int updateByExampleSelective(City record, CityExample example) {
		return this.cityMapper.updateByExampleSelective(record, example);
	}

	public int updateByExample(City record, CityExample example) {
		return this.cityMapper.updateByExample(record, example);
	}

	public int insert(City record) {
		return this.cityMapper.insert(record);
	}

	public int insertSelective(City record) {
		return this.cityMapper.insertSelective(record);
	}

}
