package com.hr.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hr.dao.ContractPathMapper;
import com.hr.dao.model.ContractPath;
import com.hr.dao.model.ContractPathExample;

@Service
public class ContractPathService {
	@Autowired
	private ContractPathMapper contractPathMapper;

	public int countByExample(ContractPathExample example) {
		return this.contractPathMapper.countByExample(example);
	}

	public ContractPath selectByPrimaryKey(Integer id) {
		return this.contractPathMapper.selectByPrimaryKey(id);
	}

	public List<ContractPath> selectByExample(ContractPathExample example) {
		return this.contractPathMapper.selectByExample(example);
	}

	public int deleteByPrimaryKey(Integer id) {
		return this.contractPathMapper.deleteByPrimaryKey(id);
	}

	public int updateByPrimaryKeySelective(ContractPath record) {
		return this.contractPathMapper.updateByPrimaryKeySelective(record);
	}

	public int updateByPrimaryKey(ContractPath record) {
		return this.contractPathMapper.updateByPrimaryKey(record);
	}

	public int deleteByExample(ContractPathExample example) {
		return this.contractPathMapper.deleteByExample(example);
	}

	public int updateByExampleSelective(ContractPath record, ContractPathExample example) {
		return this.contractPathMapper.updateByExampleSelective(record, example);
	}

	public int updateByExample(ContractPath record, ContractPathExample example) {
		return this.contractPathMapper.updateByExample(record, example);
	}

	public int insert(ContractPath record) {
		return this.contractPathMapper.insert(record);
	}

	public int insertSelective(ContractPath record) {
		return this.contractPathMapper.insertSelective(record);
	}

}
