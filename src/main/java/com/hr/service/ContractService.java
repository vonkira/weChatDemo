package com.hr.service;

import java.util.List;
import java.util.Map;

import com.hr.daoEx.ContractMapperEx;
import com.hr.daoEx.model.ContractEx;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hr.dao.ContractMapper;
import com.hr.dao.model.Contract;
import com.hr.dao.model.ContractExample;

@Service
public class ContractService {
	@Autowired
	private ContractMapper contractMapper;
	@Autowired
	private ContractMapperEx contractMapperEx;

	public int countByExample(ContractExample example) {
		return this.contractMapper.countByExample(example);
	}

	public Contract selectByPrimaryKey(Integer id) {
		return this.contractMapper.selectByPrimaryKey(id);
	}

	public List<Contract> selectByExample(ContractExample example) {
		return this.contractMapper.selectByExample(example);
	}

	public int deleteByPrimaryKey(Integer id) {
		return this.contractMapper.deleteByPrimaryKey(id);
	}

	public int updateByPrimaryKeySelective(Contract record) {
		return this.contractMapper.updateByPrimaryKeySelective(record);
	}

	public int updateByPrimaryKey(Contract record) {
		return this.contractMapper.updateByPrimaryKey(record);
	}

	public int deleteByExample(ContractExample example) {
		return this.contractMapper.deleteByExample(example);
	}

	public int updateByExampleSelective(Contract record, ContractExample example) {
		return this.contractMapper.updateByExampleSelective(record, example);
	}

	public int updateByExample(Contract record, ContractExample example) {
		return this.contractMapper.updateByExample(record, example);
	}

	public int insert(Contract record) {
		return this.contractMapper.insert(record);
	}

	public int insertSelective(Contract record) {
		return this.contractMapper.insertSelective(record);
	}

    public ContractEx selectDetail(Integer id) {
		return this.contractMapperEx.selectDetail(id);
    }

    public List<ContractEx> selectList(Map<String, Object> map) {
		return this.contractMapperEx.selectList(map);
    }

    public ContractEx checkDetail(Integer id) {
		return this.contractMapperEx.checkDetail(id);
    }
}
