package com.hr.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hr.dao.OrgMapper;
import com.hr.dao.model.Org;
import com.hr.dao.model.OrgExample;

@Service
public class OrgService {
	@Autowired
	private OrgMapper orgMapper;

	public int countByExample(OrgExample example) {
		return this.orgMapper.countByExample(example);
	}

	public Org selectByPrimaryKey(Integer id) {
		return this.orgMapper.selectByPrimaryKey(id);
	}

	public List<Org> selectByExample(OrgExample example) {
		return this.orgMapper.selectByExample(example);
	}

	public int deleteByPrimaryKey(Integer id) {
		return this.orgMapper.deleteByPrimaryKey(id);
	}

	public int updateByPrimaryKeySelective(Org record) {
		return this.orgMapper.updateByPrimaryKeySelective(record);
	}

	public int updateByPrimaryKey(Org record) {
		return this.orgMapper.updateByPrimaryKey(record);
	}

	public int deleteByExample(OrgExample example) {
		return this.orgMapper.deleteByExample(example);
	}

	public int updateByExampleSelective(Org record, OrgExample example) {
		return this.orgMapper.updateByExampleSelective(record, example);
	}

	public int updateByExample(Org record, OrgExample example) {
		return this.orgMapper.updateByExample(record, example);
	}

	public int insert(Org record) {
		return this.orgMapper.insert(record);
	}

	public int insertSelective(Org record) {
		return this.orgMapper.insertSelective(record);
	}

}
