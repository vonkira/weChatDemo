package com.hr;

public interface HrConstants {

    //合同有效期--年份
    public static Integer contractYear = 2;

    //试用期的月份
    public static Integer contractTestMonth = 2;
}
